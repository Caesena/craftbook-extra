CraftBook Extra 
===========

CraftBook is a heavily customizable plugin adding exciting new gameplay elements and advanced redstone functions!

* Modular, and extremely light on your server.
* Enable features that you need, disable ones that you don't. Even tweak features to your liking!
* Redstone ICs (AND/OR gates, lightning striker gates)
* Elevators for your server so people can jump between floors quickly (Or smoothly, if you choose so)
* Working cauldrons so players can cook inside them
* Your own custom crafting recipes
* Hidden switches for players
* Minecart booster blocks and more
* Togglable areas and bridges
* Chairs you can sit on
* Working pipes that can transfer items from place to place!
* Footprints where your players walk!
* Mobs and players drop their heads.
* The ability to bind commands to items, with cool-downs, permissions and timers.
* And much more!

See the [Gitbook](https://minecraftonline.gitlab.io/craftbook-extra/) for more information

Branch Structure
----------------

*Currently the 'master' branch contains old CraftBook.*

Look to the Sponge branch for CraftBook Extra for Sponge.

For the prior version of CraftBook for CanaryMod, the 'classicloader' branch is what you want.

Cloning/Setting up your workspace
---------

Clone the repo as you would normally, import as a *Gradle* project.
NMS (Net Minecraft Server) imports almost always will be broken.
To fix ths:

* Immediately run 'setupDecompWorkspace' forgegradle task.
* Try the 'fixMcSources' task.
* Then try 'downloadServer', and this SHOULD resolve your issues.
* If all else fails try refreshing dependencies.

Compiling
---------

*_ALWAYS_* use the build task in `gradle`. With the addition of `forgegradle`, wait for the *'build successful'* message, because reobfuscation happens at the end of the build process.

Dependencies are automatically handled by Gradle.

ForgeGrade will automatically reobfuscate NMS.

Contributing
------------

Please keep in mind the following when contributing to the CraftBook codebase:

* Keep any work with NMS (Net Minecraft Server or Notchian) inside `com/minecraftonline/util/nms`
to avoid the clutter we had with older versions of CraftBook.
* Use proper names for IC classes. No more cryptic IDs. Package them appropriately.
* Include IC name and ID in comment above class for readability. @author tag is optional.

In addition, please ensure your code is compliant with the Sun/Oracle Code
Conventions to keep things neat and readable.

Keeping to these conventions helps us update CraftBook quicker, and avoid the issues the old one had.

By submitting code, you agree to place to license your code under the
irrevocable GNU General Public License v3.

### Understanding the new system

#### New ICs

ICs need to be registered in the ICManager in the `static` block:

`registerICType(newICType<>("Sign ID here","NAME ON SIGN","IC name","Description",IC Factory Class))`

* The ID is what players type in the square brackets on line 2. 
* If the IC is both Self-Triggering AND Redstone Powered, use the Redstone version. EX: (MCX203, MCZ203)
* The ICFactory for the IC itself will be contained within the IC class. Consider the example:


```
java
/**
*  (IC NAME AS IT APPEARS ON SIGN)
* Model ID: (If Variant: Redstone Version)
*
**/
public class ICNAME extends IC implements SelfTriggeringIC {
//Implements SelfTriggeringIC only if Hybrid/ or SelfTriggered

    public ICNAME(ICFactory<ICNAME> icFactory,Location<World> block){super(icFactory,block);}

//this will ALWAYS BE HERE
    @Override
    public void trigger(){
        
    }
//This is only here if SelfTriggeringIC is above
    @Override
    public void think(){
       
    }

//
    public static class Factory implements ICFactory<ICNAME>, HybridIC {
// , HybirdIC is only present if the IC has redstone/ self trigger that does the same thing. 
//ALWAYS PRESENT
        @Override
        public ICNAME createInstance(Location<World> location){ return new ICNAME(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input" //If 1 input leave as input
                    },
                    new String[] {
                            "Outputs if block is present." //Output what happens
                    }
            };
        }
//ONLY exsists if HybridIC is present
        @Override
        public String stVariant(){ return "SelfTrigger IC ID"; }
    }
}
```

The above is the most basic and generic of templates.
