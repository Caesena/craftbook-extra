/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration;

import net.minecraft.profiler.Profiler;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.GameType;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldSettings;
import net.minecraft.world.WorldType;
import net.minecraft.world.chunk.storage.AnvilSaveHandler;
import net.minecraft.world.storage.SaveHandler;
import net.minecraft.world.storage.WorldInfo;
import org.spongepowered.api.world.DimensionType;
import org.spongepowered.common.SpongeImpl;
import org.spongepowered.common.bridge.world.WorldInfoBridge;
import org.spongepowered.common.bridge.world.chunk.ChunkProviderServerBridge;
import org.spongepowered.common.mixin.core.server.MinecraftServerAccessor;

import java.io.File;

import static org.mockito.Mockito.mock;

public class CraftBookWorldBuilder implements WorldBuilder {

    private final MinecraftServer server;
    private final ServerConfigurer configurer;
    private int seed = 0;
    private int dimId = 0;
    private boolean forceDefault = false;
    private WorldType worldType = WorldType.DEFAULT;
    private boolean chunkGenerationEnabled = false;

    public CraftBookWorldBuilder(MinecraftServer mcServer, ServerConfigurer configurer) {
        this.server = mcServer;
        this.configurer = configurer;
    }

    @Override
    public WorldBuilder seed(int seed) {
        this.seed = seed;
        return this;
    }

    @Override
    public WorldBuilder dimension(DimensionType dimension) {
        this.dimId = ((net.minecraft.world.DimensionType) (Object) dimension).getId();
        return this;
    }

    @Override
    public WorldBuilder worldType(WorldType worldType) {
        this.worldType = worldType;
        return this;
    }

    @Override
    public WorldBuilder chunkGeneration(boolean enabled) {
        this.chunkGenerationEnabled = enabled;
        return this;
    }

    @Override
    public WorldBuilder forceDefault(boolean forceDefault) {
        this.forceDefault = forceDefault;
        return this;
    }

    @Override
    public WorldServer buildAndRegister(String id) {
        WorldInfo worldInfo = new WorldInfo(new WorldSettings(seed, GameType.NOT_SET, false, false, this.worldType), "craftbook$" + id);

        // Sets its dimension id so it doesn't NPE
        ((WorldInfoBridge) worldInfo).bridge$setDimensionId(this.dimId);

        SaveHandler saveHandler;

        if (this.chunkGenerationEnabled) {
            // Save handler so that we can avoid the SpongeEmptyChunk that can't be modified, and actually load a chunk.
            saveHandler = new AnvilSaveHandler(new File("testRun"), "overworld", false, ((MinecraftServerAccessor) SpongeImpl.getServer()).accessor$getDataFixer());
        }
        else {
            saveHandler = mock(SaveHandler.class);
        }

        WorldServer worldServer = new WorldServer(
                this.server,
                saveHandler,
                worldInfo,
                this.dimId,
                mock(Profiler.class)
        );

        worldServer.init();

        if (this.chunkGenerationEnabled) {
            ChunkProviderServerBridge chunkProviderServerBridge = (ChunkProviderServerBridge) worldServer.getChunkProvider();
            chunkProviderServerBridge.bridge$setDenyChunkRequests(false); // Make this provider load chunks
        }

        configurer.registerWorld(worldServer, dimId);

        return worldServer;
    }
}
