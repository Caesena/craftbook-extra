/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.me4502.modularframework.SpongeModuleController;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import com.sk89q.worldedit.sponge.SpongeWorldEdit;
import com.sk89q.worldedit.sponge.adapter.impl.Sponge_Dev_Impl;
import net.minecraft.init.Bootstrap;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.event.CauseStackManager;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.user.UserStorageService;
import org.spongepowered.common.SpongeImpl;
import org.spongepowered.common.inject.SpongeModule;
import org.spongepowered.common.registry.SpongeGameRegistry;
import org.spongepowered.plugin.meta.PluginMetadata;
import org.spongepowered.server.plugin.MetaPluginContainer;
import sponge.PluginModule;
import sponge.TestImplementationModule;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Entry point for com.minecraftonline.craftbook.integration tests.
 * Should start minecraft and initialize sponge.
 */
public class IntegrationTestMain {

    public static void main(String[] args) {
        // Bootstrap minecraft.
        Bootstrap.register();

        // Setup injector.
        Injector injector = Guice.createInjector(new SpongeModule(), new TestImplementationModule());

        registerInjectorsForPlugin(CraftBookPlugin.class, injector);
        CraftBookPlugin.spongeInst().moduleController = mock(SpongeModuleController.class);

        SpongeWorldEdit worldEdit = new SpongeWorldEdit(); // Hola world edit!
        // Who needs mockito when you have null!
        // Load WE Adapter
        try {
            Field field = SpongeWorldEdit.class.getDeclaredField("spongeAdapter");
            field.setAccessible(true);
            field.set(worldEdit, new Sponge_Dev_Impl());
        } catch (IllegalAccessException | NoSuchFieldException e) {
            throw new IllegalStateException(e);
        }

        // Initialize registry
        SpongeGameRegistry registry = injector.getInstance(SpongeGameRegistry.class);


        registry.preRegistryInit();
        registry.preInit();
        registry.init();
        registry.postInit();


        IntegrationTestGame game = injector.getInstance(IntegrationTestGame.class);

        // Provide UserStorageService
        // Reason: There is a mixin that sets the user on the entityplayer, so in order to construct it, we must register it.
        UserStorageService mockStorageService = mock(UserStorageService.class);

        when(mockStorageService.get(any(UUID.class))).thenReturn(Optional.of(mock(User.class)));
        when(mockStorageService.get(any(String.class))).thenReturn(Optional.of(mock(User.class)));
        when(mockStorageService.get(any(org.spongepowered.api.profile.GameProfile.class))).thenReturn(Optional.of(mock(User.class)));

        game.getServiceManager().setProvider(SpongeImpl.getPlugin(), UserStorageService.class, mockStorageService);

        try (CauseStackManager.StackFrame stackFrame = SpongeImpl.getCauseStackManager().pushCauseFrame()) {
            stackFrame.pushCause(CraftBookPlugin.spongeInst().getContainer());

            CraftBookKeys.init();
        }

        try {
            new BlockBagManager().onInitialize();
        } catch (CraftBookException e) {
            throw new IllegalStateException(e);
        }
    }

    public static void registerInjectorsForPlugin(Class<?> pluginClass, Injector parentInjector) {
        Plugin pluginAnnotation = CraftBookPlugin.class.getAnnotation(Plugin.class);
        if (pluginAnnotation == null) {
            throw new IllegalArgumentException("Class " + pluginClass.getName() + " is not a plugin class! It is missing the @Plugin annotation!");
        }
        PluginMetadata meta = new PluginMetadata(pluginAnnotation.id());

        MetaPluginContainer container = new MetaPluginContainer(meta, Optional.empty());
        parentInjector.createChildInjector(new PluginModule(container, CraftBookPlugin.class));
    }
}
