/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.api.Server;
import org.spongepowered.common.SpongeGame;
import sponge.TestServer;

import java.io.Closeable;
import java.nio.file.Path;

/**
 * An implementation of SpongeGame that has both a default
 * server instance, and has the option to "swap in" a
 * different one for more advanced tests, and so that
 * the tests can be properly isolated from each-other.
 *
 * For basic com.minecraftonline.craftbook.integration tests the default server may
 * suffice, but for more complex ones and those who
 * require block manipulation, {@link #usingServer(Server)}
 * will be needed.
 */
@Singleton
public class IntegrationTestGame extends SpongeGame {

    private final TestServer defaultServer;
    private Server curServer = null;

    @Inject
    public IntegrationTestGame() {
        this.defaultServer = new TestServer();
    }

    @NotNull
    @Override
    public Path getSavesDirectory() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isServerAvailable() {
        // Sponge doesn't let us spawn entities if the server is not available.
        return true;
    }

    @NotNull
    @Override
    public Server getServer() {
        // Get the cur server if available, otherwise the default.
        return curServer == null ? defaultServer : curServer;
    }

    /**
     * Changes the game's server, such that until the returned
     * {@link Closeable} is closed, Sponge.getServer() will
     * return this server.
     *
     * @param server Server to swap in for the default.
     * @return Closeable to be closed when you want to stop using this server.
     */
    public ServerConfigurer usingServer(Server server) {
        if (this.curServer != null) {
            throw new IllegalStateException("Called usingServer() again before closing the previous returned closeable!");
        }
        this.curServer = server;
        return new ServerConfigurer(this.curServer, () -> this.curServer = null);
    }
}
