/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.PlayerList;
import net.minecraft.world.WorldServer;
import org.spongepowered.api.Server;
import org.spongepowered.api.scoreboard.Scoreboard;
import org.spongepowered.common.world.WorldManager;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ServerConfigurer implements AutoCloseable {

    private final MinecraftServer minecraftServer;
    private final Server server;
    private final Map<Integer, WorldServer> registeredWorlds = new HashMap<>();
    private final Runnable onClose;
    private boolean closed = false;

    public ServerConfigurer(Server server, final Runnable onClose) {
        this.server = server;
        this.minecraftServer = (MinecraftServer) server;
        this.onClose = onClose;

        when(minecraftServer.getPlayerList()).thenReturn(mock(PlayerList.class));
        when(server.getWorld(any(String.class))).thenCallRealMethod();
    }

    /**
     * Registers a world to the current server.
     * @param worldServer World to register
     * @param dimension the world's dimension
     */
    public void registerWorld(WorldServer worldServer, int dimension) {
        registerWorld(worldServer, dimension, false);
    }

    /**
     * Registers a world to the current server.
     * @param worldServer World to register
     * @param dimension the world's dimension
     * @param forceDefault Whether to make this world the default world.
     */
    public void registerWorld(WorldServer worldServer, int dimension, boolean forceDefault) {
        WorldManager.forceAddWorld(dimension, worldServer);

        if (forceDefault || registeredWorlds.isEmpty()) {
            when(server.getServerScoreboard()).thenReturn(Optional.of((Scoreboard) worldServer.getScoreboard()));
            when(server.getDefaultWorldName()).thenReturn(worldServer.getWorldInfo().getWorldName());
        }

        this.registeredWorlds.put(dimension, worldServer);
    }

    public WorldBuilder worldBuilder() {
        return new CraftBookWorldBuilder((MinecraftServer) this.server, this);
    }

    public void setPlayers(Collection<EntityPlayerMP> players, @Nullable WorldServer worldServer) {
        when(minecraftServer.getPlayerList().getPlayers()).thenReturn(new ArrayList<>(players));
        if (worldServer != null) {

            for (EntityPlayer oldPlayer : worldServer.playerEntities) {
                worldServer.getChunk(oldPlayer.getPosition()).removeEntity(oldPlayer);
            }

            worldServer.playerEntities.clear();
            worldServer.playerEntities.addAll(players);

            for (EntityPlayerMP player : players) {
                worldServer.getChunk(player.getPosition()).addEntity(player);
            }
        }
    }

    @Override
    public void close() {
        if (closed) {
            throw new IllegalStateException("Already closed!");
        }
        closed = true;
        for (WorldServer server : registeredWorlds.values()) {
            WorldManager.unloadWorld(server, false, true);
        }
        onClose.run();
    }
}
