/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration.test;

import com.minecraftonline.util.entity.DetailedEntityType;
import com.minecraftonline.util.entity.EntityParsingUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spongepowered.lwts.runner.LaunchWrapperTestRunner;

import static com.minecraftonline.util.entity.EntityParsingUtil.SpecialCharacter.CHILD;
import static com.minecraftonline.util.entity.EntityParsingUtil.SpecialCharacter.PASSENGER;

@RunWith(LaunchWrapperTestRunner.class)
public class EntityParsingTest {

	@Test
	public void testEntityParsing() {
		testString("akdwjdkjajwd", "garbage", false);
		testString("Pig" + PASSENGER + PASSENGER + "Pig", "double passenger symbol", false);
		testString("Eclypto19", "playername", false);
		testString(CHILD + "", "child symbol", false);
		testString("Pig" + PASSENGER + "Creeper" + CHILD + CHILD, "overflow of child", false);

		testString("Pig", "basic pig", true);
		testString("Pig" + PASSENGER + "Pig", "Pig on Pig", true);
		testString("Pig" + PASSENGER + "Pig" + CHILD + "Pig", "2 Pig on Pig", true);
	}

	private void testString(String string, String name, boolean shouldSucceed) {
		try {
			EntityParsingUtil.parseStackedAndNbt(string);
		} catch (DetailedEntityType.Error e) {
			if (shouldSucceed) {
				Assert.fail(string + ", (" + name + ") caused an error when it shouldn't have! Error " + e);
				e.printStackTrace();
			}
			return;
		}
		if (!shouldSucceed) {
			Assert.fail(string + ", (" + name + ") succeeded when it shouldn't have!");
		}

	}
}
