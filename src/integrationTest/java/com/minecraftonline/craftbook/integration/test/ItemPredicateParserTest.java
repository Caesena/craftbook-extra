/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration.test;

import com.minecraftonline.legacy.ics.chips.world.entity.ChestCollector;
import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.item.ItemParameters;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicate;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicateParser;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicateParsers;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.lwts.runner.LaunchWrapperTestRunner;

import java.util.concurrent.atomic.AtomicReference;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(LaunchWrapperTestRunner.class)
public class ItemPredicateParserTest {

    private final ItemPredicateParser TYPE_AND_DAMAGE = ItemPredicateParser.builder()
            .parameter(ItemParameters.ITEM_TYPE)
            .delimiter(":")
            .parameter(ItemParameters.DAMAGE)
            .build();

    private final ItemPredicateParser basicDamage = ItemPredicateParser.builder()
            .parameter(ItemParameters.DAMAGE)
            .build();

    @Test
    public void blank() {
        assertResult(ItemPredicateParsers.OPTIONAL_ITEM_TYPE_COLON_DAMAGE, "", null);
    }

    @Test
    public void chestCollectorBlank() throws Parameter.ParsingException, InvalidICException {
        System.out.println("Testing..");
        AtomicReference<ItemPredicate> itemPredicate = new AtomicReference<>();
        ChestCollector.ITEM_PREDICATE_PARSER_FACTORY.createBinder(null, itemPredicate::set).parse("", null);
        assertEquals(ItemPredicate.ANY, itemPredicate.get());
    }

    @Test
    public void basic() {
        assertResult(TYPE_AND_DAMAGE, "1:1", ItemPredicate.builder().itemType(ItemTypes.STONE).damage(1).build());
        assertResult(basicDamage, "10",
                ItemPredicate.builder().damage(10).build());
    }

    @Test
    public void mixed() {
        //assertResult(TYPE_AND_DAMAGE, "wool:1",
        //        ItemPredicate.builder().itemType(ItemTypes.WOOL).damage(1).build());
    }

    public void assertResult(ItemPredicateParser parser, String s, ItemPredicate expected) {
        try {
            assertEquals(expected, parser.parse(s));
        } catch (Parameter.ParsingException e) {
            throw new IllegalStateException(e);
        }
    }

    public void assertExceptionExpected(ItemPredicateParser parser, String s) {
        try {
            parser.parse(s);
            fail("Expected an exception!");
        } catch (Parameter.ParsingException ignored) {}
    }
}
