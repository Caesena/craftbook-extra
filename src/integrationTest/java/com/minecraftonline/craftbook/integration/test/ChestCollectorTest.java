/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration.test;

import com.flowpowered.math.GenericMath;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.craftbook.integration.ICLinesBuilder;
import com.minecraftonline.craftbook.integration.IntegrationTestGame;
import com.minecraftonline.craftbook.integration.IntegrationTestUtil;
import com.minecraftonline.craftbook.integration.ServerConfigurer;
import com.minecraftonline.legacy.ics.chips.world.entity.ChestCollector;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.ICManager;
import com.sk89q.craftbook.sponge.mechanics.ics.ICType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spongepowered.api.Server;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.carrier.Chest;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.common.SpongeImpl;
import org.spongepowered.common.world.storage.SpongeChunkLayout;
import org.spongepowered.lwts.runner.LaunchWrapperTestRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(LaunchWrapperTestRunner.class)
public class ChestCollectorTest {

    @Test
    public void mainTest() {

        final ICType<ChestCollector> icType = ICManager.getICType(ChestCollector.Factory.class, false);

        MinecraftServer mockServer = mock(MinecraftServer.class, "mockServer");

        when(mockServer.isCallingFromMinecraftThread()).thenReturn(true);

        Server server = (Server) mockServer;
        when(server.isMainThread()).thenReturn(true);
        when(server.getChunkLayout()).thenReturn(SpongeChunkLayout.instance);

        IntegrationTestGame testGame = (IntegrationTestGame) SpongeImpl.getGame();

        try (ServerConfigurer serverConfigurer = testGame.usingServer((Server) mockServer)) {

            WorldServer worldServer = serverConfigurer.worldBuilder()
                    .worldType(WorldType.FLAT)
                    .withChunkGeneration()
                    .buildAndRegister("dummy_world");

            final int RADIUS = 8;

            List<Text> lines = ICLinesBuilder.builder(icType)
                    .line(3, Text.of(RADIUS))
                    .build();

            World world = (World) worldServer;
            Location<World> loc = new Location<>(world, Vector3d.from(0, 100, 0));

            ChestCollector chestCollector = IntegrationTestUtil.createAndLoadICAt(
                    loc,
                    Direction.NORTH,
                    icType.getModel(),
                    mock(Player.class),
                    lines,
                    ChestCollector.class
            );

            Location<World> signPos = chestCollector.getBlock();
            Location<World> chestPos = signPos.add(0, 1, 0);
            chestPos.setBlockType(BlockTypes.CHEST);

            final double TOTAL_CIRCLE_ANGLE = 2 * Math.PI;

            final int testsPerCircle = 8;
            final int radialTests = 2;

            double angle = TOTAL_CIRCLE_ANGLE / testsPerCircle;
            double radiusPortion = ((double) RADIUS) / radialTests;

            int itemCounter = 0;

            // Stop the items stacking and make it the a better test
            Collection<ItemType> allItemTypes = Sponge.getRegistry().getAllOf(ItemType.class);
            List<ItemType> allItemTypesList = new ArrayList<>(allItemTypes);

            // Spherical co-ordinates.
            // We use it because spherical co-ordinates are good at defining points in a sphere!.
            // https://en.wikipedia.org/wiki/Spherical_coordinate_system#Cartesian_coordinates

            IntegrationTestUtil.triggerICPin(chestCollector, 0);

            Chest chest = (Chest) chestPos.getTileEntity().get();
            Inventory inv = chest.getInventory();

            boolean doneOrigin = false;

            int ownedItem = 0;

            for (int i = 0; i < testsPerCircle; i++) {
                double theta = angle * i;
                for (int j = 0; j < testsPerCircle; j++) {
                    double phi = angle * j;
                    for (int k = 0; k <= radialTests + 1; k++) {
                        ownedItem++;
                        if (k == 0) {
                            if (doneOrigin) {
                                continue;
                            }
                            doneOrigin = true;
                        }
                        // We never really need this "exactly" the radius, so taking off a 100 Epsilons never hurt anybody.
                        double radius = (radiusPortion * k) - (50*GenericMath.DBL_EPSILON);

                        System.out.println("Theta: " + theta + " Phi: " + phi + " Radius: " + radius);

                        boolean outOfRange = radius > RADIUS;
                        boolean hasOwnerTag = ownedItem % 10 == 0;

                        // x = r sin θ * cos φ
                        // y = r sin θ * sin φ
                        // z = r cos θ

                        double x = radius * Math.sin(theta) * Math.cos(phi);
                        double y = radius * Math.sin(theta) * Math.sin(phi);
                        double z = radius * Math.cos(theta);

                        Vector3d offset = Vector3d.from(x, y, z);

                        System.out.println("Offset length " + offset.length());

                        System.out.printf("Offset  x: %.2f y: %.2f z: %.2f%n", x, y, z);

                        Location<World> testPos = signPos.add(offset);
                        Vector3i chunkPos = Sponge.getServer().getChunkLayout().forceToChunk(testPos.getBlockPosition());
                        loc.getExtent().loadChunk(chunkPos, true).get();
                        Item itemEntity = (Item) testPos.createEntity(EntityTypes.ITEM);

                        ItemType itemType;
                        do {
                            itemType = allItemTypesList.get(itemCounter);
                            itemCounter++;
                        } while (itemType == ItemTypes.AIR);

                        ItemStack itemStack = ItemStack.of(itemType);

                        int amt = (itemCounter % itemStack.getMaxStackQuantity()) + 1; // Make sure its not 0.
                        itemStack.setQuantity(amt);

                        System.out.printf("ItemType: %s Quantity: %s%n", itemType.getName(), amt);

                        itemEntity.offer(Keys.REPRESENTED_ITEM, itemStack.createSnapshot());
                        if (hasOwnerTag) {
                            ((EntityItem) itemEntity).setOwner("tyhdefu");
                        }

                        boolean suc = testPos.spawnEntity(itemEntity);

                        assertTrue("Failed to spawn entity!", suc);

                        long before = System.currentTimeMillis();
                        Runnable asyncRun = chestCollector.getAsyncAction();

                        System.out.println("Took: " + (System.currentTimeMillis() - before) + "ms");

                        //try {
                            asyncRun.run();
                            //CraftBookPlugin.spongeInst().getScheduler().submitAsync(asyncRun).get(1, TimeUnit.SECONDS);
                        /*} catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (TimeoutException e) {
                            e.printStackTrace();
                            fail("Took over 1 second for task to finish!");
                        }*/
                        before = System.currentTimeMillis();
                        CraftBookPlugin.spongeInst().getScheduler().tickSync();
                        System.out.println("Took: " + (System.currentTimeMillis() - before) + "ms for Sync tasks");

                        if (hasOwnerTag) {
                            assertFalse("Entity with owner tag was removed!", itemEntity.isRemoved());
                            assertEquals("There were items in the inventory when there shouldn't have been", 0, inv.totalItems());
                        }
                        else if (!outOfRange) {
                            assertTrue("Entity was not removed!", itemEntity.isRemoved());
                            assertEquals("Total amount of items in inventory did not match", amt, inv.totalItems());

                            assertEquals("Total amount of the specified items did not match? did other items types creep in?", amt, inv.query(QueryOperationTypes.ITEM_TYPE.of(itemType)).totalItems());
                            inv.clear();
                        }
                        else {
                            assertEquals("There were items in the inventory when there shouldn't have been", 0, inv.totalItems());
                            itemEntity.remove();
                        }

                        worldServer.removeEntityDangerously((Entity) itemEntity);
                    }
                }
            }
        } catch (InvalidICException /*| InterruptedException*/ e) {
            throw new IllegalStateException("Failed to create " + icType.getName() + " ic", e);
        }
    }
}