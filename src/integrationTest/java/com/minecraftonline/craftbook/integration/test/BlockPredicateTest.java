/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration.test;

import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.Parser;
import com.minecraftonline.util.parsing.block.predicate.BlockPredicate;
import com.minecraftonline.util.parsing.block.predicate.BlockPredicateParsers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.lwts.runner.LaunchWrapperTestRunner;

import static org.junit.Assert.assertEquals;

@RunWith(LaunchWrapperTestRunner.class)
public class BlockPredicateTest {

    @Test
    public void testSimple() throws Parameter.ParsingException {
        assertParse(BlockPredicateParsers.OPTIONAL_BLOCK_TYPE_COLON_OPTIONAL_DAMAGE, "1", BlockPredicate.builder().blockType(BlockTypes.STONE).build());
    }


    public static <T> void assertParse(Parser<T> parser, String toParse, T expected) throws Parameter.ParsingException {
        T value = parser.parse(toParse);
        assertEquals(expected, value);
    }
}
