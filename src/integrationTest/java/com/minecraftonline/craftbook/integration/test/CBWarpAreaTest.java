/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration.test;

import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.minecraftonline.craftbook.integration.ICLinesBuilder;
import com.minecraftonline.craftbook.integration.IntegrationTestGame;
import com.minecraftonline.craftbook.integration.IntegrationTestUtil;
import com.minecraftonline.craftbook.integration.ServerConfigurer;
import com.minecraftonline.legacy.ics.chips.world.entity.CBWarpArea;
import com.minecraftonline.legacy.mechanics.cbwarps.CBWarp;
import com.minecraftonline.legacy.mechanics.cbwarps.CBWarps;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.ICManager;
import com.sk89q.craftbook.sponge.mechanics.ics.ICType;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.spongepowered.api.Server;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.common.SpongeImpl;
import org.spongepowered.common.world.storage.SpongeChunkLayout;
import org.spongepowered.lwts.runner.LaunchWrapperParameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(LaunchWrapperParameterized.class)
public class CBWarpAreaTest {

    @Parameterized.Parameter(0)
    public Direction signDirection;

    @Parameterized.Parameter(1)
    public String filter;

    @Parameterized.Parameters(name = "{index}: Test in {0} direction")
    public static Iterable<Object[]> makeParameters() {
        // Weird order so that we know that North when succeeds it is north rather because it is first.
        return Arrays.asList(
                new Object[] {Direction.SOUTH, ""},
                new Object[] {Direction.EAST, ""},
                new Object[] {Direction.NORTH, ""},
                new Object[] {Direction.WEST, ""},
                new Object[] {Direction.WEST, "%PLAYER"}
        );
    }

    @Test
    public void mainTest() {

        final ICType<CBWarpArea> icType = ICManager.getICType(CBWarpArea.Factory.class, false);

        MinecraftServer mockServer = mock(MinecraftServer.class, "mockServer");

        when(mockServer.isCallingFromMinecraftThread()).thenReturn(true);
        when(((Server)mockServer).getChunkLayout()).thenReturn(SpongeChunkLayout.instance);

        IntegrationTestGame testGame = (IntegrationTestGame) SpongeImpl.getGame();

        try (ServerConfigurer serverConfigurer = testGame.usingServer((Server) mockServer)) {

            WorldServer worldServer = serverConfigurer.worldBuilder()
                    .worldType(WorldType.FLAT)
                    .withChunkGeneration()
                    .buildAndRegister("dummy_world");

            Vector3d cbWarpLoc = Vector3d.from(100, 100, 100);

            CBWarp cbWarp = new CBWarp(
                    "testWarp",
                    worldServer.getWorldInfo().getWorldName(),
                    (short) worldServer.provider.getDimensionType().getId(),
                    cbWarpLoc,
                    Vector2d.ZERO,
                    "Destination Message",
                    "Welcome Message",
                    null
            );

            CBWarps.warps.add(cbWarp);

            final int WIDTH = 3;

            List<Text> lines = ICLinesBuilder.builder(icType)
                    .line(2, Text.of(cbWarp.getTitle()))
                    .line(3, Text.of("1:3:3"))
                    .build();

            World world = (World) worldServer;
            Location<World> loc = new Location<>(world, Vector3d.from(0, 100, 0));

            CBWarpArea cbWarpArea = IntegrationTestUtil.createAndLoadICAt(
                    loc,
                    this.signDirection,
                    icType.getModel(),
                    mock(Player.class),
                    lines,
                    CBWarpArea.class
            );

            Location<World> backBlockCentre = cbWarpArea.getBackBlockCentre();

            Direction signPointing = cbWarpArea.getBlock().require(Keys.DIRECTION).getOpposite();

            WidthLengthHeightLocations validLocs = new WidthLengthHeightLocations(
                    Arrays.asList(
                            0.2,
                            0.5,
                            1.0
                    ),
                    Arrays.asList(
                            0.2,
                            1.0,
                            2.0,
                            2.5,
                            3.0
                    ),
                    Arrays.asList(
                            0.0,
                            0.5
                    )
            );

            WidthLengthHeightLocations invalidLocs = new WidthLengthHeightLocations(
                    Arrays.asList(
                            1.1,
                            2.5
                    ),
                    Arrays.asList(
                            3.1,
                            4.0
                    ),
                    Arrays.asList(
                            1.0
                    )
            );

            Direction left = leftMap.get(signPointing);

            Vector3d leftShift = left.asOffset().abs().mul(0.5);

            //System.out.println("Sign facing: " + this.signDirection);
            //System.out.println("Sign facing: " + this.signDirection.asOffset());
            //System.out.println("Left shift: " + leftShift);

            Vector3d startingPos = cbWarpArea.getBlockCentre().getPosition().add(cbWarpArea.getBackBlockCentre().getPosition()).div(2)
                    .add(0, 0.5, 0)
                    .add(0, 1, 0); // Should be 1 block upward offset

            //System.out.println("CBWarpArea is at: " + cbWarpArea.getBlock().getPosition());
            //System.out.println("CBWarpArea has min: " + cbWarpArea.min);
            //System.out.println("CBWarpArea has max: " + cbWarpArea.max);
            //System.out.println("CBWarpArea back block centre is: " + backBlockCentre);
            //System.out.println("Starting pos is: " + startingPos);

            Map<EntityPlayerMP, Boolean> playerShouldTeleportMap = new HashMap<>();

            int i = 0;
            for (Vector3d pos : validLocs.getAllPositions(startingPos, left, signPointing)) {
                EntityPlayerMP player = IntegrationTestUtil.createPlayerWithFakeConnection(mockServer, worldServer, "In range player " + i, pos);
                playerShouldTeleportMap.put(player, true);
                i++;
            }
            i = 0;
            for (Vector3d pos : invalidLocs.getAllPositions(startingPos, left, signPointing)) {
                EntityPlayerMP player = IntegrationTestUtil.createPlayerWithFakeConnection(mockServer, worldServer, "Out of range player " + i, pos);
                playerShouldTeleportMap.put(player, false);
                i++;
            }

            serverConfigurer.setPlayers(playerShouldTeleportMap.keySet(), worldServer);

            IntegrationTestUtil.triggerICPin(cbWarpArea, 0);

            for (Map.Entry<EntityPlayerMP, Boolean> entry : playerShouldTeleportMap.entrySet()) {
                EntityPlayerMP player = entry.getKey();
                boolean shouldHaveTeleported = entry.getValue();
                Vector3d pos = new Vector3d(player.posX, player.posY, player.posZ);
                if (shouldHaveTeleported) {
                    assertEquals("Player '" + player.getName() + "' was not where they should have been!", cbWarpLoc, pos);
                }
                else {
                    assertNotEquals("Player '" + player.getName() + "' was not where they should have been!", cbWarpLoc, pos);
                }
            }

        } catch (InvalidICException e) {
            throw new IllegalStateException("Failed to create" + icType.getName() + " ic", e);
        } finally {
            CBWarps.warps.clear();
        }
    }

    private static final BiMap<Direction, Direction> leftMap = HashBiMap.create();

    static {
        leftMap.put(Direction.NORTH, Direction.WEST);
        leftMap.put(Direction.EAST, Direction.NORTH);
        leftMap.put(Direction.SOUTH, Direction.EAST);
        leftMap.put(Direction.WEST, Direction.SOUTH);
    }

    public static class WidthLengthHeightLocations {

        private final List<Double> widths;
        private final List<Double> lengths;
        private final List<Double> heights;

        public WidthLengthHeightLocations(List<Double> widths, List<Double> lengths, List<Double> heights) {
            this.widths = widths;
            this.lengths = lengths;
            this.heights = heights;
        }

        public List<Vector3d> getAllPositions(Vector3d start, Direction widthDir, Direction lengthDir) {
            List<Vector3d> positions = new ArrayList<>();

            Vector3d widthVector = widthDir.asOffset();
            Vector3d lengthVector = lengthDir.asOffset();

            for (Double width : this.widths) {
                for (Double length : this.lengths) {
                    for (Double height : heights) {
                        // Include negative widths but not negative lengths or heights
                        positions.add(start.add(widthVector.mul(width)).add(lengthVector.mul(length)).add(0, height, 0));
                        positions.add(start.add(widthVector.mul(-width)).add(lengthVector.mul(length)).add(0, height, 0));
                    }
                }
            }
            return positions;
        }
    }
}
