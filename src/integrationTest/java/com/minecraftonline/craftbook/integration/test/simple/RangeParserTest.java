/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration.test.simple;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.lineparser.range.RangeLimitations;
import com.minecraftonline.util.parsing.lineparser.range.RangeLineParserFactory;
import com.minecraftonline.util.parsing.rangeparser.RangeParameters;
import com.minecraftonline.util.parsing.rangeparser.RangeParser;
import com.minecraftonline.util.parsing.rangeparser.RangeParsers;
import com.minecraftonline.util.parsing.rangeparser.RelativeRange;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spongepowered.lwts.runner.LaunchWrapperTestRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(LaunchWrapperTestRunner.class)
public class RangeParserTest {

    private final RangeParser simpleParser = RangeParser.builder()
            .parameters(":", Arrays.asList(RangeParameters.WIDTH, RangeParameters.LENGTH))
            .build();

    @Test
    public void basic() {
        try {
            RelativeRange range = simpleParser.parse("10:5");
            assertEquals(
                    RelativeRange.builder().width(10).length(5).build(),
                    range
            );
        } catch (Parameter.ParsingException e) {
            throw new IllegalStateException(e);
        }
    }

    @Test
    public void basicFailBlank() {
        assertExceptionExpected(simpleParser, "");
    }

    @Test
    public void basicFailNonNumber() {
        assertExceptionExpected(simpleParser, "aaa:bbb");
    }

    @Test
    public void basicFailNotEnoughDelimitation() {
        assertExceptionExpected(simpleParser, "10");
    }

    @Test
    public void basicFailTooMuchDelimitation() {
        assertExceptionExpected(simpleParser, "3:2:1");
    }

    @Test
    public void test_HEIGHT_WIDTH_LENGTH_AND_NON_RELATIVE_OFFSET() {
        assertResult(RangeParsers.CBWARP_AREA, "1:3:3",
                RelativeRange.builder().height(1).width(3).length(3).yOffset(1).height(1).build());

        assertResult(RangeParsers.CBWARP_AREA, "1:2:3",
                RelativeRange.builder().height(1).width(2).length(3).yOffset(1).height(1).build());

        assertResult(RangeParsers.CBWARP_AREA, "1:2:3/4:5:6",
                RelativeRange.builder().height(1).width(2).length(3).offset(Vector3d.from(4, 5, 6)).build());
    }

    @Test
    public void test_WIDTH_LENGTH_AND_DEFAULT_1_Y_OFFSET() {
        assertResult(RangeParsers.WIDTH_LENGTH_AND_DEFAULT_1_Y_OFFSET, "1:2",
                RelativeRange.builder().width(1).length(2).yOffset(1).height(1).build());

        assertResult(RangeParsers.WIDTH_LENGTH_AND_DEFAULT_1_Y_OFFSET, "1:2:3",
                RelativeRange.builder().width(1).length(2).yOffset(3).height(1).build());
    }

    @Test
    public void test_RANGE_AND_OFFSET() {
        assertResult(RangeParsers.RANGE_AND_OFFSET, "",
                RelativeRange.builder().range(1).build());

        assertResult(RangeParsers.RANGE_AND_OFFSET, "5",
                RelativeRange.builder().range(5).build());

        assertResult(RangeParsers.RANGE_AND_OFFSET, "5:2:3:4",
                RelativeRange.builder().range(5).offset(Vector3d.from(2, 3, 4)).build());
    }

    private static final RangeLineParserFactory RANGE_PARSER_FACTORY = RangeLineParserFactory.builder()
            .parser(RangeParsers.OPTIONAL_X_OPTIONAL_Y_OPTIONAL_Z_OFFSET)
            .limitation(RangeLimitations.maxOffsetInSingleDirection(20))
            .withDefault(RelativeRange.builder().yOffset(1).build())
            .build();

    @Test
    public void testFactory() {
        try {
            RelativeRange range = RANGE_PARSER_FACTORY.parse("", null, null);
            System.out.println(range);
        } catch (Parameter.ParsingException | InvalidICException e) {
            throw new IllegalStateException(e);
        }
    }

    public void assertResult(RangeParser rangeParser, String s, RelativeRange expected) {
        try {
            assertEquals(expected, rangeParser.parse(s));
        } catch (Parameter.ParsingException e) {
            throw new IllegalStateException(e);
        }
    }

    public void assertExceptionExpected(RangeParser rangeParser, String s) {
        try {
            rangeParser.parse(s);
            fail("Expected an exception!");
        } catch (Parameter.ParsingException ignored) {}
    }
}
