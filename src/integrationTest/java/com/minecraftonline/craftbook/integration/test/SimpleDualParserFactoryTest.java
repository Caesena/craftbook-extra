/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration.test;

import com.minecraftonline.craftbook.integration.IntegrationTestGame;
import com.minecraftonline.craftbook.integration.ServerConfigurer;
import com.minecraftonline.legacy.ics.chips.world.block.FlexSet;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.lineparser.generic.SimpleDualParserFactory;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.InvalidICException;
import net.minecraft.server.MinecraftServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spongepowered.api.Server;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.world.DimensionTypes;
import org.spongepowered.common.SpongeImpl;
import org.spongepowered.common.world.storage.SpongeChunkLayout;
import org.spongepowered.lwts.runner.LaunchWrapperTestRunner;

import java.util.concurrent.atomic.AtomicReference;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(LaunchWrapperTestRunner.class)
public class SimpleDualParserFactoryTest {

    private final SimpleDualParserFactory<BlockState, BlockState> FACTORY = new SimpleDualParserFactory<>(
            FlexSet.BLOCK_STATE_PARSER_FACTORY,
            FlexSet.BLOCK_STATE_PARSER_FACTORY,
            "[|]"
    );

    @Test
    public void mainTest() throws Parameter.ParsingException, InvalidICException {

        MinecraftServer mockServer = mock(MinecraftServer.class, "mockServer");

        when(mockServer.isCallingFromMinecraftThread()).thenReturn(true);

        Server server = (Server) mockServer;
        when(server.isMainThread()).thenReturn(true);
        when(server.getChunkLayout()).thenReturn(SpongeChunkLayout.instance);

        IntegrationTestGame testGame = (IntegrationTestGame) SpongeImpl.getGame();

        try (ServerConfigurer serverConfigurer = testGame.usingServer((Server) mockServer)) {

            serverConfigurer.worldBuilder()
                    .forceDefault(true)
                    .dimension(DimensionTypes.OVERWORLD)
                    .buildAndRegister("world");

            AtomicReference<BlockState> block1 = new AtomicReference<>();
            AtomicReference<BlockState> block2 = new AtomicReference<>();
            ParsingConfiguration.LineParser lineParser = FACTORY.createBinder(mock(IC.class), ((blockState, blockState2) -> {
                block1.set(blockState);
                block2.set(blockState2);
            }));

            lineParser.parse("35@0|35@1", null);

            assertEquals(BlockState.builder().blockType(BlockTypes.WOOL).add(Keys.DYE_COLOR, DyeColors.WHITE).build(), block1.get());
            assertEquals(BlockState.builder().blockType(BlockTypes.WOOL).add(Keys.DYE_COLOR, DyeColors.ORANGE).build(), block2.get());
        }


    }
}
