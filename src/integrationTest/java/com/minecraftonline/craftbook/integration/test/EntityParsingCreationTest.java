/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration.test;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.Sets;
import com.minecraftonline.util.entity.DetailedEntityType;
import com.minecraftonline.util.entity.EntityParsingUtil;
import net.minecraft.profiler.Profiler;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.PlayerList;
import net.minecraft.world.GameType;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldSettings;
import net.minecraft.world.WorldType;
import net.minecraft.world.storage.ISaveHandler;
import net.minecraft.world.storage.WorldInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.BaseValue;
import org.spongepowered.api.data.value.mutable.Value;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.common.bridge.world.WorldInfoBridge;
import org.spongepowered.common.data.value.mutable.SpongeValue;
import org.spongepowered.lwts.runner.LaunchWrapperParameterized;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(LaunchWrapperParameterized.class)
public class EntityParsingCreationTest {

    @Parameterized.Parameter(0)
    public String name;

    @Parameterized.Parameter(1)
    public String string;

    @Parameterized.Parameter(2)
    public EntityType entityType;

    @Parameterized.Parameter(3)
    public Set<Value<?>> extraValues;

    @Parameterized.Parameters(name = "{index}: Test {0} ({1}) EntityType {1}, Extra values: {2}")
    public static Iterable<Object[]> makeParameters() {
        return Arrays.asList(
                of("Pig", EntityTypes.PIG),
                of("Charged Creeper", "Creeper@1", EntityTypes.CREEPER, makeValue(Keys.CREEPER_CHARGED, true)),
                of("Normal Creeper", "Creeper@0", EntityTypes.CREEPER, makeValue(Keys.CREEPER_CHARGED, false)),
                of("Wither", EntityTypes.WITHER),
                of("Steve Police Zombie", "zombie{CustomName:\"Steve Police\"}", EntityTypes.ZOMBIE, makeValue(Keys.DISPLAY_NAME, Text.of("Steve Police"))),
                of("Sponge human", "sponge:human{CustomName:\"hooman\"}", EntityTypes.HUMAN, makeValue(Keys.DISPLAY_NAME, Text.of("hooman"))),
                of("snowball", EntityTypes.SNOWBALL),
                of("egg", EntityTypes.EGG),
                of("fireball", EntityTypes.FIREBALL)
        );
    }

    @Test
    public void testCreation() throws DetailedEntityType.Error {
        DetailedEntityType detailedEntityType = EntityParsingUtil.parseStackedAndNbt(string);

        if (!detailedEntityType.isCreatable()) {
            fail("DetailedEntityType should have been creatable, but wasn't");
        }
        WorldInfo worldInfo = new WorldInfo(new WorldSettings(0, GameType.NOT_SET, false, false, WorldType.DEFAULT), "sponge$dummy_world");

        ((WorldInfoBridge)worldInfo).bridge$setDimensionId(0);

        MinecraftServer mockServer = mock(MinecraftServer.class, "mockServer");
        when(mockServer.getPlayerList()).thenReturn(mock(PlayerList.class));
        WorldServer worldServer = new WorldServer(
                mockServer,
                mock(ISaveHandler.class, "mockSaveHandler"),
                worldInfo,
                0,
                mock(Profiler.class, "mockProfiler")
        );

        Location<World> loc = new Location<>((World) worldServer, Vector3i.ZERO);
        Optional<Entity> optEntity = detailedEntityType.createEntity(loc);

        assertTrue("Attempted to create an creatable entity but it failed!", optEntity.isPresent());

        Entity entity = optEntity.get();

        assertEquals(entityType, entity.getType());

        for (Value<?> value : extraValues) {
            assertValue(entity, value);
        }
    }

    private static Object[] of(String parsingString, EntityType entityType) {
        return of(parsingString, parsingString, entityType);
    }

    private static Object[] of(String name, String parsingString, EntityType entityType, Value<?>... values) {
        return new Object[] {name, parsingString, entityType, Sets.newHashSet(values) };
    }

    private static <E> SpongeValue<E> makeValue(Key<? extends BaseValue<E>> key, E value) {
        return new SpongeValue<>(key, value);
    }

    private <T> void assertValue(DataHolder dataHolder, Value<T> value) {
        Key<? extends BaseValue<T>> key = value.getKey();
        if (!dataHolder.supports(key)) {
            throw new IllegalArgumentException("Invalid test parameters. This test will never succeed as key " + key.getName() + " isn't supported on this data holder!");
        }

        Optional<T> optionalValue = dataHolder.get(value.getKey());

        assertTrue("Data Holder had no value for key " + key.getName() + " when it should have", optionalValue.isPresent());

        assertEquals("Mismatching Data Holder value: " + key.getName(), optionalValue.get(), value.get());
    }
}
