/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration.test;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.craftbook.integration.ICLinesBuilder;
import com.minecraftonline.craftbook.integration.IntegrationTestGame;
import com.minecraftonline.craftbook.integration.IntegrationTestUtil;
import com.minecraftonline.craftbook.integration.ServerConfigurer;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.NamedNearby;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.ICManager;
import com.sk89q.craftbook.sponge.mechanics.ics.ICType;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketChat;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.spongepowered.api.Server;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.DimensionTypes;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.common.SpongeImpl;
import org.spongepowered.lwts.runner.LaunchWrapperTestRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(LaunchWrapperTestRunner.class)
public class NamedNearbyTest {

    @Test
    public void mainTest() throws InvalidICException {
        ICType<NamedNearby> icType = ICManager.getICType(NamedNearby.Factory.class, false);
        final String NAMED_NEARBY_MODEL = icType.getModel();

        // Sign lines
        List<Text> lines = ICLinesBuilder.builder(icType)
                .line(0, Text.of("64"))
                .line(2, Text.of("hello %p"))
                .build();

        MinecraftServer mockServer = mock(MinecraftServer.class, "mockServer");

        when(mockServer.isCallingFromMinecraftThread()).thenReturn(true);

        IntegrationTestGame testGame = (IntegrationTestGame) SpongeImpl.getGame();

        try (ServerConfigurer serverConfigurer = testGame.usingServer((Server) mockServer)) {
            WorldServer worldServer = serverConfigurer.worldBuilder()
                    .dimension(DimensionTypes.OVERWORLD)
                    .worldType(WorldType.FLAT)
                    .withChunkGeneration()
                    .buildAndRegister("dummy_world");

            // Fairy simple placing of the ic sign, input and their supporting blocks.
            Vector3i signPos = Vector3i.from(0, 100, 0);

            Direction dir = Direction.NORTH;

            World world = (World) worldServer;

            NamedNearby namedNearby = IntegrationTestUtil.createAndLoadICAt(
                    new Location<>(world, signPos),
                    dir,
                    NAMED_NEARBY_MODEL,
                    mock(Player.class),
                    lines,
                    NamedNearby.class
            );

            EntityPlayerMP p1 = IntegrationTestUtil.createPlayerWithFakeConnection(mockServer, worldServer, "Player 1", signPos.toDouble()); // Ontop of the sign.
            EntityPlayerMP p2 = IntegrationTestUtil.createPlayerWithFakeConnection(mockServer, worldServer, "Player 2", signPos.add(0, 50, 0).toDouble()); // Still within 64 blocks
            EntityPlayerMP p3 = IntegrationTestUtil.createPlayerWithFakeConnection(mockServer, worldServer, "Player 3", signPos.add(0, 100, 0).toDouble()); // Out of range

            List<EntityPlayerMP> players = Arrays.asList(p1, p2, p3);

            serverConfigurer.setPlayers(players, worldServer);

            IntegrationTestUtil.triggerICPin(namedNearby, 0);

            // Check we received the expected messages.
            Text message = Text.of("hello " + p1.getName());

            verify(p1.connection, times(1)).sendPacket(argThat(new SPacketChatArgumentMatcher(message)));
            verify(p2.connection, times(1)).sendPacket(argThat(new SPacketChatArgumentMatcher(message)));

            verify(p3.connection, times(0)).sendPacket(any(SPacketChat.class));
        }
    }

    public static class SPacketChatArgumentMatcher implements ArgumentMatcher<SPacketChat> {
        private final Text expected;

        public SPacketChatArgumentMatcher(Text expected) {

            this.expected = expected;
        }

        @Override
        public boolean matches(SPacketChat argument) {
            try {
                // TODO: the input from the packet is a very messy text object, that equates
                //  to the same thing, but .equals() doesn't do the job.
                Text text = readText(argument);

                String inputString = TextSerializers.FORMATTING_CODE.serialize(text);
                String expectedString = TextSerializers.FORMATTING_CODE.serialize(expected);

                return expectedString.equals(inputString);
            } catch (IOException e) {
                throw new IllegalStateException("Error reading chat packet", e);
            }
        }
    }

    private static Text readText(SPacketChat packet) throws IOException {
        String jsonText = ITextComponent.Serializer.componentToJson(packet.chatComponent);

        return TextSerializers.JSON.deserialize(jsonText);
    }
}
