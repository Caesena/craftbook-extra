/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration.test.simple;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.vector3i.BasicOffsetParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spongepowered.lwts.runner.LaunchWrapperTestRunner;

import static org.junit.Assert.assertEquals;

@RunWith(LaunchWrapperTestRunner.class)
public class BasicOffsetParserTest {

    @Test
    public void testBasic() throws Parameter.ParsingException {
        assertParserResult("X+1", Vector3i.from(1, 0, 0));
        assertParserResult("X+3", Vector3i.from(3, 0, 0));
        assertParserResult("X+8", Vector3i.from(8, 0, 0));

        assertParserResult("Y+2", Vector3i.from(0, 2, 0));
        assertParserResult("Y+4", Vector3i.from(0, 4, 0));
        assertParserResult("Y+7", Vector3i.from(0, 7, 0));

        assertParserResult("Z+5", Vector3i.from(0, 0, 5));
        assertParserResult("Z+8", Vector3i.from(0, 0, 8));
        assertParserResult("Z+9", Vector3i.from(0, 0, 9));

        assertParserResult("X+100", Vector3i.from(100, 0, 0));
        assertParserResult("Y+100", Vector3i.from(0, 100, 0));
        assertParserResult("Z+100", Vector3i.from(0, 0, 100));
    }

    @Test(expected = Parameter.ParsingException.class)
    public void testGibberish() throws Parameter.ParsingException {
        BasicOffsetParser.INSTANCE.parse("AKLDJWK");
    }

    @Test(expected = Parameter.ParsingException.class)
    public void testInvalidPlusMinus() throws Parameter.ParsingException {
        BasicOffsetParser.INSTANCE.parse("X#100");
    }

    @Test(expected = Parameter.ParsingException.class)
    public void testInvalidNumber() throws Parameter.ParsingException {
        BasicOffsetParser.INSTANCE.parse("X+1A");
    }

    @Test(expected = Parameter.ParsingException.class)
    public void testInvalidDir() throws Parameter.ParsingException {
        BasicOffsetParser.INSTANCE.parse("A+5");
    }

    public static void assertParserResult(String parse, Vector3i expected) throws Parameter.ParsingException {
        Vector3i result = BasicOffsetParser.INSTANCE.parse(parse);
        assertEquals(expected, result);
    }
}
