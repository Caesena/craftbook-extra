/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration;

import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.mechanics.ics.ICType;
import org.spongepowered.api.text.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ICLinesBuilder {

    private static final int MAX_SIGN_LINES = 4;

    private final ICType<? extends IC> icType;
    private final Map<Integer, Text> lines = new HashMap<>();

    public ICLinesBuilder(ICType<? extends IC> icType) {
        this.icType = icType;
        this.lines.put(1, Text.of("[" + this.icType.getModel() + "]"));
    }

    public static ICLinesBuilder builder(ICType<? extends IC> icType) {
        return new ICLinesBuilder(icType);
    }

    /**
     * Makes a legacy top line. This means that
     * the given text is appended to name of
     * the ic instead of overwriting it.
     * @param line Line to append to name.
     * @return This builder, for chaining
     */
    public ICLinesBuilder legacyTopLine(Text line) {
        lines.put(0, Text.of(icType.getName(), line));
        return this;
    }

    /**
     * Sets a certain line of the ic.
     * @param lineIndex Line index (0-3)
     * @param line Text of line to set.
     * @return This builder, for chaining.
     */
    public ICLinesBuilder line(int lineIndex, Text line) {
        if (lineIndex == 0) {
            if (!icType.getFactory().usesFirstLine()) {
                throw new IllegalArgumentException("IC " + icType.getName() + " doesn't support text on the top line!");
            }
        }
        if (lineIndex == 1) {
            throw new IllegalArgumentException("Cannot set line 2, that is reserved by the model! If you want to set a mode, use mode()");
        }
        if (lineIndex >= MAX_SIGN_LINES) {
            throw new IndexOutOfBoundsException("Line index out of bounds! Must be 0-3. Was: " + lineIndex);
        }
        lines.put(lineIndex, line);
        return this;
    }

    /**
     * Sets the mode of this IC.
     * The mode is a special modifier such
     * as inverse, that is appended after
     * the model text.
     * @param mode Mode to set
     * @return This builder, for chaining
     */
    public ICLinesBuilder mode(String mode) {
        lines.put(1, Text.of("[" + this.icType.getModel() + "]" + mode));
        return this;
    }

    public List<Text> build() {
        List<Text> list = new ArrayList<>();
        for (int i = 0; i < MAX_SIGN_LINES; i++) {
            Text line = lines.get(i);
            list.add(line == null ? Text.EMPTY : line);
        }
        return list;
    }
}
