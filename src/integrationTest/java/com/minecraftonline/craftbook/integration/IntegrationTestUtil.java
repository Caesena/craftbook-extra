/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook.integration;

import com.flowpowered.math.vector.Vector3d;
import com.mojang.authlib.GameProfile;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.ICManager;
import com.sk89q.craftbook.sponge.mechanics.ics.ICType;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.PlayerInteractionManager;
import net.minecraft.world.WorldServer;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;

public class IntegrationTestUtil {

    public static <T extends IC> T createAndLoadICAt(final Location<World> loc, Direction signDir, final String model, Player creator, List<Text> lines, Class<T> icClazz) throws InvalidICException {
        ICType<T> icType = (ICType<T>) ICManager.getICType("[" + model + "]");
        if (icType == null) {
            throw new IllegalArgumentException("IC model '" + model + "' doesn't exist");
        }
        ICFactory<T> factory = icType.getFactory();

        loc.getRelative(signDir.getOpposite()).setBlockType(BlockTypes.DIRT);

        loc.setBlockType(BlockTypes.WALL_SIGN);
        loc.offer(Keys.DIRECTION, signDir);
        loc.offer(Keys.SIGN_LINES, lines);

        T ic = factory.createInstance(loc);

        ic.create(creator, lines);
        ic.setMode();
        ic.load();
        return ic;
    }

    public static void triggerICPin(IC ic, int inputPin) {
        Location<World> icLoc = ic.getBlock();
        Location<World> loc = ic.getPinSet().getPinLocation(inputPin, ic);

        Vector3d offset = loc.getPosition().sub(icLoc.getPosition());

        Direction awayFromIcDirection = Direction.getClosest(offset, Direction.Division.CARDINAL);

        Direction towardsIcDirection = awayFromIcDirection.getOpposite();

        Location<World> leverLoc = icLoc.getRelative(awayFromIcDirection);
        Location<World> leverSupportBlock = leverLoc.getBlockRelative(awayFromIcDirection);

        leverSupportBlock.setBlockType(BlockTypes.DIRT);
        BlockState lever = BlockState.builder().blockType(BlockTypes.LEVER).add(Keys.DIRECTION, towardsIcDirection).add(Keys.POWERED, true).build();
        leverLoc.setBlock(lever);
        ic.setTriggeredPin(0);
        ic.trigger();
    }

    public static EntityPlayerMP createPlayerWithFakeConnection(MinecraftServer server, WorldServer world, String name, Vector3d pos) {
        EntityPlayerMP player = new EntityPlayerMP(server, world, new GameProfile(null, name), mock(PlayerInteractionManager.class));
        player.connection = mock(NetHandlerPlayServer.class);
        player.setPosition(pos.getX(), pos.getY(), pos.getZ());

        player.connection.player = player;

        try {
            doCallRealMethod().when(player.connection).setPlayerLocation(anyDouble(), anyDouble(), anyDouble(), anyFloat(), anyFloat());
            doCallRealMethod().when(player.connection).setPlayerLocation(anyDouble(), anyDouble(), anyDouble(), anyFloat(), anyFloat(), any());
        } catch (Exception e) {
            e.printStackTrace();
        }


        return player;
    }
}
