/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.command;

import com.minecraftonline.legacy.ics.ExtraDataIC;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.mechanics.ics.ICManager;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.mechanics.ics.ICType;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.blockray.BlockRay;
import org.spongepowered.api.util.blockray.BlockRayHit;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;

public class ExtraLinesHelpExecutor implements CommandExecutor {

    private final ICSocket icSocket;

    public ExtraLinesHelpExecutor(ICSocket icSocket) {
        this.icSocket = icSocket;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(Text.of("Command can only be used as a player!"));
        }
        Player player = (Player)src;

        BlockRay<World> ray = BlockRay.from(player)
                .distanceLimit(50)
                .select(BlockRay.notAirFilter())
                .build();

        if (ray.hasNext()) {
            BlockRayHit<World> hit = ray.next();
            Location<World> loc = hit.getLocation();
            if (SignUtil.isSign(loc)) {
                Optional<IC> ic = icSocket.getIC(loc);
                if (ic.isPresent() && ic.get() instanceof ExtraDataIC) {
                    ExtraDataIC extraDataIC = (ExtraDataIC) ic.get();
                    ICType<?> icType = ICManager.getICType(extraDataIC.getFactory());
                    src.sendMessage(Text.of(TextColors.DARK_AQUA, "Displaying extra data help for " + icType.getName() + ", (" + icType.getModel() + ")"));
                    src.sendMessage(extraDataIC.usageMessage());
                    return CommandResult.success();
                }
            }
        }
        src.sendMessage(Text.of(TextColors.DARK_AQUA, "Extra data allows ics to store more information, e.g longer messages"));
        src.sendMessage(Text.of(TextColors.DARK_AQUA, "Look at a specific ic to see if it supports extra data, and if it does, what it uses it for"));
        return CommandResult.success();
    }
}
