/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.command;

import com.minecraftonline.data.ExtraICLinesData;
import com.minecraftonline.legacy.ics.ExtraDataIC;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextParseException;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.util.annotation.NonnullByDefault;
import org.spongepowered.api.util.blockray.BlockRay;
import org.spongepowered.api.util.blockray.BlockRayHit;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@NonnullByDefault
public class AddExtraLinesExecutor implements CommandExecutor {

	private final ICSocket icSocket;

	public AddExtraLinesExecutor(ICSocket icSocket) {
		this.icSocket = icSocket;
	}

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (!(src instanceof Player)) {
			throw new CommandException(Text.of("Command can only be used as a player!"));
		}
		Player player = (Player)src;
		Optional<String> optBook = getBookData(player, HandTypes.MAIN_HAND);

		if (!optBook.isPresent()) {
			optBook = getBookData(player, HandTypes.OFF_HAND);
			if (!optBook.isPresent()) {
				throw new CommandException(Text.of("You must hold a book and quil to use this command!"));
			}
		}
		BlockRay<World> ray = BlockRay.from(player)
				.distanceLimit(50)
				.select(BlockRay.notAirFilter())
				.build();

		String extraData = optBook.get();

		if (extraData.isEmpty()) {
			throw new CommandException(Text.of("Cannot add empty data to a sign!"));
		}

		if (!ray.hasNext()) {
			throw new CommandException(Text.of("You must look at a sign to use this command"));
		}

		BlockRayHit<World> hit = ray.next();
		Location<World> loc = hit.getLocation();

		if (!SignUtil.isSign(loc)) {
			throw new CommandException(Text.of("The block are looking at is not a sign!"));
		}
		IC ic = icSocket.getIC(loc)
				.orElseThrow(() -> new CommandException(Text.of("The sign you are looking at is not an ic!")));
		if (!(ic instanceof ExtraDataIC)) {
			throw new CommandException(Text.of("This ic does not support extra line data!"));
		}
		Optional<ExtraICLinesData> oldData = loc.get(ExtraICLinesData.class);

		// Try to parse
		Text text;
		try {
			text = TextSerializers.JSON.deserialize(extraData);
		} catch (TextParseException e) {
			text = TextSerializers.FORMATTING_CODE.deserialize(extraData);
		}

		loc.offer(new ExtraICLinesData(text));

		ic.unload(); // Unload the ic incase it needs to do any saving
		List<Text> lines = loc.require(Keys.SIGN_LINES);
		try {
			ic.create(player, lines);
		} catch (InvalidICException e) {
			oldData.ifPresent(loc::offer); // Revert extra data if there was some previously
			player.sendMessage(Text.of(TextColors.RED, "Unable to add data, it would create an invalid IC. Reverted Changes."));
			throw new CommandException(Text.of(e.getMessage()));
		}
		ic.load();

		player.sendMessage(Text.of(TextColors.GREEN, "Successfully added extra data to this ic"));
		return CommandResult.success();
	}

	private static Optional<String> getBookData(Player player, HandType hand) {
		return player.getItemInHand(hand).flatMap(item -> {
			if (item.getType() == ItemTypes.WRITABLE_BOOK) {
				return item.get(Keys.PLAIN_BOOK_PAGES).map(list -> String.join("", list));
			}
			else if (item.getType() == ItemTypes.WRITTEN_BOOK) {
				return item.get(Keys.BOOK_PAGES).map(list -> list.stream().map(Text::toPlain).collect(Collectors.joining()));
			}
			return Optional.empty();
		});
	}
}
