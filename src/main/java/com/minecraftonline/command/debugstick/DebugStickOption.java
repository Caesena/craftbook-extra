/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.command.debugstick;

import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

public class DebugStickOption {
    private final ICDebugStick.Mode mode;
    private final String permission;
    private final List<Predicate<IC>> requirements;
    private final BiConsumer<CommandSource, IC> executor;
    private final Text optionText;
    private final Text optionHoverText;

    public DebugStickOption(final ICDebugStick.Mode mode, final String permission, final List<Predicate<IC>> requirements,
                            final BiConsumer<CommandSource, IC> executor,
                            final Text optionText, final Text optionHoverText) {
        this.mode = mode;
        this.permission = permission;
        this.requirements = requirements;
        this.executor = executor;
        this.optionText = optionText;
        this.optionHoverText = optionHoverText;
    }

    public static Builder builderFor(final ICDebugStick.Mode mode) {
        return new Builder(mode);
    }

    @Nullable
    public ICDebugStick.Mode getMode() {
        return mode;
    }

    public boolean hasPermission(User user) {
        return permission == null || user.hasPermission(this.permission);
    }

    public boolean meetsRequirements(IC ic) {
        for (Predicate<IC> requirement : this.requirements) {
            if (!requirement.test(ic)) {
                return false;
            }
        }
        return true;
    }

    public void use(Player player, IC ic) {
        executor.accept(player, ic);
    }

    @Nullable
    public Text getOptionText(Location<World> icLoc) {
        if (optionText == null) {
            return null;
        }
        Text.Builder builder = Text.builder();
        builder.onClick(TextActions.executeCallback(src -> {
            Optional<ICSocket> optICSocket = ICSocket.getInstance();
            if (!optICSocket.isPresent()) {
                src.sendMessage(Text.of(TextColors.RED, "ICSocket is now unloaded - debugging is disabled."));
                return;
            }
            Optional<IC> ic = optICSocket.get().getIC(icLoc);
            if (!ic.isPresent()) {
                src.sendMessage(Text.of(TextColors.RED, "This ic is now unloaded - load it again to be able to debug it."));
                return;
            }
            this.executor.accept(src, ic.get());
        }));

        if (this.optionHoverText != null) {
            builder.onHover(TextActions.showText(Text.of(TextColors.GREEN, this.optionHoverText)));
        }

        builder.append(Text.of(TextColors.GRAY, " - ",
                TextColors.GOLD, this.optionText));

        return builder.build();
    }

    public static class Builder {
        private final ICDebugStick.Mode mode;
        private final String permission;
        private final List<Predicate<IC>> requirements = new ArrayList<>();
        private BiConsumer<CommandSource, IC> executor = null;
        private Text optionText = null;
        private Text optionHoverText = null;

        public Builder(@Nullable final ICDebugStick.Mode mode) {
            this.mode = mode;
            this.permission = String.join(".", ICSocket.DEBUG_STICK_PERMISSION, mode.name().toLowerCase());
        }

        public Builder requirement(Predicate<IC> requirement) {
            this.requirements.add(requirement);
            return this;
        }

        public Builder optionText(Text text) {
            this.optionText = text;
            return this;
        }

        public Builder optionHoverText(Text text) {
            this.optionHoverText = text;
            return this;
        }

        public Builder executor(final BiConsumer<CommandSource, IC> executor) {
            this.executor = executor;
            return this;
        }

        public DebugStickOption build() {
            Objects.requireNonNull(this.executor, "You must supply an executor!");
            return new DebugStickOption(
                    this.mode,
                    this.permission,
                    this.requirements,
                    this.executor,
                    this.optionText,
                    this.optionHoverText
            );
        }

    }
}
