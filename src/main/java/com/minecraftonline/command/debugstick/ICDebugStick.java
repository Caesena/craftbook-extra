/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.command.debugstick;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.minecraftonline.data.ICDebugStickData;
import com.minecraftonline.data.ICDebugStickModeData;
import com.minecraftonline.ic.AbstractIC;
import com.minecraftonline.util.AreaIC;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless.Band;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless.BandInfo;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless.Bands;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless.WirelessReceiver;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless.WirelessTransmitter;
import com.sk89q.craftbook.sponge.st.SelfTriggeringMechanic;
import com.sk89q.craftbook.sponge.st.SpongeSelfTriggerManager;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import com.sk89q.worldedit.internal.cui.SelectionShapeEvent;
import com.sk89q.worldedit.sponge.SpongeWorldEdit;
import org.spongepowered.api.GameState;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.service.pagination.PaginationService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.channel.MessageReceiver;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

public class ICDebugStick {

    private static final Multimap<Location<World>, Player> CURRENTLY_DISPLAYED_AREAS = HashMultimap.create();
    private static final Map<Mode, DebugStickOption> MODE_DEBUG_OPTION_MAP = new HashMap<>();

    static {
        registerMode(DebugStickOption.builderFor(Mode.MULTI)
                .executor(ICDebugStick::showOptionsTo)
                .build());

        registerMode(DebugStickOption.builderFor(Mode.TRIGGER)
                .optionText(Text.of("Trigger IC"))
                .optionHoverText(Text.of("Click to manually trigger this ic.\nDoes not change any of the input pins."))
                .executor(ICDebugStick::manuallyTrigger)
                .build());

        registerMode(DebugStickOption.builderFor(Mode.AREA_DISPLAYER)
                .requirement(ic -> ic instanceof AreaIC)
                .optionText(Text.of("Show Area"))
                .optionHoverText(Text.of("Click to show area using WorldEdit CUI"))
                .executor(ICDebugStick::showArea)
                .build());

        registerMode(DebugStickOption.builderFor(Mode.FIELD_VIEWER)
                .optionText(Text.of("View Fields"))
                .optionHoverText(Text.of("Click to show fields of the ic"))
                .executor(ICDebugStick::fieldViewer)
                .build());

        registerMode(DebugStickOption.builderFor(Mode.RELOAD)
                .optionText(Text.of("Reload IC"))
                .optionHoverText(Text.of("Click to unload, then load this ic"))
                .executor(ICDebugStick::reloadIC)
                .build());

        registerMode(DebugStickOption.builderFor(Mode.TOGGLE_DEBUG)
                .requirement(ic -> ic instanceof AbstractIC)
                .optionText(Text.of("Toggle Debug Messages"))
                .optionHoverText(Text.of("Click to sign yourself up for debug messages."))
                .executor(ICDebugStick::toggleDebug)
                .build());

        registerMode(DebugStickOption.builderFor(Mode.LIST_DEBUGGING)
                .optionText(Text.of("List enabled Debug Messages"))
                .optionHoverText(Text.of("Click to list debug messages."))
                .executor(ICDebugStick::showDebuggingMechanics)
                .build());

        registerMode(DebugStickOption.builderFor(Mode.LIST_ST_MECHANICS)
                .optionText(Text.of("List All ST Mechanics"))
                .optionHoverText(Text.of("Click to list ST Mechanics"))
                .executor(ICDebugStick::showAllSTMechanics)
                .build());

        registerMode(DebugStickOption.builderFor(Mode.BAND_INFO)
                .requirement(ic -> ic instanceof WirelessTransmitter || ic instanceof WirelessReceiver)
                .optionText(Text.of("Band Info"))
                .optionHoverText(Text.of("Show information about this band"))
                .executor(ICDebugStick::showBandInfo)
                .build());
    }

    public static void registerMode(final DebugStickOption debugStickOption) {
        MODE_DEBUG_OPTION_MAP.put(debugStickOption.getMode(), debugStickOption);
    }

    public enum Mode {
        MULTI(Text.of("Multi"), Text.of("Multi tool that provides all options on use")),
        TRIGGER(Text.of("Trigger"), Text.of("Use to trigger ics")),
        AREA_DISPLAYER(Text.of("Area Displayer"), Text.of("Use with WorldEdit CUI to see IC's areas")),
        FIELD_VIEWER(Text.of("Field Viewer"), Text.of("Use to view ic fields")),
        RELOAD(Text.of("Reload"), Text.of("Reload the ic it is used on")),
        TOGGLE_DEBUG(Text.of("Toggle Debug"), Text.of("Toggles debug for the ic it is used on")),
        LIST_DEBUGGING(Text.of("List Debug"), Text.of("Lists all ics that have debugging enabled")),
        LIST_ST_MECHANICS(Text.of("List ST Mechanics"), Text.of("Lists all ST mechanics on use")),

        // Not in the mode cycle.
        BAND_INFO(Text.of("Receiver"), Text.of("Displays information about receivers/transmitters"));

        private final Text text;
        private final Text description;

        Mode(Text text, Text description) {
            this.text = text;
            this.description = description;
        }

        public Text getText() {
            return text;
        }

        public Text getDescription() {
            return description;
        }

        public static Mode DEFAULT_MODE = Mode.MULTI;
    }

    public static List<Mode> modeCycle = Arrays.asList(
            Mode.MULTI,
            Mode.TRIGGER,
            Mode.AREA_DISPLAYER,
            Mode.FIELD_VIEWER,
            Mode.RELOAD,
            Mode.TOGGLE_DEBUG,
            Mode.LIST_DEBUGGING,
            Mode.LIST_ST_MECHANICS
    );

    public static void onICUnload(Location<World> loc) {
        if (Sponge.getGame().getState() == GameState.SERVER_STOPPING) {
            return;
        }
        for (Player player : CURRENTLY_DISPLAYED_AREAS.get(loc)) {
            // Reset Selection on block break
            try {
                resetWECUIArea(player, loc);
            } catch (Exception e) {
                CraftBookPlugin.spongeInst().getLogger().error("Error resetting worldedit CUI area", e);
            }
        }
    }

    public static void resetWECUIArea(Player player, Location<World> loc) {
        com.sk89q.worldedit.entity.Player WEPlayer = SpongeWorldEdit.inst().wrapPlayer(player);
        LocalSession session = WorldEdit.getInstance().getSessionManager().get(WEPlayer);
        session.dispatchCUISelection(WEPlayer);
        CURRENTLY_DISPLAYED_AREAS.remove(loc, player);
    }

    public static void cycleMode(Player player, ItemStack debugStick) {
        final Mode curMode = debugStick.get(ICDebugStickModeData.class).orElseGet(ICDebugStickModeData::new)
                .require(CraftBookKeys.IC_DEBUG_STICK_MODE);


        int index = modeCycle.indexOf(curMode);
        if (index == -1) {
            player.sendMessage(Text.of(TextColors.RED, "Cannot find the current mode in the cycle!"));
            return;
        }

        Mode newMode;
        do {
            index++;

            if (index >= modeCycle.size()) {
                index = 0;
            }
            newMode = modeCycle.get(index);
            DebugStickOption debugStickOption = MODE_DEBUG_OPTION_MAP.get(newMode);
            if (debugStickOption != null && debugStickOption.hasPermission(player)) {
                break;
            }
        }
        while (newMode != curMode);

        if (newMode == curMode) {
            player.sendMessage(Text.of(TextColors.RED, "You don't have permission to use any other modes!"));
            return;
        }

        debugStick.offer(Keys.DISPLAY_NAME, getDebugStickName(newMode));
        debugStick.offer(Keys.ITEM_LORE, getDebugStickLore(newMode));
        debugStick.offer(new ICDebugStickModeData(newMode));
    }

    public static ItemStack createDebugStick() {
        final ItemStack debugStick = ItemStack.of(ItemTypes.STICK);
        debugStick.offer(Keys.DISPLAY_NAME, getDebugStickName(Mode.DEFAULT_MODE));
        debugStick.offer(Keys.ITEM_LORE, getDebugStickLore(Mode.DEFAULT_MODE));
        debugStick.offer(new ICDebugStickData(true));
        debugStick.offer(new ICDebugStickModeData());
        return debugStick;
    }

    public static Text getDebugStickName(Mode mode) {
        return Text.of(TextColors.BLUE, "IC Debug Stick - ", TextColors.GOLD, mode.getText());
    }

    public static List<Text> getDebugStickLore(Mode mode) {
        return Arrays.asList(
                Text.of(TextColors.GREEN, "Right click on an IC to use"),
                Text.of(TextColors.BLUE, "Current Mode: ", TextColors.GOLD, mode.getText()),
                Text.of(TextColors.BLUE, mode.getDescription()),
                Text.of(TextColors.GRAY, "Shift Right click the air to toggle modes")
        );
    }

    public static void runClick(Player player, Mode stickMode, IC ic) {

        final DebugStickOption debugStickOption = MODE_DEBUG_OPTION_MAP.get(stickMode);

        if (debugStickOption == null) {
            player.sendMessage(Text.of(TextColors.RED, "Unknown mode!"));
            return;
        }

        if (!debugStickOption.hasPermission(player)) {
            player.sendMessage(Text.of(TextColors.RED, "You don't have permission to use that mode!"));
            return;
        }

        if (!debugStickOption.meetsRequirements(ic)) {
            player.sendMessage(Text.of(TextColors.RED, "This IC does not support this mode!"));
            return;
        }

        debugStickOption.use(player, ic);
    }

    public static void showOptionsTo(CommandSource src, IC ic) {
        Optional<ICSocket> optICSocket = ICSocket.getInstance();
        if (!optICSocket.isPresent()) {
            src.sendMessage(Text.of(TextColors.RED, "ICSocket is unavailable, so we are unable to process your request :("));
            return;
        }
        final ICSocket icSocket = optICSocket.get();
        final boolean registeredST = ((SpongeSelfTriggerManager) CraftBookPlugin.spongeInst().getSelfTriggerManager().get()).isRegistered(icSocket, ic.getBlock());
        src.sendMessage(Text.of(TextColors.GRAY, "== ", TextColors.GOLD, ic.getClass().getSimpleName(), TextColors.GRAY, " =="));
        src.sendMessage(Text.of(TextColors.GRAY, "Is registered ST: " + registeredST));
        if (ic instanceof AbstractIC) {
            boolean corrupt = ((AbstractIC) ic).isCorrupt();
            src.sendMessage(Text.of(TextColors.GRAY, "Is Corrupt: ", (corrupt ? TextColors.RED : TextColors.GRAY), corrupt));
        }
        src.sendMessage(Text.of(TextColors.GRAY, "X: " + ic.getBlock().getX()));
        src.sendMessage(Text.of(TextColors.GRAY, "Y: " + ic.getBlock().getY()));
        src.sendMessage(Text.of(TextColors.GRAY, "Z: " + ic.getBlock().getZ()));
        src.sendMessage(Text.of(TextColors.GRAY, "World: " + ic.getBlock().getExtent().getName()));

        Location<World> loc = ic.getBlock();

        final Supplier<Optional<IC>> icSupplier = () -> icSocket.getIC(loc);

        if (!(src instanceof Player)) {
            src.sendMessage(Text.of(TextColors.RED, "Further options are only available to players!"));
            return;
        }

        Player player = (Player) src;

        for (final Mode mode : Mode.values()) {
            final DebugStickOption debugStickOption = MODE_DEBUG_OPTION_MAP.get(mode);
            if (debugStickOption == null || !debugStickOption.hasPermission(player) || !debugStickOption.meetsRequirements(ic)) {
                continue;
            }
            Text optionText = debugStickOption.getOptionText(ic.getBlock());
            if (optionText != null) {
                player.sendMessage(optionText);
            }
        }
    }

    public static void fieldViewer(CommandSource src, IC ic) {
        PaginationService paginationService = Sponge.getGame().getServiceManager().getRegistration(PaginationService.class)
                .orElseThrow(() -> new RuntimeException("Pagination Service not available!")).getProvider();
        Class<?> clazz = ic.getClass();
        boolean sentClass = true;
        PaginationList.Builder builder = paginationService.builder();
        builder.title(Text.of(TextColors.DARK_GREEN, "ChestCollector Fields"));
        List<Text> contents = new ArrayList<>();
        do {
            for (final Field field : clazz.getDeclaredFields()) {
                if (Modifier.isStatic(field.getModifiers())) {
                    continue;
                }
                field.setAccessible(true);
                try {
                    if (!sentClass) {
                        contents.add(Text.of(TextColors.DARK_GREEN, "= Superclass members - " + clazz.getSimpleName() + " ="));
                        sentClass = true;
                    }
                    contents.add(
                            Text.of(TextColors.GOLD, field.getType().getSimpleName(), " ",
                                    TextColors.BLUE, field.getName() + ": ",
                                    TextColors.GRAY, String.valueOf(field.get(ic)))
                    );
                } catch (IllegalAccessException e) {
                    CraftBookPlugin.spongeInst().getLogger().error("Error accessing field via reflection", e);
                }
            }
            clazz = clazz.getSuperclass();
            sentClass = false;
        } while (clazz != null);
        builder.contents(contents);
        builder.linesPerPage(16);
        builder.sendTo(src);
    }

    public static void reloadIC(CommandSource src, IC ic) {
        Optional<ICSocket> optICSocket = ICSocket.getInstance();
        if (!optICSocket.isPresent()) {
            src.sendMessage(Text.of(TextColors.RED, "ICSocket is unavailable, so we are unable to process your request :("));
            return;
        }
        ICSocket icSocket = optICSocket.get();
        ic.unload();
        src.sendMessage(Text.of(TextColors.GREEN, "Unloaded IC."));
        if (icSocket.isSTVariant(ic)) {
            ((SpongeSelfTriggerManager) CraftBookPlugin.spongeInst().getSelfTriggerManager().get()).unregister(icSocket, ic.getBlock());
            src.sendMessage(Text.of(TextColors.GREEN, "Unregistered ST"));
        }
        src.sendMessage(Text.of(TextColors.GREEN, "Loaded IC."));
        ic.load();
        if (icSocket.isSTVariant(ic)) {
            ((SpongeSelfTriggerManager) CraftBookPlugin.spongeInst().getSelfTriggerManager().get()).register(icSocket, ic.getBlock());
            src.sendMessage(Text.of(TextColors.GREEN, "Registered ST."));
        }
    }

    public static void manuallyTrigger(CommandSource src, IC ic) {
        ic.trigger();
        src.sendMessage(Text.of(TextColors.GREEN, "Triggered IC"));
    }

    public static void toggleDebug(CommandSource src, IC ic) {
        AbstractIC abstractIC = ((AbstractIC) ic);
        if (abstractIC.DEBUG_TARGET == null) {
            abstractIC.DEBUG_TARGET = src;
            ICSocket.DEBUG_MESSAGE_RECEIVERS.put(ic.getBlock(), src);
            src.sendMessage(Text.of(TextColors.GREEN, "Added yourself as a debug target, click again to remove"));
        } else {
            abstractIC.DEBUG_TARGET = null;
            ICSocket.DEBUG_MESSAGE_RECEIVERS.remove(ic.getBlock(), src);
            src.sendMessage(Text.of(TextColors.GREEN, "Removed yourself as a debug target, click again to re-add"));
        }
    }

    public static void showDebuggingMechanics(CommandSource src, IC ic) {
        Location<World> clickedICLoc = ic.getBlock();
        src.sendMessage(Text.of(TextColors.GREEN, "- Listeners Start -"));
        for (final Map.Entry<Location<World>, MessageReceiver> entry : ICSocket.DEBUG_MESSAGE_RECEIVERS.entrySet()) {
            Location<World> loc = entry.getKey();
            final boolean isCurrentIC = clickedICLoc.equals(loc);

            src.sendMessage(Text.of(isCurrentIC ? TextColors.GOLD : TextColors.GRAY,
                    "X: ", loc.getX(), " Y: ", loc.getY(), " Z: ", loc.getZ(), " World: " + loc.getExtent(), " Receiver: " + entry.getValue()));
        }
        src.sendMessage(Text.of(TextColors.GREEN, "- Listeners End -"));
    }

    public static void showAllSTMechanics(CommandSource src, IC ic) {
        PaginationList.Builder builder = PaginationList.builder();

        builder.title(Text.of(TextColors.BLUE, "Self Triggering Mechanics"));
        builder.linesPerPage(16);

        List<Text> content = new ArrayList<>();

        Location<World> icLoc = ic.getBlock();

        Map<SpongeSelfTriggerManager.WorldChunk, Map<Vector3i, SelfTriggeringMechanic>> map = ((SpongeSelfTriggerManager) CraftBookPlugin.spongeInst().getSelfTriggerManager().get()).getSelfTriggeringMechanics();
        for (final Map.Entry<SpongeSelfTriggerManager.WorldChunk, Map<Vector3i, SelfTriggeringMechanic>> entry : map.entrySet()) {
            World world = Sponge.getServer().getWorld(entry.getKey().uuid).get();
            for (Map.Entry<Vector3i, SelfTriggeringMechanic> mechanicEntry : entry.getValue().entrySet()) {
                Location<World> loc = world.getLocation(mechanicEntry.getKey());
                final boolean isCurrentIC = icLoc.equals(loc);

                content.add(Text.of(isCurrentIC ? TextColors.GOLD : TextColors.GRAY,
                        "X: ", loc.getX(), " Y: ", loc.getY(), " Z: ", loc.getZ(), " World: " + loc.getExtent().getName(),
                        " Mechanic: " + entry.getValue().getClass().getSimpleName()));
            }
        }
        builder.contents(content);
        builder.sendTo(src);
    }

    public static void showArea(CommandSource src, IC ic) {
        if (!(src instanceof Player)) {
            src.sendMessage(Text.of(TextColors.RED, "This feature is only available for players"));
            return;
        }
        final AreaIC areaIC = (AreaIC) ic;

        Player player = (Player) src;

        if (CURRENTLY_DISPLAYED_AREAS.get(ic.getBlock()).contains(player)) {
            src.sendMessage(Text.of(TextColors.GREEN, "Hiding area"));
            ICDebugStick.resetWECUIArea(player, ic.getBlock());
            return;
        }

        final CUIRegion cuiRegion = areaIC.getArea();

        if (cuiRegion == null) {
            src.sendMessage(Text.of(TextColors.RED, "This ic can not currently display its area"));
            return;
        }

        com.sk89q.worldedit.entity.Player WEPlayer = SpongeWorldEdit.inst().wrapPlayer(player);
        LocalSession session = WorldEdit.getInstance().getSessionManager().get(WEPlayer);
        if (!session.hasCUISupport()) {
            src.sendMessage(Text.of(TextColors.RED, "CUI support not detected!"));
        }
        // Remove any existing area - .values() gives a partially mutable collection that reflects updates (adding not allowed though)
        CURRENTLY_DISPLAYED_AREAS.values().remove(player);
        session.dispatchCUIEvent(WEPlayer, new SelectionShapeEvent(cuiRegion.getTypeID()));
        cuiRegion.describeCUI(session, WEPlayer);
        CURRENTLY_DISPLAYED_AREAS.put(ic.getBlock(), player);
        src.sendMessage(Text.of(TextColors.GREEN, "Sent region via WorldEditCUI"));
    }

    public static void showBandInfo(CommandSource src, IC ic) {
        final Band band;
        if (ic instanceof WirelessTransmitter) {
            band = ((WirelessTransmitter) ic).getBand();
        } else if (ic instanceof WirelessReceiver) {
            band = ((WirelessReceiver) ic).getBand();
        } else {
            throw new IllegalStateException("Should have been a wireless transmitter or receiver");
        }
        BandInfo bandInfo = Bands.getBandInfo(band);
        src.sendMessage(Text.of(TextColors.GOLD, "ShortBand: ", TextColors.BLUE, band.getShortBand()));
        src.sendMessage(Text.of(TextColors.GOLD, "WideBand: ", TextColors.BLUE, band.getWideBand()));
        if (bandInfo == null) {
            src.sendMessage(Text.of(TextColors.RED, "This Band is not registered."));
            return;
        }
        Location<World> loc = ic.getBlock();

        src.sendMessage(Text.of(TextColors.GREEN, "This band is registered."));
        final Text state;
        if (bandInfo.activated == null) {
            state = Text.of(TextColors.DARK_RED, "null");
        }
        else {
            state = bandInfo.activated ? Text.of(TextColors.GREEN, "on") : Text.of(TextColors.RED, "off");
        }
        src.sendMessage(Text.of(TextColors.GOLD, "This band's state is: ", state));
        src.sendMessage(Text.of(TextActions.executeCallback(cmdSrc -> {
                    BandInfo bInfo = Bands.getBandInfo(band);
                    if (bInfo == null) {
                        src.sendMessage(Text.of(TextColors.RED, "This band is no longer loaded."));
                        return;
                    }

                    PaginationList.Builder builder = PaginationList.builder();

                    builder.title(Text.of(TextColors.GOLD, "Loaded Receivers"));
                    builder.linesPerPage(10);
                    builder.header(Text.of(TextColors.GOLD, "ShortBand: ", TextColors.GRAY, "'", TextColors.BLUE, band.getShortBand(), TextColors.GRAY, "'",
                            TextColors.GOLD, " WideBand: ", TextColors.GRAY, "'", TextColors.BLUE, band.getWideBand(), TextColors.GRAY, "'"));
                    List<Text> content = new ArrayList<>();

                    for (Location<World> receiverLoc : bInfo.getReceiverLocations()) {
                        final boolean isCurrentIC = loc.equals(receiverLoc);

                        content.add(Text.of(isCurrentIC ? TextColors.GOLD : TextColors.GRAY,
                                "X: ", receiverLoc.getX(), " Y: ", receiverLoc.getY(), " Z: ", receiverLoc.getZ(), " World: " + receiverLoc.getExtent().getName()));
                    }
                    builder.contents(content);
                    builder.sendTo(src);
                }),
                TextActions.showText(Text.of(TextColors.GREEN, "Click to list loaded receivers")),
                TextColors.GRAY, " - ",
                TextColors.GOLD, "List loaded receivers"));

    }

}
