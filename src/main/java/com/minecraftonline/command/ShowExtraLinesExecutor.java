/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.command;

import com.minecraftonline.legacy.ics.ExtraDataIC;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.blockray.BlockRay;
import org.spongepowered.api.util.blockray.BlockRayHit;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class ShowExtraLinesExecutor implements CommandExecutor {

    private final ICSocket icSocket;

    public ShowExtraLinesExecutor(ICSocket icSocket) {
        this.icSocket = icSocket;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        if (!(src instanceof Player)) {
            throw new CommandException(Text.of("You must be a player to use this command!"));
        }

        Player player = (Player) src;

        BlockRay<World> ray = BlockRay.from(player)
                .distanceLimit(50)
                .select(BlockRay.notAirFilter())
                .build();

        if (!ray.hasNext()) {
            throw new CommandException(Text.of("You must face an ic to use this command!"));
        }

        BlockRayHit<World> hit = ray.next();

        Location<World> loc = hit.getLocation();

        if (!SignUtil.isSign(loc)) {
            throw new CommandException(Text.of("The block are looking at is not a sign!"));
        }

        IC ic = icSocket.getIC(loc)
                .orElseThrow(() -> new CommandException(Text.of("The sign you are looking at is not an ic!")));

        if (!(ic instanceof ExtraDataIC)) {
            throw new CommandException(Text.of("This ic does not support extra data!"));
        }

        ExtraDataIC extraDataIC = (ExtraDataIC) ic;

        Text extraData = extraDataIC.getExtraData().orElseThrow(() -> new CommandException(Text.of("This ic has no extra data, though it does support it")));
        src.sendMessage(Text.of(TextColors.GREEN, "The extra data is:"));
        src.sendMessage(extraData);
        return CommandResult.success();
    }
}
