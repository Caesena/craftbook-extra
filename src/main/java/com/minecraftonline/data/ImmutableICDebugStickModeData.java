/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.data;

import com.minecraftonline.command.debugstick.ICDebugStick;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableSingleEnumData;

public class ImmutableICDebugStickModeData extends AbstractImmutableSingleEnumData<ICDebugStick.Mode, ImmutableICDebugStickModeData, ICDebugStickModeData> {
    protected ImmutableICDebugStickModeData(ICDebugStick.Mode mode) {
        super(CraftBookKeys.IC_DEBUG_STICK_MODE, mode, ICDebugStick.Mode.DEFAULT_MODE);
    }

    @Override
    public ICDebugStickModeData asMutable() {
        return new ICDebugStickModeData(this.getValue());
    }

    @Override
    public int getContentVersion() {
        return 1;
    }
}
