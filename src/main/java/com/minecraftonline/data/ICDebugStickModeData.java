/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.data;

import com.minecraftonline.command.debugstick.ICDebugStick;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractSingleEnumData;
import org.spongepowered.api.data.merge.MergeFunction;

import java.util.Optional;

public class ICDebugStickModeData extends AbstractSingleEnumData<ICDebugStick.Mode, ICDebugStickModeData, ImmutableICDebugStickModeData> {

    public ICDebugStickModeData() {
        this(ICDebugStick.Mode.DEFAULT_MODE);
    }

    public ICDebugStickModeData(ICDebugStick.Mode value) {
        super(CraftBookKeys.IC_DEBUG_STICK_MODE, value, ICDebugStick.Mode.DEFAULT_MODE);
    }

    @Override
    public Optional<ICDebugStickModeData> fill(DataHolder dataHolder, MergeFunction overlap) {
        dataHolder.get(ICDebugStickModeData.class).ifPresent((data) -> {
            ICDebugStickModeData finalData = overlap.merge(this, data);
            setValue(finalData.getValue());
        });
        return Optional.of(this);
    }

    @Override
    public Optional<ICDebugStickModeData> from(DataContainer container) {
        Optional<String> optString = container.getString(CraftBookKeys.IC_DEBUG_STICK_MODE.getQuery());
        return optString.map(ICDebugStick.Mode::valueOf).map(ICDebugStickModeData::new);
    }

    @Override
    public ICDebugStickModeData copy() {
        return new ICDebugStickModeData(this.getValue());
    }

    @Override
    public ImmutableICDebugStickModeData asImmutable() {
        return new ImmutableICDebugStickModeData(this.getValue());
    }

    @Override
    public int getContentVersion() {
        return 1;
    }
}
