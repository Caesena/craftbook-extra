/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.data;

import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractSingleData;
import org.spongepowered.api.data.merge.MergeFunction;
import org.spongepowered.api.data.value.mutable.Value;
import org.spongepowered.api.text.Text;

import java.util.Optional;

// KeyLockData extends AbstractSingleData<ItemStackSnapshot, KeyLockData, ImmutableKeyLockData>
public class ExtraICLinesData extends AbstractSingleData<Text, ExtraICLinesData, ImmutableExtraICLinesData> {
	public ExtraICLinesData() {
		this(Text.EMPTY);
	}

	public ExtraICLinesData(Text value) {
		super(value, CraftBookKeys.EXTRA_IC_LINES);
	}

	public Value<Text> extraICLines() {
		return Sponge.getRegistry().getValueFactory()
				.createValue(CraftBookKeys.EXTRA_IC_LINES, getValue());
	}

	@Override
	protected Value<Text> getValueGetter() {
		return extraICLines();
	}

	@Override
	public Optional<ExtraICLinesData> fill(DataHolder dataHolder, MergeFunction overlap) {
		dataHolder.get(ExtraICLinesData.class).ifPresent((data) -> {
			ExtraICLinesData finalData = overlap.merge(this, data);
			setValue(finalData.getValue());
		});
		return Optional.of(this);
	}

	@Override
	public Optional<ExtraICLinesData> from(DataContainer container) {
		Optional<Text> text = container.getSerializable(CraftBookKeys.EXTRA_IC_LINES.getQuery(), Text.class);
		return text.map(ExtraICLinesData::new);
	}

	@Override
	public ExtraICLinesData copy() {
		return new ExtraICLinesData(this.getValue());
	}

	@Override
	public ImmutableExtraICLinesData asImmutable() {
		return new ImmutableExtraICLinesData(this.getValue());
	}

	@Override
	public int getContentVersion() {
		return 1;
	}
}
