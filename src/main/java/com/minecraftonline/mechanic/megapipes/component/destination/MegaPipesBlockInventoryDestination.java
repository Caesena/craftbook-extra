/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.mechanic.megapipes.component.destination;

import com.minecraftonline.mechanic.megapipes.component.MegaPipesComponent;
import com.minecraftonline.util.BlockFace;
import com.minecraftonline.util.InventoryUtil;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.BlockCarrier;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class MegaPipesBlockInventoryDestination implements MegaPipesDestination {

    private final Location<World> loc;

    public MegaPipesBlockInventoryDestination(Location<World> loc) {
        this.loc = loc;
    }

    public Inventory getInventory() {
        BlockCarrier carrier = (BlockCarrier) loc.getTileEntity().get();
        return carrier.getInventory();
    }

    @Override
    public int canAcceptHowMuch(ItemStack itemStack, BlockFace face) {
        return InventoryUtil.canFitHowMuch(getInventory(), itemStack);
    }

    @Override
    public void accept(ItemStack itemStack, BlockFace face) {
        getInventory().offer(itemStack);
    }

    public static class Factory implements MegaPipesComponent.Factory<MegaPipesBlockInventoryDestination> {

        private final BlockType[] validBlocks;

        public Factory(BlockType... validBlocks) {
            this.validBlocks = validBlocks;
        }

        @Override
        public MegaPipesBlockInventoryDestination create(Location<World> loc) {
            return new MegaPipesBlockInventoryDestination(loc);
        }

        @Override
        public String getPermission() {
            return null;
        }

        @Override
        public boolean isValid(Location<World> loc) {
            BlockType blockType = loc.getBlockType();
            for (BlockType valid : validBlocks) {
                if (blockType == valid) {
                    return true;
                }
            }
            return false;
        }
    }
}
