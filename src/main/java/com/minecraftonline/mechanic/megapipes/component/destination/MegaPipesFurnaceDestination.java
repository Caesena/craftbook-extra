/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.mechanic.megapipes.component.destination;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.mechanic.megapipes.component.MegaPipesComponent;
import com.minecraftonline.util.BlockFace;
import com.minecraftonline.util.InventoryUtil;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.carrier.Furnace;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.InventoryArchetype;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.property.SlotIndex;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.item.inventory.slot.FuelSlot;
import org.spongepowered.api.item.inventory.slot.InputSlot;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class MegaPipesFurnaceDestination implements MegaPipesDestination {

    private final Location<World> loc;

    public MegaPipesFurnaceDestination(Location<World> loc) {
        this.loc = loc;
    }

    @Override
    public int canAcceptHowMuch(ItemStack itemStack, BlockFace face) {
        Inventory inv = getInventory(face);
        if (inv == null) {
            return 0;
        }
        return InventoryUtil.canFitHowMuch(inv, itemStack);
    }

    @Nullable
    public Inventory getInventory(BlockFace face) {
        switch (face) {
            case TOP: {
                Furnace furnace = (Furnace) loc.getTileEntity().get();
                // Fuel is also an input slot sadly :(
                Inventory inventory = furnace.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(InputSlot.class));
                // Hnnng is there no better way to do this?
                for (Inventory slot : inventory.slots()) {
                    if (slot instanceof FuelSlot) {
                        continue;
                    }
                    return slot;
                }
                return inventory;
            }
            case BOTTOM: return null;
            default: {
                Furnace furnace = (Furnace) loc.getTileEntity().get();
                return furnace.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(FuelSlot.class));
            }
        }
    }

    @Override
    public BlockFace getBlockFace(Vector3i from) {
        Vector3i dirVector = from.sub(loc.getBlockPosition());
        Direction dir = Direction.getClosest(dirVector.toDouble());
        switch (dir) {
            case UP: return BlockFace.TOP;
            case DOWN: return BlockFace.BOTTOM;
        }
        return BlockFace.SIDE;
    }

    @Override
    public void accept(ItemStack itemStack, BlockFace face) {
        Inventory inventory = getInventory(face);
        if (inventory != null) {
            inventory.offer(itemStack);
        }
    }

    public static class Factory implements MegaPipesComponent.Factory<MegaPipesFurnaceDestination> {

        @Override
        public MegaPipesFurnaceDestination create(Location<World> loc) {
            return new MegaPipesFurnaceDestination(loc);
        }

        @Override
        public boolean isValid(Location<World> loc) {
            BlockType type = loc.getBlockType();
            return type == BlockTypes.FURNACE || type == BlockTypes.LIT_FURNACE;
        }
    }
}
