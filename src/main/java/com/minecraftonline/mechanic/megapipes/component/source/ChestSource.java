/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.mechanic.megapipes.component.source;

import com.minecraftonline.mechanic.megapipes.component.MegaPipesComponent;
import com.minecraftonline.mechanic.megapipes.component.MegaPipesSource;
import org.spongepowered.api.block.tileentity.carrier.Chest;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class ChestSource implements MegaPipesSource {

    private final Location<World> loc;

    public ChestSource(Location<World> loc) {
        this.loc = loc;
    }

    @Override
    public ItemPreview getPreview() {
        Chest chest = (Chest) loc.getTileEntity().get();
        Inventory inv = chest.getDoubleChestInventory().orElse(chest.getInventory());
        for (Inventory slot : inv.slots()) {
            //slot.
        }
        return null;//new ItemPreview();
    }

    public static class Factory implements MegaPipesComponent.Factory<ChestSource> {

        @Override
        public ChestSource create(Location<World> loc) {
            return new ChestSource(loc);
        }

        @Override
        public boolean isValid(Location<World> loc) {
            return false;
        }
    }
}
