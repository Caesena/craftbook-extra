/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.mechanic.megapipes.component.destination;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.mechanic.megapipes.component.MegaPipesComponent;
import com.minecraftonline.util.BlockFace;
import org.spongepowered.api.item.inventory.ItemStack;

public interface MegaPipesDestination extends MegaPipesComponent {

    int canAcceptHowMuch(ItemStack itemStack, BlockFace face);

    void accept(ItemStack itemStack, BlockFace face);

    default BlockFace getBlockFace(Vector3i from) {
        return BlockFace.IGNORED;
    }
}
