/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.mechanic.megapipes.component.destination;

import com.minecraftonline.mechanic.megapipes.component.MegaPipesSignComponent;
import com.minecraftonline.util.BlockFace;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.List;

public class MegaPipesTrashBin implements MegaPipesDestination, MegaPipesSignComponent {

    @Override
    public int canAcceptHowMuch(ItemStack itemStack, BlockFace face) {
        return itemStack.getQuantity(); // Always room for more trash!
    }

    @Override
    public void accept(ItemStack itemStack, BlockFace face) {
        // Adios, trashios!
    }

    public static class Factory extends MegaPipesSignComponent.Factory<MegaPipesTrashBin> {

        private final BlockType blockType;

        public Factory(BlockType blockType) {
            this.blockType = blockType;
        }

        @Override
        public MegaPipesTrashBin create(Location<World> loc) {
            return new MegaPipesTrashBin();
        }

        @Override
        public String getPermission() {
            return "craftbook.megapipes.trashbin";
        }


        private static final List<String> validLines = Arrays.asList("[TrashBin]");

        @Override
        public List<String> getValidLines() {
            return validLines;
        }

        @Override
        public boolean isBlockValid(Location<World> loc) {
            return loc.getBlockType() == blockType;
        }
    }
}
