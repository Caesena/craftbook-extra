/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.mechanic.megapipes.component;

import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collections;
import java.util.List;

public interface MegaPipesComponent {

    interface Factory<T extends MegaPipesComponent> {
        T create(Location<World> loc);

        @Nullable
        default String getPermission() {
            return null;
        }

        boolean isValid(Location<World> loc);

        default boolean usesSign() {
            return false;
        }

        default boolean verifyLines(SignData signData) {
            return true;
        }

        default List<String> getValidLines() {
            return Collections.EMPTY_LIST;
        }
    }
}
