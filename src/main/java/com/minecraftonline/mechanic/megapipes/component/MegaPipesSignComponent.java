/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.mechanic.megapipes.component;

import com.sk89q.craftbook.sponge.util.SignUtil;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public interface MegaPipesSignComponent extends MegaPipesComponent {

    abstract class Factory<T extends MegaPipesSignComponent> implements MegaPipesComponent.Factory<T> {

        @Override
        public final boolean isValid(Location<World> loc) {
            if (!isBlockValid(loc)) {
                return false;
            }
            boolean validSign = false;
            for (Location<World> signLoc : SignUtil.getAttachedSigns(loc)) {
                if (isBlockValid(signLoc)) {
                    validSign = true;
                }
            }
            return validSign || isValidSign(loc.getRelative(Direction.DOWN));
        }

        public abstract boolean isBlockValid(Location<World> loc);

        public final boolean isValidSign(Location<World> signLoc) {
            final Location<World> block;
            if (signLoc.getBlockType() == BlockTypes.STANDING_SIGN) {
                block = signLoc.getRelative(Direction.UP);
            }
            else if (signLoc.getBlockType() == BlockTypes.WALL_SIGN) {
                block = SignUtil.getBackBlock(signLoc);
            }
            else {
                return false;
            }
            Sign sign = (Sign) signLoc.getTileEntity().get();
            SignData signData = sign.getSignData();
            String line = signData.get(1).get().toPlain();
            boolean valid = false;
            for (String s : getValidLines()) {
                if (line.equalsIgnoreCase(s)) {
                    valid = true;
                    break;
                }
            }
            if (!valid) {
                return false;
            }
            if (!verifyLines(signData)) {
                return false;
            }
            return isValid(block);
        }

        @Override
        public final boolean usesSign() {
            return true;
        }
    }
}
