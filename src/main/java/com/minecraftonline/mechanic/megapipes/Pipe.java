/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.mechanic.megapipes;

import com.flowpowered.math.vector.Vector3i;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Pipe {
    public static final Vector3i[] DIRECTIONS = new Vector3i[]{
            Vector3i.UNIT_X, Vector3i.UNIT_X.negate(),
            Vector3i.UNIT_Y, Vector3i.UNIT_Y.negate(),
            Vector3i.UNIT_Z, Vector3i.UNIT_Z.negate(),
    };

    public static final BlockType PIPE_BLOCK_TYPE = BlockTypes.GLASS_PANE;

    private final World world;
    private final Set<Vector3i> parts;
    private boolean invalid = false;

    public Pipe(World world) {
        this.world = world;
        this.parts = new HashSet<>();
    }

    public Pipe(World world, Set<Vector3i> parts) {
        this.world = world;
        this.parts = parts;
    }

    public static Pipe fromLocation(Location<World> location) {
        if (location.getBlockType() != PIPE_BLOCK_TYPE) {
            throw new IllegalArgumentException("Location passed into fromLocation was not a pipe part!");
        }
        Set<Vector3i> found = new HashSet<>();
        Set<Vector3i> skipped = new HashSet<>();
        found.add(location.getBlockPosition());

        for (Direction direction : BlockUtil.getDirectFaces()) {
            Location<World> loc = location.getBlockRelative(direction);
            if (loc.getBlockType() == PIPE_BLOCK_TYPE) {
                found.add(loc.getBlockPosition());
                findPipes(loc, direction.getOpposite(), found, skipped);
            } else {
                skipped.add(loc.getBlockPosition());
            }
        }
        return new Pipe(location.getExtent(), found);
    }

    private static void findPipes(Location<World> location, Direction from, Set<Vector3i> found, Set<Vector3i> skipped) {
        for (Direction direction : BlockUtil.getDirectFaces()) {
            if (direction == from) {
                continue;
            }
            Location<World> loc = location.getBlockRelative(direction);
            if (loc.getBlockType() == PIPE_BLOCK_TYPE) {
                found.add(loc.getBlockPosition());
                findPipes(loc, direction.getOpposite(), found, skipped);
            } else {
                skipped.add(loc.getBlockPosition());
            }
        }
    }

    /**
     * Breaks the pipe at a specified position in the Pipe, creating
     * a list of pipes made up by the new pipes, or just the original,
     * if a pipe was removed from the end.
     *
     * @param joint Joint to be destroyed.
     * @return A list of the pipes that have been changed.
     */
    public List<Pipe> breakAt(Vector3i joint) {
        if (!parts.contains(joint)) {
            throw new IllegalArgumentException("This pipe doesn't contain that pipe!");
        }
        List<Pipe> pipes = new ArrayList<>();
        if (parts.size() == 1) {
            // Due to above, we know this pipe contains that joint, so if its removed, there is no more pipe :(
            invalidate();
            return pipes;
        }
        parts.remove(joint);
        for (Vector3i part : parts) {
            boolean found = false;
            for (Pipe pipe : pipes) {
                if (pipe.containsPart(part)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                // Create a new sub-pipe.
                Pipe pipe = Pipe.fromLocation(world.getLocation(part));
                pipes.add(pipe);
            }
        }
        if (pipes.size() != 1) {
            invalidate();
        }
        return pipes;
    }

    public boolean containsPart(Location<World> part) {
        return part.getExtent() == this.world
                && this.parts.contains(part.getBlockPosition());
    }

    public boolean containsPart(Vector3i part) {
        return this.parts.contains(part);
    }

    public List<Pipe> addPart(Vector3i part) {
        return addPart(part, new ArrayList<>());
    }

    private List<Pipe> addPart(Vector3i part, List<Pipe> cur) {
        this.parts.add(part);

        for (Direction dir : BlockUtil.getDirectFaces()) {
            Vector3i pos = part.add(dir.asBlockOffset());
            Location<World> loc = world.getLocation(pos);

            if (containsPart(pos) || loc.getBlockType() != Pipe.PIPE_BLOCK_TYPE) {
                continue;
            }

            Pipe otherPipe = MegaPipes.getPipe(loc);
            if (otherPipe == this) {
                continue;
            }

            if (otherPipe != null) {
                otherPipe.invalidate();
                this.parts.addAll(otherPipe.parts);
                cur.add(otherPipe);
            }
            else {
                addPart(pos, cur);
            }
        }
        return cur;
    }

    public World getWorld() {
        return world;
    }

    public void invalidate() {
        this.invalid = true;
    }

    public boolean isInvalid() {
        return invalid;
    }
}
