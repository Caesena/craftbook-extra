/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.mechanic.megapipes;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.mechanic.megapipes.component.MegaPipesComponent;
import com.minecraftonline.mechanic.megapipes.component.MegaPipesPipeComponent;
import com.minecraftonline.mechanic.megapipes.component.MegaPipesSource;
import com.minecraftonline.mechanic.megapipes.component.destination.MegaPipesChestDestination;
import com.minecraftonline.mechanic.megapipes.component.destination.MegaPipesDestination;
import com.minecraftonline.mechanic.megapipes.component.destination.MegaPipesFurnaceDestination;
import com.minecraftonline.mechanic.megapipes.component.destination.MegaPipesTrashBin;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeSignMechanic;
import com.sk89q.craftbook.sponge.st.SelfTriggeringMechanic;
import com.sk89q.craftbook.sponge.st.SpongeSelfTriggerManager;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.GameState;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.world.chunk.LoadChunkEvent;
import org.spongepowered.api.event.world.chunk.UnloadChunkEvent;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Module(id = "megapipes", name = "MegaPipes", onEnable="onInitialize", onDisable="onDisable")
public class MegaPipes extends SpongeSignMechanic implements SelfTriggeringMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.megapipes", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    // Start Registry

    private final List<MegaPipesComponent.Factory<?>>  registeredComponents = new ArrayList<>();
    private final List<MegaPipesComponent.Factory<?>>  registeredSignComponents = new ArrayList<>();
    private final List<MegaPipesComponent.Factory<? extends MegaPipesDestination>> destinationComponents = new ArrayList<>();
    private final List<MegaPipesComponent.Factory<? extends MegaPipesSource>> sourceComponents = new ArrayList<>();

    private final Map<String, MegaPipesComponent.Factory<?>> componentsBySignLine = new HashMap<>();

    private final List<String> validSigns = new ArrayList<>();

    private String[] validSignsArray = null;

    // End Registry

    private final Set<Pipe> pipes = new HashSet<>();
    private final SetMultimap<World, Vector3i> destinations = HashMultimap.create();
    private final Map<Location<World>, ExtractorMultiBlock> extractors = new HashMap<>();
    private final Map<Location<World>, MegaPipesPipeComponent> customPipeComponents = new HashMap<>();

    public static MegaPipes INSTANCE;


    public static Pipe getOrCreatePipe(Location<World> loc) {
        Pipe pipe = getPipe(loc);
        if (pipe != null) {
            return pipe;
        }
        // Create Pipe.
        Pipe newPipe = Pipe.fromLocation(loc);
        INSTANCE.pipes.add(newPipe);
        return newPipe;
    }

    @Nullable
    public static Pipe getPipe(Location<World> loc) {
        Vector3i pos = loc.getBlockPosition();
        for (Pipe pipe : INSTANCE.pipes) {
            if (pipe.containsPart(pos)) {
                return pipe;
            }
        }
        return null;
    }

    public static List<MegaPipesComponent.Factory<? extends MegaPipesDestination>> getDestinationComponents() {
        return INSTANCE.destinationComponents;
    }

    public static List<MegaPipesComponent.Factory<? extends MegaPipesSource>> getSourceComponents() {
        return null;
    }

    @Override
    public void onInitialize() throws CraftBookException {
        INSTANCE = this;

        registerDestination(new MegaPipesChestDestination.Factory(BlockTypes.CHEST));
        registerDestination(new MegaPipesChestDestination.Factory(BlockTypes.TRAPPED_CHEST));
        registerDestination(new MegaPipesFurnaceDestination.Factory());

        registerDestination(new MegaPipesTrashBin.Factory(BlockTypes.COAL_BLOCK));

        validSigns.add("[Extractor]");
    }

    private void registerDestination(MegaPipesComponent.Factory<? extends MegaPipesDestination> factory) {
        destinationComponents.add(factory);
        register(factory);
    }

    private void registerSource(MegaPipesComponent.Factory<? extends MegaPipesSource> factory) {
        sourceComponents.add(factory);
        register(factory);
    }

    private void register(MegaPipesComponent.Factory<?> factory) {
        if (factory.usesSign()) {
            registeredSignComponents.add(factory);
            for (String validLine : factory.getValidLines()) {
                if (componentsBySignLine.containsKey(validLine)) {
                    throw new IllegalArgumentException("There is already a sign registered for the line '" + validLine + "'");
                }
                componentsBySignLine.put(validLine, factory);
                validSigns.add(validLine);
            }
        }
        registeredComponents.add(factory);
    }

    @Override
    public void onDisable() {
        INSTANCE = null;
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData lines, @Nullable Player player) {
        // Created.
        if (lines.get(1).get().toPlain().equalsIgnoreCase("[Extractor]")) {
            createExtractor(location);
            return true;
        }
        MegaPipesComponent.Factory<?> factory = componentsBySignLine.get(SignUtil.getTextRaw(lines, 1));
        factory.verifyLines(lines);
        return true;
    }

    @Listener
    public void onServerStarted(GameStartedServerEvent e) {
        for (World world : Sponge.getServer().getWorlds()) {
            for (Chunk chunk : world.getLoadedChunks()) {
                loadInChunk(chunk);
            }
        }
    }

    @Listener
    public void onChunkLoad(LoadChunkEvent e) {
        if (Sponge.getGame().getState() == GameState.SERVER_STARTED) {
            loadInChunk(e.getTargetChunk());
        }
    }

    private void loadInChunk(Chunk chunk) {
        for (TileEntity tileEntity : chunk.getTileEntities()) {
            if (tileEntity instanceof Sign) {
                if (isValid(tileEntity.getLocation())) {
                    createExtractor(tileEntity.getLocation());
                }
            }
        }
    }

    @Listener
    public void onChunkUnload(UnloadChunkEvent e) {

    }

    @Listener
    public void onBlockChange(ChangeBlockEvent e) {
        for (Transaction<BlockSnapshot> transaction : e.getTransactions()) {
            Location<World> loc = transaction.getOriginal().getLocation().get();
            BlockType original = transaction.getOriginal().getState().getType();
            BlockType end = transaction.getFinal().getState().getType();

            if (end == original) {
                return;
            }

            if (original == BlockTypes.STANDING_SIGN || original == BlockTypes.WALL_SIGN) {
                // Mechanic may have been destroyed.
                this.extractors.remove(loc);
                return;
            }

            if (end == ExtractorMultiBlock.EXTRACTOR_BLOCK_TYPE) {
                // Extractor placed
                Direction ignoreDir = transaction.getFinal().require(Keys.DIRECTION);
                Direction ignoreDirOpposite = ignoreDir.getOpposite();

                for (Map.Entry<Location<World>, ExtractorMultiBlock> entry : extractors.entrySet()) {
                    for (Direction dir : BlockUtil.getDirectFaces()) {
                        if (dir == ignoreDir || dir == ignoreDirOpposite) {
                            continue;
                        }
                        Location<World> location = loc.getBlockRelative(dir);
                        ExtractorMultiBlock extractorMultiBlock = entry.getValue();
                        if (extractorMultiBlock.containsLocation(location)) {
                            extractorMultiBlock.discoverExtractors();
                            return;
                        }
                    }
                }
                return;
            }

            if (original == ExtractorMultiBlock.EXTRACTOR_BLOCK_TYPE) {
                // Extractor destroyed
                Direction ignoreDir = transaction.getOriginal().require(Keys.DIRECTION);
                Direction ignoreDirOpposite = ignoreDir.getOpposite();

                for (Map.Entry<Location<World>, ExtractorMultiBlock> entry : extractors.entrySet()) {
                    for (Direction dir : BlockUtil.getDirectFaces()) {
                        if (dir == ignoreDir || dir == ignoreDirOpposite) {
                            continue;
                        }
                        Location<World> location = loc.getBlockRelative(dir);
                        ExtractorMultiBlock extractorMultiBlock = entry.getValue();
                        if (extractorMultiBlock.containsLocation(location)) {
                            extractorMultiBlock.discoverExtractors(); // Re-discover thingies.
                            return;
                        }
                    }
                }
                return;
            }

            if (isPipe(loc, end)) {
                // Pipe created
                //MegaPipesPipeComponent pipeComponent = getCustomPipe(loc);
                for (Direction dir : BlockUtil.getDirectFaces()) {
                    Location<World> adjacent = loc.getBlockRelative(dir);
                    Vector3i adjacentPos = adjacent.getBlockPosition();
                    ExtractorMultiBlock extractorMultiBlock = extractors.get(adjacent);
                    Pipe newPipe = null;
                    for (Pipe pipe : pipes) {
                        if (pipe.containsPart(adjacentPos)) {
                            List<Pipe> merged = pipe.addPart(adjacentPos);
                            for (Pipe mergedPipe : merged) {
                                this.pipes.remove(mergedPipe);
                                onPipeChange(mergedPipe);
                            }
                            onPipeChange(pipe);
                            newPipe = pipe;
                            break;
                        }
                    }
                    if (newPipe == null) {
                        newPipe = Pipe.fromLocation(loc);
                    }
                    if (extractorMultiBlock != null) {
                        extractorMultiBlock.onPipeChange(newPipe);
                    }

                }
                return;
            }

            if (isPipe(loc, original)) {
                // Pipe destroyed!
                Pipe foundPipe = null;
                for (Pipe pipe : pipes) {
                    if (pipe.containsPart(loc)) {
                        foundPipe = pipe;
                    }
                }
                if (foundPipe != null) {
                    pipes.remove(foundPipe);
                    pipes.addAll(foundPipe.breakAt(loc.getBlockPosition()));
                    onPipeChange(foundPipe);
                }
                return;
            }

            boolean removed = destinations.containsEntry(loc.getExtent(), loc.getBlockPosition());
            if (removed || getDestination(loc) != null) {
                if (removed) {
                    destinations.remove(loc.getExtent(), loc.getBlockPosition());
                }
                // Destination changed.
                for (Direction dir : BlockUtil.getDirectFaces()) {
                    Location<World> adjacent = loc.getBlockRelative(dir);
                    Vector3i adjacentPos = adjacent.getBlockPosition();
                    for (Pipe pipe : pipes) {
                        if (pipe.containsPart(adjacentPos)) {
                            List<Pipe> merged = pipe.addPart(adjacentPos);
                            for (Pipe mergedPipe : merged) {
                                this.pipes.remove(mergedPipe);
                                onPipeChange(mergedPipe);
                            }
                            onPipeChange(pipe);
                        }
                    }
                }
            }
        }
    }

    public void onPipeChange(Pipe pipe) {
        for (ExtractorMultiBlock extractorMultiBlock : extractors.values()) {
            extractorMultiBlock.onPipeChange(pipe);
        }
    }

    public void createExtractor(Location<World> signLoc) {
        ExtractorMultiBlock extractorMultiBlock = new ExtractorMultiBlock(signLoc);
        extractorMultiBlock.discoverExtractors();
        extractors.put(signLoc, extractorMultiBlock);
        SpongeSelfTriggerManager selfTriggerManager = (SpongeSelfTriggerManager) CraftBookPlugin.spongeInst().getSelfTriggerManager().get();
        selfTriggerManager.register(this, signLoc);
    }

    @Override
    public String[] getValidSigns() {
        if (validSignsArray == null) {
            validSignsArray = validSigns.toArray(new String[0]);
        }
        return validSignsArray;
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    private int TICK_COUNT = 0;

    @Override
    public void onThink(Location<World> location) {
        TICK_COUNT++;
        ExtractorMultiBlock extractorMultiBlock = this.extractors.get(location);
        if (extractorMultiBlock != null && TICK_COUNT % 20 == 0) {
            System.out.println("Amount of pipes: " + pipes.size());
            extractorMultiBlock.tick();
        }
    }

    @Override
    public boolean isValid(Location<World> location) {
        BlockType blockType = location.getBlockType();
        if (blockType == BlockTypes.STANDING_SIGN) {
            Location<World> up = location.getBlockRelative(Direction.UP);
            if (!isValidBlock(location, up)) {
                return false;
            }
        }
        else if (blockType == BlockTypes.WALL_SIGN) {
            Location<World> block = SignUtil.getBackBlock(location);
            if (!isValidBlock(location, block)) {
                return false;
            }
        }
        return super.isValid(location);
    }

    public boolean isValidBlock(Location<World> signLoc, Location<World> block) {

        if (block.getBlockType() == ExtractorMultiBlock.EXTRACTOR_BLOCK_TYPE) {
            return true;
        }

        Sign sign = (Sign) signLoc.getTileEntity().get();
        SignData signData = sign.getSignData();
        String line = signData.get(1).get().toPlain();
        for (MegaPipesComponent.Factory<?> factory : registeredSignComponents) {
            for(String text : factory.getValidLines()) {
                if(text.equalsIgnoreCase(line)) {
                    return factory.isValid(block) && factory.verifyLines(signData);
                }
            }
        }
        return false;
    }

    @Override
    public String getName(String usedLine) {
        return getName() + usedLine.replaceAll("\\[\\]", "");
    }

    public static boolean isPipe(Location<World> loc, BlockType blockType) {
        return blockType == Pipe.PIPE_BLOCK_TYPE || getCustomPipe(loc) != null;
    }

    @Nullable
    public static MegaPipesPipeComponent getCustomPipe(Location<World> loc) {
        return INSTANCE.customPipeComponents.get(loc);
    }

    @Nullable
    public static MegaPipesDestination getDestination(Location<World> loc) {
        for (MegaPipesComponent.Factory<? extends MegaPipesDestination> factory : getDestinationComponents()) {
            if (factory.isValid(loc)) {
                return factory.create(loc);
            }
        }
        return null;
    }

    public static void addDestination(Location<World> destination) {
        MegaPipes.INSTANCE.destinations.put(destination.getExtent(), destination.getBlockPosition());
    }

    @Nullable
    public static MegaPipesSource getSource(Location<World> loc) {
        return null;
    }
}
