/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.mechanic.megapipes;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.mechanic.megapipes.component.destination.MegaPipesDestination;
import com.minecraftonline.util.BlockFace;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.carrier.Chest;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Set;

public class ExtractorMultiBlock {
    public static final BlockType EXTRACTOR_BLOCK_TYPE = BlockTypes.PISTON;
    public static final int MAX_BREADTH_SEARCH_AMOUNT = 150;

    private final Location<World> signLoc;
    private Map<Vector3i, ExtractorPipeCache> extractors = new HashMap<>();

    public ExtractorMultiBlock(Location<World> signLoc) {
        this.signLoc = signLoc;
    }

    public void discoverExtractors() {
        Map<Vector3i, ExtractorPipeCache> newExtractors = new HashMap<>();
        Location<World> firstBlock;
        if (signLoc.getBlockType() == BlockTypes.STANDING_SIGN) {
            firstBlock = signLoc.getBlockRelative(Direction.UP);
        }
        else if (signLoc.getBlockType() == BlockTypes.WALL_SIGN) {
            firstBlock = signLoc.getBlockRelative(signLoc.require(Keys.DIRECTION).getOpposite());
        }
        else {
            throw new IllegalStateException("Invalid Extractor signLoc, should have been a standing or wall sign.");
        }

        if (firstBlock.getBlockType() != EXTRACTOR_BLOCK_TYPE) {
            throw new IllegalStateException("Invalid Extractor, should be placed on or below a " + EXTRACTOR_BLOCK_TYPE.getName());
        }
        int searchedAmt = 0;

        Queue<Location<World>> queue = new ArrayDeque<>();
        queue.add(firstBlock);

        Set<Vector3i> searched = new HashSet<>();

        List<Pipe> pipes = new ArrayList<>();

        while (!queue.isEmpty() && searchedAmt < MAX_BREADTH_SEARCH_AMOUNT) {
            Location<World> loc = queue.remove();
            BlockType blockType = loc.getBlockType();
            Vector3i pos = loc.getBlockPosition();
            if (searched.contains(pos) || blockType != EXTRACTOR_BLOCK_TYPE) {
                continue;
            }
            searched.add(pos);
            Direction facing = loc.require(Keys.DIRECTION);
            ExtractorPipeCache pipeCache = extractors.get(pos);
            if (pipeCache == null) {
                Location<World> pipeLoc = loc.getBlockRelative(facing);
                // Create new ExtractorPipeCache
                Pipe foundPipe = null;
                if (pipeLoc.getBlockType() == Pipe.PIPE_BLOCK_TYPE) {
                    Vector3i pipePos = pos.add(facing.asBlockOffset());
                    for (Pipe pipe : pipes) {
                        if (pipe.containsPart(pipePos)) {
                            foundPipe = pipe;
                        }
                    }

                    if (foundPipe == null) {
                        foundPipe = MegaPipes.getOrCreatePipe(pipeLoc);
                        pipes.add(foundPipe);
                    }
                }

                pipeCache = new ExtractorPipeCache(foundPipe, pipeLoc);
            }
            newExtractors.put(pos, pipeCache);

            Direction facingOpposite = facing.getOpposite();

            for (Direction dir : BlockUtil.getDirectFaces()) {
                if (dir == facing || dir == facingOpposite) {
                    continue;
                }
                queue.add(loc.getBlockRelative(dir));
            }

            searchedAmt++;
        }
        this.extractors = newExtractors;
    }

    public void tick() {
        for (Map.Entry<Vector3i, ExtractorPipeCache> entry : extractors.entrySet()) {
            tickExtractor(entry.getKey(), entry.getValue());
        }

    }

    private void tickExtractor(Vector3i extractorPos, ExtractorPipeCache pipeCache) {
        Location<World> extractorLoc = this.signLoc.getExtent().getLocation(extractorPos);
        Direction facing = extractorLoc.require(Keys.DIRECTION);
        Location<World> targetLoc = extractorLoc.getBlockRelative(facing.getOpposite());

        BlockType targetLocBlockType = targetLoc.getBlockType();

        if (targetLocBlockType == BlockTypes.CHEST
                || targetLocBlockType == BlockTypes.TRAPPED_CHEST) {
            Chest chest = (Chest) targetLoc.getTileEntity().get();
            Inventory inv = chest.getDoubleChestInventory().orElse(chest.getInventory());
            ItemStack first = null;
            for (Inventory slot : inv.slots()) {
                if (!slot.peek().isPresent()) {
                    continue;
                }
                first = slot.peek().get();
                break;
            }
            if (first == null) {
                System.out.println(extractorPos + " - No items to be extracted");
                return;
            }
            Inventory queriedInv = inv.query(QueryOperationTypes.ITEM_STACK_IGNORE_QUANTITY.of(first));
            System.out.println(extractorPos + " - Found: " + first);

            Iterator<ExtractorPipeCache.FoundContainer> iterator = pipeCache.containerIterator();
            while (iterator.hasNext()) {
                ExtractorPipeCache.FoundContainer dest = iterator.next();
                Location<World> containerLoc = signLoc.getExtent().getLocation(dest.pos);
                MegaPipesDestination destination = MegaPipes.getDestination(containerLoc);

                if (destination == null) {
                    throw new IllegalStateException("No destination at " + dest + " when there should have been");
                }
                int room = destination.canAcceptHowMuch(first, dest.face);
                if (room != 0) {
                    System.out.println(pipeCache.start.getBlockPosition() + " - Transporting to " + dest + " on blockface " + dest.face.name());
                    int toTake = Math.min(room, queriedInv.totalItems());
                    ItemStack itemStack = queriedInv.poll(toTake).get();
                    destination.accept(itemStack, dest.face);
                    return;
                }
            }
            System.out.println(pipeCache.start.getBlockPosition() + " - No destination found.");
        }
        else {
            System.out.println(extractorPos + " - Nothing to extract out of");
        }
    }

    public boolean containsLocation(Location<World> loc) {
        return this.extractors.containsKey(loc.getBlockPosition());
    }

    public void onPipeChange(Pipe pipe) {
        // Fix caches.
        for (ExtractorPipeCache pipeCache : extractors.values()) {
            if (pipeCache.pipe == null) {
                if (pipe.containsPart(pipeCache.start)) {
                    pipeCache.resetWithNewPipe(pipe);
                }
            }
            else if (pipeCache.pipe == pipe) {
                Pipe newPipe = null;
                if (pipeCache.start.getBlockType() == Pipe.PIPE_BLOCK_TYPE) {
                    newPipe = MegaPipes.getOrCreatePipe(pipeCache.start);
                }
                pipeCache.resetWithNewPipe(newPipe);
            }
        }
    }

    public static class ExtractorPipeCache {

        private Pipe pipe;
        private final Location<World> start;
        private final List<FoundContainer> foundContainers = new ArrayList<>();
        private final Set<Vector3i> searched = new HashSet<>();
        private boolean finished = false;

        public static class FoundContainer {
            public final Vector3i pos;
            public final BlockFace face;

            public FoundContainer(Vector3i pos, BlockFace face) {
                this.pos = pos;
                this.face = face;
            }
        }

        public ExtractorPipeCache(@Nullable Pipe pipe, Location<World> start) {
            this.pipe = pipe;
            this.start = start;
            if (pipe == null) {
                finished = true;
            }
        }

        public void resetWithNewPipe(@Nullable Pipe newPipe) {
            this.pipe = newPipe;
            foundContainers.clear();
            searched.clear();
            finished = pipe == null;
        }

        public Iterator<FoundContainer> containerIterator() {
            return new Iterator<FoundContainer>() {

                int cur = 0;

                @Override
                public boolean hasNext() {
                    if (foundContainers.size() > cur) {
                        return true;
                    }
                    if (finished) {
                        return false;
                    }
                    findContainers();
                    return foundContainers.size() > cur;
                }

                @Override
                public FoundContainer next() {
                    if (!hasNext()) {
                        throw new NoSuchElementException();
                    }
                    return foundContainers.get(cur++);
                }

                private void findContainers() {
                    Queue<Location<World>> queue = new ArrayDeque<>();
                    queue.add(start);

                    while (!queue.isEmpty()) {
                        Location<World> loc = queue.remove();
                        Vector3i pos = loc.getBlockPosition();
                        if (searched.contains(pos)) {
                            continue;
                        }
                        searched.add(pos);
                        if (pipe.containsPart(pos)) {
                            for (Direction dir : BlockUtil.getDirectFaces()) {
                                Location<World> adj = loc.getBlockRelative(dir);
                                MegaPipesDestination megaPipesDestination = MegaPipes.getDestination(adj);
                                if (megaPipesDestination != null) {
                                    foundContainers.add(
                                            new FoundContainer(adj.getBlockPosition(), megaPipesDestination.getBlockFace(loc.getBlockPosition()))
                                    );
                                    MegaPipes.addDestination(adj);
                                }
                                else {
                                    queue.add(adj);
                                }
                            }
                        }
                    }
                    finished = true;
                }
            };
        }
    }
}