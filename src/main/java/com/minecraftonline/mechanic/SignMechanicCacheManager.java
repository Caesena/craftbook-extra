/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.mechanic;

import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeSignMechanic;
import org.spongepowered.api.GameState;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.world.chunk.LoadChunkEvent;
import org.spongepowered.api.event.world.chunk.UnloadChunkEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

public class SignMechanicCacheManager {
    private final Map<SpongeSignMechanic, SignMechanicCache<? extends CachedData>> cache = new HashMap<>();

    private boolean registered = false;

    public <T extends CachedData> void registerCache(SpongeSignMechanic mechanic, CachedDataCreator<T> cacheDataCreator) {
        cache.put(mechanic, new SignMechanicCache<>(cacheDataCreator));

        if (!registered) {
            Sponge.getEventManager().registerListeners(CraftBookPlugin.spongeInst(), this);
            registered = true;
        }
    }

    // Fire Late to try to be after world-edit so we can translate old ids.
    @Listener(order= Order.LATE)
    public void gameStartedEvent(GameStartedServerEvent e) {
        for (World world : Sponge.getServer().getWorlds()) {
            for (Chunk chunk : world.getLoadedChunks()) {
                findMechanicsInChunk(chunk);
            }
        }
    }

    @Listener
    public void onChunkLoad(LoadChunkEvent e) {
        if (Sponge.getGame().getState().equals(GameState.SERVER_STARTED)) {
            findMechanicsInChunk(e.getTargetChunk());
        }
    }

    @Listener
    public void onChunkUnload(UnloadChunkEvent e) {
        Chunk chunk = e.getTargetChunk();
        for (SignMechanicCache<?> mechanicCache : this.cache.values()) {
            List<Location<World>> toRemove = new ArrayList<>();
            for (Map.Entry<Location<World>, ? extends CachedData> cachedData : mechanicCache.cache.entrySet()) {
                if (cachedData.getKey().getExtent().equals(chunk.getWorld())
                    && chunk.containsBlock(cachedData.getKey().getBlockPosition())) {
                    cachedData.getValue().onUnload(cachedData.getKey());
                    toRemove.add(cachedData.getKey());
                }
            }
            for (Location<World> loc : toRemove) {
                mechanicCache.cache.remove(loc);
            }
        }
    }

    @Listener
    public void onBlockBreak(ChangeBlockEvent.Break breakEvent) {
        for (Transaction<BlockSnapshot> transaction : breakEvent.getTransactions()) {
            for (SignMechanicCache<?> signMechanicCache : cache.values()) {
                signMechanicCache.notifyOfTransaction(transaction);
            }
        }
    }

    private void findMechanicsInChunk(Chunk chunk) {
        for (TileEntity tileEntity : chunk.getTileEntities()) {
            for (Map.Entry<SpongeSignMechanic, SignMechanicCache<?>> entry : cache.entrySet()) {
                Location<World> loc = tileEntity.getLocation();
                entry.getValue().tryPut(loc);
            }
        }
    }

    public void mechanicCreated(SpongeSignMechanic mechanic, Location<World> location, SignData signData) {
        SignMechanicCache<?> signMechanicCache = cache.get(mechanic);
        if (signMechanicCache == null) {
            throw new IllegalStateException(mechanic.getClass().getName() + " did not register with the cache before trying to use it!");
        }
        signMechanicCache.put(location, signData);
    }

    public void mechanicChanged(SpongeSignMechanic mechanic, Location<World> location, SignData signData) {
        SignMechanicCache<?> signMechanicCache = cache.get(mechanic);
        if (signMechanicCache == null) {
            throw new IllegalStateException(mechanic.getClass().getName() + " did not register with the cache before trying to use it!");
        }
        if (signMechanicCache.get(location) == null) {
            throw new IllegalStateException("Tried to change cached data but it never existed!");
        }
        signMechanicCache.put(location, signData);
    }

    @Nullable
    public <T extends CachedData> T getCached(SpongeSignMechanic mechanic, Location<World> loc) {
        loc = new Location<>(loc.getExtent(), loc.getBlockPosition());
        SignMechanicCache<T> signMechanicCache = (SignMechanicCache<T>) cache.get(mechanic);
        if (signMechanicCache == null) {
            throw new IllegalStateException(mechanic.getClass().getName() + " did not register with the cache before trying to use it!");
        }
        return signMechanicCache.get(loc);
    }

    public interface CachedDataCreator<T> extends BiFunction<Location<World>, SignData, T> {}

    public static class SignMechanicCache<T extends CachedData> {
        private final Map<Location<World>, T> cache = new HashMap<>();
        private final CachedDataCreator<T> cacheDataCreator;

        public SignMechanicCache(CachedDataCreator<T> cacheDataCreator) {
            this.cacheDataCreator = cacheDataCreator;
        }

        public T get(Location<World> location) {
            return cache.get(location);
        }

        public boolean tryPut(Location<World> location) {
            return location.getTileEntity().filter(tileEntity -> tileEntity instanceof Sign)
                    .map(tileEntity -> (Sign)tileEntity)
                    .map(Sign::getSignData)
                    .map(signData -> tryPut(location, signData))
                    .orElse(false);
        }

        public boolean tryPut(Location<World> location, SignData signData) {
            T cached = cacheDataCreator.apply(location, signData);
            if (cached != null) {
                cache.put(location, cached);
                return true;
            }
            return false;
        }

        public void put(Location<World> location) {
            location.getTileEntity().filter(tileEntity -> tileEntity instanceof Sign)
                    .map(tileEntity -> (Sign)tileEntity)
                    .map(Sign::getSignData)
                    .ifPresent(signData -> put(location, signData));
        }

        /**
         * Puts the cached data into the cache.
         * If the cached data creator doesn't think it is
         * a valid mechanic, throws an NPE.
         * @param location Location
         * @param signData SignData
         */
        public void put(Location<World> location, SignData signData) {
            T cached = cacheDataCreator.apply(location, signData);
            if (cached == null) {
                throw new NullPointerException("Cached data created should have created cached data but it returned null!");
            }
            cache.put(location, cached);
        }

        public void notifyOfTransaction(Transaction<BlockSnapshot> transaction) {
            transaction.getOriginal().getLocation().ifPresent(loc -> {
                T cached = cache.remove(loc);
                if (cached != null) {
                    if (!tryPut(loc)) { // Maybe its still valid?
                        cached.onUnload(loc);
                    }
                }
            });
        }
    }

    public static abstract class CachedData {
        public final List<Text> lines;

        protected CachedData(List<Text> lines) {
            this.lines = lines;
        }

        public String getLinePlain(int i) {
            return lines.get(i).toPlain();
        }

        /**
         * Called when this cache is removed.
         * @param loc
         */
        public void onUnload(Location<World> loc) {}
    }

    private static final SignMechanicCacheManager INSTANCE = new SignMechanicCacheManager();

    private SignMechanicCacheManager() {}

    public static SignMechanicCacheManager getInstance() {
        return INSTANCE;
    }
}
