/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.ic;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.legacy.ics.chips.world.block.BlockPlacingIC;
import com.minecraftonline.util.parsing.lineparser.range.RangeLimitations;
import com.minecraftonline.util.parsing.lineparser.range.RangeLineParserFactory;
import com.minecraftonline.util.parsing.rangeparser.RangeParsers;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * MCX213
 */
public class Harvester extends BlockPlacingIC<Harvester> {

    private static final RangeLineParserFactory RANGE_PARSER = RangeLineParserFactory.builder()
            .parser(RangeParsers.WIDTH_LENGTH_HEIGHT_Y_OFFSET)
            .limitation(RangeLimitations.maxWidth(() -> ICSocket.maximumWidth.getValue().doubleValue()))
            .limitation(RangeLimitations.maxLength(() -> ICSocket.maximumLength.getValue().doubleValue()))
            .limitation(RangeLimitations.maxHeight(() -> ICSocket.maximumLength.getValue().doubleValue()))
            .build();

    public Harvester(ICFactory<Harvester> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    @Override
    public RangeLineParserFactory getRangeParserFactory() {
        return RANGE_PARSER;
    }

    @Override
    public void setBlocks(Vector3i start, Vector3i end, boolean set, BlockBag bag) {
        if (set) {
            return;
        }
        super.setBlocks(start, end, false, bag);
    }

    public static class Factory extends BlockPlacingIC.Factory<Harvester> {

        @Override
        public Harvester createInstance(Location<World> location) {
            return new Harvester(this, location);
        }
    }
}
