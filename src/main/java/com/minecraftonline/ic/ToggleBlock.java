/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.ic;

import com.minecraftonline.legacy.ics.chips.world.block.BlockPlacingIC;
import com.minecraftonline.legacy.ics.chips.world.block.FlexSet;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.parsing.lineparser.generic.SimpleDualParserFactory;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.function.Predicate;

public class ToggleBlock extends AbstractIC implements AreaIC {

    private static final SimpleDualParserFactory<BlockState, BlockState> BOTH_BLOCK_PARSER = new SimpleDualParserFactory<>(
            FlexSet.BLOCK_STATE_PARSER_FACTORY,
            FlexSet.BLOCK_STATE_PARSER_FACTORY,
            "[|]"
    );

    private BlockState block1;
    private BlockState block2;
    private Location<World> location;

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, BOTH_BLOCK_PARSER.createBinder(this, (block1, block2) -> {
                    this.block1 = block1;
                    this.block2 = block2;
                }))
                .lineParser(3, FlexSet.OFFSET_PARSER_FACTORY.createBinder(this, offset -> this.location = this.getBackBlock().add(offset)))
                .build();
    }

    public ToggleBlock(ICFactory<? extends IC> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    @Override
    public void onTrigger() {
        final BlockState to;
        final BlockState from;

        if (getPinSet().isAnyTriggered(this)) {
            to = this.block1;
            from = this.block2;
        }
        else {
            to = this.block2;
            from = this.block1;
        }

        BlockState cur = this.location.getBlock();
        BlockBag bag = BlockBagManager.getOrCreateMultiNearbyBag(getBlock());

        if (bag == null) {
            return;
        }

        ItemStack toConsume = IDUtil.getItemStackFromBlockState(to);
        Predicate<ItemStack> consumePredicate = BlockPlacingIC.getPredicate(toConsume);
        if (!bag.has(consumePredicate, 1)) {
            return; // Out of blocks
        }

        if (cur.getType() == BlockTypes.AIR) {
            if (location.setBlock(to)) {
                bag.remove(consumePredicate, 1);
            }
            return;
        }
        else if (!cur.equals(from)) {
            if (cur.equals(to)) {
                return; // Don't need to message, just happen to be on the wrong state right now.
            }
            final Text text = Text.of(TextColors.RED, "Toggle Block at " + getBlock().getPosition() + " has a block in the way!");
            for (Player player : Sponge.getServer().getOnlinePlayers()) {
                if (player.getLocation().getExtent() == this.getBlock().getExtent()) {
                    if (player.getLocation().getPosition().distanceSquared(this.getBlock().getPosition()) < 5*5) {
                        player.sendMessage(text);
                    }
                }
            }
            return;
        }

        ItemStack pickupStack = IDUtil.getItemStackFromBlockState(cur);
        List<ItemStack> pickupStacks = BlockPlacingIC.getPickupBlocks(pickupStack, 1);
        if (!bag.canFit(pickupStacks)) {
            return; // No room to put in the item.
        }
        if (!location.setBlock(to)) {
            return; // In case we're cancelled.
        }
        bag.add(pickupStacks);
        bag.remove(consumePredicate, 1);
    }

    @Nullable
    @Override
    public CUIRegion getArea() {
        return WorldEditUtil.toCuboidSelection(this.location.getExtent(), this.location.getPosition(), this.location.getPosition());
    }

    public static class Factory implements ICFactory<ToggleBlock> {

        @Override
        public ToggleBlock createInstance(Location<World> location) {
            return new ToggleBlock(this, location);
        }

        @Override
        public String[][] getPinHelp() {
            return new String[0][];
        }
    }
}
