/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.ic;

import com.minecraftonline.legacy.ics.HybridIC;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.SelfTriggeringIC;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.OverridingMethodsMustInvokeSuper;

public abstract class AbstractStIC extends AbstractIC implements SelfTriggeringIC {
	protected Boolean isSt = null; // If this is null, you forgot to call super.load()
	protected boolean thinking;

	public AbstractStIC(ICFactory<? extends IC> icFactory, Location<World> block) {
		super(icFactory, block);
	}

	@Override
	public void think() {
		thinking = true;
		try {
			trigger();
		} finally {
			thinking = false;
		}
	}

	@OverridingMethodsMustInvokeSuper
	@Override
	public void load() {
		super.load();
		isSt = isAlwaysST() || getFactory() instanceof HybridIC && getLine(1).contains(((HybridIC)getFactory()).stVariant());
	}
}
