/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.ic;

import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.BitSet;
import java.util.List;

/**
 * BITSHIFT
 * MC2022
 *
 * Program and Shift a queue of bits
 *
 * @author Brendan (doublehelix457)
 */
public class BitShift extends AbstractStIC{

    public BitShift(ICFactory<BitShift> icFactory, Location<World> block){super(icFactory,block);}

    private int queueSize = 2;
    private boolean[] bits;
    Base64.Decoder decoder = Base64.getDecoder();
    Base64.Encoder encoder = Base64.getEncoder();

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        int bits = 0;
        byte[] storage;


        try {
            if (SignUtil.getTextRaw(lines.get(2)).length() != 0) {
                bits = Integer.parseInt(SignUtil.getTextRaw(lines.get(2)));
            }
        } catch (NumberFormatException nfe) {
            throw new InvalidICException("Invalid Storage Size, must be a number between 2 and 64.");
        }

        if ((bits < 2 || bits > 64) && SignUtil.getTextRaw(lines.get(2)).length() != 0) {
            throw new InvalidICException("Invalid Storage Size, valid range: 2-64");
        }

        if (!SignUtil.getTextRaw(lines.get(3)).isEmpty() && !SignUtil.getTextRaw(lines.get(3)).equals("")){
            try {
                storage = decoder.decode(SignUtil.getTextRaw(lines.get(3)));
                if((storage.length > 1 && bits < 8)){
                    throw new InvalidICException("Base64 Encoded Data Size exceeds Storage Limit on Line 3.");
                }else if(storage.length * 8 > 64){
                    throw new InvalidICException("Base64 Encoded Data Size exceeds Max Storage Limit");
                }
            } catch (IllegalArgumentException iae) {
                throw new InvalidICException("Invalid Base64 Encoding.");
            }
        }
    }

    @Override
    public void load(){
        super.load();
        queueSize = Integer.parseInt(getLine(2));
        if(!getLine(3).isEmpty() && !getLine(3).equals("")){
            byte[] storage = decoder.decode(getLine(3).trim().getBytes(StandardCharsets.US_ASCII));
                bits = new boolean[storage.length*8];
                for(int i = 0;i<storage.length*8;i++) if((storage[i/8] & (1<<(7 - (i % 8)))) > 0) bits[i] = true;
                if(bits.length > queueSize) bits = Arrays.copyOf(bits, queueSize);
        } else {
            bits = new boolean[queueSize];
            for(int i = 0; i<bits.length;i++) bits[i] = false;
        }
    }

    @Override
    public void unload(){
        super.unload();
        //Bits are reversed, needs fixing
        byte[] storage = toBytes(bits);
        setLine(3, Text.of(encoder.encodeToString(storage)));
    }

    @Override
    public void onTrigger() {
        if(getPinSet().isTriggered(0, this) && getPinSet().getInput(0, this))
            bits = leftShift(bits, 1);
        else if(getPinSet().isTriggered(1, this) && getPinSet().getInput(1, this))
            bits[0] = getPinSet().getInput(2, this);
    }

    private boolean[] leftShift(boolean[] b, int n){
        boolean[] r = new boolean[b.length];
        n = Math.min(n, b.length);
        System.arraycopy(b, n, r, 0, b.length - n);
        r[r.length - 1] = b[0]; //carryover
        return r;
    }

    private byte[] toBytes(boolean[] bits){
        BitSet set = new BitSet(bits.length);
        for(int i=0;i<bits.length;i++)
            if(bits[i]) set.set(i);

        byte[] bytes = set.toByteArray();
        if(!(bytes.length * 8 >= bits.length))
        bytes = Arrays.copyOf(bytes, bits.length / 8 + (bits.length % 8 == 0 ? 0 : 1));

        for(int i=0;i<bytes.length;i++){
            bytes[i] = reverseByte(bytes[i]);
        }

        return bytes;
    }

    private byte reverseByte(byte b){
        int intSize = 8;
        byte y=0;
        for(int position=intSize-1; position>0; position--){
            y+=((b&1)<<position);
            b >>= 1;
        }
        return y;
    }

    private boolean isSet(byte b, int index){
        return (b & (1<<index)) != 0;
    }

    @Override
    public void think(){ getPinSet().setOutput(0, bits[0], this); }

    @Override
    public boolean isAlwaysST() {return true;}

    public static class Factory implements ICFactory<BitShift>, RestrictedIC {

        @Override
        public BitShift createInstance(Location<World> location) { return new BitShift(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Left Shift Bits in the Register",
                            "Set or Unset a Bit",
                            "Change the value of the Setter"
                    },
                    new String[]{
                            "Output if Bit Is Set in queue"
                    }
            };
        }
    }

}
