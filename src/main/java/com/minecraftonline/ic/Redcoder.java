/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.ic;

import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.lineparser.generic.bool.BooleanParserFactory;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless.Band;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless.Bands;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * MC6543
 *
 * Analog Transmitter
 *
 * Powers a specific redstone level, or optionally, powers
 * all up to the given redstone level.
 *
 * @see com.minecraftonline.legacy.ics.chips.world.miscellaneous.MarqueeTransmitter
 */
public class Redcoder extends AbstractIC {

    private String shortBand;

    private int lastPower = 0;

    private boolean powerAll;

    public Redcoder(ICFactory<? extends IC> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    private static final BooleanParserFactory BOOLEAN_PARSER_FACTORY = BooleanParserFactory.builder()
            .defaultValue(false)
            .string("silly")
            .build();

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, new ParsingConfiguration.LineParser() {
                    @Override
                    public void parse(String line, @Nullable Subject cause) {
                        shortBand = line;
                    }

                    @Override
                    public Text getHelp() {
                        return Text.of("Short band");
                    }
                })
                .lineParser(3, BOOLEAN_PARSER_FACTORY.createBinder(this, b -> this.powerAll = b))
                .build();
    }

    @Override
    public void load() {
        super.load();

        int inputPower = 0;
        for (int i = 0; i < getPinSet().getInputCount(); i++) {
            inputPower = Math.max(inputPower, getPinSet().getInputPowerLevel(i, this));
        }
        lastPower = inputPower;
    }

    @Override
    public void onTrigger() {
        int inputPower = 0;
        for (int i = 0; i < getPinSet().getInputCount(); i++) {
            inputPower = Math.max(inputPower, getPinSet().getInputPowerLevel(i, this));
        }
        if (lastPower == inputPower) {
            return;
        }

        if (powerAll) {
            if (inputPower < lastPower) {
                for (int i = lastPower; i > inputPower; i--) {
                    Band old = makeBand(i);
                    Bands.setActivatedBand(old, false);
                }
            }
            for (int i = lastPower; i <= inputPower; i++) {
                Band newBand = makeBand(i);
                Bands.setActivatedBand(newBand, true);
            }
        }
        else {
            Band old = makeBand(lastPower);
            Bands.setActivatedBand(old, false);

            Band newBand = makeBand(inputPower);
            Bands.setActivatedBand(newBand, true);
        }

        lastPower = inputPower;
    }

    public static class Factory implements ICFactory<Redcoder> {

        @Override
        public Redcoder createInstance(Location<World> location) {
            return new Redcoder(this, location);
        }

        @Override
        public String[][] getPinHelp() {
            return new String[0][];
        }
    }

    public Band makeBand(int num) {
        return new Band("", shortBand + num);
    }
}
