/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.ic;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.util.ParsingConfiguration;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.PinSet;
import com.sk89q.craftbook.sponge.mechanics.ics.ICManager;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.mode.Mode;
import com.sk89q.craftbook.sponge.mode.Modes;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.CauseStackManager;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.channel.MessageReceiver;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.OverridingMethodsMustInvokeSuper;
import java.util.List;

public abstract class AbstractIC implements IC {

    private final ICFactory<? extends IC> icFactory;
    public final Location<World> block;
    private final ParsingConfiguration parsingConfiguration;
    private Direction facing;
    private Sign sign;
    private PinSet pinSet;
    private int triggeredPin;
    private Mode icMode;

    public MessageReceiver DEBUG_TARGET  = null;

    private boolean setAdminOnLoad = false;

    private boolean corrupt = false;

    public AbstractIC(ICFactory<? extends IC> icFactory, Location<World> block) {
        this.icFactory = icFactory;
        this.block = block;

        this.parsingConfiguration = makeParsingConfiguration();
    }

    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.blank();
    }

    @Override
    public PinSet getPinSet() {
        if (pinSet == null) {
            pinSet = ICSocket.PINSETS.get(ICManager.getICType(icFactory).getDefaultPinSet());
        }
        return pinSet;
    }

    @OverridingMethodsMustInvokeSuper
    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        this.facing = block.require(Keys.DIRECTION);
        parsingConfiguration.parseAll(lines, player);
    }

    @Override
    public int getTriggeredPin() {
        return this.triggeredPin;
    }

    @Override
    public void setTriggeredPin(int triggeredPin) {
        this.triggeredPin = triggeredPin;
    }

    /**
     * Called when an IC is loaded into the world.
     */
    @OverridingMethodsMustInvokeSuper
    @Override
    public void load() {
        this.facing = block.require(Keys.DIRECTION);
        if (setAdminOnLoad) {
            setAdminAppearance();
        }
        try {
            parsingConfiguration.parseAll(getBlock().require(Keys.SIGN_LINES), null);
        } catch (InvalidICException e) {
            CraftBookPlugin.spongeInst().getLogger().error("Invalid " + getRegisteredName() + " IC at " + getBlock() + ". Disabling it for now", e);
            corrupt = true;
        }
    }

    @Override
    public void unload() {
    }

    @Override
    public Sign getSign() {
        if (sign == null) {
            sign = (Sign) block.getTileEntity().orElseThrow(() -> new IllegalStateException("IC given block that is not a sign! (" + block.getBlockPosition() + ") in world " + block.getExtent().getName()));
        }

        return sign;
    }

    @Override
    public String getLine(int line) {
        return SignUtil.getTextRaw(getSign(), line);
    }

    @Override
    public void setAdminAppearance() {
        setLine(1, Text.of(TextColors.LIGHT_PURPLE, getLine(1)));
    }

    @Override
    public void setAdminAppearanceOnLoad() {
        this.setAdminOnLoad = true;
    }

    @Override
    public void setLine(int line, Text text) {
        List<Text> lines = getSign().lines().get();
        lines.set(line, text);
        getSign().offer(Keys.SIGN_LINES, lines);
    }

    @Override
    public Location<World> getBlock() {
        return this.block;
    }

    @Override
    public Location<World> getBlockCentre() {
        return getBlock().add(Vector3d.from(0.5));
    }

    @Override
    public Location<World> getBackBlock() {
        return this.block.getBlockRelative(this.getFront().getOpposite());
    }

    @Override
    public Location<World> getBackBlockCentre() {
        return getBackBlock().add(Vector3d.from(0.5));
    }

    @Override
    public ICFactory<? extends IC> getFactory() {
        return this.icFactory;
    }

    public String getRegisteredName() {
        return ICManager.getICType(icFactory).getName();
    }

    /*
     * If you really need to override this, which I can't see why you would,
     * then you can remove the final, but it makes it far more likely for bugs overriding
     * the wrong function
     */
    @Override
    public final void trigger() {
        //if (DEBUG_TARGET != null) {
        //    DEBUG_TARGET.sendMessage(Text.of("In trigger"));
        //}
        if (corrupt) {
            return;
        }
        try (CauseStackManager.StackFrame frame = Sponge.getCauseStackManager().pushCauseFrame()) {
            frame.pushCause(this);
            //if (DEBUG_TARGET != null) {
            //    DEBUG_TARGET.sendMessage(Text.of("Pushed Cause and about to call onTrigger"));
            //}
            onTrigger();
        }
    }

    public abstract void onTrigger();

    @Override
    public void setMode(){
        if(!getLine(1).endsWith("]")){
            String modes = getLine(1).substring(getLine(1).lastIndexOf("]") + 1);
            icMode = new Mode(modes);
        }else{
            icMode = new Mode(" ");
        }
    }

    @Override
    public Mode getMode(){ return icMode; }

    @Override
    public boolean hasMode(){ return !(icMode.getType() == Modes.NONE); }

    public boolean isCorrupt() {
        return corrupt;
    }

    @Override
    public Direction getFront() {
        return this.facing;
    }
}
