/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.blockbags;

import com.flowpowered.math.vector.Vector3i;
import com.me4502.modularframework.module.ModuleWrapper;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager;
import com.sk89q.craftbook.sponge.mechanics.blockbags.inventory.BlockInventoryBlockBag;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.carrier.Chest;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperation;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Find a nearby chest blockbag within 5 block radius.
 *
 * @author Brendan  (doublehelix457)
 */
public class NearbyChestBlockBag extends BlockInventoryBlockBag {

    public NearbyChestBlockBag(Inventory inventory, Location<World> loc) {
        super(inventory, loc);
    }

    private NearbyChestBlockBag(Location<World> source) {
        super(source);
    }

    public static NearbyChestBlockBag fromSourcePositionNearby(Location<World> source) {
        List<Location<World>> list = new ArrayList<>();
        for (int dx = -5; dx < 5; dx++)
            for (int dy = -5; dy < 5; dy++)
                for (int dz = -5; dz < 5; dz++) {
                    Location<World> possibleContainer = new Location<>(source.getExtent(), source.getBlockX() + dx, source.getBlockY() + dy, source.getBlockZ() + dz);
                    BlockState possibleLoc = possibleContainer.getExtent().getBlock(possibleContainer.getBlockX(), possibleContainer.getBlockY(), possibleContainer.getBlockZ());
                    if (possibleLoc.getType() == BlockTypes.CHEST) {
                        list.add(possibleContainer);
                    }
                }
        Vector3i sourcePos = source.getBlockPosition();

        Location<World> closestLoc = null;
        Double distance = null;
        for (Location<World> loc : list) {
            double newDistance = loc.getBlockPosition().distance(sourcePos);
            if (distance == null || newDistance < distance) {
                closestLoc = loc;
                distance = newDistance;
            }
        }
        if (closestLoc == null) {
            return null;
        }
        Chest chest = (Chest) closestLoc.getTileEntity().get();
        return new NearbyChestBlockBag(chest.getDoubleChestInventory().orElse(chest.getInventory()), closestLoc);
    }

    public static NearbyChestBlockBag addSourcePosition(Location<World> source) {
        return new NearbyChestBlockBag(source);
    }

    public static Optional<NearbyChestBlockBag> findBlockBagAt(Location<World> loc) {
        return loc.getTileEntity().filter(te -> te instanceof Chest)
                .map(te -> (Chest) te)
                .map(chest -> new NearbyChestBlockBag(chest.getDoubleChestInventory().orElse(chest.getInventory()), loc));

    }

    @Override
    public Inventory getInventory() {
        return this.inventory;
    }
}
