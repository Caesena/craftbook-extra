/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.blockbags;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.block.tileentity.carrier.Chest;
import org.spongepowered.api.item.inventory.Carrier;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.util.Tuple;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.common.item.inventory.util.ItemStackUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MultiNearbyChestBlockBag implements BlockBag {
    List<NearbyChestBlockBag> blockBags;

    public MultiNearbyChestBlockBag() {
        this.blockBags = new ArrayList<>();
    }

    public MultiNearbyChestBlockBag(List<NearbyChestBlockBag> blockBags) {
        this.blockBags = blockBags;
    }

    public void addBlockBag(NearbyChestBlockBag bag) {
        blockBags.add(bag);
    }

    public boolean isEmpty() {
        return blockBags.isEmpty();
    }

    public static MultiNearbyChestBlockBag fromSourcePositionNearby(Location<World> source) {
        return fromSourcePositionNearby(source, Collections.EMPTY_SET);
    }

    private static Set<BlockType> CHEST_BLOCK_TYPES = null;

    public static Set<BlockType> getChestBlockTypes() {
        if (CHEST_BLOCK_TYPES == null) {
            CHEST_BLOCK_TYPES = ImmutableSet.of(BlockTypes.CHEST, BlockTypes.TRAPPED_CHEST);
        }
        return CHEST_BLOCK_TYPES;
    }

    public static final int DEFAULT_RADIUS = 5;

    public static MultiNearbyChestBlockBag fromSourceChestsNearby(Location<World> source) {
        return fromSourcePositionNearby(source, getChestBlockTypes());
    }

    public static MultiNearbyChestBlockBag fromSourcePositionNearby(Location<World> source, Set<BlockType> blockTypes) {
        return fromSourcePositionNearby(source, DEFAULT_RADIUS, blockTypes);
    }

    /**
     *
     * @param source
     * @return MultiNearbyChestBlockBag with all chests in range
     */
    public static MultiNearbyChestBlockBag fromSourcePositionNearby(Location<World> source, int radius, Set<BlockType> blockTypes) {
        MultiNearbyChestBlockBag multiBlockBag = new MultiNearbyChestBlockBag();

        List<Tuple<NearbyChestBlockBag, Double>> bagsWithDistance = new ArrayList<>();

        Set<Location<World>> skipLocations = new HashSet<>();
        for (TileEntity tileEntity : LocationUtil.getNearbyTileEntities(source, radius)) {
            Location<World> loc = tileEntity.getLocation();
            if (skipLocations.contains(loc)) {
                continue;
            }
            BlockType blockType = loc.getBlockType();
            if (!blockTypes.isEmpty() && !blockTypes.contains(blockType)) {
                continue;
            }
            if (!(tileEntity instanceof Carrier)) {
                continue;
            }
            Inventory inv;
            if (tileEntity instanceof Chest) {
                Chest chest = (Chest)tileEntity;
                inv = chest.getDoubleChestInventory().orElse(chest.getInventory());
                chest.getConnectedChests().forEach(connChest -> skipLocations.add(connChest.getLocation()));
            }
            else {
                inv = ((Carrier) tileEntity).getInventory();
            }
            bagsWithDistance.add(Tuple.of(new NearbyChestBlockBag(inv, loc),
                    loc.getPosition().distanceSquared(source.getPosition())));
        }
        bagsWithDistance.sort(Comparator.comparingDouble(Tuple::getSecond));

        for (Tuple<NearbyChestBlockBag, Double> tuple : bagsWithDistance) {
            multiBlockBag.addBlockBag(tuple.getFirst());
        }

        return multiBlockBag.isEmpty() ? null : multiBlockBag;
    }

    /**
     * Do not use. Prefer {@link #has(Predicate, int)}
     * @param itemStacks The requested items
     */
    @Override
    public boolean has(List<ItemStack> itemStacks) {
        for (NearbyChestBlockBag bag : blockBags) {
            if (bag.has(itemStacks)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean has(Predicate<ItemStack> predicate, int amountRequired) {
        int runningHas = 0;
        for (NearbyChestBlockBag bag : blockBags) {
            runningHas += bag.hasHowMany(predicate);
            if (runningHas >= amountRequired)
                return true;
        }
        return false;
    }

    @Override
    public boolean canFit(List<ItemStack> itemStacks) {
        for (NearbyChestBlockBag bag : blockBags) {
            itemStacks = bag.canFitHowMuch(itemStacks);
            if (itemStacks.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<ItemStack> add(List<ItemStack> itemStacks) {
        for (NearbyChestBlockBag bag : blockBags) {
            itemStacks = bag.add(itemStacks);
            if (itemStacks.isEmpty()) {
                return itemStacks;
            }
        }
        return itemStacks;
    }

    @Override
    public List<ItemStack> remove(List<ItemStack> itemStacks) {
        for (NearbyChestBlockBag bag : blockBags) {
            itemStacks = bag.remove(itemStacks);
            if (itemStacks.isEmpty()) {
                return itemStacks;
            }
        }
        return itemStacks;
    }

    @Override
    public int remove(Predicate<ItemStack> predicate, int amtToRemove) {
        for (NearbyChestBlockBag bag : blockBags) {
            amtToRemove = bag.remove(predicate, amtToRemove);
            if (amtToRemove == 0) {
                return amtToRemove;
            }
        }
        return amtToRemove;
    }

    @Nullable
    @Override
    public ItemStack findStack(Predicate<ItemStack> predicate, int maxSize) {
        ItemStack itemStack = null;
        for (BlockBag bag : this.blockBags) {
            ItemStack stack = bag.findStack(predicate, maxSize);
            if (stack == null) {
                continue;
            }
            if (itemStack == null) {
                itemStack = stack;
                predicate = iStack -> ItemStackUtil.compareIgnoreQuantity(stack, iStack);
            }
            else {
                itemStack.setQuantity(itemStack.getQuantity() + stack.getQuantity());
            }
            maxSize -= stack.getQuantity();

            if (maxSize == 0) {
                return itemStack;
            }
        }
        return itemStack;
    }

    public Iterable<Inventory> slots() {
        return Iterables.concat(blockBags.stream().map(NearbyChestBlockBag::getInventory)
                .map(Inventory::slots)
                .collect(Collectors.toList()));
    }
}
