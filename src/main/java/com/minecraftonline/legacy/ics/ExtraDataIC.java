/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics;

import com.minecraftonline.data.ExtraICLinesData;
import com.sk89q.craftbook.sponge.IC;
import org.spongepowered.api.data.value.mutable.Value;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

import java.util.List;
import java.util.Optional;

/**
 * <p>Any class that implements this interface has the
 * possibility to have long lines that could do with
 * being able to have loads of possible data on them,
 * This interface allows for this by storing more data
 * in the nbt of a sign using written books and
 * and command.</p>
 *
 * Extra data <b>MUST</b> be checked {@link IC#create(Player, List)} to prevent
 * getting invalid data made on them.
 */
public interface ExtraDataIC extends IC {
	default Optional<Text> getExtraData() {
		return this.getBlock().get(ExtraICLinesData.class)
				.map(ExtraICLinesData::extraICLines)
				.map(Value::get);
	}

	default Optional<String> getExtraDataPlain() {
		return getExtraData().map(Text::toPlain);
	}

	/**
	 * Gets a message to inform the player how
	 * this extra information will be used, in
	 * order to assist them in creating a valid
	 * IC.
	 *
	 * @return Text message to send to the player
	 */
	Text usageMessage();
}
