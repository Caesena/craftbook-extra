/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.sensor;

import com.minecraftonline.legacy.ics.HybridIC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.SelfTriggeringIC;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

/**
 * WATER SENSOR
 * Model ID: MC1260
 *
 * @author sk89q (Legacy)
 * @author Brendan (doublehelix457)
 */
public class SenseWater extends AbstractIC implements SelfTriggeringIC {

    public SenseWater(ICFactory<SenseWater> icFactory, Location<World> block){super(icFactory,block);}

    int y;

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException{
        super.create(player, lines);
        String yOffsetLine = lines.get(2).toPlain();

        try {
            if (yOffsetLine.length() > 0) {
                Integer.parseInt(yOffsetLine);
            }
        } catch (NumberFormatException e) {
            throw new InvalidICException("The third line must be a number or be blank.");
        }
    }

    @Override
    public void load(){
        super.load();
        y = getBackBlock().getBlockY();
        try{
            String yOffsetLine = getLine(2);

            if (yOffsetLine.length() > 0) {
                y += Integer.parseInt(yOffsetLine);
            } else {
                y -= 1;
            }
        } catch (NumberFormatException e) {
            y -= 1;
        }
        y= Math.min(Math.max(0,y),255);
    }

    @Override
    public void onTrigger() {
        if(!getPinSet().isAnyTriggered(this)) return;

        BlockType block = getBlock().getExtent().getBlock(getBackBlock().getBlockX(), y, getBackBlock().getBlockZ()).getType();
        getPinSet().setOutput(0,block == BlockTypes.WATER || block == BlockTypes.FLOWING_WATER,this);
    }

    @Override
    public void think() {
        BlockType block = getBlock().getExtent().getBlock(getBackBlock().getBlockX(), y, getBackBlock().getBlockZ()).getType();
        getPinSet().setOutput(0,block == BlockTypes.WATER || block == BlockTypes.FLOWING_WATER,this);
    }

    public static class Factory implements ICFactory<SenseWater>, HybridIC {

        @Override
        public SenseWater createInstance(Location<World> location) { return new SenseWater(this,location);}

        @Override
        public String[][] getPinHelp() {
            return new String[][] {
                    new String[] {
                          "Input"
                    },
                    new String[]{
                           "Output High if Water is present."
                    }
            };
        }

        @Override
        public String stVariant() { return "MC0260"; }
    }
}
