/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.miscellaneous;

import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

/**
 * SERVER LOG
 * ModelID: MCX515
 *
 * @author Brendan (doublehelix457)
 * @author sk89q (Legacy)
 */
public class ServerLog extends AbstractIC {

    public ServerLog(ICFactory<ServerLog> icFactory, Location<World> block){super(icFactory, block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if(lines.get(2).toPlain().isEmpty() && lines.get(3).toPlain().isEmpty()){
            throw new InvalidICException("A message on the 3rd or 4th line is required.");
        }
    }

    @Override
    public void onTrigger() {
        if(!getPinSet().isAnyTriggered(this)){
            getPinSet().setOutput(0,false,this);
            return;
        }

        CraftBookPlugin.spongeInst().getLogger().info("[CB!] " + getLine(2) + getLine(3));
        getPinSet().setOutput(0,true,this);
    }

    public static class Factory implements ICFactory<ServerLog>, RestrictedIC {
        @Override
        public ServerLog createInstance(Location<World> location) { return new ServerLog(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                          "Input"
                    },
                    new String[]{
                            "Send server log message."
                    }
            };
        }
    }
}
