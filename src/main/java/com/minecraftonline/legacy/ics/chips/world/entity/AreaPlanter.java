/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicate;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicateParsers;
import com.minecraftonline.util.parsing.lineparser.itempredicate.ItemPredicateParserFactory;
import com.minecraftonline.util.parsing.lineparser.itempredicate.ItemPredicateRestrictions;
import com.minecraftonline.util.parsing.lineparser.range.RangeLimitations;
import com.minecraftonline.util.parsing.lineparser.range.RangeLineParserFactory;
import com.minecraftonline.util.parsing.rangeparser.RangeParsers;
import com.minecraftonline.util.parsing.rangeparser.RangeUtil;
import com.minecraftonline.util.parsing.rangeparser.RelativeRange;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import net.minecraft.entity.item.EntityItem;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.Iterator;

/**
 * MCX215
 * Area Planter
 * Has length/width/yOffset, works similarly to {@link Planter}
 * Format
 * 3: itemid[@damage]
 * 4: w:l:yOffset
 * @author tyhdefu
 */

public class AreaPlanter extends AbstractStIC implements AreaIC {
    // How far to get items added onto range
    private static final int pickupFromFurthest = 3;

    private ItemPredicate itemPredicate;

    private RelativeRange range;
    private Vector3d min;
    private Vector3d max;

    private static final ItemPredicateParserFactory ITEM_PREDICATE_PARSER_FACTORY = ItemPredicateParserFactory.builder()
            .parser(ItemPredicateParsers.ITEM_TYPE_OPTIONAL_AT_SYMBOL_DAMAGE)
            .restriction(ItemPredicateRestrictions.PLANTABLES)
            .build();

    private static final RangeLineParserFactory RANGE_LINE_PARSER_FACTORY = RangeLineParserFactory.builder()
            .parser(RangeParsers.WIDTH_LENGTH_AND_DEFAULT_1_Y_OFFSET)
            .limitation(RangeLimitations.maxWidthOrLength(() -> (double) ICSocket.maxWidthAreaPlanter.getValue()))
            .build();

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, ITEM_PREDICATE_PARSER_FACTORY.createBinder(this, itemPredicate -> this.itemPredicate = itemPredicate))
                .lineParser(3, RANGE_LINE_PARSER_FACTORY.createBinder(this, range -> {
                    this.range = range;
                    Direction facing = getBlock().require(Keys.DIRECTION).getOpposite();
                    Vector3d pos = getBlockCentre().add(facing.asOffset().mul(0.5)).add(0, -.5, 0).getPosition();

                    Vector3d start = range.getStart(pos, facing).floor();
                    Vector3d end = range.getEnd(pos, facing).floor();

                    this.min = start.min(end);
                    this.max = start.max(end).sub(1, 1, 1);
                }))
                .build();
    }

    public AreaPlanter(ICFactory<AreaPlanter> icFactory, Location<World> block) {
        super(icFactory,block);
    }

    @Override
    public void onTrigger() {
        // if self triggering, only activate when thinking
        if (thinking != isSt) {
            return;
        }
        if (getPinSet().isAnyTriggered(this) == isSt) {
            return;
        }
        Collection<EntityItem> items = getNearbyItems();

        if (items == null)
            return;

        boolean didPlant = false;
        Iterator<Location<World>> iter = RangeUtil.iterateBlocks(getBlock().getExtent(), min.toInt(), max.toInt());

        Item curItem = null;
        ItemStack curStack = null;

        while (iter.hasNext()) {
            Location<World> loc = iter.next();
            if (!loc.getBlockType().equals(BlockTypes.AIR))
                continue;
            Location<World> blockLoc = loc.add(0,-1,0);

            if (curItem == null || curItem.isRemoved()) {
                Iterator<EntityItem> itemIter = items.iterator();
                if (!itemIter.hasNext()) {
                    return;
                }
                curItem = (Item) itemIter.next();
                curStack = curItem.require(Keys.REPRESENTED_ITEM).createStack();

                itemIter.remove();
            }

            ItemType itemType = curStack.getType();
            int dmg = IDUtil.getDamagefromItem(curItem);

            if (!itemType.equals(ItemTypes.DYE) && !Planter.itemPlantableOnBlock(itemType, dmg, blockLoc.getBlock())) {
                continue;
            }

            if (Planter.tryToPlant(loc, curStack.getType(), IDUtil.getDamageFromItemStack(curStack), curItem)) {
                didPlant = true;
                if (curStack.getQuantity() > 1) {
                    curStack.setQuantity(curStack.getQuantity() - 1);
                }
            }
        }

        getPinSet().setOutput(0, didPlant, this);
    }

    private Collection<EntityItem> getNearbyItems() {
        Location<World> loc = getSign().getLocation().add(0, range.getXyzOffset().getY(), 0);

        double dist = Math.max(range.getWidth(), range.getLength()) + pickupFromFurthest;
        return LocationUtil.getNearbyItems(loc, dist, itemPredicate);
    }

    @Nullable
    @Override
    public CUIRegion getArea() {
        return WorldEditUtil.toCuboidSelection(getBlock().getExtent(), min, max);
    }

    public static class Factory implements ICFactory<AreaPlanter>, HybridIC {
        @Override
        public AreaPlanter createInstance(Location<World> location) {
            return new AreaPlanter(this,location);
        }

        @Override
        public String[][] getPinHelp() {
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Plants plants."
                    }
            };
        }

        @Override
        public String stVariant() {
            return "MCZ215";
        }
    }
}
