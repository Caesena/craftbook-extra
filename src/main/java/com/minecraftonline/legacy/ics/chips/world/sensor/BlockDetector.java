/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.sensor;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.parsing.block.predicate.BlockPredicate;
import com.minecraftonline.util.parsing.block.predicate.BlockPredicateParsers;
import com.minecraftonline.util.parsing.lineparser.blockpredicate.BlockPredicateParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.SimpleDualParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.enums.EnumParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerRestrictions;
import com.minecraftonline.util.parsing.lineparser.generic.integer.OptionalIntegerParser;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractStIC;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import net.minecraft.block.state.IBlockState;
import net.minecraft.world.chunk.BlockStateContainer;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.Optional;

/**
 * BLOCK DETECTOR
 * Model ID: MCX205
 *
 * @author Brendan (doublehelix457)
 * @author sk89q (Legacy)
 **/
public class BlockDetector extends AbstractStIC implements AreaIC {

    private static final BlockPredicateParserFactory BLOCK_STATE_PARSER_FACTORY = BlockPredicateParserFactory.builder()
            .parser(BlockPredicateParsers.OPTIONAL_BLOCK_TYPE_COLON_OPTIONAL_DAMAGE)
            .withDefault(BlockPredicate.builder().blockType(BlockTypes.AIR).negated().build())
            .build();


    public BlockDetector(ICFactory<BlockDetector> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    private BlockPredicate blockPredicate;

    private static final SimpleDualParserFactory<Direction, Integer> BLOCK_LOCATION_PARSER_FACTORY = new SimpleDualParserFactory<Direction, Integer>(
            EnumParserFactory.builderFor(Direction.class)
                    .whitelist(Direction.UP, Direction.DOWN)
                    .caseInsensitive()
                    .build(),

            IntegerParserFactory.builder()
                .parser(OptionalIntegerParser.range())
                .withDefault(CraftBookPlugin.MINECRAFT_WORLD_HEIGHT_MAX)
                .restriction(IntegerRestrictions.atLeast(1))
                .restriction(IntegerRestrictions.atMost(CraftBookPlugin.MINECRAFT_WORLD_HEIGHT_MAX))
                .build(),
            ":",
            true
    );

    private Direction direction;
    private int startY;
    private int endY;
    private int lastFoundY = -1;
    private int x;
    private int z;

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, BLOCK_STATE_PARSER_FACTORY.createBinder(this, p -> this.blockPredicate = p))
                .lineParser(3, BLOCK_LOCATION_PARSER_FACTORY.createBinder(this, (dir, dist) -> {
                    this.direction = dir;
                    if (dir == Direction.UP) {
                        this.endY = Math.min(getBackBlock().getBlockY() + dist, CraftBookPlugin.MINECRAFT_WORLD_HEIGHT_MAX);
                        this.startY = getBlock().getBlockY() + 1;
                    }
                    else {
                        this.endY = Math.max(getBackBlock().getBlockY() - dist, CraftBookPlugin.MINECRAFT_WORLD_HEIGHT_MIN);
                        this.startY = getBlock().getBlockY() - 1;
                    }
                }))
                .build();
    }

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
    }

    @Override
    public void load() {
        super.load();
        Location<World> loc = getBackBlock();
        this.x = loc.getBlockX();
        this.z = loc.getBlockZ();
    }

    @Override
    public void onTrigger() {
        if (!isSt && !getPinSet().isAnyTriggered(this)) return;

        this.lastFoundY = searchForBlock();

        getPinSet().setOutput(0, this.lastFoundY != -1, this); // TODO: Refactor from -1, will cause problems once they expand downwards.
    }

    private static final int Y_CHANGE = 1 << 8;
    private static final int WITHIN_CHUNK_MASK = 15;

    protected int searchForBlock() {

        World world = getBlock().getExtent();

        if (lastFoundY != -1) {
            // Lets see if where we last found the block has changed.
            // The one that worked recently is the one most likely to work next.
            // If it doesn't, continue normal flow
            Location<World> quickLoc = world.getLocation(x, lastFoundY, z);
            if (blockPredicate.test(quickLoc.getBlock())) {
                return lastFoundY;
            }
        }

        Location<World> loc = world.getLocation(x, this.startY, z);

        Optional<Chunk> chunk = loc.getExtent().getChunk(loc.getChunkPosition());
        if (!chunk.isPresent()) {
            return -1;
        }
        net.minecraft.world.chunk.Chunk mcChunk = (net.minecraft.world.chunk.Chunk) chunk.get();

        ExtendedBlockStorage[] blockStorages = mcChunk.getBlockStorageArray();

        int yIndexStart = this.startY >> 4;
        int withinChunkY = this.startY & WITHIN_CHUNK_MASK;

        final int yIndexEnd = this.endY >> 4;
        final int yInChunkEnd = this.endY & WITHIN_CHUNK_MASK;

        final int withinChunkZ = this.z & WITHIN_CHUNK_MASK;
        final int withinChunkX = this.x & WITHIN_CHUNK_MASK;

        int XZBlockIndex = withinChunkZ << 4 | withinChunkX;
        int blockIndex = withinChunkY << 8 | XZBlockIndex;

        boolean isAirValid = blockPredicate.test(BlockTypes.AIR.getDefaultState());

        if (direction == Direction.UP) {
            for (int yIndex = yIndexStart; yIndex <= yIndexEnd; yIndex++) {
                ExtendedBlockStorage blockStorage = blockStorages[yIndex];
                if (blockStorage == null) {
                    if (isAirValid) {
                        return yIndex << 4;
                    }
                    continue;
                }
                BlockStateContainer blockStateContainer = blockStorage.getData();
                for (int y = withinChunkY; y < LocationUtil.CHUNK_SIZE; y++) {
                    IBlockState iBlockState = blockStateContainer.get(blockIndex);

                    if (blockPredicate.test((BlockState) iBlockState)) {
                        return (yIndex << 4) | y;
                    }

                    blockIndex += Y_CHANGE;

                    if (yIndex == yIndexEnd && y == yInChunkEnd) {
                        return -1;
                    }
                }
                // Take it back now yall.
                blockIndex = XZBlockIndex;
                withinChunkY = 0;
            }
        }
        else {
            for (int yIndex = yIndexStart; yIndex >= yIndexEnd; yIndex--) {
                ExtendedBlockStorage blockStorage = blockStorages[yIndex];
                if (blockStorage == null) {
                    if (isAirValid) {
                        return yIndex << 4 | 15;
                    }
                    continue;
                }
                BlockStateContainer blockStateContainer = blockStorage.getData();
                for (int y = withinChunkY; y >= 0; y--) {
                    IBlockState iBlockState = blockStateContainer.get(blockIndex);

                    if (blockPredicate.test((BlockState) iBlockState)) {
                        return (yIndex << 4) | y;
                    }

                    blockIndex -= Y_CHANGE;

                    if (yIndex == yIndexEnd && y == yInChunkEnd) {
                        return -1;
                    }
                }
                // Take it back now yall.
                withinChunkY = 15;
                blockIndex = (15 << 8) | XZBlockIndex;
            }
        }


        return -1;
    }

    @Nullable
    @Override
    public CUIRegion getArea() {
        Vector3d backBlock = this.getBackBlock().getPosition().mul(1, 0, 1);
        return WorldEditUtil.toCuboidSelection(
                this.block.getExtent(),
                backBlock.add(0, this.startY, 0),
                backBlock.add(0, this.endY, 0)
        );
    }

    public static class Factory implements ICFactory<BlockDetector>, HybridIC {

        @Override
        public BlockDetector createInstance(Location<World> location) {
            return new BlockDetector(this, location);
        }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Input"
                    },
                    new String[]{
                            "Outputs if block is present."
                    }
            };
        }

        @Override
        public String stVariant() {
            return "MCZ205";
        }
    }
}
