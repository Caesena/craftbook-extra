/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.entity.PlayerParsingUtil;
import com.minecraftonline.util.parsing.entity.PlayerMatcherParser;
import com.minecraftonline.util.parsing.entity.PlayerMatcherParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerRestrictions;
import com.minecraftonline.util.parsing.lineparser.generic.integer.OptionalIntegerParser;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Humanoid;
import org.spongepowered.api.event.cause.entity.damage.DamageTypes;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSource;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Hit Player Above
 * ModelID: MCX131/MCU131 (ST)
 *
 * Damages player above the ic, in the first empty space it can find.
 *
 * @author tyhdefu
 */

public class HitPlayerAbove extends AbstractStIC {

    private PlayerParsingUtil.HumanoidMatcher matcher;
    private int damage;

    private static final PlayerMatcherParserFactory MATCHER_PARSER = PlayerMatcherParserFactory.builder()
            .parser(new PlayerMatcherParser())
            .withDefault(new PlayerParsingUtil.AnyHumanoid())
            .build();

    private static final IntegerParserFactory DAMAGE_PARSER = IntegerParserFactory.builder()
            .parser(OptionalIntegerParser.range())
            .withDefault(1)
            .restriction(IntegerRestrictions.atLeast(1))
            .build();

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, MATCHER_PARSER.createBinder(this, matcher -> this.matcher = matcher))
                .lineParser(3, DAMAGE_PARSER.createBinder(this, damage -> this.damage = damage))
                .build();
    }

    public HitPlayerAbove(ICFactory<HitPlayerAbove> icFactory, Location<World> block) {super(icFactory, block);}

    @Override
    public void onTrigger() {
        // thinking will always be false when not ST
        if (isSt && !thinking)
            return;
        if (!getPinSet().isAnyTriggered(this))
            return;
        boolean output = false;
        Location<World> loc = getBackBlock().add(0,1,0);
        for (int i = 0; (loc.getBlockY() + i) <= CraftBookPlugin.MINECRAFT_WORLD_HEIGHT_MAX; i++) {
            if (loc.add(0, i, 0).getBlock().getType().equals(BlockTypes.AIR)) {
                loc = loc.add(0.5 ,i,0.5);
                // Found possible spot
                Collection<Entity> nearbyEntities = loc.getExtent().getNearbyEntities(loc.getPosition(), 0.5);
                if (nearbyEntities.size() == 0)
                    return;

                List<Entity> filteredlist = nearbyEntities.stream()
                        .filter(entity -> entity instanceof Humanoid)
                        .filter(this.matcher)
                        .collect(Collectors.toList());

                if (filteredlist.size() != 0)
                    output = true;
                for (Entity entity : filteredlist) {
                    entity.damage(this.damage, DamageSource.builder().type(DamageTypes.CUSTOM).bypassesArmor().build());
                }
                break;
            }
        }
        getPinSet().setOutput(0, output, this);
    }

    @Override
    public void load() {
        super.load();
        if (getLine(1).contains("[" + "MCU131" + "]"))
            isSt = true;
        if (getLine(3).length() == 0) {
            setLine(3, Text.of(1));
        }
    }

    public static class Factory implements ICFactory<HitPlayerAbove>, HybridIC, RestrictedIC {
        @Override
        public String stVariant() {
            return "MCU131";
        }

        @Override
        public HitPlayerAbove createInstance(Location<World> location){return new HitPlayerAbove(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Outputs if a player was damaged"
                    }
            };
        }
    }
}