/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.logic;

import com.sk89q.craftbook.sponge.ICFactory;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * MULTIPLEXER
 * ModelID: MC3040
 *
 * @author Lymia (Legacy)
 * @author Brendan (doublehelix457)
 */
public class Multiplexer extends AbstractIC {

    public Multiplexer(ICFactory<Multiplexer> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void onTrigger() {
        if(getPinSet().isTriggered(0,this)) return;
        boolean swapper = getPinSet().getInput(0,this);
        getPinSet().setOutput(0,swapper ? getPinSet().getInput(1,this) : getPinSet().getInput(2,this),this);
    }

    public static class Factory implements ICFactory<Multiplexer>, RestrictedIC {

        @Override
        public Multiplexer createInstance(Location<World> location) { return new Multiplexer(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                        "Input",
                            "Input",
                            "Input"
                    },
                    new String[]{
                        "Output Input 1 when Input 0  is low, Output Input 2 when high. "
                    }
            };
        }
    }
}
