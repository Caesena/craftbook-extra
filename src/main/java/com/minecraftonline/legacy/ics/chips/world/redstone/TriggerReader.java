/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.redstone;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.WorldEditUtil;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractStIC;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

/**
 * TRIGGER READER
 * ModelID: MCX295
 *
 * @author Brendan  (doublehelix457)
 * @author drathus (Legacy)
 */
public class TriggerReader extends AbstractStIC implements AreaIC {
    public TriggerReader(ICFactory<TriggerReader> icFactory, Location<World> block){super(icFactory,block);}

    private int x;
    private int y;
    private int z;
    private String[] targetRelative;
    private boolean invert = false;

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        String[] targetRelative = lines.get(2).toPlain().split(":");
        int x;
        int y;
        int z;

        if (targetRelative.length != 3) {
            throw new InvalidICException("Invalid relative location, use XXX:YYY:ZZZ");
        } else {
            if (targetRelative[0].startsWith("!")) {
                targetRelative[0] = targetRelative[0].substring(1);
            }

            try {
                x = Integer.parseInt(targetRelative[0]);
                y = Integer.parseInt(targetRelative[1]);
                z = Integer.parseInt(targetRelative[2]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                throw new InvalidICException("Invalid value found for X:Y:Z offset" + targetRelative[0] + targetRelative[1] + targetRelative[2]);
            }

            if (x < -999 || x > 999) {
                throw new InvalidICException("X range must be +/- 999");
            }
            if (z < -999 || z > 999) {
                throw new InvalidICException("Z range must be +/- 999");
            }
            if (y < -255 || y > 255) {
                throw new InvalidICException("Y range must be +/- 255");
            }
        }
    }

    @Override
    public void load(){
        super.load();
        targetRelative = getSign().lines().get(2).toPlain().split(":");

        if (targetRelative[0].startsWith("!")) {
            invert = true;

            targetRelative[0] = targetRelative[0].substring(1);
        }

        try {
            x = Integer.parseInt(targetRelative[0]);
            y = Integer.parseInt(targetRelative[1]);
            z = Integer.parseInt(targetRelative[2]);
        } catch (NumberFormatException e) {
            //eat it
        }
    }

    @Override
    public void onTrigger(){

        if(getPinSet().isAnyTriggered(this)){

            if (targetRelative.length < 3) return;
            mirrorRSState(x, y, z);

        }
    }

    @Override
    public void think(){
        thinking = true;
        if (targetRelative.length < 3) return;
        mirrorRSState(x, y, z);
        thinking = false;
    }

    protected void mirrorRSState(int x, int y, int z) {

        Vector3i mVec = new Vector3i(getBlock().getX() + x,getBlock().getY() + y,getBlock().getZ() + z);
        if (getBlock().getExtent().getChunkAtBlock(mVec).isPresent()) {
            boolean chipRS = BlockUtil.isDirectlyPowered(getBlock().getExtent().getLocation(mVec));

            if (invert) {
                getPinSet().setOutput(0,!chipRS,this);
            } else {
                getPinSet().setOutput(0,chipRS,this);
            }
        }
    }

    @Override
    public CUIRegion getArea() {
        Vector3d target = getBlock().getPosition().add(x, y, z);
        return WorldEditUtil.toCuboidSelection(getBlock().getExtent(), target, target);
    }

    public static class Factory implements ICFactory<TriggerReader>, HybridIC {

        @Override
        public TriggerReader createInstance(Location<World> location){return new TriggerReader(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Outputs if a redstone signal is present at a location."
                    }
            };
        }

        @Override
        public String stVariant() {
            return "MCZ295";
        }


    }
}
