/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.weather;

import com.minecraftonline.legacy.ics.HybridIC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractStIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

/*
* Holy Smite
* MCX256
* @author tyhdefu
 */


public class HolySmite extends AbstractStIC {

    private int range = 5;

    public HolySmite(ICFactory<HolySmite> icFactory, Location<World> block) {super(icFactory, block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if (lines.get(2).toPlain().length() != 0)
            throw new InvalidICException("Line 3 must be blank");
        if (lines.get(3).toPlain().length() != 0) {
            try {
                Integer.parseInt(lines.get(3).toPlain());
            } catch (NumberFormatException e) {throw new InvalidICException("Line 4 must be a number for range"); }
        }
    }

    @Override
    public void load() {
        super.load();
        if (getLine(3).length() == 0) {
            setLine(3, Text.of("5"));
        }
        else {
            range = Integer.parseInt(getLine(3));
        }
    }

    @Override
    public void onTrigger() {
        if (!getPinSet().isAnyTriggered(this))
            return;
        Collection<Entity> nearbyEntities = getBlock().getExtent().getNearbyEntities(getBlock().getPosition().add(0.5,0,0.5), range);
        Stream<Entity> entityStream = nearbyEntities.stream();
        entityStream.forEach(entity -> {
            Entity lightning = entity.getLocation().createEntity(EntityTypes.LIGHTNING);
            entity.getLocation().spawnEntity(lightning);
        });
    }

    public static class Factory implements ICFactory<HolySmite>, HybridIC, RestrictedIC {

        @Override
        public HolySmite createInstance(Location<World> location){return new HolySmite(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "None"
                    }
            };
        }

        @Override
        public String stVariant() {
            return "MCZ256";
        }
    }
}
