/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.legacy.ics.ProjectileLaunchingIC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

/**
 * EGG SHOOTER
 * ModelID: MCX244
 *
 * @author Brendan (doublehelix457)
 * @author sk89q (Legacy)
 */
public class EggShooter extends ProjectileLaunchingIC {

    float speed = 0.5F;
    float spread = 12F;
    float vertVel = 0F;
    String speedSpreadLine;
    String vertVelLine;
    Vector3d backDir;
    Vector3d firePos;

    public EggShooter(ICFactory<EggShooter> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        String speedSpreadLine = lines.get(2).toPlain();
        String vertVelLine = lines.get(3).toPlain();

        try {
            if (speedSpreadLine.length() > 0) {
                String[] parts = speedSpreadLine.split(":");

                float speed = Float.parseFloat(parts[0]);
                if (speed < 0.3 || speed > 2) {
                    throw new InvalidICException("Speed must be >= 0.3 and <= 2.");
                }

                if (parts.length > 1) {
                    float spread = Float.parseFloat(parts[1]);
                    if (spread < 0 || spread > 50) {
                        throw new InvalidICException("Spread must be >= 0 and <= 50.");
                    }
                }
            }

            if (vertVelLine.length() > 0) {
                float speed = Float.parseFloat(vertVelLine);
                if (speed < -1 || speed > 1) {
                    throw new InvalidICException("Vertical velocity must be between or equal to -1 and 1.");
                }
            }
        } catch (NumberFormatException e) {
            throw new InvalidICException("Speed is the third line and spread is the fourth line.");
        }
    }

    @Override
    public void load(){
        super.load();
        speedSpreadLine = getLine(2);
        vertVelLine = getLine(3);

        try {
            if (speedSpreadLine.length() > 0) {
                String[] parts = speedSpreadLine.split(":");

                speed = Float.parseFloat(parts[0]);

                if (parts.length > 1) {
                    spread = Float.parseFloat(parts[1]);
                }
            }

            if (vertVelLine.length() > 0) {
                vertVel = Float.parseFloat(vertVelLine);
            }
        } catch (NumberFormatException e) {
        }

        backDir = getBackBlockCentre().getPosition().sub(getBlockCentre().getPosition());
        firePos = getBackBlockCentre().getPosition().add(backDir.mul(0.55)); // 0.05 more so they don't hit the block they shoot from
    }

    @Override
    public void onTrigger() {
        if(getPinSet().isAnyTriggered(this)){
            shoot(speed, spread, vertVel, EntityTypes.EGG, new Location<>(getBackBlockCentre().getExtent(), firePos));
        }
    }

    public static class Factory implements ICFactory<EggShooter>, RestrictedIC {
        @Override
        public EggShooter createInstance(Location<World> location) { return new EggShooter(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                           "Input"
                    },
                    new String[]{
                         "Fire a single egg."
                    }
            };
        }
    }
}
