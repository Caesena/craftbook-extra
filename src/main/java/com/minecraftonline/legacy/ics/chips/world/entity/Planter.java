/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicate;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicateParsers;
import com.minecraftonline.util.parsing.lineparser.itempredicate.ItemPredicateParserFactory;
import com.minecraftonline.util.parsing.lineparser.itempredicate.ItemPredicateRestrictions;
import com.minecraftonline.util.parsing.lineparser.range.RangeLineParserFactory;
import com.minecraftonline.util.parsing.rangeparser.RangeParsers;
import com.minecraftonline.util.parsing.rangeparser.RelativeRange;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.sk89q.craftbook.sponge.mechanics.ics.SelfTriggeringIC;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import net.minecraft.entity.item.EntityItem;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.TreeType;
import org.spongepowered.api.data.type.TreeTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * PLANTER
 * Model ID: MCX216
 *
 * @author Brendan (doublehelix457)
 * @author Drathus (Legacy)
 *
 * //nosefish, if you're reading this and youve contributed, (im sure you have)
 * //Im sorry for omitting your name here
 * Sapling planter
 *
 * Hybrid variant of MCX206 and MCX203 chest collector
 *
 * When there is a sapling or seed item drop in range it will auto plant it above the IC.
 *
 *
 **/
public class Planter extends AbstractStIC implements SelfTriggeringIC, AreaIC {

    public Planter(ICFactory<Planter> icFactory, Location<World> block) {
        super(icFactory,block);
    }

    private ItemPredicate itemPredicate;

    public static final double radius = 5;
    private int offsetY = 1;

    // Purposely doesn't contain cocoa beans
    public static Map<ItemType, BlockType> itemBlockMap = new HashMap<>();
    static {
        itemBlockMap.put(ItemTypes.WHEAT_SEEDS, BlockTypes.WHEAT);
        itemBlockMap.put(ItemTypes.CARROT, BlockTypes.CARROTS);
        itemBlockMap.put(ItemTypes.BEETROOT_SEEDS, BlockTypes.BEETROOTS);
        itemBlockMap.put(ItemTypes.POTATO, BlockTypes.POTATOES);
        itemBlockMap.put(ItemTypes.NETHER_WART, BlockTypes.NETHER_WART);
        itemBlockMap.put(ItemTypes.SAPLING, BlockTypes.SAPLING);
    }

    private static final ItemPredicateParserFactory ITEM_PREDICATE_PARSER_FACTORY = ItemPredicateParserFactory.builder()
            .parser(ItemPredicateParsers.ITEM_TYPE_OPTIONAL_AT_SYMBOL_DAMAGE)
            .restriction(ItemPredicateRestrictions.PLANTABLES)
            .build();


    private static final RangeLineParserFactory RANGE_LINE_PARSER_FACTORY = RangeLineParserFactory.builder()
            .parser(RangeParsers.OPTIONAL_Y_OFFSET)
            .withDefault(RelativeRange.builder().yOffset(1).build())
            .build();

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, ITEM_PREDICATE_PARSER_FACTORY.createBinder(this, itemPredicate -> this.itemPredicate = itemPredicate))
                .lineParser(3, RANGE_LINE_PARSER_FACTORY.createBinder(this, range -> this.offsetY = range.getXyzOffset().getFloorY()))
                .build();
    }

    @Override
    public void load(){
        super.load();

        // TODO: check if needed
        if(offsetY < 1) offsetY = 1;
    }

    @Override
    public void onTrigger(){
        if(isSt ? !thinking : !getPinSet().isAnyTriggered(this)) return;

        CraftBookPlugin.spongeInst().getScheduler().scheduleAsyncBatch(() -> {
            Vector3i target = getTargetBlock();

            BlockState blockState = getBlock().getExtent().getBlock(target.add(0,-1,0));

            Collection<EntityItem> finalStacks = getNearbyItemStacks(new Location<>(getBlock().getExtent(), target.add(0.5,0,0.5)));

            CraftBookPlugin.spongeInst().getScheduler().scheduleSync(() -> {
                if (!getBlock().getExtent().getChunk(getBlock().getChunkPosition()).isPresent()) {
                    return;
                }
                boolean itemPlanted = false;
                for (EntityItem e : finalStacks){
                    if (e.isDead) {
                        continue;
                    }
                    Item item = (Item) e;
                    int damage = IDUtil.getDamagefromItem(item);
                    if (!isPlantable(item.getItemType(), damage)) {
                        continue;
                    }
                    if (!(item.getItemType() == ItemTypes.DYE && damage == 3) && !itemPlantableOnBlock(item.getItemType(), damage, blockState)) {
                        continue;
                    }

                    itemPlanted = tryToPlant(new Location<>(getBlock().getExtent(), target), item.getItemType(), damage, item);
                    break;
                }

                getPinSet().setOutput(0, itemPlanted, this);
            });
        });
    }

    /**
     * Attempt to plant a plant Block at given block location.
     * @param block
     * @param plantable
     * @return if planted anything
     */
    public static boolean tryToPlant(Location<World> block, ItemType type, int dmg, Item plantable) {
        if (block.getBlockType() != BlockTypes.AIR) {
            return false;
        }
        if (Planter.itemBlockMap.containsKey(type)) {
            org.spongepowered.api.block.BlockType plantblock = Planter.itemBlockMap.get(type);
            block.setBlock(type.equals(ItemTypes.SAPLING) ? plantblock.getDefaultState().with(Keys.TREE_TYPE, Planter.SaplingTypes[dmg]).get() : plantblock.getDefaultState());
            Planter.removePlantable(plantable);
            return true;
        }
        else if (type.equals(ItemTypes.DYE)) {
            for (Direction dir : BlockUtil.getDirectHorizontalFaces()) {
                if (block.add(dir.asBlockOffset()).getBlock().getType().equals(BlockTypes.LOG)
                        && block.add(dir.asBlockOffset()).getBlock().get(Keys.TREE_TYPE).get().equals(TreeTypes.JUNGLE)) {
                    BlockState plantblock = BlockState.builder().blockType(BlockTypes.COCOA).build();
                    Direction cocoabeanDir = Direction.getClosestHorizontal(block.add(dir.asBlockOffset()).getBlockPosition().sub(block.getBlockPosition()).toDouble());
                    block.setBlock(plantblock.with(Keys.DIRECTION, cocoabeanDir).get());
                    Planter.removePlantable(plantable);
                    return true;
                }
            }
        }
        return false;
    }

    public static TreeType[] SaplingTypes = {
            TreeTypes.OAK,
            TreeTypes.SPRUCE,
            TreeTypes.BIRCH,
            TreeTypes.JUNGLE,
            TreeTypes.ACACIA,
            TreeTypes.DARK_OAK
    };
    /**
     * Remove a plantable Item Entity
     *
     * @param plantable Item Entity
     */
    public static void removePlantable(Item plantable){
            if(plantable.item().get().getQuantity() > 1) {
                ItemStack snap = plantable.item().get().createStack();
                snap.setQuantity(plantable.item().get().getQuantity() - 1);
                plantable.offer(Keys.REPRESENTED_ITEM, snap.createSnapshot());
            }else{
                plantable.remove();
            }
    }

    /**
     * Get ItemStacks near farmland within a specified radius.
     * @param location Location of container itself
     *
     * This method is specific to this IC.
     */
    private Collection<EntityItem> getNearbyItemStacks(Location<World> location){
        return LocationUtil.getNearbyItemsAsync(location, radius, itemPredicate);
    }

    /**
     * Check if Item is plantable using itemType
     *
     * @param itemType
     * @param damage
     * @return boolean if its plantable
     */
    public static boolean isPlantable(ItemType itemType, int damage) {
        return itemType.equals(ItemTypes.SAPLING)
                || itemType.equals(ItemTypes.WHEAT_SEEDS) || itemType.equals(ItemTypes.CARROT)
                || itemType.equals(ItemTypes.BEETROOT_SEEDS) || itemType.equals(ItemTypes.POTATO)
                || itemType.equals(ItemTypes.NETHER_WART)
                || (itemType.equals(ItemTypes.DYE) && damage == 3);
    }

    /**
     * Check if the provided item is plantable on the block.
     * @param itemType
     * @param damage
     * @param blockstate
     * @return if its plantable on that block
     *
     */
    public static boolean itemPlantableOnBlock(ItemType itemType, int damage, BlockState blockstate) {
        if (itemType.equals(ItemTypes.SAPLING) && (blockstate.getType().equals(BlockTypes.DIRT) || blockstate.getType().equals(BlockTypes.GRASS))) {
            // Saplings can go on Dirt or Grass
            return true;
        } else if ((itemType.equals(ItemTypes.WHEAT_SEEDS) || itemType.equals(ItemTypes.CARROT) || itemType.equals(ItemTypes.BEETROOT_SEEDS) || itemType.equals(ItemTypes.POTATO))
                && blockstate.getType().equals(BlockTypes.FARMLAND)) {
            // Seeds, carrots, beetroot, and potatoes can only go on farmland
            return true;
        } else if (itemType.equals(ItemTypes.NETHER_WART) && blockstate.getType().equals(BlockTypes.SOUL_SAND)) {
            // Netherwart on soulsand
            return true;
        }
        else if (itemType.equals(ItemTypes.DYE) && damage == 3 && (blockstate.getType().equals(BlockTypes.LOG) && blockstate.get(Keys.TREE_TYPE).get().equals(TreeTypes.JUNGLE))) {
            //cocoa bean on Jungle wood
            return true;
        }
        // can't plant this item on this block
        return false;
    }

    public Vector3i getTargetBlock() {
        return getBackBlock().getBlockPosition().add(0, offsetY, 0);
    }

    @Override
    public CUIRegion getArea() {
        Vector3d target = getTargetBlock().toDouble();
        return WorldEditUtil.toCuboidSelection(getBlock().getExtent(), target, target);
    }

    public static class Factory implements ICFactory<Planter>, HybridIC {

        @Override
        public Planter createInstance(Location<World> location){ return new Planter(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Plants a plant."
                    }
            };
        }

        @Override
        public String stVariant(){ return "MCZ216"; }
    }
}
