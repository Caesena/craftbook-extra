/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.sensor;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.entity.DetailedEntityType;
import com.minecraftonline.util.entity.EntityParsingUtil;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractStIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.List;

/*
* In Area
* MCX140
* Outputs if specified entity was detected in the area.
* @author tyhdefu
 */
public class InArea extends AbstractStIC {

    private DetailedEntityType BasedetailedEntityType;
    private DetailedEntityType RiderdetailedEntityType;

    private Vector3d range = new Vector3d(3,1,3);
    private Vector3d offset = new Vector3d(0,1,0);

    public InArea(ICFactory<InArea> icFactory, Location<World> block) {super(icFactory, block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);

        String[] entitytypes = lines.get(2).toPlain().split("[+]");
        if (entitytypes[0].length() != 0) {
            try {
                if (EntityParsingUtil.parseEntity(entitytypes[0]) == null)
                    throw new InvalidICException("Unknown base entity specified");
            }
            catch (DetailedEntityType.Error e) {throw new InvalidICException("Base entity: " + e.getMessage());}
            if (entitytypes.length == 2) {
                try {
                    if (EntityParsingUtil.parseEntity(entitytypes[1]) == null)
                        throw new InvalidICException("Unknown rider entity specified");
                }
                catch (DetailedEntityType.Error e) {throw new InvalidICException("Rider entity: " + e.getMessage());}
            }
        }
        else {
            throw new InvalidICException("Must specify an entity on the third line");
        }
        if (lines.get(3).toPlain().length() != 0) {
            String err = isValidDimensions(lines.get(3).toPlain(), "3");
            if (err != null) { throw new InvalidICException(err); }
        }
    }

    @Override
    public void load() {
        super.load();
        try {
            String[] entities = getLine(2).split("[+]", 2);
            this.BasedetailedEntityType = EntityParsingUtil.parseEntity(entities[0]);
            if (entities.length == 2)
                this.RiderdetailedEntityType = EntityParsingUtil.parseEntity(entities[1]);
        }
        catch (DetailedEntityType.Error e) { CraftBookPlugin.spongeInst().getLogger().error(e.getMessage() + " error in In Area ic, at: " + getBlock());}
        if (getLine(3).length() != 0) {
            String[] settings = getLine(3).split("/");
            try {
                if (settings.length > 0) {
                    String[] splitsettings = settings[0].split(":");
                    range = new Vector3d(Integer.parseInt(splitsettings[0]), Integer.parseInt(splitsettings[1]) ,Integer.parseInt(splitsettings[2]));
                }
                if (settings.length == 2) {
                    String[] splitsettings = settings[1].split(":");
                    offset = new Vector3d(Integer.parseInt(splitsettings[0]), Integer.parseInt(splitsettings[1]) ,Integer.parseInt(splitsettings[2]));
                }
            } catch (NumberFormatException e) {CraftBookPlugin.spongeInst().getLogger().error(e.getLocalizedMessage() + " error in In Area ic, at: " + getBlock());}
        }
    }

    @Override
    public void onTrigger() {
        if (!getPinSet().isAnyTriggered(this))
            return;
        Vector3d middleloc = getBlock().getPosition().add(offset);

        // highest range xyz, then refine into a box.
        Collection<Entity> foundentities = getBlock().getExtent().getNearbyEntities(middleloc, Math.max(range.getX(), Math.max(range.getY(), range.getZ())) / 2.0);

        getPinSet().setOutput(0, foundentities.stream().filter( entity -> {
            Vector3d entitypos = entity.getLocation().getPosition();
            return entitypos.getX() < (middleloc.getX() + range.getX()) && entitypos.getX() > (middleloc.getX() - range.getX())
                    && entitypos.getY() < (middleloc.getY() + range.getY()) && entitypos.getY() > (middleloc.getY() - range.getY())
                    && entitypos.getZ() < (middleloc.getZ() + range.getZ()) && entitypos.getZ() > (middleloc.getZ() - range.getZ());
        }).anyMatch(entity -> {
            if (BasedetailedEntityType.matches(entity)) {
                if (RiderdetailedEntityType != null) {
                    List<Entity> riders = entity.getPassengers();
                    return riders.size() > 0 && RiderdetailedEntityType.matches(riders.get(0));
                }
                return true;
            }
            return false;
        }), this);
    }

    public static String isValidDimensions(String settings, String lineNumber)
    {
        String[] args = settings.split("/", 2);
        String[] dim = args[0].split(":", 3);
        if(dim.length != 3)
            return lineNumber+" line format: width:height:length/x-offset:y-offset:z-offset";

        final int MinRadius = 1;
        final int MinOffset = -16;
        final int MaxRadius = 16;
        final int MaxOffset = 10;

        try
        {
            int width = Integer.parseInt(dim[0]);
            int height = Integer.parseInt(dim[1]);
            int length = Integer.parseInt(dim[2]);
            if(width < MinRadius || width > MaxRadius)
                return "width must be a number from "+ MinRadius +" to "+MaxRadius;
            if(height < MinRadius || height > MaxRadius)
                return "height must be a number from "+ MinRadius +" to "+MaxRadius;
            if(length < MinRadius || length > MaxRadius)
                return "length must be a number from "+ MinRadius +" to "+MaxRadius;

            if(args.length > 1)
            {
                String[] offsets = args[1].split(":", 3);
                if(offsets.length != 3)
                    return lineNumber+" line format: width:height:length/x-offset:y-offset:z-offset";

                int offx = Integer.parseInt(offsets[0]);
                int offy = Integer.parseInt(offsets[1]);
                int offz = Integer.parseInt(offsets[2]);

                if(offx < MinOffset || offx > MaxOffset)
                    return "offset-x must be a number from "+MinOffset+" to "+MaxOffset;
                if(offy < MinOffset || offy > MaxOffset)
                    return "offset-y must be a number from "+MinOffset+" to "+MaxOffset;
                if(offz < MinOffset || offz > MaxOffset)
                    return "offset-z must be a number from "+MinOffset+" to "+MaxOffset;
            }
        }
        catch(NumberFormatException e)
        {
            return lineNumber+" line format: width:height:length/x-offset:y-offset:z-offset";
        }

        return null;
    }

    public static class Factory implements ICFactory<InArea>, HybridIC, RestrictedIC {

        @Override
        public InArea createInstance(Location<World> location){return new InArea(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Outputs if a mob is near"
                    }
            };
        }

        @Override
        public String stVariant() {
            return "MCU140";
        }
    }
}
