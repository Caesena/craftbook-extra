/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.miscellaneous;

import com.minecraftonline.legacy.ics.HybridIC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractStIC;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/*
* Command Controlled IC
*
* ModelID: MCX120
* Changes output due to result of command
*
* See {@link https://minecraftonline.com/wiki/Craftbook#MCX120_-_Command_controlled_IC}
* Seperate to PasswordControlled
* @author tyhdefu
 */

public class CommandControlled extends AbstractStIC {

    private String bandname;

    public static HashMap<String, Boolean> ICStates = new HashMap<>();

    public CommandControlled(ICFactory<CommandControlled> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if (lines.get(3).toPlain().length() != 0)
            throw new InvalidICException("Line 4 must be blank!");
    }

    @Override
    public void load() {
        super.load();
        bandname = getLine(2);
        if (!ICStates.containsKey(bandname)) {
            ICStates.put(bandname, null);
        }
    }

    @Override
    public void unload() {

    }

    @Override
    public void onTrigger() {
        Boolean state = ICStates.get(bandname);
        if (state != null)
            getPinSet().setOutput(0, state, this);
    }

    public static class MainCommand implements CommandExecutor {

        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String bandname = args.<String>getOne(Text.of("Band")).orElseThrow(() -> new CommandException(Text.of("bandname not provided")));
            String Mode = args.<String>getOne(Text.of("Mode")).orElse(null);

            if (CommandControlled.ICStates.containsKey(bandname)) {
                if (Mode == null) {
                    Boolean state = CommandControlled.ICStates.get(bandname);
                    if (state == null) {
                        throw new CommandException(Text.of("Cannot toggle band - no current state"));
                    }
                    CommandControlled.ICStates.replace(bandname, !CommandControlled.ICStates.get(bandname));
                    src.sendMessage(Text.of(TextColors.GREEN, "Toggled the band."));
                }
                else if (Mode.equalsIgnoreCase("on")) {
                    CommandControlled.ICStates.replace(bandname, true);
                    src.sendMessage(Text.of(TextColors.GREEN, "Turned on the band."));
                }
                else if (Mode.equalsIgnoreCase("off")) {
                    CommandControlled.ICStates.replace(bandname, false);
                    src.sendMessage(Text.of(TextColors.GREEN, "Turned off the band."));
                }
                else if (Mode.equalsIgnoreCase("state")) {
                    Boolean state = CommandControlled.ICStates.get(bandname);
                    if (state == null) {
                        throw new CommandException(Text.of("No state."));
                    }
                    src.sendMessage(Text.of(TextColors.GREEN, "The band is " + (state ? "on" : "off")));
                }
                else {
                    throw new CommandException(Text.of("Allowed modes are: on, off, state"));
                }
                return CommandResult.success();
            }
            else {
                throw new CommandException(Text.of("Band is not loaded or does not exist."));
            }
        }
    }

    public static class ListCommand implements CommandExecutor {

        @Override
        public @NotNull CommandResult execute(@NotNull CommandSource src, @NotNull CommandContext args) throws CommandException {
            List<Text> contents = new HashSet<>(ICStates.entrySet()).stream()
                    .map(entry -> Text.of(TextColors.BLUE, entry.getKey(), ":", encodeState(entry.getValue())))
                    .collect(Collectors.toList());
            PaginationList.builder()
                    .title(Text.of("Currently loaded CommandControlled bands"))
                    .contents(contents)
                    .build()
                    .sendTo(src);
            return CommandResult.success();
        }

        private Text encodeState(Boolean state) {
            if (state == null) {
                return Text.of(TextColors.GRAY, "Unknown");
            }
            return Text.of(state ? TextColors.GREEN : TextColors.RED, Boolean.toString(state));
        }
    }

    public static class Factory implements ICFactory<CommandControlled>, HybridIC {

        @Override
        public CommandControlled createInstance(Location<World> location) { return new CommandControlled(this,location); }

        @Override
        public String stVariant() {
            return "MCZ120";
        }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Redstone"
                    },
                    new String[]{
                            "On/Off depending on command"
                    }
            };
        }
    }
}
