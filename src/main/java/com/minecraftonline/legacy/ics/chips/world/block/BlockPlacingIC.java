/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.block;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.ic.AbstractIC;
import com.minecraftonline.legacy.blockbags.MultiNearbyChestBlockBag;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.nms.LocationWorldUtil;
import com.minecraftonline.util.parsing.lineparser.range.RangeLineParserFactory;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.blockbags.AdminBlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.craftbook.sponge.util.TextUtil;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.Chunk;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.StoneTypes;
import org.spongepowered.api.data.type.TreeType;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.extent.Extent;
import org.spongepowered.common.event.tracking.phase.plugin.BasicPluginContext;
import org.spongepowered.common.event.tracking.phase.plugin.PluginPhase;
import org.spongepowered.common.util.Constants;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

/**
 * @author Brendan  (doublehelix457)
 * <p>
 * Extend this class for IC's such as: MCX207, 208, 209, 210 etc..
 */
public abstract class BlockPlacingIC<T extends BlockPlacingIC<T>> extends AbstractIC implements AreaIC {

    public BlockPlacingIC(ICFactory<T> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    protected Vector3i start;
    protected Vector3i end;
    protected BlockState blockstate;
    protected ItemStack itemStack;

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(3, getRangeParserFactory().createBinder(this, range -> {
                    // It was awfully quiet all of a sudden :P
                    // Lucky you
                    Direction looking = getBlock().require(Keys.DIRECTION).getOpposite();
                    Vector3d pos = getBlockCentre().add(looking.asOffset().mul(1.5)).add(0, -.5, 0).getPosition();


                    Vector3i a = range.getStart(pos, looking).floor().toInt();
                    Vector3i b = range.getEnd(pos, looking).floor().toInt();

                    this.start = a.min(b);
                    this.end = a.max(b).sub(1, 1, 1);
                }))
                .build();
    }

    public abstract RangeLineParserFactory getRangeParserFactory();

    public boolean doesForce() {
        return false;
    }

    @Override
    public void onTrigger() {
        if (blockstate == null) {
            return; // Invalid
        }

        //CraftBookPlugin.spongeInst().getLogger().info("In blockplacing IC at " + getBlock());

        Set<Chunk> chunks = LocationUtil.getChunksWithoutMarkingActiveInRadius(getBlock(), MultiNearbyChestBlockBag.DEFAULT_RADIUS);


        for (Chunk chunk : chunks) {
            if (chunk == null || chunk.unloadQueued) {
                //CraftBookPlugin.spongeInst().getLogger().info("Unload queued for chunk: " + chunk + " at " + getBlock());
                return;
            }
        }

        if (!LocationWorldUtil.isAllLoaded(getBlock().getExtent(), start, end)) {
            return;
        }

        if (getBlockBag(getBlock()) == null) {
            messageNearby(Text.of(TextColors.RED, "You have no where to store the blocks!"));
            return;
        }
        //CraftBookPlugin.spongeInst().getLogger().info("Activating at " + getBlock());

        setBlocks(start, end, getPinSet().isAnyTriggered(this), getBlockBag(getBlock()));
    }

    /**
     * Set Blocks in a specified Area.
     *
     * @param start Starting Position of Placement
     * @param end   Ending Position of Placement
     * @param set   True to place the blocks, false to remove them.
     * @param bag   The BlockBag for the IC.
     */
    public void setBlocks(Vector3i start, Vector3i end, boolean set, BlockBag bag) {
        if (!getLine(1).endsWith("*")) {
            set(set, bag);
        } else {
            if (areaContainsBlock(start, end, blockstate) && !doesForce()) {
                messageMiningNearbyPlayers(blockstate.getType());
            } else {
                authorizeIC();
                set(set, bag);
            }
        }
    }

    /**
     * Set the blocks with an Extent View.
     *
     * @param set
     * @param bag
     */
    private void set(boolean set, BlockBag bag) {
        if (!BlockUtil.doesStatePassFilters(ICSocket.allowedICBlocks.getValue(), blockstate)) {
            messageNearby(Text.of(TextColors.RED, TextUtil.stripIdAndCapitalize(blockstate.getType().getName()), " is not a valid block type for this IC!"));
            return;
        }

        final int maxToAffect;

        List<ItemStack> cachedUseBlocks = null;

        net.minecraft.world.World world = (net.minecraft.world.World) getBlock().getExtent();

        if (set) {
            Predicate<ItemStack> predicate = getPredicate(itemStack);
            int amountToTake = itemStack.getQuantity() * countRequiredBlocks();

            int failedToTake = bag.remove(predicate, amountToTake);
            if (itemStack.getQuantity() != 1) {
                int remainder = failedToTake % itemStack.getQuantity();
                bag.add(getPickupBlocks(itemStack, remainder)); // Refund if we didn't have enough to make a whole block.
            }
            maxToAffect = (amountToTake - failedToTake) / itemStack.getQuantity();
        } else {
            cachedUseBlocks = getPickupBlocks(itemStack, countPickupBlocks());
            if (!bag.canFit(cachedUseBlocks)) {
                return;
            }
            maxToAffect = -1;
        }

        if (maxToAffect == 0) {
            return;
        }

        BlockState desiredState = set ? this.blockstate : BlockTypes.AIR.getDefaultState();



        IBlockState nmsDesiredState = (IBlockState) desiredState;
        Block nmsDesiredBlock = nmsDesiredState.getBlock();

        //int blockUpdates = 0;

        Set<BlockPos> missedBlocks = new HashSet<>();

        int affected = 0;
        final Predicate<BlockState> blockMatcher = getBlockPredicate();
        try (BasicPluginContext phaseState = PluginPhase.State.BLOCK_WORKER.createPhaseContext()
                .source(this)) {
            phaseState.buildAndSwitch();
            outer:
            for (int x = this.start.getX(); x <= this.end.getX(); x++) {
                for (int z = this.start.getZ(); z <= this.end.getZ(); z++) {
                    for (int y = this.start.getY(); y <= this.end.getY(); y++) {
                        BlockPos blockPos = new BlockPos(x, y, z);
                        IBlockState oldState = world.getBlockState(blockPos);

                        if (set) {
                            if (!canPlaceThrough((BlockType) oldState.getBlock())) {
                                missedBlocks.add(blockPos);
                                continue;
                            }
                        } else {
                            if (!blockMatcher.test((BlockState) oldState)) {
                                missedBlocks.add(blockPos);
                                continue;
                            }
                        }
                        if (set && affected >= maxToAffect) {
                            break outer;
                        }
                        if (!world.setBlockState(blockPos, nmsDesiredState, Constants.BlockChangeFlags.NOTIFY_CLIENTS)) {
                            continue;
                        }
                        affected++;

                        if (x == this.start.getX()) {
                            world.neighborChanged(new BlockPos(x - 1, y, z), nmsDesiredBlock, blockPos);
                            //blockUpdates++;
                        }
                        if (x == this.end.getX()) {
                            world.neighborChanged(new BlockPos(x + 1, y, z), nmsDesiredBlock, blockPos);
                            //blockUpdates++;
                        }

                        if (y == this.start.getY()) {
                            // Notify below.
                            world.neighborChanged(new BlockPos(x, y - 1, z), nmsDesiredBlock, blockPos);
                            //blockUpdates++;
                        }
                        if (y == this.end.getY()) {
                            // Notify above
                            world.neighborChanged(new BlockPos(x, y + 1, z), nmsDesiredBlock, blockPos);
                            //blockUpdates++;
                        }

                        if (z == this.start.getZ()) {
                            world.neighborChanged(new BlockPos(x, y, z - 1), nmsDesiredBlock, blockPos);
                            //blockUpdates++;
                        }
                        if (z == this.end.getZ()) {
                            world.neighborChanged(new BlockPos(x, y, z + 1), nmsDesiredBlock, blockPos);
                            //blockUpdates++;
                        }
                    }
                }
            }
        }

        // Incase we missed went around some blocks.

        for (BlockPos blockPos : missedBlocks) {
            BlockPos[] possibleUpdateSources = new BlockPos[]{blockPos.up(), blockPos.down(), blockPos.north(), blockPos.east(), blockPos.south(), blockPos.east()};
            for (BlockPos updateSource : possibleUpdateSources) {
                if (missedBlocks.contains(updateSource)) {
                    continue;
                }
                if (updateSource.getX() < this.start.getX() || updateSource.getX() > this.end.getX()
                        || updateSource.getY() < this.start.getY() || updateSource.getY() > this.end.getY()
                        || updateSource.getZ() < this.start.getZ() || updateSource.getZ() > this.end.getZ()) {
                    continue;
                }
                world.neighborChanged(blockPos, nmsDesiredBlock, updateSource);
                //blockUpdates++;
            }
        }

        //System.out.println("Notified " + blockUpdates + " blocks");

        if (set) {
            if (maxToAffect != affected) {
                if (affected > maxToAffect) {
                    CraftBookPlugin.spongeInst().getLogger().error("Affected more than the max in block placing ic " + getBlock());
                }
                int refundAmount = maxToAffect - affected;
                List<ItemStack> refundStacks = getPickupBlocks(itemStack, refundAmount);
                for (ItemStack left : bag.add(refundStacks)) {
                    // Shouldn't ever really happen
                    Item item = (Item) getBlock().getExtent().createEntity(EntityTypes.ITEM, getBlock().getPosition());
                    item.offer(Keys.REPRESENTED_ITEM, left.createSnapshot());
                    block.getExtent().spawnEntity(item);
                }
            }
        } else {
            if (maxToAffect != affected) {
                // actual is different to expected. should never be more than expected, so should still fit
                cachedUseBlocks = getPickupBlocks(itemStack, affected);
            }
            for (ItemStack left : bag.add(cachedUseBlocks)) {
                // Shouldn't ever really happen
                Item item = (Item) getBlock().getExtent().createEntity(EntityTypes.ITEM, getBlock().getPosition());
                item.offer(Keys.REPRESENTED_ITEM, left.createSnapshot());
                block.getExtent().spawnEntity(item);
            }
        }
    }

    /**
     * Gets blocks to be put back into the block bag.
     * Put special cases here.
     *
     * @param pickedUpBlocks blocks removed
     * @return List<ItemStack> List of appropriate items.
     */
    public static List<ItemStack> getPickupBlocks(ItemStack itemStack, int pickedUpBlocks) {
        if (itemStack.getType().equals(ItemTypes.STONE) && itemStack.require(Keys.STONE_TYPE).equals(StoneTypes.STONE)) {
            return IDUtil.multiplyItemStack(ItemStack.of(ItemTypes.COBBLESTONE), pickedUpBlocks);
        }
        return IDUtil.multiplyItemStack(itemStack, pickedUpBlocks);
    }

    public Predicate<BlockState> getBlockPredicate() {
        if (this.blockstate.getType() == BlockTypes.LEAVES || this.blockstate.getType() == BlockTypes.LEAVES2) {
            TreeType treeType = this.blockstate.require(Keys.TREE_TYPE);
            return block -> this.blockstate.getType() == block.getType()
                    && block.get(Keys.TREE_TYPE).filter(type -> type == treeType).isPresent();
        }

        return block -> this.blockstate.equals(block);
    }

    public static boolean canPlaceThrough(BlockType block) {
        return block == BlockTypes.AIR
                || block == BlockTypes.LAVA
                || block == BlockTypes.FLOWING_LAVA
                || block == BlockTypes.WATER
                || block == BlockTypes.FLOWING_WATER;
    }

    /**
     * Creates a predicate for getting the desired items from a blockbag
     * Put special cases here.
     *
     * @param itemStack to create predicate to find
     * @return Predicate for the ItemStack
     */
    public static Predicate<ItemStack> getPredicate(ItemStack itemStack) {
        if (itemStack.getType().equals(ItemTypes.STONE) && itemStack.require(Keys.STONE_TYPE).equals(StoneTypes.STONE)) {
            final ItemStack stone = ItemStack.of(ItemTypes.STONE);
            final ItemStack cobble = ItemStack.of(ItemTypes.COBBLESTONE);
            return itemStack1 -> {
                ItemStack copy = itemStack1.copy();
                copy.setQuantity(1);
                return copy.equalTo(stone) || copy.equalTo(cobble);
            };
        }
        return new IgnoreQuantityPredicate(itemStack);
    }

    @Override
    public CUIRegion getArea() {
        return WorldEditUtil.toCuboidSelection(getBlock().getExtent(), start.toDouble(), end.toDouble());
    }

    public static class IgnoreQuantityPredicate implements Predicate<ItemStack> {
        private final ItemStack itemStack;

        public IgnoreQuantityPredicate(ItemStack itemStack) {
            this.itemStack = itemStack.copy();
            this.itemStack.setQuantity(1);
        }

        @Override
        public boolean test(ItemStack that) {
            ItemStack copy = that.copy();
            copy.setQuantity(1);
            return copy.equalTo(itemStack);
        }
    }

    /**
     * Mining Protection done here.
     * <p>
     * Remove the * flag from the sign so that it becomes a normal bridge/door IC.
     */
    private void authorizeIC() {
        if (getLine(1).contains("*")) {
            setLine(1, Text.of(getLine(1).substring(0, getLine(1).length() - 1)));
        }
    }

    /**
     * Tell players close to the chip why it's not working.
     */
    private void messageMiningNearbyPlayers(BlockType type) {
        Text theMessage = Text.builder((getLine(1) + " Mining protection: Remove all blocks of type " + type + " from the area of this IC to make it work!")).color(TextColors.RED).build();
        messageNearby(theMessage);
    }

    /**
     * Message anyone nearby
     *
     * @param message
     */
    private void messageNearby(Text message) {
        for (Entity e : getBlock().getExtent().getNearbyEntities(getBlock().getPosition(), 5)) {
            if (e.getType() == EntityTypes.PLAYER) {
                ((Player) e).sendMessage(message);
                break;
            }
        }
    }

    /**
     * Searches the area on which the IC operates for existing blocks
     */
    private boolean areaContainsBlock(Vector3i start, Vector3i end, BlockState blockstate) {
        Extent extent = getBlock().getExtent();

        for (int z = start.getZ(); z <= end.getZ(); z++) {
            for (int y = start.getY(); y <= end.getY(); y++) {
                for (int x = start.getX(); x <= end.getX(); x++) {
                    if (extent.getBlock(x, y, z).equals(blockstate)) return true;
                }
            }
        }
        return false;
    }

    private int countPickupBlocks() {
        Extent extent = getBlock().getExtent();
        int blocks = 0;
        for (int x = start.getX(); x <= end.getX(); x++) {
            for (int y = start.getY(); y <= end.getY(); y++) {
                for (int z = start.getZ(); z <= end.getZ(); z++) {
                    if (extent.getBlock(x, y, z).equals(blockstate)) {
                        blocks++;
                    }
                }
            }
        }

        return blocks;
    }

    private int countRequiredBlocks() {
        Extent extent = getBlock().getExtent();
        int blocks = 0;
        for (int x = start.getX(); x <= end.getX(); x++) {
            for (int y = start.getY(); y <= end.getY(); y++) {
                for (int z = start.getZ(); z <= end.getZ(); z++) {
                    if (canPlaceThrough(extent.getBlockType(x, y, z))) {
                        blocks++;
                    }
                }
            }
        }

        return blocks;
    }

    public BlockBag getBlockBag(Location<World> location) {
        return BlockBagManager.getOrCreateCachedBlockBag(BlockBagManager.BlockBagType.MULTI_NEARBY_CHEST, location, MultiNearbyChestBlockBag.DEFAULT_RADIUS, MultiNearbyChestBlockBag.getChestBlockTypes());
    }

    /**
     * WARNING: It gets messy and legacy down there... Sure you want to continue?
     */
    @Override
    public void load() {
        super.load();

        String[] line2parts = getLine(2).split(":");

        String id = line2parts.length == 3 ? line2parts[1] : line2parts[0];
        Optional<BlockType> type = IDUtil.getBlockTypeFromAnyID(id);
        if (!type.isPresent()) {
            CraftBookPlugin.spongeInst().getLogger().warn("Corrupted BlockPlacingIC at " + getBlock() + ", could not get blockType from: " + id);
            this.blockstate = null;
            return;
        }

        int damage = 0;
        if (line2parts.length > 1
                && !(line2parts[0].equalsIgnoreCase("f") && line2parts.length == 2)) {
            damage = Integer.parseInt(line2parts[line2parts.length - 1]);
        }

        blockstate = IDUtil.createBlockStateWithDamage(type.get(), damage);

        if (!(getBlockBag(getBlock()) instanceof AdminBlockBag)) {
            itemStack = IDUtil.getItemStackFromBlockState(blockstate);
            if (itemStack.isEmpty()) {
                CraftBookPlugin.spongeInst().getLogger().error("BlockPlacingIC at " + getBlock() + " could not get item from blockstate: " + blockstate + " it will not function.");
                blockstate = null;
                return;
            }
        } else {
            itemStack = ItemStack.empty();
        }
    }

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);

        String[] line2parts = lines.get(2).toPlain().split(":");

        boolean force = false;

        if (line2parts.length < 1) {
            throw new InvalidICException("Not enough parameters. " + lines.get(2).toPlain());
        } else if (line2parts.length > 3)
            throw new InvalidICException("Too many parameters. " + lines.get(2).toPlain());

        if (line2parts.length == 2 || line2parts.length == 3) {
            if (line2parts[0].equalsIgnoreCase("f")) {
                if (!doesForce())
                    throw new InvalidICException("Too many parameters. " + lines.get(2).toPlain());
                else
                    force = true;
            } else if (line2parts.length == 3)
                throw new InvalidICException("Invalid force value. " + lines.get(2).toPlain());
        }

        String id = force ? line2parts[1] : line2parts[0];

        int damage = 0;
        if (line2parts.length > 1) {
            String strdamage = (line2parts.length == 2 ? line2parts[1] : line2parts[2]);
            try {
                damage = Integer.parseInt(strdamage);
            } catch (NumberFormatException e) {
                throw new InvalidICException("Expected number for damage value, got: " + strdamage);
            }
        }
        BlockType type = IDUtil.getBlockTypeFromAnyID(id).orElseThrow(() -> new InvalidICException("Invalid block type: " + lines.get(2).toPlain() + "."));

        blockstate = IDUtil.createBlockStateWithDamage(type, damage);

        if (!BlockUtil.doesStatePassFilters(ICSocket.allowedICBlocks.getValue(), IDUtil.createBlockStateWithDamage(type, damage))) {
            throw new InvalidICException("Block type not allowed.");
        }
    }

    public abstract static class Factory<T extends BlockPlacingIC> implements ICFactory<T> {

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Redstone Power."
                    },
                    new String[]{
                            "Places a set amount of blocks at a location."
                    }
            };
        }
    }

    protected int[] getDimensions(String line) {
        String[] values = line.split(":", 3);

        int[] out = new int[3];

        if (values.length == 1)
            return null;

        try {
            for (int i = 0; i < values.length; i++)
                out[i] = Integer.parseInt(values[i]);
        } catch (NumberFormatException e) {
            return null;
        }

        if (out[0] <= 0 || out[1] <= 0 || out[0] > 11 || out[2] > 10 || out[2] < -10)
            return null;

        if (out[1] > ICSocket.maximumLength.getValue())
            out[1] = ICSocket.maximumLength.getValue();

        if (out[0] > ICSocket.maximumWidth.getValue())
            out[0] = ICSocket.maximumWidth.getValue();

        return out;
    }

    protected int[] getType(String line) {
        String[] type = line.split(":", 3);

        int[] out = new int[]{0, 0, 0};

        String id;
        String color = "";

        if (type.length == 3) {
            if (type[0].equalsIgnoreCase("f") && doesForce())
                out[0] = 1;
            else
                out[0] = 0; //unknown option error

            id = type[1];
            color = type[2];
        } else if (type.length == 2) {
            if (type[0].equalsIgnoreCase("f") && doesForce()) {
                out[0] = 1;
                id = type[1];
            } else {
                id = type[0];
                color = type[1];
            }
        } else id = line;


        if (id.length() > 0) out[1] = getItem(id);


        if (color.length() > 0) {
            try {
                int colorid = Integer.parseInt(color);
                if (colorid < 0 || colorid > 15)
                    out[2] = 0; //not a valid color value. Must be between 0 and 15
                else
                    out[2] = colorid;
            } catch (NumberFormatException e) {
                out[2] = 0; //not a valid color value.
            }
        }
        return out;
    }

    private int getItem(String id) {
        try {
            return Integer.parseInt(id);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
