/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.sensor;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.entity.DetailedEntityType;
import com.minecraftonline.util.entity.EntityParsingUtil;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.AABB;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * MCM116
 * MOB ABOVE?
 * Checks if entity is in the first block above the ic.
 *
 * @author tyhdefu
 */

public class MobAbove extends AbstractStIC implements AreaIC {

    private DetailedEntityType detailedEntityType;

    public MobAbove(ICFactory<MobAbove> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        String line3 = lines.get(2).toPlain();
        if (line3.isEmpty())
            return;
        try {
            EntityParsingUtil.parseStackedAndNbt(line3);
        } catch (DetailedEntityType.Error error) {
            throw new InvalidICException(error.getMessage());
        }
    }

    @Override
    public void onTrigger() {
        if (!isSt && !getPinSet().isAnyTriggered(this))
            return;
        Optional<AABB> aabb = getAABB();
        if (!aabb.isPresent()) {
            return;
        }
        Collection<Entity> entities = getBlock().getExtent().getIntersectingEntities(aabb.get());
        boolean foundEntity = entities.stream().anyMatch(detailedEntityType);
        getPinSet().setOutput(0, foundEntity, this);
    }

    public Optional<AABB> getAABB() {
        return LocationUtil.findEmptySpot(getBackBlockCentre()).map(Location::getBlockPosition).map(pos -> new AABB(
                new Vector3i(pos.getX() - 1, pos.getY(), pos.getZ() - 1),
                new Vector3i(pos.getX() + 1, pos.getY() + 1, pos.getZ() + 1)
        ));
    }

    @Override
    public void load() {
        super.load();
        if (getLine(2).isEmpty()) {
            detailedEntityType = new DetailedEntityType.Creature();
            return;
        }

        try {
            detailedEntityType = EntityParsingUtil.parseStackedAndNbt(getLine(2));
        } catch (DetailedEntityType.Error error) {
            detailedEntityType = new DetailedEntityType.Creature();
            CraftBookPlugin.spongeInst().getLogger().error("Error loading MobAbove ic at " + getBlock()
                    + ", using any creature instead of the specific given ('" + getLine(2) + "'", error);
        }
    }

    public static void checkEntityType(DetailedEntityType entityType) throws InvalidICException {
        if (!(entityType instanceof DetailedEntityType.Mob)) {
            if (entityType instanceof DetailedEntityType.SpecificType) {
                DetailedEntityType.SpecificType type = (DetailedEntityType.SpecificType) entityType;
                if (type.isAnimal() || type.isMob()) {
                    throw new InvalidICException("Invalid entity specified, must be mob/animal.");
                }
            } else {
                throw new InvalidICException("Invalid entity specified, must be mob/animal.");
            }
        }
    }

    @Override
    public CUIRegion getArea() {
        return getAABB().map(aabb -> WorldEditUtil.toCuboidSelection(getBlock().getExtent(), aabb.getMin(), aabb.getMax()))
                .orElse(null);
    }

    public static class Factory implements ICFactory<MobAbove>, HybridIC {

        @Override
        public MobAbove createInstance(Location<World> location) {
            return new MobAbove(this, location);
        }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Input"
                    },
                    new String[]{
                            "Outputs if a mob is above"
                    }
            };
        }

        @Override
        public String stVariant() {
            return "MCO116";
        }
    }
}
