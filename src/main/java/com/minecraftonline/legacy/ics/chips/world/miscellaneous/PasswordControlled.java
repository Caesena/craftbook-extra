/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.miscellaneous;

import com.google.common.collect.Sets;
import com.minecraftonline.legacy.ics.HybridIC;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractStIC;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/*
 * Command Controlled IC
 *
 * ModelID: MCX121
 * Changes output due to result of command
 *
 * See {@link https://minecraftonline.com/wiki/MCX121}
 *
 * @author tyhdefu
 */

public class PasswordControlled extends AbstractStIC {

    private static final String FILE_NAME = "mcx121.txt";

    private String bandname;

    public static Map<String, Entry> ICStates = new ConcurrentHashMap<>();
    public static Set<String> PENDING_WRITE = Sets.newConcurrentHashSet();

    public static class Entry {

        private static final Entry BLANK = new Entry("", null);

        private final String password; // Blank string if none
        @Nullable
        private final Boolean state;

        private Entry(final String password, final @Nullable Boolean state) {
            this.password = password;
            this.state = state;
        }

        public static Entry of(final String password, final Boolean state) {
            return new Entry(password, state);
        }

        public static Entry ofPassword(final String password) {
            return new Entry(password, null);
        }

        public static Entry ofState(final Boolean state) {
            return new Entry("", state);
        }

        public static Entry blank() {
            return BLANK;
        }

        public boolean hasPassword() {
            return password.length() != 0;
        }

        public boolean passwordMatches(final String password) {
            return password.equals(this.password);
        }

        public String getPassword() {
            return this.password;
        }

        @Nullable
        public Boolean getState() {
            return state;
        }

        public Entry withNewPassword(final String password) {
            return new Entry(password, this.state);
        }

        public Entry withNewState(final Boolean state) {
            return new Entry(this.password, state);
        }
    }

    public PasswordControlled(ICFactory<PasswordControlled> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if (lines.get(2).toPlain().isEmpty()) {
            throw new InvalidICException("Band name cannot be blank!");
        }
        if (lines.get(3).toPlain().length() != 0)
            throw new InvalidICException("Line 4 must be blank!");
    }

    @Override
    public void load() {
        super.load();
        bandname = getLine(2);
        if (bandname.isEmpty()) {
            CraftBookPlugin.spongeInst().getLogger().error("PasswordControlled with blank line at " + getBlock() + " IC will not operate.");
            return;
        }
        if (ICStates.containsKey(bandname)) {
            return;
        }
        try {
            File file = new File(CraftBookPlugin.spongeInst().getWorkingDirectory(), FILE_NAME);
            if (file.isFile()) {
                try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        String[] splitLine = line.split(":", 2);
                        if (splitLine[0].equals(bandname)) {
                            String password = splitLine.length == 1 ? "" : splitLine[1]; // Password could be blank if never set.
                            ICStates.put(bandname, Entry.of(password, this.getPinSet().getOutput(0, this)));
                            return;
                        }
                    }
                }
                ICStates.put(bandname, Entry.ofState(this.getPinSet().getOutput(0, this))); // Put it if it doesn't exist.
            } else {
                CraftBookPlugin.spongeInst().getLogger().error("PasswordControlled file: '" + FILE_NAME + "' not found");
                ICStates.put(bandname, Entry.blank());
            }
        } catch (FileNotFoundException e) {
            CraftBookPlugin.spongeInst().getLogger().warn("File not found" + e.getLocalizedMessage());
        } catch (IOException e) {
            CraftBookPlugin.spongeInst().getLogger().warn("IOException: " + e.getLocalizedMessage());
        }
    }

    public static void flushAsync(String bandname) {
        final Entry entry = ICStates.get(bandname);
        if (entry == null || !entry.hasPassword())
            return;
        CraftBookPlugin.spongeInst().getScheduler().submitAsyncSingleThread((() -> {
            CraftBookPlugin.spongeInst().getLogger().info("Saving Password controlled IC: " + bandname);
            try {
                File file = new File(CraftBookPlugin.spongeInst().getWorkingDirectory(), FILE_NAME);
                if (file.isFile() || file.createNewFile()) {
                    final StringBuilder stringBuilder = new StringBuilder();
                    boolean inFile = false;
                    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                        String line;
                        while ((line = reader.readLine()) != null) {
                            if (line.isEmpty()) {
                                continue;
                            }
                            if (line.split(":")[0].equals(bandname)) {
                                if (inFile) {
                                    CraftBookPlugin.spongeInst().getLogger().warn("Duplicate entries in " + FILE_NAME + " for band '" + bandname + "'. Removing them.");
                                    continue;
                                }
                                inFile = true;
                                stringBuilder.append(bandname).append(":").append(ICStates.get(bandname).getPassword()).append('\n');
                            }
                            else {
                                stringBuilder.append(line).append('\n');
                            }
                        }
                    }

                    if (!inFile) {
                        try (final BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
                            writer.newLine();
                            writer.write(bandname + ":" + entry.getPassword());
                        }
                    }
                    else {
                        try (final BufferedWriter writer = new BufferedWriter(new FileWriter(file, false))) {
                            writer.write(stringBuilder.toString());
                        }
                    }
                }
                else {
                    CraftBookPlugin.spongeInst().getLogger().error("Could not create file: " + FILE_NAME);
                }
            } catch (FileNotFoundException e) {
                CraftBookPlugin.spongeInst().getLogger().warn("File not found" + e.getLocalizedMessage());
            } catch (IOException e) {
                CraftBookPlugin.spongeInst().getLogger().warn("IOException: " + e.getLocalizedMessage());
            }
        }));
    }

    @Override
    public void onTrigger() {
        if (bandname.isEmpty()) {
            return;
        }
        Entry entry = ICStates.get(bandname);
        if (entry != null && entry.getState() != null) {
            getPinSet().setOutput(0, entry.getState(), this);
        }
    }

    public static class MainCommand implements CommandExecutor {

        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String bandname = args.<String>getOne(Text.of("Band")).orElseThrow(() -> new CommandException(Text.of("bandname not provided")));
            String password = args.<String>getOne(Text.of("Password")).orElseThrow(() -> new CommandException(Text.of("password not provided")));
            String mode = args.<String>getOne(Text.of("Mode")).orElse(null);

            password = encrypt(password);
            Entry entry = PasswordControlled.ICStates.get(bandname);
            if (entry == null) {
                throw new CommandException(Text.of("Band is not loaded or does not exist."));
            }

            if (!entry.hasPassword()) {
                throw new CommandException(Text.of("Password has not been set yet!"));
            }
            if (!entry.passwordMatches(password)) {
                throw new CommandException(Text.of(TextColors.RED, "Password is incorrect."));
            }
            if (mode == null) {
                if (entry.getState() == null) {
                    throw new CommandException(Text.of("Cannot toggle the state as it is unknown. Use /mcx121 <band> on|off"));
                }
                PasswordControlled.ICStates.replace(bandname, entry.withNewState(!entry.getState()));
                src.sendMessage(Text.of(TextColors.GREEN, "Toggled the band."));
            } else if (mode.equalsIgnoreCase("on")) {
                PasswordControlled.ICStates.replace(bandname, entry.withNewState(true));
                src.sendMessage(Text.of(TextColors.GREEN, "Turned on the band."));
            } else if (mode.equalsIgnoreCase("off")) {
                PasswordControlled.ICStates.replace(bandname, entry.withNewState(false));
                src.sendMessage(Text.of(TextColors.GREEN, "Turned off the band."));
            } else if (mode.equalsIgnoreCase("state")) {
                src.sendMessage(Text.of(TextColors.GREEN, "The band is " + (PasswordControlled.ICStates.get(bandname).getState() ? "on" : "off")));
            } else {
                throw new CommandException(Text.of("Allowed modes are: on, off, state or blank to invert"));
            }
            return CommandResult.success();
        }
    }

    public static class AddPassword implements CommandExecutor {

        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String bandname = args.<String>getOne(Text.of("Band")).get();
            String password = args.<String>getOne(Text.of("Password")).get();
            password = encrypt(password);
            if (!ICStates.containsKey(bandname))
                throw new CommandException(Text.of("Band not loaded or does not exist - Place the IC first."));

            if (ICStates.get(bandname).hasPassword()) {
                throw new CommandException(Text.of("Password is already set."));
            }
            ICStates.replace(bandname, ICStates.get(bandname).withNewPassword(password));
            flushAsync(bandname);
            src.sendMessage(Text.of(TextColors.GREEN, "Successfully set password"));
            return CommandResult.success();
        }
    }

    public static class ChangePassword implements CommandExecutor {

        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String bandname = args.<String>getOne(Text.of("Band")).get();
            String oldpassword = args.<String>getOne(Text.of("OldPassword")).get();
            String newpassword = args.<String>getOne(Text.of("NewPassword")).get();

            oldpassword = encrypt(oldpassword);

            if (!ICStates.containsKey(bandname))
                throw new CommandException(Text.of("Band not loaded or does not exist"));

            if (ICStates.get(bandname).passwordMatches(oldpassword)) {
                ICStates.replace(bandname, ICStates.get(bandname).withNewPassword(encrypt(newpassword)));
                flushAsync(bandname);
                src.sendMessage(Text.of(TextColors.GREEN, "Changed Password"));
            }
            else {
                throw new CommandException(Text.of("Incorrect password."));
            }
            return CommandResult.success();
        }
    }

    public static class HasPassword implements CommandExecutor {

        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String bandname = args.<String>getOne(Text.of("Band")).get();
            if (!ICStates.containsKey(bandname))
                throw new CommandException(Text.of("Band not loaded or does not exist."));

            src.sendMessage(ICStates.get(bandname).hasPassword() ? Text.of("The band has a password")
                    : Text.of("The band does not have a password"));
            return CommandResult.success();
        }
    }

    public static class ListPasswordControlled implements CommandExecutor {

        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            Task.builder()
                    .name("CBX - PasswordControlled reading all " + UUID.randomUUID().toString())
                    .execute(() -> {
                        final File file = new File(CraftBookPlugin.spongeInst().getWorkingDirectory(), FILE_NAME);
                        if (file.isFile()) {
                            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                                String line;
                                while ((line = reader.readLine()) != null) {
                                    String[] splitLine = line.split(":");
                                    ICStates.putIfAbsent(splitLine[0], Entry.ofPassword(splitLine.length == 2 ? splitLine[1] : ""));
                                }
                            } catch (IOException e) {
                                CraftBookPlugin.spongeInst().getLogger().error("Error reading " + FILE_NAME, e);
                                src.sendMessage(Text.of(TextColors.RED, "Error executing command: " + e.getMessage()));
                            }

                        }
                        else {
                            src.sendMessage(Text.of(TextColors.RED, "File doesn't exist - This is expected until some PasswordControlled ics have been unloaded."));
                        }
                        List<Text> content = new HashSet<>(ICStates.entrySet()).stream()
                                .map(entry -> Text.of(TextColors.BLUE, entry.getKey(), ":", encodeState(entry.getValue().getState())))
                                .collect(Collectors.toList());

                        PaginationList.builder()
                                .title(Text.of("All PasswordControlled bands"))
                                .contents(content)
                                .build()
                                .sendTo(src);
                    })
                    .submit(CraftBookPlugin.spongeInst());
            return CommandResult.success();
        }

        private Text encodeState(Boolean state) {
            if (state == null) {
                return Text.of(TextColors.GRAY, "Unknown");
            }
            return Text.of(state ? TextColors.GREEN : TextColors.RED, Boolean.toString(state));
        }
    }

    public static String encrypt(String password) {
        return "" + password.hashCode();
    }

    public static class Factory implements ICFactory<PasswordControlled>, HybridIC {

        @Override
        public PasswordControlled createInstance(Location<World> location) {
            return new PasswordControlled(this, location);
        }

        @Override
        public String stVariant() {
            return "MCZ121";
        }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Redstone"
                    },
                    new String[]{
                            "On/Off depending on command"
                    }
            };
        }
    }
}
