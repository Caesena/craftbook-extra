/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.block;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.ic.AbstractIC;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.parsing.SimpleParserFactory;
import com.minecraftonline.util.parsing.block.state.BlockStateParsers;
import com.minecraftonline.util.parsing.lineparser.generic.SimpleDualParserFactory;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * ADMIN FLEX SET
 * ModelID: MC1207
 *
 * @author Brendan (doublehelix457)
 * (deleted lines to make it Admin by @author tyhdefu)
 */
public class FlexSetAdmin extends AbstractIC implements AreaIC {

    private boolean hold = false;
    private BlockState type;
    private Location<World> location;

    public FlexSetAdmin(ICFactory<FlexSetAdmin> icFactory, Location<World> block){super(icFactory,block);}

    private static final SimpleDualParserFactory<Vector3i, BlockState> OFFSET_AND_BLOCK_FACTORY = new SimpleDualParserFactory<>(
            FlexSet.OFFSET_PARSER_FACTORY,
            SimpleParserFactory.of(BlockStateParsers.TYPE_AT_OPTIONAL_DAMAGE),
            ":"
    );

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, OFFSET_AND_BLOCK_FACTORY.createBinder(this, (offset, blockState) -> {
                    this.location = getBackBlock().add(offset);
                    this.type = blockState;
                }))
                .lineParser(3, FlexSet.BOOLEAN_PARSER_FACTORY.createBinder(this, hold -> this.hold = hold))
                .build();
    }

    @Override
    public void onTrigger() {
        if(getPinSet().isAnyTriggered(this)){
            if(location.getBlockType() == type.getType()) return;
            location.setBlock(type);
        }else{
            if(!hold || location.getBlockType() != type.getType()) return;
            location.setBlockType(BlockTypes.AIR);
        }
    }

    @Nullable
    @Override
    public CUIRegion getArea() {
        return WorldEditUtil.toCuboidFromPoint(location);
    }

    public static class Factory implements ICFactory<FlexSetAdmin>, RestrictedIC {

        @Override
        public FlexSetAdmin createInstance(Location<World> location) { return new FlexSetAdmin(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Redstone"
                    },
                    new String[]{
                            "Place a block at a location"
                    }
            };
        }
    }
}
