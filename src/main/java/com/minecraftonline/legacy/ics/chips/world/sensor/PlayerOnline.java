/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.sensor;

import com.minecraftonline.legacy.ics.HybridIC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractStIC;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

/**
 * PLAYER ONLINE?
 * ModelID: MC1500
 *
 * @author Brendan (doublehelix457)
 * @author Tom (tmhrtly) (Legacy)
 */
public class PlayerOnline extends AbstractStIC {

    public PlayerOnline(ICFactory<PlayerOnline> icFactory, Location<World> block){super(icFactory,block);}

    private String name;

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if(lines.get(2).isEmpty()) throw new InvalidICException("Put a player's name on line 3 with no spaces.");
    }

    @Override
    public void load(){
        super.load();
        name = getLine(2);
    }

    @Override
    public void onTrigger() {
        boolean online = false;
        for(Player player : Sponge.getServer().getOnlinePlayers()){
            if(player.getName().contains(name)){
                online = true;
                break;
            }
        }
        getPinSet().setOutput(0,online,this);
    }

    public static class Factory implements ICFactory<PlayerOnline>, HybridIC {
        @Override
        public String stVariant() { return "MC0500"; }

        @Override
        public PlayerOnline createInstance(Location<World> location) { return new PlayerOnline(this,location);}

        @Override
        public String[][] getPinHelp() {
            return new String[0][];
        }
    }
}
