/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.math.DoubleMath;
import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.legacy.blockbags.MultiNearbyChestBlockBag;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.AsyncIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicate;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicateParsers;
import com.minecraftonline.util.parsing.lineparser.itempredicate.ItemPredicateParserFactory;
import com.minecraftonline.util.parsing.lineparser.range.RangeLimitations;
import com.minecraftonline.util.parsing.lineparser.range.RangeLineParserFactory;
import com.minecraftonline.util.parsing.lineparser.range.RangeRestrictions;
import com.minecraftonline.util.parsing.rangeparser.RangeParsers;
import com.minecraftonline.util.parsing.rangeparser.RelativeRange;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.craftbook.sponge.util.prompt.ItemStackSnapshotDataPrompt;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import net.minecraft.entity.item.EntityItem;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Chest Collector
 * ModelID: MCX203
 *
 * @author Brendan  (doublehelix457)
 * @author nosefish (Legacy)
 */
public class ChestCollector extends AbstractStIC implements AreaIC, AsyncIC {

    private static final ItemStackSnapshotDataPrompt ITEMS_PROMPT = new ItemStackSnapshotDataPrompt(1,9,"Enter Item To collect!");

    private ItemPredicate itemPredicate;
    private BlockType containerType;
    private Set<BlockType> allowedContainerTypes;
    private final Location<World> origin = getBlock();
    private boolean requiresOffset;
    private RelativeRange range;

    public static final ItemPredicateParserFactory ITEM_PREDICATE_PARSER_FACTORY = ItemPredicateParserFactory.builder()
            .parser(ItemPredicateParsers.OPTIONAL_ITEM_TYPE_COLON_DAMAGE)
            .withDefault(ItemPredicate.ANY)
            .build();

    private static final RangeLineParserFactory RANGE_PARSER_FACTORY = RangeLineParserFactory.builder()
            .parser(RangeParsers.RANGE_AND_OFFSET)
            .withDefault(RelativeRange.builder().range(8).build())
            .restriction(RangeRestrictions.minRange(1))
            .limitation(RangeLimitations.limitRange(8))
            .limitation(RangeLimitations.maxOffsetInSingleDirection(8))
            .build();

    public static Map<Character, BlockType> containerMap;

    static {
        containerMap = new HashMap<>();
        containerMap.put('c', BlockTypes.CHEST);
        containerMap.put('t', BlockTypes.TRAPPED_CHEST);
        containerMap.put('d', BlockTypes.DISPENSER);
        containerMap.put('r', BlockTypes.DROPPER);
        containerMap.put('h', BlockTypes.HOPPER);
    }

    //CHEST COLLECTOR ModelID = MCX203
    public ChestCollector(ICFactory<ChestCollector> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    @Override
    public void load() {
        super.load();

        if (isCorrupt()) {
            return;
        }

        if (this.range.getRange() > 8) {
            this.range = RelativeRange.builder().from(range).range(8).build();
        }

        if (!range.getXyzOffset().equals(Vector3d.ZERO)) {
            requiresOffset = true;
        }
    }

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(1, (s, p) -> {
                    char c = Character.toLowerCase(s.charAt(s.length() - 1));
                    this.containerType = containerMap.get(c);
                    if (this.containerType != null) {
                        this.allowedContainerTypes = Collections.singleton(containerType);
                    }
                    else {
                        this.allowedContainerTypes = MultiNearbyChestBlockBag.getChestBlockTypes();
                    }
                })
                .lineParser(2, ITEM_PREDICATE_PARSER_FACTORY.createBinder(this, p -> this.itemPredicate = p))
                .lineParser(3, RANGE_PARSER_FACTORY.createBinder(this, r -> this.range = r))
                .build();
    }

    @Override
    public void onTrigger() {
        if (!thinking && !getPinSet().isAnyTriggered(this)) {
            return;
        }
        CraftBookPlugin.spongeInst().getScheduler().scheduleAsyncBatch(getAsyncAction());
    }

    @Override
    public Runnable getAsyncAction() {
        return () -> {
            final List<EntityItem> stacks = getNearbyItemStacks(origin);

            if (stacks.isEmpty()) {
                return;
            }

            // Put back onto the main thread
            CraftBookPlugin.spongeInst().getScheduler().scheduleSync(() -> {
                BlockBag inv;
                if (requiresOffset) {
                    Location<World> offset = origin.add(range.getXyzOffset());
                    inv = getBlockBagAt(offset);
                } else {
                    inv = BlockBagManager.getOrCreateCachedBlockBag(BlockBagManager.BlockBagType.MULTI_NEARBY_CHEST, getBlock(), MultiNearbyChestBlockBag.DEFAULT_RADIUS, allowedContainerTypes);
                }
                if (inv == null) {
                    return;
                }
                boolean itemCollected = false;
                if (!getBlock().getExtent().getChunk(getBlock().getChunkPosition()).isPresent()) {
                    return; // We've been unloaded :(
                }

                final int size = stacks.size();
                for (int i = 0; i < size; i++) {
                    Item item = (Item) stacks.get(i);
                    if (!item.isRemoved()) {
                        if (tryToStore(inv, item)) {
                            itemCollected = true;
                        }
                    }
                }

                getPinSet().setOutput(0, itemCollected, this);
            });
        };
    }

    private boolean tryToStore(BlockBag bag, Item finalStack) {
        if (finalStack.isRemoved()) {
            return false;
        }
        ItemStack istack = finalStack.item().get().createStack();
        ItemStack result = bag.add(istack.copy());
        if (result.isEmpty()) {
            finalStack.remove();
            return true;
        }
        else if (result.getQuantity() != istack.getQuantity()) {
            finalStack.offer(Keys.REPRESENTED_ITEM, result.createSnapshot());
            return true;
        }
        return false;
    }

    private BlockBag getBlockBagAt(Location<World> loc) {
        if (containerType == null || loc.getBlockType() == containerType) {
            return BlockBagManager.getOrCreateCachedBlockBag(BlockBagManager.BlockBagType.SINGLE_INVENTORY, loc, 0, allowedContainerTypes);
        }
        return null;
    }

    private final int FAKE_ITEM_PICKUP_DELAY = 32767;

    /**
     * Get ItemStacks near the chest within a specified radius.
     * @param containerLoc Location of container itself
     * This method is specific to this IC.
     * @return
     */
    private List<EntityItem> getNearbyItemStacks(Location<World> containerLoc) {
        return LocationUtil.getNearbyItemsAsync(containerLoc, range.getRange(), itemPredicate)
                .stream()
                .filter(e -> !e.isDead && (e.getOwner() == null || e.getOwner().isEmpty()) && e.pickupDelay != FAKE_ITEM_PICKUP_DELAY)
                .collect(Collectors.toList());
    }

    @Override
    public CUIRegion getArea() {
        return WorldEditUtil.toSphereSelection(getBlock().getExtent(), getBlock().getPosition(), (int) range.getRange());
    }

    public static class Factory implements ICFactory<ChestCollector>, HybridIC{

        @Override
        public ChestCollector createInstance(Location<World> location) { return new ChestCollector(this, location );}

        @Override
        public String[][] getPinHelp() {
            return new String[][] {
                    new String[]{
                            "Chest Collector"
                    },
                    new String[]{
                            "Collects Items within specified range."
                    }
            };
        }

        @Override
        public String stVariant(){
            return "MCZ203";
        }
    }
}
