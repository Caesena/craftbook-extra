/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.block;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.ic.AbstractIC;
import com.minecraftonline.legacy.blockbags.MultiNearbyChestBlockBag;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.parsing.block.state.BlockStateParsers;
import com.minecraftonline.util.parsing.block.state.BlockStateRestrictions;
import com.minecraftonline.util.parsing.lineparser.blockstate.BlockStateParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.SimpleDualParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.bool.BooleanParserFactory;
import com.minecraftonline.util.parsing.lineparser.vector3i.OffsetParserFactory;
import com.minecraftonline.util.parsing.vector3i.BasicOffsetParser;
import com.minecraftonline.util.parsing.vector3i.Vector3iLimitations;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.function.Predicate;

/**
 * FLEX SET
 * ModelID: MCX206
 *
 * @author Brendan (doublehelix457)
 */
public class FlexSet extends AbstractIC implements AreaIC {

    private boolean hold = false;
    private BlockState type;
    private Location<World> location;
    private ItemStack item;

    public static final OffsetParserFactory OFFSET_PARSER_FACTORY = OffsetParserFactory.builder()
            .parser(BasicOffsetParser.INSTANCE)
            .limitation(Vector3iLimitations.maxInSingleDirection(9))
            .build();

    public static final BlockStateParserFactory BLOCK_STATE_PARSER_FACTORY = BlockStateParserFactory.builder()
            .parser(BlockStateParsers.TYPE_AT_OPTIONAL_DAMAGE)
            .restriction(BlockStateRestrictions.HAS_ITEM_FORM)
            .build();

    private static final SimpleDualParserFactory<Vector3i, BlockState> OFFSET_AND_BLOCK_FACTORY = new SimpleDualParserFactory<>(
            OFFSET_PARSER_FACTORY,
            BLOCK_STATE_PARSER_FACTORY,
            ":"
    );

    public static final BooleanParserFactory BOOLEAN_PARSER_FACTORY = BooleanParserFactory.builder()
            .string("h")
            .ignoreCase()
            .build();

    public FlexSet(ICFactory<FlexSet> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, OFFSET_AND_BLOCK_FACTORY.createBinder(this, (offset, blockState) -> {
                    this.location = getBackBlock().add(offset);
                    this.type = blockState;
                    this.item = IDUtil.getItemStackFromBlockState(blockState);
                }))
                .lineParser(3, BOOLEAN_PARSER_FACTORY.createBinder(this, hold -> this.hold = hold))
                .build();
    }

    @Override
    public void load(){
        super.load();
    }

    @Override
    public void onTrigger() {
        if (type == null) {
            return;
        }
        if(!type.getType().getItem().isPresent()) return;
        MultiNearbyChestBlockBag bag = getBlockBag(getBlock());
        if(bag == null) {
            for (Entity e : getBlock().getExtent().getNearbyEntities(getBlock().getPosition(), 5)) {
                if (e.getType() == EntityTypes.PLAYER) {
                    ((Player) e).sendMessage(Text.of(TextColors.RED, "You have no where to store the blocks!"));
                    break;
                }
            }
            return;
        }
        if(getPinSet().isAnyTriggered(this)){
            if(location.getBlockType() == type.getType()) return;
            Predicate<ItemStack> predicate = BlockPlacingIC.getPredicate(item);
            if(bag.has(predicate, item.getQuantity())) {
                if(bag.remove(predicate, item.getQuantity()) == 0) {
                    location.setBlock(type);
                    // TODO: refund if unsuccessful
                }
            }
            else {
                for (Entity e : getBlock().getExtent().getNearbyEntities(getBlock().getPosition(), 5)) {
                    if (e.getType() == EntityTypes.PLAYER) {
                        ((Player) e).sendMessage(Text.of(TextColors.RED, "You do not have enough blocks!"));
                        break;
                    }
                }
            }
        }else{
            if(!hold || location.getBlockType() != type.getType()) return;
            location.setBlockType(BlockTypes.AIR);
            if (location.getBlockType() != BlockTypes.AIR) return; // Blocked for some reason, i.e Region protection
            for(ItemStack left : bag.add(BlockPlacingIC.getPickupBlocks(item, item.getQuantity()))) {
                Item item = (Item) getBlock().getExtent().createEntity(EntityTypes.ITEM, getBlock().getPosition());
                item.offer(Keys.REPRESENTED_ITEM, left.createSnapshot());
                block.getExtent().spawnEntity(item);
            }
        }
    }

    private MultiNearbyChestBlockBag getBlockBag(Location<World> location){
        return (MultiNearbyChestBlockBag) BlockBagManager.getOrCreateCachedBlockBag(BlockBagManager.BlockBagType.MULTI_NEARBY_CHEST, location, MultiNearbyChestBlockBag.DEFAULT_RADIUS, MultiNearbyChestBlockBag.getChestBlockTypes());
    }

    @Nullable
    @Override
    public CUIRegion getArea() {
        return WorldEditUtil.toCuboidFromPoint(location);
    }

    public static class Factory implements ICFactory<FlexSet> {

        @Override
        public FlexSet createInstance(Location<World> location) { return new FlexSet(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Redstone"
                    },
                    new String[]{
                            "Place a block at a location"
                    }
            };
        }
    }
}
