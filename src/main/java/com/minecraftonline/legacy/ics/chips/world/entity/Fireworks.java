/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.legacy.ics.ProjectileLaunchingIC;
import com.minecraftonline.util.nms.EntityUtil;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.ICFactory;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.UUID;

/**
 * FIREWORKS
 * Model ID: MC1250
 *
 * @author Brendan (doublehelix457)
 * @author sk89q (Legacy)
 */

public class Fireworks extends ProjectileLaunchingIC {

    public Fireworks(ICFactory<Fireworks> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if(getBlock().getBlockY() >= 125) throw new InvalidICException("IC was placed too high! Build below Y: 125!");
    }

    @Override
    public void onTrigger() {
        if(getPinSet().isAnyTriggered(this)){
            Entity arrow = launch(getBlock());
            Firework firework = new Firework(arrow);
            (new Thread(firework)).start();
        }
    }

    /**
     * Create/ Shoot Arrow Entity
     */
    protected Entity launch(Location<World> location){
        return shoot(1.05F,20,50F,EntityTypes.TIPPED_ARROW,location.setPosition(new Vector3d(
                location.getPosition().getX()-.05D,
                location.getPosition().getY(),
                location.getPosition().getZ()-0.5D)));
    }

    /**
     * Makes TNT go boom.
     *
     * @param location A Location at specific coords where the tnt should be detonated
     */
    protected static void explodeTNT(Location<World> location) {
        Entity tnt = location.createEntity(EntityTypes.PRIMED_TNT);
        tnt.setLocation(location);
        location.spawnEntity(tnt);
        EntityUtil.onUpdate(tnt);
    }

    /**
     * Single firework.
     */
    public class Firework implements Runnable {
        /**
         * Last arrow Y position for comparison.
         */
        private double lastY;
        /**
         * Arrow.
         */
        private final Entity arrow;
        /**
         * Expiration time.
         */
        private final long expireTime;

        /**
         * Construct the object.
         *
         * @param arrow
         */
        public Firework(Entity arrow) {
            expireTime = System.currentTimeMillis() + 5000;
            this.arrow = arrow;
            lastY = arrow.getLocation().getY();
        }

        /**
         * Runs the firework.
         */
        public void run() {
            try {
                while (true) {

                    final double arrowY = arrow.getLocation().getY();

                    if (arrowY < lastY) {
                        Task.builder()
                                .name("CBX Fireworks " + UUID.randomUUID().toString())
                                .execute(() -> {
                                    arrow.remove();

                                    //arrow.bi.f(arrow);
                                    // Make TNT explode
                                    // explodeTNT(arrow.getLocation());
                                    // explodeTNT(arrow.aL,
                                    // arrowX + r.nextDouble() * 2 - 1,
                                    // arrowY + r.nextDouble() * 1,
                                    // arrowZ + r.nextDouble() * 2 - 1);
                        }).submit(CraftBookPlugin.spongeInst());

                        break;
                    } else {
                        lastY = arrowY;
                    }

                    if (System.currentTimeMillis() > expireTime) {
                        break;
                    }

                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
            }
        }
    }

    public static class Factory implements ICFactory<Fireworks>, RestrictedIC {

        @Override
        public Fireworks createInstance(Location<World> location) { return new Fireworks(this,location);}

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[] {
                          "Input"
                    },
                    new String[] {
                           "Shoots TNT-based Fireworks"
                    }
            };
        }
    }


}
