/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.logic;

import com.sk89q.craftbook.sponge.ICFactory;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * HALF ADDER
 * ModelID: MC4010
 *
 * @author Lymia (Legacy)
 * @author Brendan (doublehelix457)
 */
public class HalfAdder extends AbstractIC {

    boolean B, C;
    public HalfAdder(ICFactory<HalfAdder> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void onTrigger() {
        B = getPinSet().getInput(1,this);
        C = getPinSet().getInput(2,this);

        getPinSet().setOutput(0,B^C,this);
        getPinSet().setOutput(1,B&C,this);
        getPinSet().setOutput(2,B&C,this);
    }

    public static class Factory implements ICFactory<HalfAdder>, RestrictedIC {

        @Override
        public HalfAdder createInstance(Location<World> location) { return new HalfAdder(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
              new String[]{
                    "Nothing",
                    "Input",
                    "Input"
              },
              new String[]{
                     "Output",
                     "Output",
                     "Output"
              }
            };
        }
    }
}
