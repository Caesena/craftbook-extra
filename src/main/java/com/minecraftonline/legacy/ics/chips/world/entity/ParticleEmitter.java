/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.util.IDUtil;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleOptions;
import org.spongepowered.api.effect.particle.ParticleType;
import org.spongepowered.api.effect.particle.ParticleTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.Optional;

/**
 * MCX250
 * PARTICLE
 * Creates particles. Can be offset.
 * @author tyhdefu
 */
public class ParticleEmitter extends AbstractIC {

    ParticleEffect effect = null;
    Vector3d offset = new Vector3d();

    public ParticleEmitter(ICFactory<ParticleEmitter> icFactory, Location<World> block) {super(icFactory, block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);

        for (ParticleType type : Sponge.getRegistry().getAllForMinecraft(ParticleType.class)) {
            CraftBookPlugin.spongeInst().getLogger().info("mc particle: " + type.getName());
        }

        for (ParticleType type : Sponge.getRegistry().getAllForSponge(ParticleType.class)) {
            CraftBookPlugin.spongeInst().getLogger().info("sponge particle" + type.getName());
        }

        String line4 = lines.get(3).toPlain();
        if (!line4.isEmpty()) {
            if ((line4.length() > 2))
                throw new InvalidICException("Invalid offset. X|Y|Z then number");
            switch (line4.charAt(0)) {
                case 'X': break;
                case 'Y': break;
                case 'Z': break;
                default: {
                    throw new InvalidICException("Invalid offset. X|Y|Z then number");
                }
            }
            try {
                Integer.parseInt(line4.substring(1));
            } catch (NumberFormatException e) {throw new InvalidICException("Invalid number for offset");}
        }
        String line3 = lines.get(2).toPlain().toLowerCase();

        String[] parts = line3.split(":", 2);

        Optional<ParticleType> optParticleType = Sponge.getRegistry().getType(ParticleType.class, parts[0]);
        if (!optParticleType.isPresent())
            throw new InvalidICException("Invalid particle type");
        if (optParticleType.get().equals(ParticleTypes.BREAK_BLOCK)) {
            if (parts.length == 1)
                return;
            String[] blockparts = parts[1].split("@", 2);
            Optional<BlockType> blockType = IDUtil.getBlockTypeFromAnyID(blockparts[0]);
            if (!blockType.isPresent())
                throw new InvalidICException("Invalid blocktype specified");

            if (blockparts.length == 2) {
                try {
                    Integer.parseInt(blockparts[1]);
                } catch (NumberFormatException e) { throw new InvalidICException("");}
            }
            return;
        }
        else if (parts.length > 1){
            throw new InvalidICException("Too many parameters for this effect.");
        }
    }

    @Override
    public void load() {
        super.load();
        String line4 = getLine(3);
        if (!line4.isEmpty()) {
            switch (line4.charAt(0)) {
                case 'X': {
                    offset = new Vector3d(1,0,0);
                    break;
                }
                case 'Y': {
                    offset = new Vector3d(0,1,0);
                    break;
                }
                case 'Z': {
                    offset = new Vector3d(0,0,1);
                    break;
                }
            }
            offset = offset.mul(Integer.parseInt(line4.substring(1)));
        }
        String line3 = getLine(2);
        String[] parts = line3.split(":", 2);
        ParticleType effecttype = null;
        effecttype = Sponge.getRegistry().getType(ParticleType.class, parts[0]).get();

        if (effecttype.equals(ParticleTypes.BREAK_BLOCK)) {
            BlockState blockState = null;
            if (parts.length == 2) {
                String[] blockparts = parts[1].split("@", 2);
                int dmg = 0;
                if (blockparts.length == 2)
                    dmg = Integer.parseInt(blockparts[1]);

                BlockType type = IDUtil.getBlockTypeFromAnyID(blockparts[0]).get();
                blockState = IDUtil.createBlockStateWithDamage(type, dmg);
            }
            else {
                blockState = BlockState.builder().blockType(BlockTypes.DIRT).build();
            }
            effect = ParticleEffect.builder().type(effecttype).option(ParticleOptions.BLOCK_STATE, blockState).build();
        }
        else {
            effect = ParticleEffect.builder().type(effecttype).build();
        }
    }

    @Override
    public void onTrigger() {
        getBackBlock().getExtent().spawnParticles(effect, getBackBlock().getPosition().add(0.5,0,0.5).add(offset));
    }

    public static class Factory implements ICFactory<ParticleEmitter>, RestrictedIC {

        @Override
        public ParticleEmitter createInstance(Location<World> location){return new ParticleEmitter(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            ""
                    }
            };
        }
    }
}
