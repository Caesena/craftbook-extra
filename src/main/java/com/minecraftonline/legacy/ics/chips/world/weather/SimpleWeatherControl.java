/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.weather;

import com.minecraftonline.util.nms.PacketUtil;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.mode.Modes;
import net.minecraft.network.Packet;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.weather.Weathers;

import java.util.List;

/**
 * WEATHER CONTROL
 * ModelID: MCX233
 *
 * @author Shaun (Sturmeh)
 * @author Brendan (doublehelix457)
 *
 * SEARGE:
 * Packet<?>
 */
public class SimpleWeatherControl extends AbstractIC {

    int duration;
    Packet<?> rain;
    Packet<?> noRain;

    public SimpleWeatherControl(ICFactory<SimpleWeatherControl> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        String id = lines.get(2).toPlain();

        if (id.length() > 0)
        {
            try
            {
                int duration = Integer.parseInt(id);
                if(duration < 1 || duration > 24000)
                    throw new InvalidICException("Duration must be from 1 to 24000");
            }
            catch(NumberFormatException e)
            {
                throw new InvalidICException("The third line must be a number.");
            }
        }
    }

    @Override
    public void load(){
        super.load();
        if(getLine(2).length() > 0)
            duration = Integer.parseInt(getLine(2));
        else
            duration = 24000;

        rain = new PacketUtil.PacketBuilder().asGameStateChange(2,0).build();
        noRain = new PacketUtil.PacketBuilder().asGameStateChange(1,0).build();
    }

    @Override
    public void onTrigger() {
        if(getPinSet().isAnyTriggered(this)){
            PacketUtil.sendPacketToAllPlayers(getBlock().getExtent(),noRain);
        }else{
            PacketUtil.sendPacketToAllPlayers(getBlock().getExtent(),rain);
        }

        if(getPinSet().isAnyTriggered(this)){
            if(getMode().getType() == Modes.THUNDER_STORM) {
                getBlock().getExtent().setWeather(Weathers.THUNDER_STORM,duration);
                return;
            }
            getBlock().getExtent().setWeather(Weathers.RAIN,duration);
        }

        getPinSet().setOutput(0,getPinSet().getInput(0,this),this);
    }

    public static class Factory implements ICFactory<SimpleWeatherControl>, RestrictedIC {
        @Override
        public SimpleWeatherControl createInstance(Location<World> location) { return new SimpleWeatherControl(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                        "On for weather, off for no weather."
                    },
                    new String[]{
                            "Output if input is high."
                    }
            };
        }
    }
}
