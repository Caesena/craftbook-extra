/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.minecraftonline.ic.AbstractIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.nms.LocationWorldUtil;
import com.minecraftonline.util.parsing.BandLineParser;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.mode.Modes;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.Humanoid;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;

public class Transporter extends AbstractIC {

    private String band;

    public Transporter(ICFactory<? extends IC> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, BandLineParser.bandName(band -> this.band = band))
                .build();
    }

    @Override
    public void onTrigger() {

        if (!getPinSet().isAnyTriggered(this)) {
            return;
        }

        // Get destination.
        final Destination destination = Destination.getDestination(this.band);
        if (destination == null) {
            return;
        }
        final Location<World> origin;

        if (getMode().getType() == Modes.TELEPORT_PAD || getMode().getType() == Modes.TELEPORT_PAD_FORCED_PRESSURE_PLATE) {
            final Direction forward = getBlock().require(Keys.DIRECTION);
            origin = getBlockCentre().getRelative(forward).add(0, 2, 0);
        }
        else {
            final Optional<Location<World>> optLoc = LocationUtil.findEmptySpot(getBackBlockCentre());
            if (!optLoc.isPresent()) {
                return;
            }
            origin = optLoc.get();
        }

        boolean didAThing = false;
        for (final Humanoid humanoid : LocationUtil.getNearbyEntitiesByType(origin, 0.9, Humanoid.class)) {
            if (destination.teleportTo(humanoid.getBaseVehicle())) {
                didAThing = true;
                if (getMode().getType() == Modes.TELEPORT_PAD_FORCED_PRESSURE_PLATE) {
                    Location<World> footLoc = humanoid.getLocation();
                    final BlockType blockType = footLoc.getBlockType();
                    if (blockType == BlockTypes.WOODEN_PRESSURE_PLATE
                            || blockType == BlockTypes.STONE_PRESSURE_PLATE
                            || blockType == BlockTypes.LIGHT_WEIGHTED_PRESSURE_PLATE
                            || blockType == BlockTypes.HEAVY_WEIGHTED_PRESSURE_PLATE) {
                        footLoc.offer(Keys.POWERED, false);
                        LocationWorldUtil.updatePressurePlate(footLoc);
                    }
                }
            }
        }
        getPinSet().setOutput(0, didAThing, this);
    }

    public static class Factory implements ICFactory<Transporter>, RestrictedIC {
        @Override
        public Transporter createInstance(Location<World> location) { return new Transporter(this, location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Redstone"
                    },
                    new String[0]
            };
        }
    }
}
