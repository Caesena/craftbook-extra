/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.sensor;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.ic.AbstractIC;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.nms.LocationWorldUtil;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerParser;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerRestrictions;
import com.minecraftonline.util.parsing.lineparser.range.RangeLimitations;
import com.minecraftonline.util.parsing.lineparser.range.RangeLineParserFactory;
import com.minecraftonline.util.parsing.rangeparser.RangeParsers;
import com.minecraftonline.util.parsing.rangeparser.RelativeRange;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.SelfTriggeringIC;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * SENSE LIGHT
 * Model ID: MC1262
 *
 * @author sk89q (Legacy)
 * @author Brendan (doublehelix457)
 */

public class SenseLight extends AbstractIC implements SelfTriggeringIC {

    public SenseLight(ICFactory<SenseLight> icFactory, Location<World> block){super(icFactory, block);}

    private static final IntegerParserFactory MIN_LIGHT_PARSER_FACTORY = IntegerParserFactory.builder()
            .parser(new IntegerParser(Text.of("MinLight")))
            .restriction(IntegerRestrictions.inRange(0, 15))
            .build();

    private static final RangeLineParserFactory RANGE_PARSER_FACTORY = RangeLineParserFactory.builder()
            .parser(RangeParsers.OPTIONAL_X_OPTIONAL_Y_OPTIONAL_Z_OFFSET)
            .limitation(RangeLimitations.maxOffsetInSingleDirection(20))
            .withDefault(RelativeRange.builder().yOffset(1).build())
            .build();

    private int minLight;
    private int x,y,z;

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, MIN_LIGHT_PARSER_FACTORY.createBinder(this, minLight -> this.minLight = minLight))
                .lineParser(3, RANGE_PARSER_FACTORY.createBinder(this, range -> {
                    Vector3d offset = range.getXyzOffset();
                    Vector3i backBlock = getBackBlock().getBlockPosition();
                    this.x = backBlock.getX() + (int) offset.getX();
                    this.y = backBlock.getY() + (int) offset.getY();
                    this.z = backBlock.getZ() + (int) offset.getZ();
                }))
                .build();
    }

    @Override
    public void onTrigger() {
        if(!getPinSet().isAnyTriggered(this)) return;
        senseLight();
    }

    @Override
    public void think() { senseLight(); }

    public void senseLight() {
        getPinSet().setOutput(0,LocationWorldUtil.getLightLevel(getBlock().getExtent(),x,y,z) >= minLight, this);
    }

    public static class Factory implements ICFactory<SenseLight>, HybridIC {

        @Override
        public String stVariant() { return "MC0262"; }

        @Override
        public SenseLight createInstance(Location<World> location) { return new SenseLight(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[0][];
        }
    }
}
