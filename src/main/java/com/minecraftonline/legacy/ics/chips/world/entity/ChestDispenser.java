/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.ic.AbstractIC;
import com.minecraftonline.legacy.blockbags.NearbyChestBlockBag;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicate;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicateParsers;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerRestrictions;
import com.minecraftonline.util.parsing.lineparser.generic.integer.OptionalIntegerParser;
import com.minecraftonline.util.parsing.lineparser.itempredicate.ItemPredicateParserFactory;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collections;
import java.util.List;

/**
 * Container Dispenser
 * ModelID: MCX202
 *
 * @author Brendan  (doublehelix457)
 * @author Stefan Steinheimer (nosefish)
 * @author sk89q (Legacy)
 *
 */
public class ChestDispenser extends AbstractIC {

    public ChestDispenser(ICFactory<ChestDispenser> icFactory, Location<World> block){super(icFactory,block);}

    private ItemPredicate itemPredicate;
    int quantity = 1;
    BlockBag bag;

    private static final ItemPredicateParserFactory ITEM_PARSER_FACTORY = ItemPredicateParserFactory
            .builder()
            .parser(ItemPredicateParsers.OPTIONAL_ITEM_TYPE_WITH_MINUS_1_COLON_DAMAGE)
            .build();

    private static final IntegerParserFactory QUANTITY_PARSER_FACTORY = IntegerParserFactory.builder()
            .parser(new OptionalIntegerParser(Text.of("Quantity")))
            .restriction(IntegerRestrictions.atLeast(1))
            .restriction(IntegerRestrictions.atMost(9*6*64)) // Double chest.
            .withDefault(1)
            .build();

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, ITEM_PARSER_FACTORY.createBinder(this, itemPredicate -> this.itemPredicate = itemPredicate))
                .lineParser(3, QUANTITY_PARSER_FACTORY.createBinder(this, quantity -> this.quantity = quantity))
                .build();
    }

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
    }

    @Override
    public void load(){
        super.load();

        //if(ncbb == null){ this.unload();}
        bag = NearbyChestBlockBag.fromSourcePositionNearby(getBlock());
    }

    @Override
    public void onTrigger() {
        if (bag == null) {
            bag = NearbyChestBlockBag.fromSourcePositionNearby(getBlock());
            if (bag == null) {
                return;
            }
        }
        if(!getPinSet().isAnyTriggered(this)){ return;} //Make sure it was triggered when powered

        Vector3i pos = getBackBlock().getBlockPosition();
        int x = pos.getX();
        int z = pos.getZ();
        int maxY = Math.min(255, pos.getY() + 10);
        World world = getBlock().getExtent();
        //if (! world.isChunkLoaded(x, 0, z)) return;



        for (int y = pos.getY() + 1; y <= maxY; y++) {
            Location<World> loc = world.getLocation(x,y,z);
            if (loc.getBlockType() == BlockTypes.AIR) {
                int quantity = this.quantity;
                while (quantity > 0) {
                    quantity = retrieveAndDispense(loc, quantity);
                }
                return;
            }
        }

    }

    private int retrieveAndDispense(Location<World> loc, int quantity) {
        ItemStack stack = bag.findStack(itemPredicate, quantity);

        if (stack == null) {
            return 0;
        }

        Entity entity = loc.createEntity(EntityTypes.ITEM);
        Item item = (Item) entity;
        item.offer(Keys.REPRESENTED_ITEM, stack.createSnapshot());
        boolean spawned = loc.spawnEntity(item);
        if(spawned){
            bag.remove(Collections.singletonList(stack));
        }
        return quantity - stack.getQuantity();
    }

    // Removed since i think there is no use anymore
    /* itemID > 0 && !(itemID >= 21 && itemID <= 34) && itemID != 36
    public static Set<ItemType> banneditems = new HashSet<>(Arrays.asList(
            ItemTypes.LAPIS_ORE,
            ItemTypes.LAPIS_BLOCK,
            ItemTypes.DISPENSER,
            ItemTypes.SANDSTONE,
            ItemTypes.NOTEBLOCK,
            ItemTypes.BED,
            ItemTypes.GOLDEN_RAIL,
            ItemTypes.DETECTOR_RAIL,
            ItemTypes.STICKY_PISTON,
            ItemTypes.WEB,
            ItemTypes.TALLGRASS,
            ItemTypes.DEADBUSH,
            ItemTypes.PISTON));*/

    /**
     * Get an item from its name or ID.
     *
     * @param id
     * @return
     */
    private int getItem(String id) {
        try {
            return Integer.parseInt(id.trim());
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static class Factory implements ICFactory<ChestDispenser>{
        @Override
        public ChestDispenser createInstance(Location<World> location) { return new ChestDispenser(this,location);}

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                        "Input"
                    },
                    new String[]{
                            "Outputs an item from the chest."
                    }
            };
        }
    }
}
