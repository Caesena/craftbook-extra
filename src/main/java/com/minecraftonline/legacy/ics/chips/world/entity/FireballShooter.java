/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.legacy.ics.ProjectileLaunchingIC;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

/**
 * FIREBALL
 * Model ID: MCX246
 *
 * @author Brendan (doublehelix457)
 * @author sk89q (Legacy)
 */
public class FireballShooter extends ProjectileLaunchingIC {

    public FireballShooter(ICFactory<FireballShooter> icFactory,Location<World> block){ super(icFactory,block);}

    float power = 1.0F;
    float speed = 0.2F;
    float rotation = 0F;
    float pitch = 0F;
    Location<World> location;
    Vector3d pitchYawRoll;
    Vector3d velocity;


    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        String dest = lines.get(2).toPlain();
        String settings = lines.get(3).toPlain();

        try
        {
            if(!dest.isEmpty())
            {
                String[] args = dest.split(":", 2);

                double speed = Double.parseDouble(args[0]);
                if(speed > 5D || speed <= 0D)
                    throw new InvalidICException("Speed must be a number between 0 and 5");

                if(args.length > 1)
                {
                    float power = Float.parseFloat(args[1]);
                    if(power > 10 || power < 0.1)
                        throw new InvalidICException("Power must be a number from 0.1 to 10");
                }
            }
        }
        catch(NumberFormatException e)
        {
            throw new InvalidICException("3rd line must be numbers "+e);
        }

        try
        {
            if(!settings.isEmpty())
            {
                String[] args = settings.split(":", 2);
                float rotation = Float.parseFloat(args[0]);

                if(rotation > 90 || rotation < -90)
                    throw new InvalidICException("rotation must be a number from -90 to 90");

                if(args.length > 1)
                {
                    float pitch = Float.parseFloat(args[1]);

                    if(pitch > 1 || pitch < -1)
                        throw new InvalidICException("pitch must be a number from -1 to 1");
                }

            }
        }
        catch(NumberFormatException e)
        {
            throw new InvalidICException("4th line must be numbers");
        }
    }

    @Override
    public void load(){
        super.load();
        String dest = getLine(2);
        String settings = getLine(3);

        try {
            if (!dest.isEmpty()) {
                String[] args = dest.split(":", 2);

                speed = Float.parseFloat(args[0]);
                if(args.length > 1)
                    power = Float.parseFloat(args[1]);
            }

            if (!settings.isEmpty()) {
                String[] args = settings.split(":", 2);

                rotation = Float.parseFloat(args[0]);
                if(args.length > 1)
                    pitch = Float.parseFloat(args[1]);
            }
        } catch (NumberFormatException e) {
        }

        //Create a velocity that launches an entity away from the back of the IC block.
        velocity = new Vector3d(getBackBlock().getBlockX()-getBlock().getBlockX(),0,getBackBlock().getBlockZ()-getBlock().getBlockZ());

        location = new Location<World>(getBackBlock().getExtent(), getBackBlock().getBlockX()+velocity.getX(), getBackBlock().getBlockY()+1, getBackBlock().getBlockZ()+velocity.getFloorZ());
        pitchYawRoll = new Vector3d(pitch*=90,rotation += SignUtil.getFacing(getBlock()).asOffset().getX(),0);
    }

    @Override
    public void onTrigger() {
        if(getPinSet().isAnyTriggered(this)){
            Entity ent = launch(location,velocity, EntityTypes.FIREBALL);
            ent.setRotation(pitchYawRoll);
        }
    }


    public static class Factory implements ICFactory<FireballShooter>, RestrictedIC {
        @Override
        public FireballShooter createInstance(Location<World> location) { return new FireballShooter(this,location);}

        @Override
        public String[][] getPinHelp() {
            return new String[][] {
                    new String[]{
                      "Input"
                    },
                    new String[]{
                        "Launches a Ghast Fireball"
                    }
            };
        }
    }

}
