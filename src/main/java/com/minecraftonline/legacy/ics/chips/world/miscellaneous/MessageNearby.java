/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.miscellaneous;

import com.flowpowered.math.vector.Vector3i;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

/**
 * MESSAGE NEARBY
 * ModelID: MCX512
 *
 * @author Brendan (doublhelix457)
 * @author Drathus??? (Legacy)
 */
public class MessageNearby extends AbstractIC {
    private static final String TITLE = "MessageNearby";
    private String message;
    private boolean shouldLoadLine = false;
    protected int distance = 64;

    public MessageNearby(ICFactory<MessageNearby> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if(!lines.get(0).isEmpty())
        {
            try
            {
                distance = Integer.parseInt(lines.get(0).toPlain());
                if(distance < 1 || distance > 64)
                    throw new InvalidICException("The distance must be a number from 1 to 64");
            }
            catch(NumberFormatException e)
            {
                throw new InvalidICException("The distance must be a number from 1 to 64");
            }
        }

        if(lines.get(2).isEmpty() && lines.get(3).isEmpty())
        {
            throw new InvalidICException("A message on the 3rd or 4th line is required.");
        }

        shouldLoadLine = true;
    }

    protected void processMessage(String message)
    {
        if(distance > 64) distance = 64;

        for(Player player: getBlock().getExtent().getPlayers())
        {
            Vector3i loc = player.getLocation().getBlockPosition();
            int playerDist = (int)Math.floor(loc.distance(getBackBlock().getBlockPosition()));
            if(getBlock().getExtent() == player.getWorld() && playerDist <= distance)
            {
                String msg = message.replaceAll("%p", player.getName());
                String[] lines = msg.split("/n", 5);
                for(String line : lines)
                {
                    player.sendMessage(Text.of(line));
                }
            }
        }
    }

    @Override
    public void load() {
        super.load();
        if (shouldLoadLine) {
            setLine(0, Text.of(TITLE + distance));
        }
        message = getLine(2) + getLine(3);
        int length = TITLE.length();
        String line1 = getLine(0);
        if (line1.length() == length) {
            distance = 64;
        }
        else {
            distance = Integer.parseInt(line1.substring(length));
        }
    }

    @Override
    public void onTrigger() {
        if(!getPinSet().isAnyTriggered(this)){
            getPinSet().setOutput(0,false,this);
            return;
        }
        processMessage(message);
        getPinSet().setOutput(0,true,this);
    }

    public static class Factory implements ICFactory<MessageNearby>, RestrictedIC {
        @Override
        public MessageNearby createInstance(Location<World> location) { return new MessageNearby(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Input"
                    },
                    new String[]{
                            "Output if message was sent."
                    }
            };
        }

        @Override
        public boolean usesFirstLine() {
            return true;
        }
    }
}
