/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.entity.DetailedEntityType;
import com.minecraftonline.util.entity.EntityParsingUtil;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.cause.entity.damage.DamageTypes;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSource;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/*
* Hit Mob Above
* ModelID: MCX132/MCU132 (ST)
*
* Damages mobs above the ic, in the first empty space it can find.
*
* @author tyhdefu
 */

public class HitMobAbove extends AbstractStIC {

    private DetailedEntityType detailedEntityType;

    private int damage = 0;

    public HitMobAbove(ICFactory<HitMobAbove> icFactory, Location<World> block) {super(icFactory, block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if (lines.get(3).toPlain().length() != 0) {
            try {
                damage = Integer.parseInt(lines.get(3).toPlain());
                if (damage <= 0)
                    throw new InvalidICException("Damage must be more than 0");
            }
            catch (NumberFormatException e) {
                throw new InvalidICException("Damage on line 4 must be a number");
            }
        }
        String line3 = lines.get(2).toPlain();
        if (line3.length() == 0)
            return;
        try {
            EntityParsingUtil.parseEntity(line3);
        } catch (DetailedEntityType.Error e) {
            throw new InvalidICException(e.getMessage());
        }
    }

    @Override
    public void onTrigger() {
        // thinking will always be false when not ST
        if (isSt && !thinking)
            return;
        if (!getPinSet().isAnyTriggered(this))
            return;
        Location<World> loc = getBackBlock().add(0,1,0);
        Optional<Location<World>> optloc = LocationUtil.findEmptySpot(loc);
        if (!optloc.isPresent())
            return; // no empty spot above.
        loc = optloc.get().add(0.5, 0, 0.5);
        Collection<Entity> entities = loc.getExtent().getNearbyEntities(loc.getPosition(), 0.5);
        boolean foundentity = false;
        for (Entity entity : entities) {
            if (detailedEntityType.matches(entity)) {
                entity.damage(damage, DamageSource.builder().type(DamageTypes.CUSTOM).bypassesArmor().build());
                foundentity = true;
            }
        }
        getPinSet().setOutput(0, foundentity, this);
    }

    @Override
    public void load() {
        super.load();
        if (getLine(3).length() == 0) {
            setLine(3, Text.of(1));
            damage = 1;
        }
        else
            damage = Integer.parseInt(getLine(3));
        String line3 = getLine(2);
        if (line3.length() == 0) {
            detailedEntityType = new DetailedEntityType() {
                @Override
                public boolean matches(Entity entity) {
                    return entity instanceof Creature;
                }
            };
        }
        else {
            try {
                detailedEntityType = EntityParsingUtil.parseEntity(line3);
            } catch (DetailedEntityType.Error error) {
                error.printStackTrace();
            }
        }
    }

    public static class Factory implements ICFactory<HitMobAbove>, HybridIC, RestrictedIC {
        @Override
        public String stVariant() {
            return "MCU132";
        }

        @Override
        public HitMobAbove createInstance(Location<World> location){return new HitMobAbove(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Outputs if a mob was damaged"
                    }
            };
        }
    }
}
