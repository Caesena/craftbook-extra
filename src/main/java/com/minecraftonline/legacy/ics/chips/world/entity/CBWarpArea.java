/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.legacy.ics.chips.world.sensor.InArea;
import com.minecraftonline.legacy.mechanics.cbwarps.CBWarp;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.entity.DetailedEntityType;
import com.minecraftonline.util.entity.EntityParsingUtil;
import com.minecraftonline.util.nms.LocationWorldUtil;
import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.rangeparser.RangeParsers;
import com.minecraftonline.util.parsing.rangeparser.RelativeRange;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import org.apache.commons.lang3.StringUtils;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.AABB;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.List;

/**
 * MCX144
 * AREA CBWARP
 * Warps everything matching filter within area to a cbwarp
 *
 * @author tyhdefu
 */
public class CBWarpArea extends AbstractStIC implements AreaIC {

    private DetailedEntityType entityType;

    private String warpName;
    private boolean activated = false;

    public Vector3d min;
    public Vector3d max;

    public CBWarpArea(ICFactory<CBWarpArea> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        String line1 = lines.get(0).toPlain();

        // Check for warp.
        if (!CBWarp.exists(lines.get(2).toPlain()))
            throw new InvalidICException("Line 3 is not a valid CBWarp");

        if (lines.get(3).toPlain().length() != 0) {
            String err = InArea.isValidDimensions(lines.get(3).toPlain(), "3");
            if (err != null) { throw new InvalidICException(err); }
        }

        if (StringUtils.isBlank(line1)) {
            return;
        }

        if (line1.charAt(0) != '@' || line1.length() < 2) {
            throw new InvalidICException("Must begin with @ or be blank");
        }
        String entityType = lines.get(0).toPlain().substring(1);
        try {
            EntityParsingUtil.parseStackedAndNbt(entityType);
        } catch (DetailedEntityType.Error error) {
            throw new InvalidICException(error.getMessage());
        }
    }

    @Override
    public void onTrigger() {
        if (!thinking) {
            // Actual lever trigger, not self triggering.
            activated = getPinSet().isAnyTriggered(this); // Update cache.

            if (isSt) {
                return; // Self trigger have no need to continue.
            }
        }

        if (!activated)
            return;

        CBWarp warp = CBWarp.getWarp(warpName);

        if (warp == null) {
            return;
        }

        if (!LocationWorldUtil.isAllLoaded(getBlock().getExtent(), min.toInt(), max.toInt())) {
            return;
        }

        Collection<Entity> foundentities;

        foundentities = getBlock().getExtent().getIntersectingEntities(new AABB(min, max));

        foundentities.stream()
                .filter(entity -> !entity.getVehicle().isPresent() && entityType.matches(entity))
                .forEach(entity -> warp.tpToWarp(entity, null, null));
    }

    @Override
    public void load() {
        super.load();

        activated = getPinSet().isAnyTriggered(this);

        warpName = getLine(2);
        CBWarp warp = CBWarp.getWarp(warpName);
        if (warp == null) {
            CraftBookPlugin.spongeInst().getLogger().warn("CBWarpArea at: " + getBlock() + " has an invalid warp name");
            return;
        }

        if (StringUtils.isBlank(getLine(0)) || getLine(0).equals("CBWARP")) { // Legacy
            entityType = new DetailedEntityType.Any();
        }
        else {
            try {
                entityType = EntityParsingUtil.parseStackedAndNbt(getLine(0).substring(1));
            }
            catch (DetailedEntityType.Error e) { CraftBookPlugin.spongeInst().getLogger().error(e.getMessage() + " error in In CBWarpArea ic, at: " + getBlock());}
        }

        RelativeRange range = null;
        if (getLine(3).length() != 0) {
            try {
                range = RangeParsers.CBWARP_AREA.parse(getLine(3));
            } catch (Parameter.ParsingException e) {
                CraftBookPlugin.spongeInst().getLogger().error("Failed to parse range on load!");
            }
        }
        if (range == null) {
            range = RelativeRange.builder().width(3).height(1).length(3).yOffset(1).build();
        }

        Vector3d startingPos = getBlockCentre().getPosition().add(getBackBlockCentre().getPosition()).div(2)
                .add(0, 0.5, 0);
        Direction looking = getBlock().require(Keys.DIRECTION).getOpposite();

        this.min = range.getStart(startingPos, looking);
        this.max = range.getEnd(startingPos, looking);
    }

    @Override
    public CUIRegion getArea() {
        Vector3d minPos = min.min(max);
        Vector3d maxPos = min.max(max);
        return WorldEditUtil.toCuboidSelection(getBlock().getExtent(), minPos, maxPos, true);
    }

    public static class Factory implements ICFactory<CBWarpArea>, HybridIC, RestrictedIC {

        @Override
        public CBWarpArea createInstance(Location<World> location) { return new CBWarpArea(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Redstone"
                    },
                    new String[]{
                            "Teleports entities to a CBWarp"
                    }
            };
        }
        @Override
        public boolean usesFirstLine() {return true;}

        @Override
        public String stVariant() {
            return "MCU144";
        }
    }
}
