/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.weather;

import com.minecraftonline.util.PermissionUtil;
import com.minecraftonline.util.nms.PacketUtil;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import net.minecraft.network.Packet;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

/**
 *  FALSE WEATHER
 * Model ID: MCX235
 *
 * @author Brendan  (doublehelix457)
 * @author Shaun (sturmeh)
 *
 * SEARGE:
 * Packet<?>
 **/
public class FalseWeather extends AbstractIC {

    Packet rain;
    Packet noRain;

    public FalseWeather(ICFactory<FalseWeather> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if (getLine(2).length() != 0 &&
                (getLine(2).charAt(1) != ':'
                        || getLine(2).contains(" ")
                )) {
            throw new InvalidICException("Incorrect format on line 3.");
        }

        if (getLine(3).length() != 0) {
            throw new InvalidICException("Fourth line needs to be blank");
        }
    }

    @Override
    public void load(){
        super.load();
        rain = new PacketUtil.PacketBuilder().asGameStateChange(2, 0).build();
        noRain = new PacketUtil.PacketBuilder().asGameStateChange(1, 0).build();
    }

    @Override
    public void onTrigger(){
        int type = 0;
        String id = "";
        if(getLine(2).length() > 0 && getLine(2).charAt(1) == ':')
        {
            switch(getLine(2).charAt(0))
            {
                case 'G':
                case 'g':
                    type++;
                case 'P':
                case 'p':
                    type++;
                    id = getLine(2).substring(2);
                    break;
            }
        }

        Packet packet;
        if(!getPinSet().isAnyTriggered(this))
            packet = noRain;
        else if(getBlock().getExtent().getProperties().isRaining())
            return;
        else
            packet = rain;

        boolean out = getPinSet().isTriggered(0,this);
        switch(type)
        {
            case 0:
                PacketUtil.sendPacketToAllPlayers(getBlock().getExtent(),packet);
                break;
            case 1:
                Player player = null;
                for(Player anyPerson : Sponge.getServer().getOnlinePlayers()){
                    if(anyPerson.getName().contains(id)) player = anyPerson;
                }
                if(player != null)
                    PacketUtil.sendPacketToPlayer(player, packet);
                else
                    out = false;
                break;
            case 2:
                for(Player plyr : Sponge.getServer().getOnlinePlayers())
                {
                    if(PermissionUtil.isInGroup(plyr, getLine(2).split(":")[1]))
                        PacketUtil.sendPacketToPlayer(plyr, packet);
                }
                break;
        }

        getPinSet().setOutput(0,out,this);
    }

    public static class Factory implements ICFactory<FalseWeather>, RestrictedIC {
        @Override
        public FalseWeather createInstance(Location<World> location){ return new FalseWeather(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                        "Input"
                    },
                    new String[] {
                        "Causes players to see rain/snow, when it isn't raining/snowing."
                    }
            };
        }
    }
}
