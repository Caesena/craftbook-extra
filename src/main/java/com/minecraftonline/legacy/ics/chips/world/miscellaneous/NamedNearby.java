/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.miscellaneous;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.legacy.ics.ExtraDataIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerLimitations;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerRestrictions;
import com.minecraftonline.util.parsing.lineparser.generic.integer.OptionalLegacyTopLineIntegerParser;
import com.minecraftonline.util.parsing.lineparser.text.TextParserFactory;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.ICManager;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

/**
 * NAMED NEARBY
 * ModelID: MCX513
 *
 * @author Brendan (doublehelix457)
 * @author drathus (Legacy)
 */
public class NamedNearby extends AbstractIC implements ExtraDataIC {

    protected Text message = null;
    protected int distance;

    private static final IntegerParserFactory DISTANCE_PARSER_FACTORY = IntegerParserFactory.builder()
            .parser(new OptionalLegacyTopLineIntegerParser(Text.of("Distance"), () -> "NamedNearby"))
            .restriction(IntegerRestrictions.atLeast(1))
            .limitation(IntegerLimitations.atMost(64))
            .withDefault(64)
            .build();

    private static final TextParserFactory MESSAGE_PARSER_FACTORY = TextParserFactory.builder()
            .parser(new TextParserFactory.FormattingCodeTextParser(Text.of("Message (uses '&' for colors)"), false))
            .build();

    public NamedNearby(ICFactory<NamedNearby> icFactory, Location<World> block) {
        super(icFactory,block);
    }

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(0, DISTANCE_PARSER_FACTORY.createBinder(this, d -> this.distance = d))
                .multiLineParser(MESSAGE_PARSER_FACTORY.createBinder(this, msg -> this.message = msg), 2, 3)
                .build();
    }

    @Override
    public void onTrigger() {
        if(!getPinSet().isAnyTriggered(this)){
            getPinSet().setOutput(0,false,this);
            return;
        }

        processMessage();

        getPinSet().setOutput(0,true,this);
    }

    @Override
    public void load() {
        super.load();

        message = getExtraData()
                .orElse(TextSerializers.FORMATTING_CODE.deserialize(getLine(2)+getLine(3)));

        //try {
        //    this.distance = Integer.parseInt(getLine(0));
        //} catch (NumberFormatException e) {
        //    this.distance = Integer.parseInt(getLine(0).substring("NamedNearby".length()));
        //}

    }

    //TODO the idea here was to get the nearest player. This might just get the first player within distance but thats how it was.
    protected void processMessage()
    {
        World world = getBlock().getExtent();

        Player closestPlayer = null;
        double closestDistanceSquared = 0;

        Vector3d icPos = getBlock().getPosition();

        for (Player player : world.getPlayers()) {
            double distSquared = player.getLocation().getPosition().distanceSquared(icPos);
            if (closestPlayer == null || distSquared < closestDistanceSquared) {
                closestPlayer = player;
                closestDistanceSquared = distSquared;
            }
        }

        double icMaxDistanceSquared = distance*distance;

        if (closestPlayer == null || closestDistanceSquared > icMaxDistanceSquared) {
            return;
        }

        for(Player player: world.getPlayers())
        {
            Vector3d loc = player.getLocation().getPosition();

            if(loc.distanceSquared(icPos) > icMaxDistanceSquared) {
                continue;
            }
            Text msg = message.replace("%p", Text.of(closestPlayer.getName()));
            Text lines = msg.replace("/n", Text.NEW_LINE);
            player.sendMessage(lines);
        }
    }

    @Override
    public Text usageMessage() {
        return Text.of(TextColors.DARK_AQUA, "If present, ignores the message on the sign, and uses the text from the extra data, either in nbt or formatting code form");
    }

    public static class Factory implements ICFactory<NamedNearby>, RestrictedIC {

        @Override
        public boolean usesFirstLine() {
            return true;
        }

        @Override
        public NamedNearby createInstance(Location<World> location) { return new NamedNearby(this, location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                       "Input"
                    },
                    new String[]{
                       "Message Named Nearby."
                    }
            };
        }
    }
}
