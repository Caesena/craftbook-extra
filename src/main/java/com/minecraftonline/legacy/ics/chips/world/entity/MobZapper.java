/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.entity.DetailedEntityType;
import com.minecraftonline.util.entity.EntityParsingUtil;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import net.minecraft.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/*
 * MobZapper
 * ModelID: MCX130
 *
 * @author tyhdefu
 */

public class MobZapper extends AbstractStIC {

    int range = 0;
    private DetailedEntityType detailedEntityType = new DetailedEntityType.Mob(EntityParsingUtil.MobModes.Mobs);

    EntityType mobType = null; // null if not Specific

    public MobZapper(ICFactory<MobZapper> icFactory, Location<World> block) {super(icFactory, block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        try {
            String line4 = lines.get(3).toPlain();
            if (line4.length() != 0) {
                range = Integer.parseInt(line4);
                if (range > 64)
                    throw new InvalidICException("Range cannot be above 64");
                else if (range <= 0) {
                    throw new InvalidICException("Range must be greater than 0");
                }
            }
        }
        catch (NumberFormatException e) {
            throw new InvalidICException("Expected number, got: " + lines.get(3).toPlain());
        }
        String line3 = lines.get(2).toPlain();
        if (line3.length() == 0)
            return;

        if (line3.equalsIgnoreCase("mobs") || line3.equalsIgnoreCase("mob"))
            return;
        if (line3.equalsIgnoreCase("animals"))
            return;
        if (EntityParsingUtil.getMobs().stream().anyMatch(entityType -> entityType.getName().equalsIgnoreCase(line3))
                || EntityParsingUtil.getAnimals().stream().anyMatch(entityType -> entityType.getName().equalsIgnoreCase(line3)))
            return;
        throw new InvalidICException("Invalid third line");
    }

    @Override
    public void load() {
        super.load();

        String line4 = getLine(3);
        if (line4.length() == 0) {
            setLine(3, Text.of(5));
            range = 5;
        }
        else
            range = Integer.parseInt(line4);
        String line3 = getLine(2);

        if (line3.isEmpty()) {
            return;
        }

        try {
            detailedEntityType = EntityParsingUtil.parseStackedAndNbt(line3);
        } catch (DetailedEntityType.Error error) {
            CraftBookPlugin.spongeInst().getLogger().error("Error in MobZapper ic " + getBlock(), error);
        }
    }

    @Override
    public void onTrigger() {
        if (isSt ? !thinking : !getPinSet().isAnyTriggered(this))
            return;
        CraftBookPlugin.spongeInst().getScheduler().scheduleAsyncBatch(() -> {
            Collection<Entity> nearbyEntities = LocationUtil.getNearbyEntities(getBlock().add(0.5, 0, 0.5), range, true);
            CraftBookPlugin.spongeInst().getScheduler().scheduleSync(() -> {
                if (!getBlock().getExtent().getChunk(getBlock().getChunkPosition()).isPresent()) {
                    return;
                }
                if (nearbyEntities.size() == 0) {
                    getPinSet().setOutput(0, false, this);
                    return;
                }
                AtomicBoolean output = new AtomicBoolean(false);
                nearbyEntities.stream().map(e -> (org.spongepowered.api.entity.Entity) e).filter(e -> detailedEntityType.matches(e))
                        .forEach(e -> {
                            output.set(true);
                            e.remove();
                        });

                getPinSet().setOutput(0, output.get(), this);
            });
        });
    }

    public static class Factory implements ICFactory<MobZapper>, HybridIC, RestrictedIC {

        @Override
        public MobZapper createInstance(Location<World> location){return new MobZapper(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Outputs if a mob is near"
                    }
            };
        }

        @Override
        public String stVariant() {
            return "MCZ130";
        }
    }
}