/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.block;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.util.IDUtil;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

/**
 *  SET BLOCK BELOW
 * Model ID: MC1206
 *
 * @author Brendan (doublehelix457)
 * @author sk89q (Legacy)
 **/
public class SetBlockBelow extends AbstractIC {

    public SetBlockBelow(ICFactory<SetBlockBelow> icFactory, Location<World> block){super(icFactory,block);}


    BlockType id;
    int damage;
    boolean force;

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException{
        super.create(player, lines);
        String id;
        if(lines.get(2).toPlain().contains(":")){
            id = lines.get(2).toPlain().split(":")[0];
        }else {
            id = lines.get(2).toPlain();
        }
        String force = lines.get(3).toPlain();

        if (id.length() == 0) {
            throw new InvalidICException("Specify a block type on the third line.");
        } else if (!IDUtil.getBlockTypeFromAnyID(id).isPresent()) {
            throw new InvalidICException("Not a valid block type: " + getLine(2) + ".");
        }

        if (force.length() != 0 && !force.equalsIgnoreCase("Force")) {
            throw new InvalidICException("Fourth line needs to be blank or 'Force'.");
        }
    }

    @Override
    public void load(){
        super.load();
        if(getLine(2).contains(":")){
            String[] parts = getLine(2).split(":");
            id = IDUtil.getBlockTypeFromAnyID(parts[0]).orElseThrow(() -> new IllegalStateException("Invalid minecraft id '" + getLine(2) + "'"));
            damage = getItem(parts[1]);
        }else {
            id = IDUtil.getBlockTypeFromAnyID(getLine(2)).orElseThrow(() -> new IllegalStateException("Invalid minecraft id '" + getLine(2) + "'"));
        }
        force = getLine(3).equalsIgnoreCase("Force");
    }

    @Override
    public void onTrigger(){
        if(!getPinSet().isAnyTriggered(this)) return;
        Vector3i pos = getBackBlock().getBlockPosition();
        int y = pos.getY() - 2;
        int x = pos.getX();
        int z = pos.getZ();

        if (y <= 255 - 1 && (force || getBlock().getExtent().getBlock(x, y, z).getType() == BlockTypes.AIR)) {
            BlockState blockState = IDUtil.createBlockStateWithDamage(id, damage);
            getBlock().getExtent().setBlock(x,y,z, blockState);
            getPinSet().setOutput(0,true,this);

        } else getPinSet().setOutput(0,false,this);
        return;
    }



    private int getItem(String id) {
        try {
            return Integer.parseInt(id.trim());
        } catch (NumberFormatException e) {
            //oh well
            return -1;
        }
    }

    public static class Factory implements ICFactory<SetBlockBelow>, RestrictedIC {
        @Override
        public SetBlockBelow createInstance(Location<World> location){ return new SetBlockBelow(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Set a block below the block the IC is situated on."
                    }
            };
        }
    }
}