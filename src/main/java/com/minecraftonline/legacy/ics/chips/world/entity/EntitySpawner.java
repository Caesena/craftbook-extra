/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.minecraftonline.legacy.ics.ExtraDataIC;
import com.minecraftonline.util.entity.DetailedEntityType;
import com.minecraftonline.util.entity.EntityParsingUtil;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.Optional;

/*
* MCX200
* Spawns an Entity on the next empty spot there is above the IC
* @author tyhdefu
 */

public class EntitySpawner extends AbstractIC implements ExtraDataIC {

    private int amount = 1;
    private DetailedEntityType detailedEntityType;

    public EntitySpawner(ICFactory<EntitySpawner> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        String lineData = lines.get(2).toPlain() + lines.get(3).toPlain();
        Optional<String> extraData = getExtraDataPlain();
        if (extraData.isPresent()) {
            lineData += extraData.get();
        }
        String[] lineParts = lineData.split("[*]", 2);


        if (lineParts.length == 2) {
            try {
                amount = Integer.parseInt(lineParts[1]);
                if (amount < 1)
                    throw new InvalidICException("Amount cannot be less than 1");
                if (amount > 100)
                    throw new InvalidICException("Cannot spawn than 100 mobs at once");
            } catch (NumberFormatException e) {throw new InvalidICException("Amount on line 4 was not a number.");}
        }
        try {
            // not // Convert : to @ to make it match the correct format
            DetailedEntityType type = EntityParsingUtil.parseStackedAndNbt(lineParts[0]);
            if (!type.isCreatable())
                throw new InvalidICException("Entity is uncreateable.");
        } catch (DetailedEntityType.Error e) { throw new InvalidICException(e.getMessage()); }
    }

    @Override
    public void load() {
        super.load();
        String lines = getLine(2) + getLine(3);
        Optional<String> extraData = getExtraDataPlain();
        if (extraData.isPresent()) {
            lines += extraData.get();
        }
        String[] lineParts = lines.split("[*]", 2);

        if (lineParts.length == 2) {
            amount = Integer.parseInt(lineParts[1]);
        }
        try {
            detailedEntityType = EntityParsingUtil.parseStackedAndNbt(lineParts[0]);
        } catch (DetailedEntityType.Error e) {
            // edited sign
            CraftBookPlugin.spongeInst().getLogger().warn("Broken EntitySpawner IC at: " + getBlock() + " was it edited manually instead of placed?");
        }
    }

    @Override
    public void onTrigger() {
        if (!getPinSet().isAnyTriggered(this))
            return;
        Optional<Location<World>> optloc = LocationUtil.findEmptySpot(getBackBlock());
        if (!optloc.isPresent())
            return; // no available space to spawn
        Location<World> loc = optloc.get().add(0.5,0,0.5);
        for (int i = 0; i < amount; i++) {
            Entity entity = detailedEntityType.createEntity(loc).get();
            loc.spawnEntity(entity);
        }
    }

    @Override
    public Text usageMessage() {
        return Text.of(TextColors.GOLD, "This IC treats extra data as if it was appended to the current ic mob lines.");
    }

    public static class Factory implements ICFactory<EntitySpawner>, RestrictedIC {

        @Override
        public EntitySpawner createInstance(Location<World> location) { return new EntitySpawner(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Input"
                    },
                    new String[]{
                            "Outputs when spawning enitities"
                    }
            };
        }
    }
}
