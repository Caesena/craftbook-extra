/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.legacy.ics.HybridIC;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractStIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.mode.Modes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.Humanoid;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
* Humans Only
* MCX133
* Kills non human entities.
* @author tyhdefu
 */
public class HumansOnly extends AbstractStIC {

    private int range = 0;

    public HumansOnly(ICFactory<HumansOnly> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void onTrigger() {
        if (!getPinSet().isAnyTriggered(this))
            return;
        Vector3d searchloc = getBlock().getPosition().add(0.5, 0, 0.5);
        Collection<Entity> nearbyEntities = getBlock().getExtent().getNearbyEntities(searchloc, range);
        Stream<Entity> entityStream = nearbyEntities.stream();
        if (getMode().getType().equals(Modes.HUMANSONLYEXTRA)) {
            entityStream = entityStream.filter(this::ExtraFilter);
        }
        else {
            entityStream = entityStream.filter(this::playerFilter);
        }
        List<Entity> entityList = entityStream.collect(Collectors.toList());

        if (entityList.size() > 0) {
            getPinSet().setOutput(0, true, this);
            for (Entity entity : entityList) {
                entity.remove();
            }
        }
        else {
            getPinSet().setOutput(0, false, this);
        }
    }

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if (lines.get(2).toPlain().length() != 0)
            throw new InvalidICException("Line 3 must be blank");
        try {
            if (lines.get(3).toPlain().length() == 0) {
                return;
            }
            range = Integer.parseInt(lines.get(3).toPlain());
            if (!(range > 0))
                throw new InvalidICException("Range must be greater than 0.");
            if (!(range <= 64))
                throw new InvalidICException("Range must not be greater than 64");
        }
        catch (NumberFormatException e) { throw new InvalidICException("Line 4 must be a number or blank.");}
    }

    @Override
    public void load() {
        super.load();
        if (getLine(3).length() == 0) {
            setLine(3, Text.of("5"));
            range = 5;
        }
        else {
            try {
                range = Integer.parseInt(getLine(3));
            } catch (NumberFormatException e) {
                CraftBookPlugin.spongeInst().getLogger().warn("Sign at " + getBlock() + " (HumansOnly), is malformed");
                range = 5;
            }
        }
    }

    public boolean playerFilter(Entity entity) {
        return !(entity instanceof Humanoid);
    }

    public boolean ExtraFilter(Entity entity) {
        EntityType type = entity.getType();
        return !(type instanceof Humanoid)
                && !(type instanceof Minecart)
                && !(type.equals(EntityTypes.ENDER_PEARL))
                && !(type.equals(EntityTypes.EYE_OF_ENDER))
                && !(type.equals(EntityTypes.BOAT))
                && !(type.equals(EntityTypes.FISHING_HOOK))
                && !(type.equals(EntityTypes.WOLF) && entity.get(Keys.TAMED_OWNER).get().isPresent())
                && !(type.equals(EntityTypes.OCELOT) && entity.get(Keys.TAMED_OWNER).get().isPresent());
    }

    public static class Factory implements ICFactory<HumansOnly>, HybridIC, RestrictedIC {

        @Override
        public HumansOnly createInstance(Location<World> location) { return new HumansOnly(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Redstone"
                    },
                    new String[]{
                            "On/Off depending on command"
                    }
            };
        }

        @Override
        public String stVariant() {
            return "MCZ133";
        }
    }
}
