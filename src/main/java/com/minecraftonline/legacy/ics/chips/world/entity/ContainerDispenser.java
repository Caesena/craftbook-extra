/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.legacy.blockbags.NearbyChestBlockBag;
import com.minecraftonline.util.IDUtil;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Container Dispenser
 * ModelID: MC1202
 *
 * @author Brendan  (doublehelix457)
 * @author sk89q (Legacy)
 */
public class ContainerDispenser extends AbstractIC {

    public ContainerDispenser(ICFactory<ContainerDispenser> icFactory, Location<World> block){super(icFactory,block);}

    String itemID = "";
    int quantity = 1;
    BlockBag bag;

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        String id = lines.get(2).toPlain();
        String quantity = lines.get(3).toPlain();

        if (id.length() == 0) {
            throw new InvalidICException("Specify a item type on the third line.");
        } else if (!IDUtil.getItemTypeFromAnyID(id).isPresent()) {
            throw new InvalidICException("Not a valid item type: " + id + ".");
        }

        if (quantity.length() > 0) {
            try {
                Math.min(64, Math.max(1, Integer.parseInt(quantity)));
            } catch (NumberFormatException e) {
                throw new InvalidICException("Not a valid quantity: " + quantity + ".");
            }
        }
    }

    @Override
    public void load(){
        super.load();
        itemID = getLine(2);
        if(getLine(3).length() > 0) {
            quantity = Math.min(64, Math.max(1, Integer.parseInt(getLine(3))));
        }

        NearbyChestBlockBag ncbb = NearbyChestBlockBag.fromSourcePositionNearby(getBlock());

        if(ncbb == null){ this.unload();}
        bag = ncbb;


    }

    @Override
    public void onTrigger() {
        if(!getPinSet().isAnyTriggered(this)){ return;} //Make sure it was triggered when powered
        Optional<ItemType> itemType = IDUtil.getItemTypeFromAnyID(itemID);
        if (itemType.isPresent()) {
            Vector3i pos = getBlock().getBlockPosition();
            int x = pos.getX();
            int z = pos.getZ();
            int maxY = Math.min(255, pos.getY() + 10);
            World world = getBlock().getExtent();
            //if (! world.isChunkLoaded(x, 0, z)) return;

            ArrayList<ItemStack> stacks = new ArrayList<>();
            stacks.add(ItemStack.builder().itemType(itemType.get()).quantity(quantity).build());
            for (int y = pos.getY() + 1; y <= maxY; y++) {

                    if(bag.has(stacks)){
                        Entity entity = world.getLocation(x,y,z).createEntity(EntityTypes.ITEM);
                        Item item = (Item) entity;
                        item.offer(Keys.REPRESENTED_ITEM,stacks.get(0).createSnapshot());
                         boolean spawned = world.getLocation(x,y,z).spawnEntity(item);
                         if(spawned){
                             bag.remove(stacks);
                         }
                        return;
                    }
            }
        }

    }

    /**
     * Get an item from its name or ID.
     *
     * @param id
     * @return
     */
    private int getItem(String id) {
        try {
            return Integer.parseInt(id.trim());
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static class Factory implements ICFactory<ContainerDispenser>, RestrictedIC {
        @Override
        public ContainerDispenser createInstance(Location<World> location) { return new ContainerDispenser(this,location);}

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                        "Input"
                    },
                    new String[]{
                            "Outputs an item from the chest."
                    }
            };
        }
    }
}
