/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.miscellaneous;

import com.flowpowered.math.vector.Vector3d;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.mode.Modes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * Server Log Nearby+
 * MCX517
 * Logs nearest player with message. %a can be used to list all players within range.
 * @author tyhdefu
 */
public class ServerLogNearbyPlus extends AbstractIC {
    private static final String TITLE = "S-LOG NEARBY+";
    private boolean shouldLoadLine = false;
    private String logmessage;
    private Vector3d signPosition;
    private int range = 64;

    public ServerLogNearbyPlus(ICFactory<ServerLogNearbyPlus> icFactory, Location<World> block) {super(icFactory, block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if (lines.get(0).toPlain().length() != 0) {
            try {
                range = Integer.parseInt(lines.get(0).toPlain());
                if (range < 1 || range >= 64)
                    throw new InvalidICException("Range must be between 1-64");
            } catch (NumberFormatException e) {throw new InvalidICException("Range on first line be number");}
        }
        if (lines.get(2).toPlain().length() == 0)
            throw new InvalidICException("Line 3 must have a message");
        shouldLoadLine = true;
    }

    @Override
    public void onTrigger() {
        if (!getPinSet().isAnyTriggered(this))
            return;
        Collection<Entity> inrangeEntities = getBlock().getExtent().getNearbyEntities(signPosition, range);

        Set<Player> inrangePlayers = inrangeEntities.stream().filter(entity -> entity.getType().equals(EntityTypes.PLAYER)).map(entity -> (Player) entity)
                .collect(Collectors.toCollection(HashSet::new));
        Player closestPlayer = null;
        double closestdistance = range + 1;
        if (inrangePlayers.size() > 0) {
            StringBuilder stringBuilder = new StringBuilder();

            boolean firstplayer = true;
            for (Player player : inrangePlayers) {
                double distance = signPosition.distance(player.getPosition());
                if (firstplayer) {
                    stringBuilder.append(player.getName()).append(" distance: ").append(distance);
                    firstplayer = false;
                }
                else
                    stringBuilder.append(" ").append(player.getName()).append(" distance: ").append(distance);
                if (distance < closestdistance) {
                    closestPlayer = player;
                    closestdistance = distance;
                }
            }
            stringBuilder = new StringBuilder(logmessage.replaceAll("%a", stringBuilder.toString())
                    .replaceAll("%p",closestPlayer.getName() + " distance: " + closestdistance));

            stringBuilder.insert(0, "[CB!] In range players: ");

            CraftBookPlugin.spongeInst().getLogger().info(stringBuilder.toString());
            getPinSet().setOutput(0, true, this);
            if (getMode().getType().equals(Modes.LOGTONEARBYPLAYERS)) {
                for (Player player : inrangePlayers) {
                    player.sendMessage(Text.of(stringBuilder.toString()));
                }
            }
        }
        else {
            String msg = "[CB!] " + logmessage.replaceAll("%p", "[NONE_FOUND]");
            CraftBookPlugin.spongeInst().getLogger().info(msg);
            getPinSet().setOutput(0, false, this);
        }

    }

    @Override
    public void load() {
        super.load();
        if (shouldLoadLine) {
            setLine(0, Text.of(TITLE + range));
        }
        signPosition = getBlock().getPosition();
        logmessage = getLine(2) + getLine(3);

        int length = TITLE.length();
        String line1 = getLine(0);
        if (line1.length() == length) {
            range = 64;
        }
        else {
            String s = line1.substring(length);
            range = Integer.parseInt(s);
        }
    }

    public static class Factory implements ICFactory<ServerLogNearbyPlus>, RestrictedIC {

        @Override
        public ServerLogNearbyPlus createInstance(Location<World> location){return new ServerLogNearbyPlus(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Outputs when logs"
                    }
            };
        }

        @Override
        public boolean usesFirstLine() {
            return true;
        }
    }
}
