/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.miscellaneous;

import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.Optional;

/**
 * MESSAGE PLAYER
 * Model ID: MC1510
 * Broadcasts a message to a specified player.
 *
 * @author Brendan (doublehelix457)
 * @author Tom (tmhrtly) (Legacy)
 */
public class SendMessage extends AbstractIC {

    public SendMessage(ICFactory<SendMessage> icFactory, Location<World> block){super(icFactory,block);}

    String message;

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        String id = lines.get(2).toPlain();

        if (id.length() == 0 || id.contains(" ")) {
            throw new InvalidICException("Put a player's name on line 3, with no spaces.");
        }

        if (lines.get(3).isEmpty()) {
            throw new InvalidICException("Please put the message to broadcast on line 4.");
        }
    }

    @Override
    public void load(){
        super.load();
        message = getLine(3);
    }

    @Override
    public void onTrigger() {
        if(getPinSet().isAnyTriggered(this)){
            Optional<Player> player = Sponge.getServer().getPlayer(getLine(2));
            if(player.isPresent()) {
                player.get().sendMessage(Text.of(message));
                getPinSet().setOutput(0,true,this);
                return;
            }
            getPinSet().setOutput(0,false,this);
        }
    }

    public static class Factory implements ICFactory<SendMessage>, RestrictedIC {

        @Override
        public SendMessage createInstance(Location<World> location) { return new SendMessage(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{

            };
        }
    }
}
