/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.sensor;

import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.entity.PlayerParsingUtil;
import com.minecraftonline.util.parsing.entity.PlayerMatcherParser;
import com.minecraftonline.util.parsing.entity.PlayerMatcherParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerLimitations;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerRestrictions;
import com.minecraftonline.util.parsing.lineparser.generic.integer.OptionalIntegerParser;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.Humanoid;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.Collection;

/**
 * PLAYER NEAR?
 * ModelID: MCX118
 *
 * @author Brendan (doublehelix457)
 * @author sk89q (Legacy)
 */
public class PlayerNear extends AbstractStIC implements AreaIC {

    private double dist;
    private PlayerParsingUtil.HumanoidMatcher matcher;

    private static final PlayerMatcherParserFactory MATCHER_PARSER = PlayerMatcherParserFactory.builder()
            .parser(new PlayerMatcherParser())
            .withDefault(new PlayerParsingUtil.AnyHumanoid())
            .build();

    private static final IntegerParserFactory RANGE_PARSER = IntegerParserFactory.builder()
            .parser(OptionalIntegerParser.range())
            .withDefault(5)
            .restriction(IntegerRestrictions.atLeast(1))
            .limitation(IntegerLimitations.atMost(64))
            .build();

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, MATCHER_PARSER.createBinder(this, matcher -> this.matcher = matcher))
                .lineParser(3, RANGE_PARSER.createBinder(this, range -> this.dist = range))
                .build();
    }

    public PlayerNear(ICFactory<PlayerNear> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    @Override
    public void onTrigger() {
        if (!isSt && !getPinSet().isAnyTriggered(this)) return;
        CraftBookPlugin.spongeInst().getScheduler().scheduleAsyncBatch(() -> {
            ArrayList<Humanoid> players = getNearbyPlayers(getBackBlockCentre().add(0, -0.5, 0));

            CraftBookPlugin.spongeInst().getScheduler().scheduleSync(() -> {
                if (!getBlock().getExtent().getChunk(getBlock().getChunkPosition()).isPresent()) {
                    return;
                }
                // TODO?: Reinstate quick-shortcut for if we're not checking any extra info.
                boolean foundPlayer = players.stream().anyMatch(this.matcher);
                getPinSet().setOutput(0, foundPlayer, this);
            });
        });
    }

    private ArrayList<Humanoid> getNearbyPlayers(Location<World> location) {
        ArrayList<Humanoid> players = new ArrayList<>();
        Collection<Humanoid> stack = LocationUtil.getNearbyEntitiesByTypeAsync(location, dist, Humanoid.class);
        for (Humanoid e : stack) {
            if (e.get(Keys.GAME_MODE).map(gameMode -> gameMode == GameModes.SPECTATOR).orElse(false)) {
                continue;
            }
            players.add(e);
        }
        return players;
    }

    @Override
    public CUIRegion getArea() {
        return WorldEditUtil.toSphereSelection(getBlock().getExtent(), getBackBlock().getPosition(), (int) dist);
    }

    public static class Factory implements ICFactory<PlayerNear>, HybridIC {
        @Override
        public PlayerNear createInstance(Location<World> location) {
            return new PlayerNear(this, location);
        }

        @Override
        public String stVariant() {
            return "MCZ118";
        }

        @Override
        public String[][] getPinHelp() {
            return new String[0][];
        }
    }
}
