/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

/*
* Dispenser 2.0
* MCX201
* Spawns Items in the world.
* @author tyhdefu
 */

import com.minecraftonline.ic.AbstractIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.item.itemstack.ItemStackParsers;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerRestrictions;
import com.minecraftonline.util.parsing.lineparser.generic.integer.OptionalIntegerParser;
import com.minecraftonline.util.parsing.lineparser.itemstack.ItemStackParserFactory;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;

public class ItemSpawner extends AbstractIC {
    private ItemStack itemStack = null;

    public ItemSpawner(ICFactory<ItemSpawner> icFactory, Location<World> block){super(icFactory,block);}

    private static final ItemStackParserFactory ITEM_STACK_PARSER_FACTORY = ItemStackParserFactory.builder()
            .parser(ItemStackParsers.ITEM_TYPE_OPTIONAL_DAMAGE)
            .build();

    private static final IntegerParserFactory AMOUNT_PARSER_FACTORY = IntegerParserFactory.builder()
            .parser(new OptionalIntegerParser(Text.of("Quantity")))
            .withDefault(1)
            .restriction(IntegerRestrictions.inRange(1, 64))
            .build();

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, ITEM_STACK_PARSER_FACTORY.createBinder(this, itemStack -> this.itemStack = itemStack))
                .lineParser(3, AMOUNT_PARSER_FACTORY.createBinder(this, amt -> {
                    if (itemStack == null) {
                        CraftBookPlugin.spongeInst().getLogger().error("Item stack was null!!");
                    }
                    itemStack.setQuantity(amt);
                }))
                .build();
    }

    @Override
    public void onTrigger() {
        if (!getPinSet().isAnyTriggered(this))
            return;
        Optional<Location<World>> optionalLoc = LocationUtil.findEmptySpot(getBackBlockCentre());
        if (!optionalLoc.isPresent())
            return;

        Entity item = getBlock().getExtent().createEntity(EntityTypes.ITEM, optionalLoc.get().getBlockPosition());
        item.offer(Keys.REPRESENTED_ITEM, itemStack.createSnapshot());
        getBlock().getExtent().spawnEntity(item);
    }

    public static class Factory implements ICFactory<ItemSpawner>, RestrictedIC {

        @Override
        public ItemSpawner createInstance(Location<World> location) { return new ItemSpawner(this, location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Input"
                    },
                    new String[]{
                            "Outputs when spawning items"
                    }
            };
        }
    }
}
