/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.entity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.BandLineParser;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Destination extends AbstractStIC {

    private String band;
    private boolean isActive = false;
    private Vector3d rotation;

    private static final Map<String, Destination> DESTINATION_MAP = new HashMap<>();

    @Nullable
    public static Destination getDestination(final String band) {
        return DESTINATION_MAP.get(band);
    }

    public Destination(ICFactory<? extends IC> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, BandLineParser.bandName(band -> this.band = band))
                .build();
    }

    public boolean teleportTo(Entity entity) {
        Optional<Location<World>> optSpot = LocationUtil.findEmptySpot(getBackBlockCentre());
        if (!optSpot.isPresent()) {
            return false;
        }
        entity.getBaseVehicle().setLocationAndRotation(optSpot.get(), this.rotation);
        entity.setRotation(this.rotation);
        return true;
    }

    @Override
    public void load() {
        super.load();

        this.isActive = getPinSet().isAnyTriggered(this);
        if (isActive) {
            registerIfNotRegistered();
        }
        this.rotation = LocationUtil.cartesianToEuler(getBlock().require(Keys.DIRECTION).getOpposite().asOffset());
    }

    @Override
    public void unload() {
        if (this.isActive) {
            DESTINATION_MAP.remove(this.band);
        }
    }

    public void registerIfNotRegistered() {
        if (DESTINATION_MAP.containsKey(this.band)) {
            LocationUtil.getNearbyPlayers(getBlock(), 10)
                    .forEach(p -> p.sendMessage(Text.of(TextColors.RED, "A destination with this band is already in operation. This destination ic will not operate until it is fixed and reactivated.")));
            return;
        }
        DESTINATION_MAP.put(this.band, this);
    }

    @Override
    public void onTrigger() {
        boolean shouldBeActive;
        if (thinking) {
            shouldBeActive = true;
            for (int i = 0; i < getPinSet().getInputCount(); i++) {
                Location<World> pinLoc = getPinSet().getPinLocation(i, this);
                if (BlockUtil.isPowerSource(pinLoc)) {
                    shouldBeActive = getPinSet().isAnyTriggered(this);
                    break;
                }
            }
        }
        else {
            shouldBeActive = getPinSet().isAnyTriggered(this);
        }
        if (shouldBeActive == isActive) {
            return;
        }
        this.isActive = shouldBeActive;
        if (this.isActive) {
            registerIfNotRegistered();
        }
        else {
            DESTINATION_MAP.remove(this.band);
        }
    }

    @Override
    public boolean isAlwaysST() {
        return true;
    }

    public static class Factory implements ICFactory<Destination>, RestrictedIC {
        @Override
        public Destination createInstance(Location<World> location) { return new Destination(this, location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                            "Redstone"
                    },
                    new String[0]
            };
        }
    }
}
