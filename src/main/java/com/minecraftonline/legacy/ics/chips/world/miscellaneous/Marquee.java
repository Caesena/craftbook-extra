/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.miscellaneous;

import com.sk89q.craftbook.sponge.ICFactory;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.mode.Modes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * MARQUEE
 * Model ID: MC2999
 *
 * @author Brendan (doublehelix457)
 *
 * The Marquee IC, not to be confused with the mechanic (mechanism in CBX)
 * also not to be confused with someone named marky.
 */
public class Marquee extends AbstractIC {

    int currentPin;

    public Marquee(ICFactory<Marquee> icFactory, Location<World> block){super(icFactory,block);}

    @Override
    public void load(){
        super.load();
        setMode();
        if (getLine(2).length() == 0)
            currentPin = 1;
        else
            this.currentPin = Integer.parseInt(getLine(2));
    }

    @Override
    public void unload() {
        setLine(2,Text.of(currentPin));
    }

    @Override
    public void onTrigger() {
        if(!getPinSet().isAnyTriggered(this)) return;

        for(int x=0;x<getPinSet().getOutputCount();x++) getPinSet().setOutput(x,false,this);

        //since chip output order is different, translate
        switch(currentPin)
        {
            case 1:
                getPinSet().setOutput(1,true,this);
                break;
            case 2:
                getPinSet().setOutput(0,true, this);
                break;
            case 3:
                getPinSet().setOutput(2,true, this);
                break;
        }

        if(getMode().getType() == Modes.REVERSE)
            currentPin--;
            else currentPin++;

            if(currentPin == 4) currentPin = 1;
            else if(currentPin == 0) currentPin = 3;
    }

    public static class Factory implements ICFactory<Marquee> {
        @Override
        public Marquee createInstance(Location<World> location) { return new Marquee(this,location);}

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                        "Input"
                    },
                    new String[]{
                            "Cycle the output when turned on."
                    }

            };
        }
    }
}
