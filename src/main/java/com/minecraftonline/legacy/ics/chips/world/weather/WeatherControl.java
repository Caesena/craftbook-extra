/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.weather;

import com.minecraftonline.util.nms.PacketUtil;
import com.sk89q.craftbook.sponge.ICFactory;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import net.minecraft.network.Packet;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.weather.Weathers;


/**
 * WEATHER CONTROL
 * ModelID: MCT233
 *
 * @author sk89q (Legacy)
 * @author Brendan (doublehelix457)
 *
 * SEARGE:
 * Packet<?>
 */
public class WeatherControl extends AbstractIC {

    public WeatherControl(ICFactory<WeatherControl> icFactory, Location<World> block){super(icFactory,block);}

    int duration;
    Packet<?> rain = new PacketUtil.PacketBuilder().asGameStateChange(2,0).build();
    Packet<?> noRain = new PacketUtil.PacketBuilder().asGameStateChange(1,0).build();

    @Override
    public void onTrigger() {
        if(getPinSet().isTriggered(0,this) && getPinSet().getInput(0,this)){

            if(getPinSet().getInput(1,this)){
                duration = 24000;
                if(!getBlock().getExtent().getProperties().isRaining()) PacketUtil.sendPacketToAllPlayers(getBlock().getExtent(),noRain);

            }else{
                duration = 0;
                if(getBlock().getExtent().getProperties().isRaining()) PacketUtil.sendPacketToAllPlayers(getBlock().getExtent(),rain);
            }

            if(getPinSet().getInput(1,this))
            getBlock().getExtent().setWeather(Weathers.RAIN,duration);

            if(!getPinSet().getInput(2,this)) {
                duration = 0;
                getBlock().getExtent().setWeather(Weathers.THUNDER_STORM,duration);
            }

        }
    }

    public static class Factory implements ICFactory<WeatherControl>, RestrictedIC {

        @Override
        public WeatherControl createInstance(Location<World> location) { return new WeatherControl(this,location); }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{
                    new String[]{
                          "Start weather.",
                            "Set Rain.",
                            "Set Storm."
                    },
                    new String[]{
                        "Ouput if the IC is running."
                    }
            };
        }
    }
}
