/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.sensor;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.AreaIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.WorldEditUtil;
import com.minecraftonline.util.entity.DetailedEntityType;
import com.minecraftonline.util.entity.EntityParsingUtil;
import com.minecraftonline.util.entity.PlayerParsingUtil;
import com.minecraftonline.util.nms.LocationWorldUtil;
import com.minecraftonline.util.parsing.entity.PlayerMatcherParser;
import com.minecraftonline.util.parsing.entity.PlayerMatcherParserFactory;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.SelfTriggeringIC;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.worldedit.internal.cui.CUIRegion;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Humanoid;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.AABB;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * MCX116
 * PLAYER ABOVE
 * Detects players in the first empty block above the ic.
 * @author tyhdefu
 */
public class PlayerAbove extends AbstractStIC implements SelfTriggeringIC, AreaIC {

    private PlayerParsingUtil.HumanoidMatcher matcher;

    private static final PlayerMatcherParserFactory MATCHER_PARSER = PlayerMatcherParserFactory.builder()
            .parser(new PlayerMatcherParser())
            .withDefault(new PlayerParsingUtil.AnyHumanoid())
            .build();

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, MATCHER_PARSER.createBinder(this, matcher -> this.matcher = matcher))
                .build();
    }

    public PlayerAbove(ICFactory<PlayerAbove> icFactory, Location<World> block) {super(icFactory, block);}

    @Override
    public void onTrigger() {
        if (!isSt && !getPinSet().isAnyTriggered(this))
            return;
        Optional<AABB> aabb = getAABB();

        if (!aabb.isPresent()) {
            return;
        }
        if (!LocationWorldUtil.isAllLoaded(getBlock().getExtent(), aabb.get().getMin().toInt(), aabb.get().getMax().toInt())) {
            return;
        }

        Stream<Entity> entities = getBlock().getExtent().getIntersectingEntities(aabb.get()).stream()
                .filter(entity -> entity instanceof Humanoid);
        getPinSet().setOutput(0, entities.anyMatch(matcher), this);
    }

    public Optional<AABB> getAABB() {
        return LocationUtil.findEmptyPlayerSpot(getBackBlock()).map(spot -> new AABB(
                Vector3d.from(spot.getX() - 1, spot.getY(), spot.getZ() - 1),
                Vector3d.from(spot.getX() + 1, spot.getY() + 1, spot.getZ() + 1)
        ));
    }

    @Nullable
    @Override
    public CUIRegion getArea() {
        return getAABB().map(aabb -> WorldEditUtil.toCuboidSelection(getBlock().getExtent(), aabb.getMin(), aabb.getMax()))
                .orElse(null);
    }

    public static class Factory implements ICFactory<PlayerAbove>, HybridIC {

        @Override
        public PlayerAbove createInstance(Location<World> location){return new PlayerAbove(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Outputs if a player was above"
                    }
            };
        }
        @Override
        public String stVariant() {
            return "MCZ116";
        }
    }
}
