/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.miscellaneous;

import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless.Band;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless.Bands;
import com.sk89q.craftbook.sponge.mode.Modes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

/**
 * Marquee Transmitter
 *
 * Given a band prefix ("myband") and a low and high counter values, the
 * IC will sequentially power all bands, eg:
 *
 * myband:0:3
 *
 * myband0
 * myband1
 * myband2
 * myband3
 *
 * Incrementing the band with each input 1 toggle. The marquee will restart at
 * low after reaching high, or on input 2 toggle.
 *
 * Input three holds the marquee and prevents further increments while on.
 *
 * MARQUEETRANSMIT
 * ModelID: MC3456
 *
 * @author Drathus (Legacy)
 * @author Brendan (doublehelix457)
 *
 */
public class MarqueeTransmitter extends AbstractIC {

    public MarqueeTransmitter(ICFactory<MarqueeTransmitter> icFactory, Location<World> block){super(icFactory,block);}

    //private WirelessTransmitter.Factory.WirelessData wirelessData;
    private String shortband;
    private String wideband;
    private int start;
    private int end;
    private int current;

    boolean shouldLoadLine = false;

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        String lineThree = lines.get(2).toPlain();
        String lineFour = lines.get(3).toPlain();
        String[] parts = lineThree.split(":");
        start = 0;
        end = 0;

        if (parts.length != 3) {
            throw new InvalidICException("Specify a band name, low, and high values. Band:Low#:High#");
        } else if (parts[0].length() == 0) {
            throw new InvalidICException("You must specify a band name.");
        }

        shortband = parts[0];
        start = Integer.parseInt(parts[1]);
        end = Integer.parseInt(parts[2]);

        if (start < 0) {
            throw new InvalidICException("Low value must be >= 0");
        } else if (start > end) {
            throw new InvalidICException("Low value cannot be greater than high value.");
        }

        if (lineFour.equals("uuid")) {
            wideband = player.getUniqueId().toString();
            shouldLoadLine = true;
        }
    }

    @Override
    public void load(){
        super.load();
        if (shouldLoadLine) {
            setLine(3, Text.of(wideband));
        }
        else {
            wideband = getLine(3);
            String[] parts = getLine(2).split(":");
            shortband = parts[0];
            start = Integer.parseInt(parts[1]);
            end = Integer.parseInt(parts[2]);
            if (parts.length == 3) {
                current = getMode().getType() == Modes.REVERSE ? end : start;
                setLine(2, Text.of(getLine(2) + ":" + current));
            }
            else
                current = Integer.parseInt(parts[3]);
            Bands.setActivatedBand(getCurrentBand(), true);
            //WirelessTransmitter.wirelessStates.add(new Tuple<>(shortband + current,wideband));
        }
    }

    @Override
    public void unload() {
        //WirelessTransmitter.wirelessStates.remove(new Tuple<>(shortband+current,wideband));
    }

    public Band getCurrentBand() {
        return new Band(wideband,shortband+current);
    }

    @Override
    public void onTrigger() {
        if(getPinSet().isAnyTriggered(this)){
            if (getTriggeredPin() == 1) {
                // Reset activation
                current = getMode().getType() != Modes.REVERSE ? start : end;
            }
            else if (getTriggeredPin() == 0 && !getPinSet().getInput(2, this)) { // Check it is not held by pin 3
                //WirelessTransmitter.wirelessStates.remove(new Tuple<>(shortband+current,wideband));
                Bands.setActivatedBand(getCurrentBand(), false);
                if(getMode().getType() == Modes.REVERSE){
                    if(current - 1 < start) current = end;
                    else current--;
                }else{
                    if(current + 1 > end) current = start;
                    else current++;
                }
                //WirelessTransmitter.wirelessStates.add(new Tuple<>(shortband+current,wideband));
                Bands.setActivatedBand(getCurrentBand(), true);
            }
        }
    }

    public static class Factory implements ICFactory<MarqueeTransmitter>, RestrictedIC {

        @Override
        public MarqueeTransmitter createInstance(Location<World> location) {
            return new MarqueeTransmitter(this,location);
        }

        @Override
        public String[][] getPinHelp() {
            return new String[][]{

            };
        }
    }
}
