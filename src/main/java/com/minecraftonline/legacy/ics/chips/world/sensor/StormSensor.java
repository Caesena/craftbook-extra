/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.sensor;

import com.minecraftonline.legacy.ics.HybridIC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.mechanics.ics.RestrictedIC;
import com.sk89q.craftbook.sponge.mechanics.ics.SelfTriggeringIC;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * STORM SENSOR
 * Model ID: MCX231
 *
 * @author Brendan  (doublehelix457)
 * @author Shaun "sturmeh" (Legacy)
 *
 * (Not to be confused with Strom sensor,
 * which detects the prescence of stromhurst)
 */
public class StormSensor extends AbstractIC implements SelfTriggeringIC {

    public StormSensor(ICFactory<StormSensor> icFactory,Location<World> block){super(icFactory,block);}

    @Override
    public void onTrigger(){
        if(getPinSet().isAnyTriggered(this)){
            getPinSet().setOutput(0,getBlock().getExtent().getProperties().isThundering(),this);
        }
    }

    @Override
    public void think(){
        getPinSet().setOutput(0,getBlock().getExtent().getProperties().isThundering(),this);
    }

    public static class Factory implements ICFactory<StormSensor>, HybridIC, RestrictedIC {

        @Override
        public StormSensor createInstance(Location<World> location){ return new StormSensor(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Outputs if it is Storming."
                    }
            };
        }

        @Override
        public String stVariant(){ return "MCZ231"; }
    }
}
