/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.sensor;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.entity.EntityParsingUtil;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractStIC;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import net.minecraft.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

/**
* MobNear
* ModelID: MCX119
*
* @author tyhdefu
 */

public class MobNear extends AbstractStIC {

    int range = 0;

    private EntityParsingUtil.MobModes mobMode;

    EntityType mobType = null; // null if not Specific

    public MobNear(ICFactory<MobNear> icFactory, Location<World> block) {super(icFactory, block);}

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        try {
            String line4 = lines.get(3).toPlain();
            if (line4.length() != 0) {
                range = Integer.parseInt(line4);
                if (range > 64) // TODO: should this change to ICSocket's max radius?
                    throw new InvalidICException("Range cannot be above 64");
                else if (range <= 0) {
                    throw new InvalidICException("Range must be greater than 0");
                }
            }
        }
        catch (NumberFormatException e) {
            throw new InvalidICException("Expected number, got: " + lines.get(3).toPlain());
        }
        String line3 = lines.get(2).toPlain();
        if (line3.length() == 0)
            return;

        if (line3.equalsIgnoreCase("mobs") || line3.equalsIgnoreCase("mob"))
            return;
        if (line3.equalsIgnoreCase("animals") || line3.equalsIgnoreCase("animal"))
            return;
        if (EntityParsingUtil.getMobs().stream().anyMatch(entityType -> entityType.getName().equalsIgnoreCase(line3))
                || EntityParsingUtil.getAnimals().stream().anyMatch(entityType -> entityType.getName().equalsIgnoreCase(line3)))
            return;
        throw new InvalidICException("Invalid third line");
    }

    @Override
    public void load() {
        super.load();
        String line4 = getLine(3);
        if (line4.length() == 0) {
            setLine(3, Text.of(5));
            range = 5;
        }
        else
            range = Integer.parseInt(line4);
        String line3 = getLine(2);
        if (line3.length() == 0)
            mobMode = EntityParsingUtil.MobModes.All;
        else if (line3.equalsIgnoreCase("mobs")
                || line3.equalsIgnoreCase("mob"))
            mobMode = EntityParsingUtil.MobModes.Mobs;
        else if (line3.equalsIgnoreCase("animals")
        || line3.equalsIgnoreCase("animal"))
            mobMode = EntityParsingUtil.MobModes.Animals;
        else {
            for (EntityType type : EntityParsingUtil.getMobs()) {
                if (line3.equalsIgnoreCase(type.getName())) {
                    mobMode = EntityParsingUtil.MobModes.Specific;
                    mobType = type;
                    return;
                }
            }
            for (EntityType type : EntityParsingUtil.getAnimals()) {
                if (line3.equalsIgnoreCase(type.getName())) {
                    mobMode = EntityParsingUtil.MobModes.Specific;
                    mobType = type;
                    return;
                }
            }
            CraftBookPlugin.spongeInst().getLogger().warn("Broken MobNear IC, invalid mobtype at: " + getSign().getLocation().toString());
        }
    }

    @Override
    public void unload() {}

    @Override
    public void onTrigger() {
        CraftBookPlugin.spongeInst().getScheduler().scheduleAsyncBatch(() -> {
            Collection<net.minecraft.entity.Entity> nearbyEntities = LocationUtil.getNearbyEntities(getBlockCentre(), range, true);
            boolean found = findMatchingEntity(nearbyEntities);
            CraftBookPlugin.spongeInst().getScheduler().scheduleSync(() -> {
                getPinSet().setOutput(0, found, this);
            });
        });
    }

    public boolean findMatchingEntity(Collection<Entity> nearbyEntities) {
        Stream<org.spongepowered.api.entity.Entity> stream = nearbyEntities.stream().map(e -> (org.spongepowered.api.entity.Entity) e);
        switch (mobMode) {
            case All: return stream.anyMatch(entity -> EntityParsingUtil.getMobs().contains(entity.getType()) || EntityParsingUtil.getAnimals().contains(entity.getType()));
            case Mobs: return stream.anyMatch(entity -> EntityParsingUtil.getMobs().contains(entity.getType()));
            case Animals: return stream.anyMatch(entity -> EntityParsingUtil.getAnimals().contains(entity.getType()));
            case Specific: return stream.anyMatch(entity -> entity.getType() == mobType);
            default: throw new IllegalStateException("Missing enum case for mobmode!");
        }
    }

    public static class Factory implements ICFactory<MobNear>, HybridIC {

        @Override
        public MobNear createInstance(Location<World> location){return new MobNear(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Outputs if a mob is near"
                    }
            };
        }

        @Override
        public String stVariant() {
            return "MCZ119";
        }
    }
}