/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics;

import com.minecraftonline.ic.AbstractStIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.lineparser.generic.integer.IntegerRestrictions;
import com.minecraftonline.util.parsing.lineparser.generic.integer.comparable.ComparableIntegerParser;
import com.minecraftonline.util.parsing.lineparser.generic.integer.comparable.ComparableIntegerParserFactory;
import com.minecraftonline.util.parsing.lineparser.generic.integer.comparable.ComparableIntegerRestrictions;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.ICFactory;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * Outputs high if the time of day is between the two numbers
 * provided.
 *
 * [MCX207]
 * start
 * [end]
 *
 * MCZ207 (Self Triggering)
 */
public class BetweenTime extends AbstractStIC {

    private static final int TICKS_IN_A_DAY = 24_000;

    private Comparable<Integer> start;
    private Comparable<Integer> end;

    public BetweenTime(ICFactory<? extends IC> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    public static final ComparableIntegerParserFactory START_TIME_PARSER = makeDayTickParser(Text.of("StartTicks")).build();
    public static final ComparableIntegerParserFactory END_TIME_PARSER = makeDayTickParser(Text.of("EndTicks")).withDefault(TICKS_IN_A_DAY).build();

    private static ComparableIntegerParserFactory.Builder makeDayTickParser(Text helpMessage) {
        return ComparableIntegerParserFactory.builder()
                .parser(new ComparableIntegerParser(helpMessage, 5)) // 24000 is 5 long.
                .restriction(ComparableIntegerRestrictions.ifInt(IntegerRestrictions.inRange(0, TICKS_IN_A_DAY)));
    }

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, START_TIME_PARSER.createBinder(this, start -> this.start = start))
                .lineParser(3, END_TIME_PARSER.createBinder(this, end -> this.end = end))
                .build();
    }

    @Override
    public void onTrigger() {
        int timeOfDay = (int) (getBlock().getExtent().getProperties().getWorldTime() % TICKS_IN_A_DAY);

        boolean activated = start.compareTo(timeOfDay) <= 0 && end.compareTo(timeOfDay) >= 0;
        getPinSet().setOutput(0, activated, this);
    }

    public static class Factory implements ICFactory<BetweenTime>, HybridIC {

        @Override
        public BetweenTime createInstance(Location<World> location) {
            return new BetweenTime(this, location);
        }

        @Override
        public String[][] getPinHelp() {
            return new String[0][];
        }

        @Override
        public String stVariant() {
            return "MCZ027";
        }
    }
}
