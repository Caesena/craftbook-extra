/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.powerable.SimplePowerable;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.event.CauseStackManager;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collections;
import java.util.List;

@Module(id = "lightnetherrack", name = "LightNetherrack", onEnable="onInitialize", onDisable="onDisable")
public class LightNetherrack extends SimplePowerable {
    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.",
            new SpongeBlockFilter(BlockTypes.NETHERRACK), TypeToken.of(SpongeBlockFilter.class));

    @Override
    public void updateState(Location<?> location, boolean powered) {
        try (CauseStackManager.StackFrame frame = Sponge.getCauseStackManager().pushCauseFrame()) {
            frame.addContext(EventContextKeys.PLUGIN, CraftBookPlugin.inst());

            Location<?> fireLoc = location.getRelative(Direction.UP);
            if (powered) {
                if (fireLoc.getBlockType() == BlockTypes.AIR) {
                    fireLoc.setBlock(BlockTypes.FIRE.getDefaultState());
                }
            }
            else {
                if (fireLoc.getBlockType() == BlockTypes.FIRE) {
                    fireLoc.setBlock(BlockTypes.AIR.getDefaultState());
                }
            }
        }
    }

    private static final List<Direction> additionalUpdateDirections = Collections.singletonList(Direction.UP);

    @Override
    public List<Direction> getAdditionalUpdateDirections() {
        return additionalUpdateDirections;
    }

    @Override
    public boolean getState(Location<?> location) {
        return location.getRelative(Direction.UP).getBlockType() == BlockTypes.FIRE;
    }

    @Override
    public boolean isValid(Location<World> location) {
        return BlockUtil.doesStatePassFilter(allowedBlocks.getValue(), location.getBlock());
    }
}
