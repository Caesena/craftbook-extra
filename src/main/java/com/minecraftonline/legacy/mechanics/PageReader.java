/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.TernaryState;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeSignMechanic;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.ArgumentParseException;
import org.spongepowered.api.command.args.CommandArgs;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.channel.MessageReceiver;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.util.StartsWithPredicate;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Module(id = "pagereader", name = "PageReader", onEnable="onInitialize", onDisable="onDisable")
public class PageReader extends SpongeSignMechanic {

    private Map<String, Book> cachedPages = null;
    private Set<Player> used = null;
    private File mainDir = null;

    private final Map<UUID, List<String>> cachedBookNames = new HashMap<>();

    private CommandMapping cbPageCommand = null;

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.pagereader.create", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);
    private final SpongePermissionNode usePermissions = new SpongePermissionNode("craftbook.pagereader.use", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "What material(s) this mechanic can be placed on",
            new SpongeBlockFilter(BlockTypes.BOOKSHELF), TypeToken.of(SpongeBlockFilter.class));
    private final ConfigValue<Boolean> readWhenHoldingBlock = new ConfigValue<>("read-with-block",
            "Whether to allow the player to read a book whilst holding a block", false);
    private final ConfigValue<TernaryState> sneakState = new ConfigValue<>("sneak-state", "Sets how the player must be sneaking in order to use a hidden bookshelf.", TernaryState.FALSE,
            TypeToken.of(TernaryState.class));


   private final Text BOOK_KEY = Text.of("book");
   private final Text PAGE_NUM_KEY = Text.of("page#");
   private final Text TEXT_CONTENT_ARG = Text.of("text");
   private final Text TITLE_KEY = Text.of("title");

   public class BookNameCommandElement extends CommandElement {

       protected BookNameCommandElement(@Nullable Text key) {
           super(key);
       }

       @Nullable
       @Override
       protected Object parseValue(@NotNull CommandSource source, CommandArgs args) throws ArgumentParseException {
           return args.next();
       }

       @Override
       public @NotNull List<String> complete(@NotNull CommandSource src, @NotNull CommandArgs args, @NotNull CommandContext context) {
           if (!(src instanceof Player)) {
               return Collections.emptyList();
           }
           Player player = ((Player)src);
           List<String> cachedNames = getBookNames(player);
           if (!args.hasNext()) {
               return cachedNames;
           }
           return cachedNames.stream()
                   .filter(new StartsWithPredicate(args.nextIfPresent().get()))
                   .collect(Collectors.toList());
       }
   }

    @Override
    public void onInitialize() throws CraftBookException {
        this.cachedPages = new ConcurrentHashMap<>();
        this.used = new HashSet<>();

        usePermissions.register();
        createPermissions.register();

        allowedBlocks.load(config);
        readWhenHoldingBlock.load(config);
        sneakState.load(config);

        this.mainDir = new File(
                new File(CraftBookPlugin.spongeInst().getWorkingDirectory(), "mechanics"),
                "pagereader");
        // InteractBlockEvent is fired twice by sponge per right click
        // So we keep record of who has used it, and clear every 2 ticks.
        Sponge.getScheduler().createTaskBuilder()
                .name("Clear used - CBX PageReader")
                .intervalTicks(2)
                .execute(used::clear)
                .submit(CraftBookPlugin.spongeInst());

        CommandElement bookArg = new BookNameCommandElement(BOOK_KEY);
        CommandElement pageArg = GenericArguments.integer(PAGE_NUM_KEY);
        CommandElement textArg = GenericArguments.remainingJoinedStrings(TEXT_CONTENT_ARG);
        CommandElement titleArg = GenericArguments.text(TITLE_KEY, TextSerializers.FORMATTING_CODE, true);

        CommandSpec displayPage = CommandSpec.builder()
                .arguments(bookArg, pageArg)
                .executor((src, args) -> {
                    int pageNum = args.requireOne(PAGE_NUM_KEY);
                    final String bookName = args.requireOne(BOOK_KEY);
                    if (pageNum < 1) {
                        throw new CommandException(Text.of("Page number must be at least 1!"));
                    }
                    SendPageToPlayer sendPageToPlayer = new SendPageToPlayer(PageReader.this, src, getNamespace(src.getName()), bookName, pageNum);
                    schedulePageSend(src, sendPageToPlayer.dir, sendPageToPlayer.bookName, book -> {
                        sendPageToPlayer.run();
                        if (book != null) {
                            Text pageTurner = Text.of(
                                    pageNum > 1 ? Text.of(TextColors.GOLD,
                                            TextActions.runCommand("/cbpage page " + bookName + " " + (pageNum - 1)),
                                            TextActions.showText(Text.of("Previous page")),
                                            "«")
                                            : Text.of(TextColors.GRAY, TextActions.showText(Text.of("No previous page")), "«"),

                                    Text.of(TextColors.GOLD, "  ===  "),

                                    book.hasPage(pageNum + 1) ? Text.of(TextColors.GOLD,
                                            TextActions.runCommand("/cbpage page " + bookName + " " + (pageNum + 1)),
                                            TextActions.showText(Text.of("Next page")),
                                            "»")
                                            : Text.of(TextColors.GRAY, TextActions.showText(Text.of("No next page")), "»")
                            );
                            src.sendMessage(pageTurner);
                        }
                    });
                    return CommandResult.success();
                })
                .build();

        CommandSpec createBook = CommandSpec.builder()
                .arguments(bookArg)
                .executor((src, args) -> {
                    final String bookName = args.requireOne(BOOK_KEY);
                    saveBook(getNamespace(src.getName()), bookName, new Book(bookName), src);
                    src.sendMessage(Text.of(TextColors.GREEN, "Created book: " + bookName));
                    if (src instanceof Player) {
                        getBookNames((Player) src).add(bookName);
                    }
                    return CommandResult.success();
                })
                .build();

        CommandSpec deleteBook = CommandSpec.builder()
                .arguments(bookArg)
                .executor((src, args) -> {
                    final String bookName = args.requireOne(BOOK_KEY);
                    final String namespace = getNamespace(src.getName());
                    File file = new File(namespace, bookName);
                    if (!file.exists()) {
                        throw new CommandException(Text.of("Book " + bookName + " does not exist!"));
                    }
                    if (!file.delete()) {
                        throw new CommandException(Text.of("Failed to delete book " + bookName + "!"));
                    }
                    src.sendMessage(Text.of("Successfully deleted book " + bookName));
                    cachedPages.remove(makeCachedPageKey(namespace, bookName));

                    if (src instanceof Player) {
                        getBookNames((Player) src).remove(bookName);
                    }

                    return CommandResult.success();
                })
                .build();

        CommandSpec viewTitle = CommandSpec.builder()
                .arguments(bookArg)
                .executor((src, args) -> {
                    // Page 0 is always the title.
                    schedulePageSend(new SendPageToPlayer(PageReader.this, src, getNamespace(src.getName()), args.requireOne(BOOK_KEY), 0));
                    return CommandResult.success();
                })
                .build();

        CommandSpec setTitle = CommandSpec.builder()
                .arguments(bookArg, titleArg)
                .executor((src, args) -> {
                    final String namespace = getNamespace(src.getName());
                    final String bookName = args.requireOne(BOOK_KEY);
                    final Text newTitle = args.requireOne(TITLE_KEY);
                    Book book = getBook(namespace, args.requireOne(BOOK_KEY), src);

                    if (book == null) {
                        throw new CommandException(Text.of("Book " + args.requireOne(BOOK_KEY) + " does not exist in namespace " + namespace));
                    }

                    ImmutableList<Book.Line> content = ImmutableList.<Book.Line>builder()
                            .add(new Book.Title(newTitle))
                            .addAll(book.content.subList(1, book.content.size()))
                            .build();
                    Book newBook = new Book(content);
                    cachedPages.put(makeCachedPageKey(namespace, bookName), newBook);
                    saveBook(namespace, bookName, newBook, src);
                    src.sendMessage(Text.of(TextColors.GREEN, "Set the title to: ", newTitle));
                    return CommandResult.success();
                })
                .build();

        CommandSpec appendPage = CommandSpec.builder()
                .arguments(bookArg, textArg)
                .executor((src, args) -> {
                    final String namespace = getNamespace(src.getName());
                    final String bookName = args.requireOne(BOOK_KEY);

                    Text[] textSplitContent = parseTextInput(args);

                    Book book = getBook(namespace, args.requireOne(BOOK_KEY), src);

                    if (book == null) {
                        throw new CommandException(Text.of("Book " + args.requireOne(BOOK_KEY) + " does not exist in namespace " + namespace));
                    }

                    ImmutableList<Book.Line> content = ImmutableList.<Book.Line>builder()
                            .addAll(book.content)
                            .add(new Book.LineBody(textSplitContent))
                            .build();
                    Book newBook = new Book(content);
                    cachedPages.put(makeCachedPageKey(namespace, bookName), newBook);
                    saveBook(namespace, bookName, newBook, src);
                    src.sendMessage(Text.of(TextColors.GREEN, "Appended a page to " + bookName));
                    return CommandResult.success();
                })
                .build();

        CommandSpec removePage = CommandSpec.builder()
                .arguments(bookArg, pageArg)
                .executor((src, args) -> {
                    final String namespace = getNamespace(src.getName());
                    final String bookName = args.requireOne(BOOK_KEY);
                    final int page = args.requireOne(PAGE_NUM_KEY);

                    if (page < 1) {
                        throw new CommandException(Text.of("Page number must be at least 1!"));
                    }

                    Book book = getBook(namespace, args.requireOne(BOOK_KEY), src);

                    if (book == null) {
                        throw new CommandException(Text.of("Book " + args.requireOne(BOOK_KEY) + " does not exist in namespace " + namespace));
                    }

                    if (!book.hasPage(page)) {
                        throw new CommandException(Text.of("Book " + bookName + " only has " + book.getPageCount() + " pages. Cannot delete page " + page));
                    }

                    ImmutableList.Builder<Book.Line> contentBuilder = ImmutableList.builder();

                    for (int i = 0; i < book.content.size(); i++) {
                        if (i == page) {
                            continue;
                        }
                        contentBuilder.add(book.content.get(i));
                    }

                    Book newBook = new Book(contentBuilder.build());
                    cachedPages.put(makeCachedPageKey(namespace, bookName), newBook);
                    saveBook(namespace, bookName, newBook, src);
                    src.sendMessage(Text.of(TextColors.GREEN, "Removed page " + page + " from book " + bookName));
                    return CommandResult.success();
                })
                .build();

        CommandSpec setPage = CommandSpec.builder()
                .arguments(bookArg, pageArg, textArg)
                .executor((src, args) -> {
                    final String namespace = getNamespace(src.getName());
                    final String bookName = args.requireOne(BOOK_KEY);
                    final int page = args.requireOne(PAGE_NUM_KEY);

                    if (page < 1) {
                        throw new CommandException(Text.of("Page number must be at least 1!"));
                    }

                    Text[] textSplitContent = parseTextInput(args);

                    Book book = getBook(namespace, args.requireOne(BOOK_KEY), src);

                    if (book == null) {
                        throw new CommandException(Text.of("Book " + args.requireOne(BOOK_KEY) + " does not exist in namespace " + namespace));
                    }

                    if (!book.hasPage(page)) {
                        throw new CommandException(Text.of("Book " + bookName + " only has " + book.getPageCount() + " pages. Cannot delete page " + page));
                    }

                    ImmutableList.Builder<Book.Line> contentBuilder = ImmutableList.builder();

                    for (int i = 0; i < book.content.size(); i++) {
                        if (i == page) {
                            contentBuilder.add(new Book.LineBody(textSplitContent));
                            continue;
                        }
                        contentBuilder.add(book.content.get(i));
                    }

                    Book newBook = new Book(contentBuilder.build());
                    cachedPages.put(makeCachedPageKey(namespace, bookName), newBook);
                    saveBook(namespace, bookName, newBook, src);
                    src.sendMessage(Text.of("Set value of page " + page + " in book " + bookName));
                    return CommandResult.success();
                })
                .build();

        CommandSpec appendToPage = CommandSpec.builder()
                .arguments(bookArg, pageArg, textArg)
                .executor((src, args) -> {
                    final String namespace = getNamespace(src.getName());
                    final String bookName = args.requireOne(BOOK_KEY);
                    final int page = args.requireOne(PAGE_NUM_KEY);

                    if (page < 1) {
                        throw new CommandException(Text.of("Page number must be at least 1!"));
                    }

                    Book book = getBook(namespace, args.requireOne(BOOK_KEY), src);

                    if (book == null) {
                        throw new CommandException(Text.of("Book " + args.requireOne(BOOK_KEY) + " does not exist in namespace " + namespace));
                    }

                    if (book.hasPage(page)) {
                        throw new CommandException(Text.of("Book " + bookName + " only has " + book.getPageCount() + " pages."));
                    }

                    ImmutableList.Builder<Book.Line> contentBuilder = ImmutableList.builder();

                    for (int i = 0; i < book.content.size(); i++) {
                        if (i == page) {
                            Book.LineBody lineBody = (Book.LineBody) book.content.get(i);
                            if (lineBody.lines.length != 0) {
                                final int last = lineBody.lines.length - 1;
                                Text old = lineBody.lines[last];

                                Text[] toAppend = parseTextInput(args);

                                if (toAppend.length == 0) {
                                    break;
                                }
                                // Merge the line.
                                String newString = TextSerializers.FORMATTING_CODE.serialize(old) + TextSerializers.FORMATTING_CODE.serialize(toAppend[0]);

                                String[] mergedLines = newString.split("<br>");

                                final int requiredLength = lineBody.lines.length + mergedLines.length - 1;

                                Text[] newArray;
                                if (requiredLength != lineBody.lines.length) {
                                   newArray  = Arrays.copyOf(lineBody.lines, requiredLength);
                                }
                                else {
                                    newArray = lineBody.lines;
                                }


                                for (int j = 0; j < mergedLines.length; j++) {
                                    newArray[j + last] = TextSerializers.FORMATTING_CODE.deserialize(mergedLines[j]);
                                }

                                if (toAppend.length > 1) {
                                    // Any extra lines?
                                    System.arraycopy(toAppend, 1, newArray, last + mergedLines.length, newArray.length - 1);
                                }

                                contentBuilder.add(new Book.LineBody(newArray));
                            }
                            else {
                                contentBuilder.add(new Book.LineBody(parseTextInput(args)));
                            }
                            continue;
                        }
                        contentBuilder.add(book.content.get(i));
                    }

                    Book newBook = new Book(contentBuilder.build());
                    cachedPages.put(makeCachedPageKey(namespace, bookName), newBook);
                    saveBook(namespace, bookName, newBook, src);
                    src.sendMessage(Text.of(TextColors.GREEN, "Appended to page " + page + " in book " + bookName));
                    return CommandResult.success();
                })
                .build();

        CommandSpec insertPage = CommandSpec.builder()
                .arguments(bookArg, pageArg, textArg)
                .executor((src, args) -> {
                    final String namespace = getNamespace(src.getName());
                    final String bookName = args.requireOne(BOOK_KEY);
                    final int page = args.requireOne(PAGE_NUM_KEY);

                    if (page < 1) {
                        throw new CommandException(Text.of("Page number must be at least 1!"));
                    }

                    Text[] textSplitContent = parseTextInput(args);

                    Book book = getBook(namespace, args.requireOne(BOOK_KEY), src);

                    if (book == null) {
                        throw new CommandException(Text.of("Book " + args.requireOne(BOOK_KEY) + " does not exist in namespace " + namespace));
                    }

                    if (book.hasPage(page)) {
                        throw new CommandException(Text.of("Book " + bookName + " only has " + book.getPageCount() + " pages. Cannot delete page " + page));
                    }

                    ImmutableList.Builder<Book.Line> contentBuilder = ImmutableList.builder();

                    for (int i = 0; i < book.content.size(); i++) {
                        if (i == page) {
                            contentBuilder.add(new Book.LineBody(textSplitContent));
                        }
                        contentBuilder.add(book.content.get(i));
                    }

                    Book newBook = new Book(contentBuilder.build());
                    cachedPages.put(makeCachedPageKey(namespace, bookName), newBook);
                    saveBook(namespace, bookName, newBook, src);
                    src.sendMessage(Text.of(TextColors.GREEN, "Inserted a blank page at " + page + " in book " + bookName));
                    return CommandResult.success();
                })
                .build();

        CommandSpec listBooks = CommandSpec.builder()
                .executor((src, args) -> {
                    if (!(src instanceof Player)) {
                        throw new CommandException(Text.of("You must be a player to execute this command!"));
                    }
                    PaginationList.builder()
                            .title(Text.of(TextColors.GOLD, "Your books"))
                            .contents(getBookNames((Player) src).stream().map(Text::of).collect(Collectors.toList()))
                            .build()
                            .sendTo(src);
                    return CommandResult.success();
                })
                .build();

        CommandSpec cbPage = CommandSpec.builder()
                .child(displayPage, "page")
                .child(createBook, "create")
                .child(deleteBook, "delete")
                .child(listBooks, "list")
                .child(viewTitle, "title")
                .child(setTitle, "settitle")
                .child(appendPage, "add")
                .child(removePage, "remove")
                .child(insertPage, "^")
                .child(setPage, "=")
                .child(appendToPage, "+")
                .build();

        cbPageCommand = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), cbPage, "cbpage").orElse(null);
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData lines, @Nullable Player player) {
        if (!lines.get(0).isPresent() || lines.get(0).get().isEmpty()) {
            if (player != null) {
                player.sendMessage(Text.of(TextColors.RED, "Namespace line (1st line) cannot be blank!"));
            }
            return false;
        }
        String namespaceLine = lines.get(0).get().toPlain();
        if (!namespaceLine.equals("@") && (player == null || !namespaceLine.equals(player.getName()))) {
            if (player != null) {
                player.sendMessage(Text.of(TextColors.RED, "You do not have permission to access the '" + namespaceLine + "' namespace"));
            }
            return false;
        }
        final String line3 = lines.get(2).map(Text::toPlain).orElse("");
        if (line3.isEmpty()) {
            if (player != null) {
                player.sendMessage(Text.of(TextColors.RED, "Book name (3rd line) cannot be blank!"));
            }
            return false;
        }
        File file = new File(new File(mainDir, getNamespace(namespaceLine)), getBookFilename(line3));
        if (!file.exists()) {
            if (player != null) {
                player.sendMessage(Text.of(TextColors.RED, "Book '" + line3 + "' does not exist in namespace " + namespaceLine + "!"));
            }
            return false;
        }
        final String line4 = lines.get(3).map(Text::toPlain).orElse("");
        if (line4.isEmpty()) {
            return true;
        }
        String[] split = line4.split(":", 2);
        int start = 1;
        try {
            start = Integer.parseInt(split[0]);
            if (start < 1) {
                if (player != null) {
                    player.sendMessage(Text.of(TextColors.RED, "Start page must be at least 1!"));
                }
                return false;
            }
        } catch (NumberFormatException e) {
            if (player != null) {
                player.sendMessage(Text.of(TextColors.RED, "Start page must be a number, was: '" + split[0] + "'"));
            }
        }

        try {
            int end = Integer.parseInt(split[0]);
            if (end < start) {
                if (player != null) {
                    player.sendMessage(Text.of(TextColors.RED, "End page must be greater than or equal to start page."));
                }
                return false;
            }
        } catch (NumberFormatException e) {
            if (player != null) {
                player.sendMessage(Text.of(TextColors.RED, "End page must be a number, was: '" + split[0] + "'"));
            }
        }
        return true;
    }

    public Text[] parseTextInput(CommandContext args) {
        final String content = args.requireOne(TEXT_CONTENT_ARG);

        String[] array = content.split("<br>");
        Text[] lines = new Text[array.length];


        for (int i = 0; i < array.length; i++) {
            lines[i] = TextSerializers.FORMATTING_CODE.deserialize(array[i]);
        }
        return lines;
    }

    public void schedulePageSend(SendPageToPlayer sendPageToPlayer) {
        Sponge.getScheduler().createTaskBuilder()
                .name("CBX - PageReader Async SendPageToPlayer " + UUID.randomUUID().toString())
                .async()
                .execute(sendPageToPlayer)
                .submit(CraftBookPlugin.spongeInst());
    }

    public void schedulePageSend(MessageReceiver messageReceiver, String dir, String bookName, Consumer<Book> bookConsumer) {
        Sponge.getScheduler().createTaskBuilder()
                .name("CBX - PageReader Async Book Consumer " + UUID.randomUUID().toString())
                .async()
                .execute(() -> {
                    Book book = cachedPages.get(dir + "/" + bookName);

                    if (book == null) {
                        book = getBook(dir, bookName, messageReceiver);
                    }
                    bookConsumer.accept(book);
                })
                .submit(CraftBookPlugin.spongeInst());
    }

    @Override
    public void onDisable() {
        if (cbPageCommand != null) {
            Sponge.getCommandManager().removeMapping(cbPageCommand);
        }
    }

    @Override
    public String[] getValidSigns() {
        return new String[] {"[Book]", "[Book][X]"};
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Listener
    public void onRightClick(InteractBlockEvent.Secondary e, @First Player player) {
        BlockSnapshot snapshot = e.getTargetBlock();
        if (snapshot.getState().getType() != BlockTypes.BOOKSHELF
                || !snapshot.getLocation().isPresent()
                || this.used.contains(player)) {
            return;
        }
        this.used.add(player);

        Location<World> loc = snapshot.getLocation().get();
        Sign sign = null;
        for (Direction dir : BlockUtil.getDirectHorizontalFaces()) {
            Location<World> side = loc.getBlockRelative(dir);
            if (isValid(side)) {
                sign = (Sign) side.getTileEntity().get();
                break;
            }
        }
        if (sign == null) {
            return;
        }
        List<Text> lines = sign.require(Keys.SIGN_LINES);
        String dir = getNamespace(lines.get(0).toPlain());
        String book = lines.get(2).toPlain();
        String[] args = lines.get(3).toPlain().split(":", 3);

        int cur;

        try {
            if (args.length == 1) {
                cur = args[0].isEmpty() ? 1 : Integer.parseInt(args[0]);
            }
            else if (args.length == 2) {
                cur = Integer.parseInt(args[0]);
                lines.set(3, Text.of(args[0] + ":" + args[0] + ":" + args[1]));
                sign.offer(Keys.SIGN_LINES, lines);
            }
            else {
                cur = Integer.parseInt(args[0]);
                int start = Integer.parseInt(args[1]);
                int end = Integer.parseInt(args[2]);

                if (cur >= end || cur >= 9999) {
                    cur = start;
                }
                else if (cur < start) {
                    cur = start;
                }
                else {
                    cur++;
                }
                lines.set(3, Text.of(cur + ":" + args[1] + ":" + args[2]));
                sign.offer(Keys.SIGN_LINES, lines);
            }
        } catch (NumberFormatException ex) {
            player.sendMessage(Text.of(TextColors.RED, "Page, start or end values on fourth line were not numbers"));
            return;
        }

        Sponge.getScheduler().createTaskBuilder()
                .name("CBX - PageReader Async SendPageToPlayer " + UUID.randomUUID().toString())
                .async()
                .execute(new SendPageToPlayer(this, player, dir, book, cur))
                .submit(CraftBookPlugin.spongeInst());
    }

    @Override
    public boolean isValid(Location<World> location) {
        if (location.getBlockType() == BlockTypes.WALL_SIGN) {
            Sign sign = (Sign) location.getTileEntity().get();

            BlockType backBlock = location.getRelative(location.require(Keys.DIRECTION).getOpposite()).getBlockType();

            return backBlock == BlockTypes.BOOKSHELF && isMechanicSign(sign.getSignData());
        }

        return false;
    }

    public static class SendPageToPlayer implements Runnable {
        private final PageReader instance;
        private final MessageReceiver messageReceiver;
        private final String dir;
        private final String bookName;
        private final int page;

        public SendPageToPlayer(PageReader instance, MessageReceiver messageReceiver, String dir, String book, int page) {
            this.instance = instance;
            this.messageReceiver = messageReceiver;
            this.dir = dir;
            this.bookName = book;
            this.page = page;
        }

        @Override
        public void run() {
            Book book = instance.cachedPages.get(dir + "/" + bookName);

            if (book == null) {
                book = instance.getBook(dir, bookName, messageReceiver);
            }
            if (book != null) {
                if (!book.hasPage(page)) {
                    messageReceiver.sendMessage(Text.of(TextColors.RED, "Book " + this.bookName + " does not have a page " + page + ", it has " + book.getPageCount() + " pages!"));
                    return;
                }
                // Page 0 is title, so if theres more than one page, send page number.
                if (book.content.size() > 2 && page > 1) {
                    book.getTitle().sendToPlayer(messageReceiver, page);
                } else {
                    book.getTitle().sendToPlayer(messageReceiver);
                }

                if (page == 0) {
                    return; // Only want the title.
                }

                book.read(page).sendToPlayer(messageReceiver);
            }
            else {
                messageReceiver.sendMessage(Text.of(TextColors.RED, "Error fetching page " + page + " from " + (dir.isEmpty() ? "" : dir + "/") + bookName));
            }
        }
    }

    public static class Book {
        private final ImmutableList<Book.Line> content; // First entry is title

        public Book(String title) {
            this.content = ImmutableList.<Line>builder().add(new Title(Text.of(title))).build();
        }

        public Book(ImmutableList<Book.Line> content) {
            this.content = content;
        }

        public Book(String[] content) {
            if (content.length == 0) {
                throw new IllegalArgumentException("Book given content with no length");
            }
            ImmutableList.Builder<Book.Line> builder = ImmutableList.builder();

            Text title = TextSerializers.FORMATTING_CODE.deserialize(content[0]);
            if (title.getColor() == TextColors.NONE) {
                title = title.toBuilder().color(TextColors.GOLD).build();
            }
            builder.add(new Book.Title(title));
            for (int i = 1; i < content.length; i++) {
                String[] splitContent = content[i].split("<br>", 20);
                Text[] lines = new Text[splitContent.length];
                for (int j = 0; j < splitContent.length; j++) {
                    lines[j] = TextSerializers.FORMATTING_CODE.deserialize(splitContent[j]);
                }
                builder.add(new Book.LineBody(lines));
            }
            this.content = builder.build();
        }

        /**
         * Reads the given page.
         * Page 0 is title, after is all content.
         * @param page index
         * @return Text given page (or title if page is 0)
         */
        public Book.Line read(int page) {
            Preconditions.checkElementIndex(page, content.size(), "Invalid page number.");
            return content.get(page);
        }

        public boolean hasPage(int page) {
            return page >= 0 && page < content.size();
        }

        public int getPageCount() {
            return content.size() - 1;
        }

        public Book.Title getTitle() {
            return (Book.Title) content.get(0);
        }

        public interface Line {
            /**
             * Sends the line to the player
             */
            void sendToPlayer(MessageReceiver player);

            String serialize();
        }

        public static class Title implements Book.Line {
            private final Text title;
            public Title(Text title) {
                this.title = title;
            }

            @Override
            public void sendToPlayer(MessageReceiver player) {
                player.sendMessage(title);
            }

            @Override
            public String serialize() {
                return TextSerializers.FORMATTING_CODE.serialize(title);
            }

            public void sendToPlayer(MessageReceiver messageReceiver, int page) {
                messageReceiver.sendMessage(title.toBuilder().append(Text.of(TextColors.GRAY, " [" + page + "]")).build());
            }
        }

        public static class LineBody implements Book.Line {
            Text[] lines;
            public LineBody(Text[] lines) {
                this.lines = lines;
            }

            @Override
            public void sendToPlayer(MessageReceiver messageReceiver) {
                for (Text text : lines) {
                    messageReceiver.sendMessage(text);
                }
            }

            @Override
            public String serialize() {
                return Arrays.stream(lines).map(TextSerializers.FORMATTING_CODE::serialize).collect(Collectors.joining("<br>"));
            }
        }
    }

    public static String getNamespace(final String namespace) {
        if (namespace.equals("@")) {
            return "global";
        }

        // Probably a good idea. We don't want old players to bring the server crashing down.
        if (isValidName(namespace)) {
            return "~" + namespace;
        }
        return "";
    }

    public static boolean isValidName(String name) {
        return name.length() > 0 && name.length() <= 30
                && name.matches("^[A-Za-z0-9_\\- ]+$");
    }

    public static String makeCachedPageKey(final String dir, final String bookName) {
        return dir + "/" + bookName;
    }

    public static String getBookFilename(final String bookName) {
        return bookName + ".txt";
    }

    public Book getBook(String dir, String bookName, MessageReceiver messageReceiver) {
        mainDir.mkdirs();
        File file = new File(new File(mainDir, dir), getBookFilename(bookName));
        if (!file.exists()) {
            return null;
        }
        String[] content;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            content = br.lines().toArray(String[]::new);
        } catch (FileNotFoundException e) {
            messageReceiver.sendMessage(Text.of(TextColors.RED, "Book is given directory doesn't exist"));
            CraftBookPlugin.spongeInst().getLogger().error("Error reading book", e);
            return null;
        }
        Book book = new Book(content);
        cachedPages.put(dir + "/" + bookName, book);
        return book;
    }

    public void saveBook(String dir, String bookName, Book book, MessageReceiver messageReceiver) {
        File namespaceFolder = new File(mainDir, dir);
        namespaceFolder.mkdirs();
        File file = new File(namespaceFolder, getBookFilename(bookName));

        try {
            final BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            for (Book.Line line : book.content) {
                bw.write(line.serialize());
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {
            messageReceiver.sendMessage(Text.of(TextColors.RED, "Error writing book!"));
            CraftBookPlugin.spongeInst().getLogger().error("Error writing book", e);
        }
    }

    public List<String> getBookNames(Player player) {
        List<String> cachedNames = cachedBookNames.get(player.getUniqueId());
        if (cachedNames == null) {
            String[] files = new File(mainDir, getNamespace(player.getName())).list();
            if (files != null) {
                cachedNames = Arrays.stream(files)
                        .filter(s -> s.endsWith(".txt"))
                        .map(s -> s.substring(0, s.length() - ".txt".length())) // Strip .txt off the end.
                        .collect(Collectors.toList());
            }
            else {
                cachedNames = new ArrayList<>();
            }

            cachedBookNames.put(player.getUniqueId(), cachedNames);
        }
        return cachedNames;
    }
}
