/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.cbwarps;

import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import org.apache.commons.lang3.StringUtils;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Tuple;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Format in text files. [] = optional.
 * name[:world]:dim:x:y:z:rot:pitch:[pass:][destmsg]:[welcomemsg]
 */
public class CBWarp {
    protected String title;
    private String worldname;
    private short dim;
    private Vector3d coordinates;
    private Vector2d looking;
    private String destinationmsg;
    private String welcomemsg;
    @Nullable
    private String password;

    public CBWarp() {}

    public CBWarp(String title, String worldname, short dim, Vector3d coordinates, Vector2d looking, String destinationmsg, String welcomemsg, @Nullable String password) {
        this.title = title;
        this.worldname = worldname;
        this.dim = dim;
        this.coordinates = coordinates;
        this.looking = looking;
        this.destinationmsg = destinationmsg;
        this.welcomemsg = welcomemsg;
        this.password = password;
    }

    public static CBWarp fromLine(String line) {
        line = line.replace("\n", "");
        List<String> parts = splitLine(line);

        if (parts == null) {
            CraftBookPlugin.spongeInst().getLogger().error("Invalid CBWarp line given to CBWarp fromLine: " + line);
            return null;
        }
        if (StringUtils.isNumeric(parts.get(1))) {
            parts.add(1, Sponge.getServer().getDefaultWorldName());
        }
        if (!(parts.size() == 10 || parts.size() == 11)) {
            CraftBookPlugin.spongeInst().getLogger().error("String given to CBWarp fromLine did not have first 10/11 parts: " + line);
            return null;
        }
        CBWarp warp = new CBWarp();
        warp.title = parts.get(0);
        warp.worldname = parts.get(1);
        warp.dim = Short.parseShort(parts.get(2));
        warp.coordinates = new Vector3d(
                Double.parseDouble(parts.get(3)),
                Double.parseDouble(parts.get(4)),
                Double.parseDouble(parts.get(5))
        );
        warp.looking = new Vector2d(
                Double.parseDouble(parts.get(7)),
                Double.parseDouble(parts.get(6))
        );
        if (parts.size() == 10) {
            warp.destinationmsg = parts.get(8);
            warp.welcomemsg = parts.get(9);
        }
        else {
            warp.password = parts.get(8);
            warp.destinationmsg = parts.get(9);
            warp.welcomemsg = parts.get(10);
        }
        return warp;
    }

    public String getTitle() {return title;}
    public String getWorldname() {return worldname;}
    public short getDim() { return dim; }
    public Vector3d getCoordinates() { return coordinates; }
    public Vector2d getLooking() { return looking; }
    public String getDestinationmsg() { return destinationmsg; }
    public String getWelcomemsg() { return welcomemsg; }

    public boolean hasPassword() {return password != null;}
    public String getPassword() {return password;}
    public File getFile() {return CBWarps.file;}

    private static List<String> splitLine(String line) {
        List<String> list = new ArrayList<>();
        String[] colonParts = line.split(":", -1);
        if (colonParts.length < 3) {
            CraftBookPlugin.spongeInst().getLogger().warn("Not enough colons in CBWarp line");
            return null;
        }
        int commaPart = colonParts[0].contains(",") ? 0 : 1;
        String[] commaParts = colonParts[commaPart].split(",");
        for (int i = 0; i < colonParts.length; i++) {
            if (i == commaPart) {
                list.addAll(Arrays.asList(commaParts));
            }
            else {
                list.add(colonParts[i]);
            }
        }
        return list;
    }

    @Nullable
    public String tpToWarp(Entity entity, String password, @Nullable Player caller) {
        if (entity.getVehicle().isPresent()) {
            throw new IllegalArgumentException("You should not be trying to teleport the non-base entity!");
        }

        World world = null;
        if (dim == 0) {
            world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName()).get();
        }
        else if (dim == -1)
            world = Sponge.getServer().getWorld("DIM-1").get();
        else if (dim == 1)
            world = Sponge.getServer().getWorld("DIM1").get();

        if (world == null) {
            CraftBookPlugin.spongeInst().getLogger().error("In CBWarp.tpToWarp was given a invalid dimension. dim number: " + dim);
            return "An error occured, check console for details: invalid dimension";
        }
        if (password != null) {
            password = encrypt(password);
            if (!password.equals(getPassword())) {
                if (caller != null) {
                    CraftBookPlugin.spongeInst().getLogger().info(caller.getName() + " tried to tp to CBWarp " + getTitle() + " but entered the incorrect password");
                }
                else {
                    CraftBookPlugin.spongeInst().getLogger().info("Null player, tried to tp " + entity.getType().getName() + " to " + getTitle() + " but entered the incorrect password");
                }
                return "Invalid password";
            }
        }
        else if (this.password != null) {
            if (caller != null) {
                CraftBookPlugin.spongeInst().getLogger().info(caller.getName() + " tried to tp to CBWarp " + getTitle() + " but didn't enter a password");
            }
            return "Missing password";
        }

        // Imagine if we could just teleport the base entity without
        // everything completely glitching out.

        // Instead we dismount all players and remember who they need to be re-attached to.
        // This also serves as a list of players to message for destination arrival + logging.
        Map<Player, Entity> toRemount = new LinkedHashMap<>();

        // Since we're using this map for messaging too, add the base entity
        // if its a player with null to reattach
        if (entity instanceof Player) {
            toRemount.put((Player) entity, null);
        }

        dismountAndFillRemountMap(entity, toRemount);

        for (Player player : toRemount.keySet()) {
            if (!destinationmsg.isEmpty())
                player.sendMessage(Text.of(TextColors.GREEN, destinationmsg));
        }

        Location<World> loc = new Location<>(world, coordinates);
        loc.getExtent().loadChunk(loc.getChunkPosition(), false);
        if (!entity.setLocationAndRotation(loc, looking.toVector3())) {
            CraftBookPlugin.spongeInst().getLogger().warn(entity + " unable to tp using a cbwarp, as the movement event was cancelled by another plugin.");
            return "Unable to cbwarp due to a plugin";
        }



        for (Player player : toRemount.keySet()) {
            if (!welcomemsg.isEmpty()) {
                player.sendMessage(Text.of(TextColors.GREEN,welcomemsg));
            }

            StringBuilder stringBuilder = new StringBuilder("Player: ");
            stringBuilder.append(player.getName())
                    .append(" (")
                    .append(player.getUniqueId())
                    .append(") was tp'd to: ")
                    .append(getTitle());
            if (caller != null) {
                stringBuilder.append(" ").append("by player: ").append(caller.getName());
            }
            CraftBookPlugin plugin = CraftBookPlugin.spongeInst();
            plugin.getLogger().info(stringBuilder.toString());
        }

        // Remount players.
        for (Map.Entry<Player, Entity> entry : toRemount.entrySet()) {
            Entity rideable = entry.getValue();
            // Null if it was just the base entity we added for messaging purposes.
            if (rideable != null) {
                entry.getKey().setLocationAndRotation(loc, looking.toVector3());
                rideable.addPassenger(entry.getKey());
            }
        }

        return null;
    }

    public static void dismountAndFillRemountMap(Entity entity, Map<Player, Entity> remountMap) {
        for (Entity passenger : entity.getPassengers()) {
            if (passenger instanceof Player) {
                Player player = (Player) passenger;
                remountMap.put(player, entity);
                ((net.minecraft.entity.Entity) passenger).dismountRidingEntity();
            }
            dismountAndFillRemountMap(passenger, remountMap);
        }
    }

    public static boolean exists(String warpname) {
        for (CBWarp warp : CBWarps.warps) {
            if (warp.getTitle().equals(warpname))
                return true;
        }
        return false;
    }

    public static CBWarp getWarp(String title) {
        for (CBWarp warp : CBWarps.warps) {
            if (warp.getTitle().equalsIgnoreCase(title))
                return warp;
        }
        return null;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(getTitle());
        stringBuilder.append(":").append(getWorldname())
                .append(",").append(getDim());
        Vector3d coordinates = getCoordinates();

        stringBuilder.append(",").append(coordinates.getX())
                .append(",").append(coordinates.getY())
                .append(",").append(coordinates.getZ())
                .append(",").append(getLooking().getY())
                .append(",").append(getLooking().getX());

        if (hasPassword())
            stringBuilder.append(":").append(encrypt(password));
        stringBuilder.append(":").append(getDestinationmsg())
                .append(":").append(getWelcomemsg()).append("\n");
        return stringBuilder.toString();
    }

    public static void addWarp(File file, CBWarp warp) {
        try {
            if (!file.isFile() && !file.createNewFile()) {
                CraftBookPlugin.spongeInst().getLogger().error("Failed to create new file, " + file.getAbsolutePath());
                return;
            }
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));
            bufferedWriter.write(warp.toString());
            bufferedWriter.close();
            if (warp.hasPassword())
                CBWarps.p_warps.add(warp);
            else
                CBWarps.warps.add(warp);
        } catch (IOException e) {CraftBookPlugin.spongeInst().getLogger().error(e.getMessage());}
    }

    public static String removeWarp(File file, String title, String password) {
        if (!file.isFile()) {
            return "File not found";
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, false));
            boolean foundwarp = false;
            for (CBWarp warp : password == null ? CBWarps.warps : CBWarps.p_warps) {
                if (warp.getTitle().equals(title)) {
                    foundwarp = true;
                    if (!warp.hasPassword() || warp.getPassword().equals(password)) {
                        break;
                    }
                    else {
                        return "Invalid Password";
                    }
                }
            }
            if (!foundwarp)
                return "Could not find warp with name: " + title;
            bufferedReader.lines().forEach(line -> {
                if (!line.startsWith(title)) {
                    try {
                        bufferedWriter.write(line);
                    } catch (IOException e) {
                        CraftBookPlugin.spongeInst().getLogger().error(e.getMessage());
                    }
                }
            });
        }
        catch (IOException e) {
            CraftBookPlugin.spongeInst().getLogger().error(e.getMessage());
            return "IOException";
        }
        return "Successfully removed warp";
    }

    public static List<CBWarp> loadFile(File file) {
        try {
            return Files.lines(file.toPath())
                    .filter(line -> !line.isEmpty() && !line.startsWith("#"))
                    .map(CBWarp::fromLine)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            CraftBookPlugin.spongeInst().getLogger().error(e.getMessage());
            return null;
        }
    }

    public static String encrypt(String password) {
        return String.valueOf(password.hashCode());
    }
}
