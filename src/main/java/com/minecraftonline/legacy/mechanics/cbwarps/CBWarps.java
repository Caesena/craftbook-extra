/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.cbwarps;


import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;
import com.google.common.collect.ImmutableMap;
import com.me4502.modularframework.module.Module;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeMechanic;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.*;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

@Module(id = "cbwarps", name = "CBWarps", onEnable="onInitialize", onDisable="onDisable")
public class CBWarps extends SpongeMechanic {

    private CommandMapping MainCommandMapping;
    private CommandMapping ReloadCommandMapping;
    private CommandMapping PassMainCommandMapping;

    static File file;
    static File passfile;

    public static List<CBWarp> warps = new LinkedList<>();
    public static List<CBWarp> p_warps = new LinkedList<>();


    @Override
    public void onInitialize() throws CraftBookException {
        file = new File(CraftBookPlugin.spongeInst().getWorkingDirectory(), "warps.txt");
        passfile = new File(CraftBookPlugin.spongeInst().getWorkingDirectory(), "p_warps.txt");

        warps = CBWarp.loadFile(file);
        p_warps = CBWarp.loadFile(passfile);

        CommandSpec Reload = CommandSpec.builder()
                .executor(new Reload())
                .permission("craftbook.cbwarps.reload")
                .build();

        ReloadCommandMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), Reload, "cbwarpsreload").get();

        CommandSpec ManualAdd = CommandSpec.builder()
                .arguments(
                        GenericArguments.string(Text.of("title")),
                        GenericArguments.world(Text.of("world")),
                        GenericArguments.dimension(Text.of("dim")),
                        GenericArguments.vector3d(Text.of("coordinates")),
                        GenericArguments.doubleNum(Text.of("yaw")),
                        GenericArguments.doubleNum(Text.of("pitch")),
                        GenericArguments.string(Text.of("destination msg")),
                        GenericArguments.string(Text.of("welcome msg"))
                )
                .executor(new ManualAdd())
                .build();



        CommandSpec AutoAdd = CommandSpec.builder()
                .arguments(
                        GenericArguments.string(Text.of("title")),
                        GenericArguments.string(Text.of("destinationmsg")),
                        GenericArguments.string(Text.of("welcomemsg")),
                        GenericArguments.optional(GenericArguments.player(Text.of("player"))),
                        GenericArguments.optional(GenericArguments.choices(Text.of("alignment"),
                                ImmutableMap.of("aligncorner", "aligncorner", "aligncentre", "aligncentre"),
                                true))
                        )
                .executor(new AutoAdd())
                .build();

        CommandSpec Add = CommandSpec.builder()
                .child(ManualAdd, "manual")
                .child(AutoAdd, "auto")
                .permission("craftbook.cbwarps.add")
                .build();

        CommandSpec tp = CommandSpec.builder()
                .arguments(GenericArguments.string(Text.of("warpname")),
                        GenericArguments.optional(GenericArguments.player(Text.of("player"))))
                .permission("craftbook.cbwarps.tp")
                .executor(new Tp())
                .build();

        CommandSpec Listwarps = CommandSpec.builder()
                .permission("craftbook.cbwarps.list")
                .executor(new Listwarps())
                .build();

        CommandSpec Help = CommandSpec.builder()
                .permission("craftbook.cbwarps.help")
                .executor(new Help())
                .build();

        CommandSpec Remove = CommandSpec.builder()
                .permission("craftbook.cbwarps.remove")
                .arguments(GenericArguments.string(Text.of("warp")))
                .executor(new Remove())
                .build();

        CommandSpec MainCommand = CommandSpec.builder()
                .permission("craftbook.cbwarps.maincommand")
                .child(Add, "add")
                .child(Remove, "remove")
                .child(tp, "tp")
                .child(Listwarps, "list")
                .child(Help, "help")
                .build();

        MainCommandMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), MainCommand, "cbwarp").get();

        CommandSpec PassAdd = CommandSpec.builder()
                .arguments(GenericArguments.string(Text.of("password")))
                .child(ManualAdd, "manual")
                .child(AutoAdd, "auto")
                .permission("craftbook.cbwarps.passadd")
                .build();

        CommandSpec Passtp = CommandSpec.builder()
                .arguments(GenericArguments.string(Text.of("warpname")),
                        GenericArguments.string(Text.of("password")),
                        GenericArguments.optional(GenericArguments.player(Text.of("player"))))
                .permission("craftbook.cbwarps.passtp")
                .executor(new Tp())
                .build();

        CommandSpec PassRemove = CommandSpec.builder()
                .permission("craftbook.cbwarps.passremove")
                .arguments(GenericArguments.string(Text.of("warp")),
                        GenericArguments.string(Text.of("password")))
                .executor(new Remove())
                .build();

        CommandSpec PassMainCommand = CommandSpec.builder()
                .permission("craftbook.cbwarps.passmaincommand")
                .child(PassAdd, "add")
                .child(PassRemove, "remove")
                .child(Passtp, "tp")
                .build();

        PassMainCommandMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), PassMainCommand, "cbwarpx").get();
    }

    @Override
    public void onDisable() {
        if (ReloadCommandMapping != null) {
            Sponge.getCommandManager().removeMapping(ReloadCommandMapping);
        }
        if (MainCommandMapping != null) {
            Sponge.getCommandManager().removeMapping(MainCommandMapping);
        }
        if (PassMainCommandMapping != null) {
            Sponge.getCommandManager().removeMapping(PassMainCommandMapping);
        }
    }

    public static class Reload implements CommandExecutor {
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            warps.clear();
            p_warps.clear();
            warps = CBWarp.loadFile(file);
            p_warps = CBWarp.loadFile(passfile);
            src.sendMessage(Text.of("Reloaded warps from file"));
            return CommandResult.success();
        }
    }

    public static class ManualAdd implements CommandExecutor {
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

            String password = args.<String>getOne(Text.of("password")).orElse(null);
            String title = args.<String>getOne("title").get();
            String worldname = args.<World>getOne("world").get().getName();
            Dimension dimension = args.<Dimension>getOne("dim").get();
            Vector3d coordinates = args.<Vector3d>getOne("coordinates").get();
            double pitch = args.<Double>getOne("pitch").get();
            double yaw = args.<Double>getOne("yaw").get();
            String destinationmsg = args.<String>getOne("destinationmsg").get();
            String welcomemsg = args.<String>getOne("welcomemsg").get();

            short dim = getDimNumFromDim(dimension);

            CBWarp warp = new CBWarp(title, worldname, dim, coordinates, new Vector2d(pitch, yaw), destinationmsg, welcomemsg, password);
            CBWarp.addWarp(password == null ? file : passfile, warp);

            src.sendMessage(Text.of(TextColors.GREEN, "Warp: " + title + " successfully created"));
            return CommandResult.success();
        }
    }

    public static class AutoAdd implements CommandExecutor {
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String password = args.<String>getOne(Text.of("password")).orElse(null);
            Player player = args.<Player>getOne("player").orElse(null);
            String title = args.<String>getOne("title").get();
            String destinationmsg = args.<String>getOne("destinationmsg").get();
            String welcomemsg = args.<String>getOne("welcomemsg").get();
            String alignment = args.<String>getOne("alignment").orElse(null);

            if (player == null && !(src instanceof Player)) {
                throw new CommandException(Text.of("You must specify a player, be a player to use Auto add"));
            }
            if (player != null)
                player = (Player) src;

            // Vector3d, because roll is included, it is always 0
            Vector2d rotation = player.getRotation().toVector2();
            short dim = getDimNumFromDim(player.getWorld().getDimension());

            Vector3d position = player.getLocation().getPosition();

            if (alignment != null) {
                if (alignment.equals("aligncorner")) {
                    position = player.getLocation().getPosition().toInt().toDouble();
                }
                else if (alignment.equals("aligncentre")) {
                    position = player.getLocation().getPosition().toInt().toDouble().add(0.5,0,0.5);
                }
            }

            CBWarp warp = new CBWarp(title, player.getWorld().getName(), dim, position, rotation, destinationmsg, welcomemsg, password);
            CBWarp.addWarp(password == null ? file : passfile, warp);

            src.sendMessage(Text.of(TextColors.GREEN, "Warp: " + title + " successfully created"));
            return CommandResult.success();
        }
    }

    public static class Tp implements CommandExecutor {
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String password = args.<String>getOne("password").orElse(null);
            String warpname = args.<String>getOne(Text.of("warpname")).get();
            Player player = args.<Player>getOne(Text.of("player")).orElse(null);

            if (player == null) {
                if (src instanceof Player) {
                    player = (Player) src;
                }
                else {
                    throw new CommandException(Text.of("Must be called from a player or give player as an argument"));
                }
            }
            for (CBWarp warp : password == null ? warps : p_warps) {
                if (warp.title.equals(warpname)) {
                    String msg = warp.tpToWarp(player, password, src instanceof Player ? (Player) src : player);
                    if (msg == null) {
                        return CommandResult.success();
                    }
                    throw new CommandException(Text.of(msg));
                }
            }
            throw new CommandException(Text.of("Warp was not found"));
        }
    }

    public static class Listwarps implements CommandExecutor {
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            StringBuilder stringBuilder = new StringBuilder("The CBwarps are:");
            for (CBWarp warp : warps) {
                stringBuilder.append(" ").append(warp.getTitle()).append(",");
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            src.sendMessage(Text.of(TextColors.GREEN, stringBuilder.toString()));
            return CommandResult.success();
        }
    }

    public static class Remove implements CommandExecutor {
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            String password = args.<String>getOne(Text.of("password")).orElse(null);
            String warpname = args.<String>getOne(Text.of("warp")).get();
            if (password != null)
                password = String.valueOf(password.hashCode());

            String msg = CBWarp.removeWarp(password == null ? file : passfile, warpname, password);

            if (msg.equals("Successfully removed warp"))
                src.sendMessage(Text.of(TextColors.GREEN, "Removed all CBWarps with name: " + warpname));
            else {
                throw new CommandException(Text.of(msg));
            }
            return CommandResult.success();
        }
    }

    public static class Help implements CommandExecutor {
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            src.sendMessage(Text.of(TextColors.GREEN, "Used to add, list or tp to a CBwarp. Use quotes (\") around a single arguments if you want to make a multiple word argument, eg. for welcome message"));
            return CommandResult.success();
        }
    }

    public static short getDimNumFromDim(Dimension dim) {
        if (dim.getType().equals(DimensionTypes.OVERWORLD))
            return 0;
        else if (dim.getType().equals(DimensionTypes.NETHER))
            return -1;
        else if (dim.getType().equals(DimensionTypes.THE_END))
            return 1;
        return -10;
    }
}
