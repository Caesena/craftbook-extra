/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.TernaryState;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Module(id = "cartdelay", name = "CartDelay", onEnable="onInitialize", onDisable="onDisable")
public class CartDelay extends SpongeCartBlockMechanic {
    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartdelay", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<Double> launchSpeed = new ConfigValue<>("launch-speed-multiplier", "Multiplier for launch speed after delay", 1.0, TypeToken.of(Double.class));
    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.",
            getDefaultFilter(), TypeToken.of(SpongeBlockFilter.class));

    @Override
    public void onInitialize() throws CraftBookException {
        launchSpeed.load(config);
        allowedBlocks.load(config);
        createPermissions.register();
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {
        if (minor || isActive(blocks) == TernaryState.FALSE) {
            return false;
        }
        minecart.setVelocity(Vector3d.ZERO);
        int delay;
        try {
            delay = Integer.parseInt(
                    SignUtil.getTextRaw((Sign)blocks.getSign().getTileEntity().get(), 2)
            );
        } catch (NumberFormatException e) {
            CraftBookPlugin.spongeInst().getLogger().warn("Corrupted Delay mechanic at: " + blocks.getSign() + ", delay wasn't a number!");
            return false;
        }
        Vector3d velocity = blocks.getSign().get(Keys.DIRECTION).get().getOpposite().asOffset().mul(launchSpeed.getValue());
        Sponge.getScheduler()
                .createTaskBuilder()
                .name("CBX - CartDelay " + UUID.randomUUID().toString())
                .execute(new LaunchCart(minecart, velocity))
                .delay(delay, TimeUnit.SECONDS) // TODO: REVIEW FOR CAUSE?
                .submit(CraftBookPlugin.spongeInst());
        return false;
    }

    public static class LaunchCart implements Runnable {
        private final Location<World> startLoc;
        private final Minecart minecart;
        private final Vector3d launchVelocity;

        public LaunchCart(Minecart minecart, Vector3d launchVelocity) {
            this.startLoc = minecart.getLocation();
            this.minecart = minecart;
            this.launchVelocity = launchVelocity;
        }

        @Override
        public void run() {
            // If minecart has moved, don't launch it.
            Vector3d velocity = minecart.getVelocity();
            if (velocity.getX() != 0 || velocity.getY() != 0 || velocity.getZ() != 0 || !minecart.getLocation().equals(startLoc)) {
                return;
            }
            minecart.setVelocity(launchVelocity);
        }
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        try {
            int delay = Integer.parseInt(SignUtil.getTextRaw(signData, 2));
            if (delay <= 0) {
                if (player != null) {
                    player.sendMessage(Text.of(TextColors.RED, "delay in seconds must be greater than 0!"));
                }
                return false;
            }
        } catch (NumberFormatException e) {
            if (player != null) {
                player.sendMessage(Text.of(TextColors.RED, "delay on third line must be a number!"));
            }
            return false;
        }
        return true;
    }

    @Override
    public SpongeBlockFilter getBlockFilter() {
        return allowedBlocks.getValue();
    }

    public SpongeBlockFilter getDefaultFilter() {
        return new SpongeBlockFilter(
                BlockState.builder().blockType(BlockTypes.WOOL).build().with(Keys.DYE_COLOR, DyeColors.YELLOW).get());
    }

    @Override
    public boolean requiresSign() {
        return true;
    }

    @Override
    public String[] getValidSigns() {
        return new String[] {"[Delay]"};
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }
}
