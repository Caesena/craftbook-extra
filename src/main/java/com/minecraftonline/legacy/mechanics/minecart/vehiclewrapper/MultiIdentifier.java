/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart.vehiclewrapper;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class MultiIdentifier {
    private final List<VehicleIdentifier> identifiers;

    public MultiIdentifier(List<VehicleIdentifier> identifiers) {
        this.identifiers = identifiers;
    }

    public Optional<VehicleIdentifier> fromString(String id) {
        return identifiers.stream()
                .filter(identifier -> identifier.getName().equalsIgnoreCase(id))
                .findAny();
    }

    public VehicleIdentifier fromItemStack(ItemStack itemStack) {
        return identifiers.stream()
                .filter(identifier -> identifier.getItemPredicate().test(itemStack))
                .findAny().orElseThrow(() -> new RuntimeException("ItemStack given to MultiIdentifier was not supported by any of the identifiers."));
    }

    public VehicleIdentifier fromEntity(Entity entity) {
        return identifiers.stream()
                .filter(identifer -> identifer.getEntityPredicate().test(entity))
                .findAny().orElse(null);
    }

    public Predicate<ItemStack> getItemPredicate() {
        return itemStack -> identifiers.stream()
                .map(VehicleIdentifier::getItemPredicate)
                .anyMatch(predicate -> predicate.test(itemStack));
    }
}
