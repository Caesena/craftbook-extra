/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart.vehiclewrapper;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.extent.Extent;

import java.util.function.Predicate;

public abstract class MinecartIdentifier implements VehicleIdentifier {
    protected ItemType itemType;
    protected EntityType entityType;

    public MinecartIdentifier(ItemType itemType, EntityType entityType) {
        this.itemType = itemType;
        this.entityType = entityType;
    }

    @Override
    public Predicate<ItemStack> getItemPredicate() {
        return itemStack -> itemStack.getType() == itemType;
    }

    @Override
    public Predicate<Entity> getEntityPredicate() {
        return entity -> entity.getType() == entityType;
    }

    @Override
    public Location<World> findValidSpawnLocation(Location<World> fromLoc, Direction dir, int maxDistance) {
        int i = 0;
        Location<World> loc = fromLoc;
        do {
            loc = loc.getRelative(dir);
            i++;
            if (i > maxDistance) {
                return null;
            }
        } while (!isRail(loc.getBlockType()));

        return fromLoc.getExtent().getLocation(loc.getBlockPosition().add(0.5, 1, 0.5));
    }

    private boolean isRail(BlockType blockType) {
        return blockType == BlockTypes.RAIL
                || blockType == BlockTypes.ACTIVATOR_RAIL
                || blockType == BlockTypes.DETECTOR_RAIL
                || blockType == BlockTypes.GOLDEN_RAIL;
    }

    @Override
    public Entity createEntity(Extent extent, Vector3d position, ItemStack itemStack) {
        return extent.createEntity(entityType, position);
    }

    @Override
    public ItemStack createItem(Entity entity) {
        return ItemStack.of(itemType);
    }
}
