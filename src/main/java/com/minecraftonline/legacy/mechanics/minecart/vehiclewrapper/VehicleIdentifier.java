/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart.vehiclewrapper;

import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.extent.Extent;

import java.util.function.Predicate;

/**
 * Used for CartDispenser, to get type by name
 */
public interface VehicleIdentifier {

    /**
     * Gets the name that would be used
     * on a sign to indicate this type
     * @return name
     */
    String getName();

    /**
     * Gets a filter to return the matching item
     * @return Predicate to find corresponding item
     */
    Predicate<ItemStack> getItemPredicate();

    /**
     * Gets a filter for entities that this identifier supports
     * @return Predicate to find valid entities
     */
    Predicate<Entity> getEntityPredicate();

    /**
     * Gets a valid spawn location, in the given direction
     * and the given from location, as close as possible
     * to the fromLoc
     * @param fromLoc where to start the search location
     * @param dir direction to search
     * @param maxDistance Max distance to search before giving
     *                    up and returning null.
     *
     * @return The closest valid spawn location within the
     *         specified maxDistance or null if none was found.
     */
    Location<World> findValidSpawnLocation(Location<World> fromLoc, Direction dir, int maxDistance);

    /**
     *
     * @param extent to create entity in
     * @param position to create entity at
     * @param itemStack that was used to create this entity. Only currently used for boats
     * @return Entity created
     */
    Entity createEntity(Extent extent, Vector3d position, ItemStack itemStack);

    /**
     * Creates an item version of the entity, eg. boat to boat item.
     * @param entity to make item from
     * @return ItemStack
     */
    ItemStack createItem(Entity entity);
}
