/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;

// @author tyhdefu

@Module(id = "stationclear", name = "StationClear", onEnable="onInitialize", onDisable="onDisable")
public class StationClear extends SpongeCartBlockMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.stationclear", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.",
            new SpongeBlockFilter(BlockState.builder().blockType(BlockTypes.WOOL).build().with(Keys.DYE_COLOR, DyeColors.BROWN).get()), TypeToken.of(SpongeBlockFilter.class));

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        allowedBlocks.load(config);

        createPermissions.register();
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        if (SignUtil.getTextRaw(signData, 0).length() != 0
                || SignUtil.getTextRaw(signData, 2).length() != 0
                || SignUtil.getTextRaw(signData, 3).length() != 0) {
            if (player != null)
                player.sendMessage(Text.of(TextColors.RED, "Only line 2 can have text on it"));
            return false;
        }
        return true;
    }

    @Override
    public String[] getValidSigns() {
        return new String[0];
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {

        if (minor) {
            return false;
        }
        for (Entity passenger : minecart.getPassengers()) {
            if (passenger.getType().equals(EntityTypes.PLAYER)) {
                if (Station.currentDestinations.remove(passenger.getUniqueId()) != null)
                    ((Player) passenger).sendMessage(Text.of(TextColors.GREEN, "Your destination has been automatically removed."));
            }
        }
        return false;
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public SpongeBlockFilter getBlockFilter() {
        return allowedBlocks.getValue();
    }

    @Override
    public boolean requiresSign() { return false; }
}
