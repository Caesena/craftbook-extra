/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.legacy.mechanics.cbwarps.CBWarp;
import com.minecraftonline.util.CartUtil;
import com.sk89q.craftbook.core.util.BlockFilter;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.Optional;

@Module(id = "cartwarp", name = "CartWarp", onEnable="onInitialize", onDisable="onDisable")
public class CartWarp extends SpongeCartBlockMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartwarp", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.", getDefaultFilter(), TypeToken.of(SpongeBlockFilter.class));

    private SpongeBlockFilter getDefaultFilter() {
        return new SpongeBlockFilter(BlockState.builder().blockType(BlockTypes.WOOL).add(Keys.DYE_COLOR, DyeColors.PURPLE).build());
    }

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        allowedBlocks.load(config);
        createPermissions.register();
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData lines, @Nullable Player player) {
        String cbwarp = lines.get(0).get().toPlain();
        if (cbwarp.isEmpty()) {
            if (player != null) {
                player.sendMessage(Text.of("CartWarp destination created"));
            }
            return true;
        }
        return CBWarp.getWarp(cbwarp) != null
                && CartSorting.match(null, lines.get(2).get().toPlain(), true)
                && CartSorting.match(null, lines.get(3).get().toPlain(), true);
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {
        final Sign sign = (Sign) blocks.getSign().getTileEntity().get();
        final List<Text> lines = sign.getSignData().lines().get();
        final String cbwarpName = lines.get(0).toPlain();
        if (cbwarpName.isEmpty()) {
            return false;
        }
        final CBWarp cbWarp = CBWarp.getWarp(cbwarpName);
        if (cbWarp == null) {
            return false;
        }
        boolean shouldTeleport = CartSorting.match(minecart, lines.get(2).toPlain())
                || CartSorting.match(minecart, lines.get(3).toPlain());
        if (!shouldTeleport) {
            return false;
        }
        Player cause = minecart.getPassengers().stream()
                .filter(entity -> entity instanceof Player)
                .map(entity -> (Player) entity)
                .findAny()
                .orElse(null);
        double originalSpeed = minecart.getVelocity().length();
        String error = cbWarp.tpToWarp(minecart, null, cause);

        if (error == null) {
            Direction dir;
            Location<World> newLoc = minecart.getLocation();
            Optional<CartMechanismBlocks> otherBlocks = CartMechanismBlocks.findByRail(newLoc).filter(CartMechanismBlocks::hasSign);
            if (otherBlocks.isPresent()) {
                dir = otherBlocks.get().getSign().require(Keys.DIRECTION);

            }
            else {
                dir = CartLift.CartDestination.getDefaultDirection(newLoc);
                if (dir == null) {
                    dir = Direction.NONE;
                }
            }
            minecart.setVelocity(dir.asOffset().mul(originalSpeed));
        }

        return false;
    }

    @Override
    public BlockFilter<BlockState> getBlockFilter() {
        return allowedBlocks.getValue();
    }

    @Override
    public String[] getValidSigns() {
        return new String[] { "[CartWarp]" };
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public boolean requiresSign() {
        return true;
    }
}
