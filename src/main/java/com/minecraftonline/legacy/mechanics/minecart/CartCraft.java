/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.TernaryState;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.apache.commons.lang3.tuple.Pair;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.ChestMinecart;
import org.spongepowered.api.entity.vehicle.minecart.HopperMinecart;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.*;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.item.recipe.Recipe;
import org.spongepowered.api.item.recipe.crafting.*;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.TypeTokens;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Module(id = "cartcraft", name = "CartCraft", onEnable = "onInitialize", onDisable = "onDisable")
public class CartCraft extends SpongeCartBlockMechanic {
    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartcraft", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.",
            new SpongeBlockFilter(BlockState.builder().blockType(BlockTypes.WOOL).build().with(Keys.DYE_COLOR, DyeColors.GRAY).get()), TypeToken.of(SpongeBlockFilter.class));

    private final ConfigValue<Boolean> returnWaterBuckets = new ConfigValue<>("return-water-buckets", "Whether to refund water buckets with water buckets", true, TypeTokens.BOOLEAN_TOKEN);

    private CommandMapping recipesMapping = null;

    private final Map<UUID, Instant> queryRecipesCooldown = new HashMap<>();

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        allowedBlocks.load(config);
        returnWaterBuckets.load(config);

        createPermissions.register();

        CommandSpec recipesCommand = CommandSpec.builder()
                .arguments(GenericArguments.flags().valueFlag(GenericArguments.plugin(Text.of("plugin")), "p", "-plugin")
                        .buildWith(GenericArguments.optional(GenericArguments.string(Text.of("query"))))
                )
                .executor((src, args) -> {
                    if (src instanceof Player && !src.hasPermission("craftbook")) {
                        UUID uuid = ((Player) src).getUniqueId();
                        Instant instant = queryRecipesCooldown.get(uuid);
                        if (instant != null && instant.isAfter(Instant.now())) {
                            throw new CommandException(Text.of("Please wait before using this command again."));
                        }
                        queryRecipesCooldown.put(uuid, Instant.now().plus(500, ChronoUnit.MILLIS));
                    }
                    final String query = args.<String>getOne("query").map(String::toLowerCase).orElse(null);
                    Task.builder()
                            .async()
                            .execute(() -> {
                                final PluginContainer pluginContainer = args.<PluginContainer>getOne("plugin").orElse(null);
                                final String pluginId = pluginContainer == null ? null : pluginContainer.getId();
                                if (query == null) {
                                    if (pluginId == null) {
                                        src.sendMessage(Text.of(TextColors.RED, "You must provide atleast a plugin or a query"));
                                        return;
                                    }
                                    List<Text> content = Sponge.getRegistry().getCraftingRecipeRegistry().getAll().stream()
                                            .map(r -> Pair.<Recipe, String>of(r, r.getId().split(":", 2)[1].replaceAll("_","")))
                                            .filter(r -> r.getKey().getId().split(":", 2)[0].equalsIgnoreCase(pluginId))
                                            .sorted(Map.Entry.comparingByValue())
                                            .map(this::recipeToText)
                                            .collect(Collectors.toList());

                                    PaginationList.builder()
                                            .title(Text.of(TextColors.GRAY, "Recipes from plugin ", TextColors.LIGHT_PURPLE, pluginId))
                                            .contents(content)
                                            .sendTo(src);
                                    return;
                                }
                                List<Pair<Recipe, String>> exactMatches = new ArrayList<>();
                                List<Pair<Recipe, String>> beginsWithMatches = new ArrayList<>();
                                List<Pair<Recipe, String>> containsMatches = new ArrayList<>();
                                Sponge.getRegistry().getCraftingRecipeRegistry().getAll().forEach(r -> {
                                    String[] splitId = r.getId().toLowerCase().split(":", 2);
                                    if (pluginId != null && !splitId[0].equalsIgnoreCase(pluginId)) {
                                        return;
                                    }
                                    final String id = splitId[1].replaceAll("_", "");
                                    Pair<Recipe, String> pair = Pair.of(r, id);
                                    if (id.equals(query)) {
                                        exactMatches.add(pair);
                                    } else if (id.startsWith(query)) {
                                        beginsWithMatches.add(pair);
                                    } else if (id.contains(query)) {
                                        containsMatches.add(pair);
                                    }
                                });
                                exactMatches.sort(Map.Entry.comparingByValue());
                                beginsWithMatches.sort(Map.Entry.comparingByValue());
                                containsMatches.sort(Map.Entry.comparingByValue());
                                List<Pair<Recipe, String>> results = new ArrayList<>(exactMatches);
                                results.addAll(beginsWithMatches);
                                final int MAX_RESULTS = 100;
                                if (results.size() < 100) {
                                    int toGet = Math.min(containsMatches.size(), MAX_RESULTS - results.size());
                                    results.addAll(containsMatches.subList(0, toGet));
                                }
                                if (results.isEmpty()) {
                                    src.sendMessage(Text.of(TextColors.RED, "No results found for query: '" + query + "'"));
                                    return;
                                }
                                List<Text> contents = results.stream().map(this::recipeToText).collect(Collectors.toList());

                                Text.Builder titleBuilder = Text.builder()
                                        .append(Text.of(TextColors.GRAY, "Recipes matching '",
                                                TextColors.LIGHT_PURPLE, query, TextColors.GRAY, "'"));

                                if (pluginId != null) {
                                    titleBuilder.append(Text.of(TextColors.GRAY, " from plugin ", TextColors.LIGHT_PURPLE, pluginId));
                                }

                                PaginationList.builder()
                                        .title(titleBuilder.build())
                                        .contents(contents)
                                        .sendTo(src);
                            })
                            .submit(CraftBookPlugin.spongeInst());
                    return CommandResult.success();
                })
                .build();

        recipesMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), recipesCommand, "cbrecipes").orElse(null);
    }

    private Text recipeToText(Pair<Recipe, String> pair) {
        final String namespace = pair.getKey().getId().split(":", 2)[0];
        Text namespaceText = namespace.equals("minecraft") ?
                Text.of(TextColors.GREEN,  "Vanilla Minecraft recipe") :
                Text.of(TextColors.GREEN, "Custom recipe from ", TextColors.LIGHT_PURPLE, namespace);
        return Text.of(TextColors.GOLD, "* ",
                TextActions.showText(
                        Text.of(pair.getKey().getId(), Text.NEW_LINE,
                                namespaceText)),
                TextColors.GRAY, pair.getValue());
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        String recipeName = SignUtil.getTextRaw(signData, 2) + SignUtil.getTextRaw(signData, 3);
        Optional<CraftingRecipe> recipe = getRecipeFromName(recipeName);
        if (!recipe.isPresent()) {
            if (player != null)
                player.sendMessage(Text.of("Unknown recipe, they have all _'s removed."));
            return false;
        }
        else if (!(recipe.get() instanceof ShapedCraftingRecipe || recipe.get() instanceof ShapelessCraftingRecipe)){
            if (player != null)
                player.sendMessage(Text.of("This recipe is not supported."));
            return false;
        }
        else
            return true;
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {
        if (minor || isActive(blocks) == TernaryState.FALSE) {
            return false;
        }

        final Inventory inv;

        if (minecart.getType().equals(EntityTypes.CHESTED_MINECART))
            inv = ((ChestMinecart) minecart).getInventory();
        else if (minecart.getType().equals(EntityTypes.HOPPER_MINECART))
            inv = ((HopperMinecart) minecart).getInventory();
        else
            return false;

        Map<List<ItemStackSnapshot>, Integer> ingredients; // Map of allowed items for each slot to the amount needed.

        ItemStack result;
        Map<ItemType, Integer> additionalResults = new HashMap<>();

        List<Text> lines = ((Sign)blocks.getSign().getTileEntity().get()).lines().get();
        String recipeName = lines.get(2).toPlain() + lines.get(3).toPlain();
        Optional<CraftingRecipe> optRecipe = getRecipeFromName(recipeName);
        if (!optRecipe.isPresent()) {
            return false;
        }
        CraftingRecipe recipe = optRecipe.get();
        if (recipe instanceof ShapedCraftingRecipe) {
            ShapedCraftingRecipe shapedCraftingRecipe = (ShapedCraftingRecipe) recipe;
            ingredients = getAllIngredients(shapedCraftingRecipe);
            result = shapedCraftingRecipe.getExemplaryResult().createStack();
        }
        else if (recipe instanceof ShapelessCraftingRecipe){
            ShapelessCraftingRecipe shapelessCraftingRecipe = (ShapelessCraftingRecipe) recipe;
            ingredients = getAllIngredients(shapelessCraftingRecipe);
            result = shapelessCraftingRecipe.getExemplaryResult().createStack();
        }
        else {
            return false;
        }

        for (Map.Entry<List<ItemStackSnapshot>, Integer> ingredient : ingredients.entrySet()) {
            if (ingredient.getKey().size() == 1) {
                ItemType type = ingredient.getKey().get(0).getType();
                if (type == ItemTypes.MILK_BUCKET || type == ItemTypes.LAVA_BUCKET || type == ItemTypes.WATER_BUCKET) {
                    ItemType returnType = ItemTypes.BUCKET;
                    if (type == ItemTypes.WATER_BUCKET && returnWaterBuckets.getValue()) {
                        returnType = ItemTypes.WATER_BUCKET;
                    }
                    additionalResults.compute(returnType, (k,v) -> {
                        if (v == null) {
                            return ingredient.getValue();
                        }
                        return v + ingredient.getValue();
                    });
                }
            }
        }

        if (!inv.canFit(result))
            return false;

        // Inventory is inv to poll from, int is amount to poll.
        Map<Inventory, Integer> ingredientInvs = new HashMap<>();

        // Check if they have enough to craft.
        for (Map.Entry<List<ItemStackSnapshot>, Integer> ingredient : ingredients.entrySet()) {

            Predicate<ItemStack> ingredientfilter = itemStack -> {
                for (ItemStackSnapshot allowedIngredient : ingredient.getKey()) {
                    if (itemStack.getType().equals(ItemTypes.AIR))
                        continue;
                    itemStack = ItemStack.builder().from(itemStack).quantity(1).build();
                    if (itemStack.toContainer().equals(allowedIngredient.createStack().toContainer())) {
                        return true;
                    }
                }
                return false;
            };

            Inventory matchingingredients = inv.query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(ingredientfilter));
            if (!(matchingingredients.totalItems() >= ingredient.getValue()))
                return false;
            else
                ingredientInvs.put(matchingingredients, ingredient.getValue());
        }

        // Take from their inv
        for (Map.Entry<Inventory, Integer> entryset : ingredientInvs.entrySet()) {
            entryset.getKey().poll(entryset.getValue());
        }
        inv.offer(result);

        for (Map.Entry<ItemType, Integer> entry : additionalResults.entrySet()) {
            inv.offer(ItemStack.of(entry.getKey(), entry.getValue()));
        }
        return false;
    }

    @Override
    public void onDisable() {
        super.onDisable();

        allowedBlocks.save(config);
        returnWaterBuckets.save(config);

        if (recipesMapping != null) {
            Sponge.getCommandManager().removeMapping(recipesMapping);
        }
    }

    public Map<List<ItemStackSnapshot>, Integer> getAllIngredients(ShapedCraftingRecipe recipe) {
        int width = recipe.getWidth();
        int height = recipe.getHeight();
        Ingredient ingredient;
        Map<List<ItemStackSnapshot>, Integer> ingredients = new HashMap<>();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                ingredient = recipe.getIngredient(x, y);
                if (ingredient.displayedItems().size() == 0)
                    continue; // no possible options, it is air.
                if (ingredients.containsKey(ingredient.displayedItems())) {
                    ingredients.replace(ingredient.displayedItems(), ingredients.get(ingredient.displayedItems()) + 1);
                }
                else {
                    ingredients.put(ingredient.displayedItems(), 1);
                }
            }
        }
        return ingredients;
    }

    public Map<List<ItemStackSnapshot>, Integer> getAllIngredients(ShapelessCraftingRecipe recipe) {
        Map<List<ItemStackSnapshot>, Integer> ingredients = new HashMap<>();
        for (Ingredient ingredient : recipe.getIngredientPredicates()) {
            if (ingredients.containsKey(ingredient.displayedItems())) {
                ingredients.replace(ingredient.displayedItems(), ingredients.get(ingredient.displayedItems()) + 1);
            }
            else {
                ingredients.put(ingredient.displayedItems(), 1);
            }
        }
        return ingredients;
    }


    public Optional<CraftingRecipe> getRecipeFromName(String name) {
        for (CraftingRecipe recipe : Sponge.getRegistry().getCraftingRecipeRegistry().getAll()) {
            if (recipe.getName().replaceAll("_","").split(":", 2)[1].equalsIgnoreCase(name))
                return Optional.of(recipe);
        }
        return Optional.empty();
    }

    @Override
    public String[] getValidSigns() {
        return new String[] {
                "[Craft]"
        };
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public SpongeBlockFilter getBlockFilter() {
        return allowedBlocks.getValue();
    }

    @Override
    public boolean requiresSign() { return true; }
}
