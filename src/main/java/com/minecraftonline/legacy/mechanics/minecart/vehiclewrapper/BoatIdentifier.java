/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart.vehiclewrapper;

import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.TreeType;
import org.spongepowered.api.data.type.TreeTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.vehicle.Boat;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.extent.Extent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

public class BoatIdentifier implements VehicleIdentifier {

    private static Map<ItemType, TreeType> itemTreeType = null;

    public BoatIdentifier() {
        if (itemTreeType == null) initialiseItemTreeTypeMap();
    }

    private static void initialiseItemTreeTypeMap() {
        itemTreeType = new HashMap<>();
        itemTreeType.put(ItemTypes.BOAT, TreeTypes.OAK);
        itemTreeType.put(ItemTypes.ACACIA_BOAT, TreeTypes.ACACIA);
        itemTreeType.put(ItemTypes.BIRCH_BOAT, TreeTypes.BIRCH);
        itemTreeType.put(ItemTypes.DARK_OAK_BOAT, TreeTypes.DARK_OAK);
        itemTreeType.put(ItemTypes.JUNGLE_BOAT, TreeTypes.JUNGLE);
        itemTreeType.put(ItemTypes.SPRUCE_BOAT, TreeTypes.SPRUCE);
    }

    @Override
    public String getName() {
        return "boat";
    }

    @Override
    public Predicate<ItemStack> getItemPredicate() {
        return itemStack -> itemTreeType.containsKey(itemStack.getType());
    }

    @Override
    public Predicate<Entity> getEntityPredicate() {
        return entity -> entity instanceof Boat;
    }

    @Override
    public Location<World> findValidSpawnLocation(Location<World> fromLoc, Direction dir, int maxDistance) {
        Location<World> belowLoc = fromLoc.getRelative(Direction.DOWN);
        int i = 0;
        do {
            belowLoc = belowLoc.getRelative(dir);
            i++;
            if (i > maxDistance) {
                return null;
            }
        } while (!isWater(belowLoc) || belowLoc.getRelative(Direction.UP).getBlockType() != BlockTypes.AIR);

        return fromLoc.getExtent().getLocation(belowLoc.getBlockPosition().add(0.5, 1, 0.5));
    }

    private static boolean isWater(Location<World> loc) {
        BlockType blockType = loc.getBlockType();
        return blockType == BlockTypes.FLOWING_WATER || blockType == BlockTypes.WATER;
    }

    @Override
    public Entity createEntity(Extent extent, Vector3d position, ItemStack itemStack) {
        Entity entity = extent.createEntity(EntityTypes.BOAT, position);
        ItemType type = itemStack.getType();
        TreeType treeType = itemTreeType.get(type);
        if (treeType == null)
            return null;
        entity.offer(Keys.TREE_TYPE, treeType);
        return entity;
    }

    @Override
    public ItemStack createItem(Entity entity) {
        TreeType treeType = entity.get(Keys.TREE_TYPE).get();
        for (Map.Entry<ItemType, TreeType> entry : itemTreeType.entrySet()) {
            if (treeType == entry.getValue()) {
                return ItemStack.of(entry.getKey());
            }
        }
        return null;
    }
}
