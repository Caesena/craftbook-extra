/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.TernaryState;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.RideEntityEvent;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Optional;

/**
 * Makes Carts stop on entering the rail above the cart
 * and depart when a player enters the cart
 * @author tyhdefu
 */
@Module(id = "cartlaunch", name = "CartLaunch", onEnable="onInitialize", onDisable="onDisable")
public class CartLaunch extends SpongeCartBlockMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartload", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.", getDefaultFilter(), TypeToken.of(SpongeBlockFilter.class));
    private final ConfigValue<Double> launchSpeed = new ConfigValue<>("launch-speed", "Speed multiplier to launch the minecart at", 1.0, TypeToken.of(Double.class));

    @Override
    public void onInitialize() {
        allowedBlocks.load(config);
        launchSpeed.load(config);
        createPermissions.register();
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {
        if (isActive(blocks) == TernaryState.FALSE || minor) {
            return false;
        }
        if (minecart.getPassengers().isEmpty()) {
            String line = SignUtil.getTextRaw((Sign)blocks.getSign().getTileEntity().get(), 3);
            if (line.isEmpty() || CartSorting.match(minecart, line)) {
                minecart.setVelocity(Vector3d.ZERO);
            }
        }
        return false;
    }

    @Listener
    public void onEntityEnterVehicle(RideEntityEvent.Mount e) {
        if (e.getTargetEntity().getType().equals(EntityTypes.RIDEABLE_MINECART)) {
            Optional<CartMechanismBlocks> optBlocks = CartMechanismBlocks.find(e.getTargetEntity().getLocation());
            if (optBlocks.isPresent() && isValid(optBlocks.get())) {
                CartMechanismBlocks blocks = optBlocks.get();
                Sign sign = (Sign)blocks.getSign().getTileEntity().get();
                String line = SignUtil.getTextRaw(sign, 2);
                Minecart minecart = (Minecart)e.getTargetEntity();
                if (line.isEmpty() || CartSorting.match(minecart, line)) {
                    // Boost minecart
                    Vector3d velocity = sign.getLocation().get(Keys.DIRECTION).get().getOpposite().asOffset().mul(launchSpeed.getValue());
                    minecart.setVelocity(velocity);
                }
            }
        }
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        return CartSorting.match(null, SignUtil.getTextRaw(signData, 2), true)
                && CartSorting.match(null, SignUtil.getTextRaw(signData, 3), true);
    }

    @Override
    public SpongeBlockFilter getBlockFilter() {
        return allowedBlocks.getValue();
    }

    public SpongeBlockFilter getDefaultFilter() {
        return new SpongeBlockFilter(BlockState.builder().blockType(BlockTypes.WOOL).build().with(Keys.DYE_COLOR, DyeColors.LIME).get());
    }

    @Override
    public String[] getValidSigns() {
        return new String[] {"[Launch]"};
    }

    @Override
    public boolean requiresSign() {
        return true;
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }
}
