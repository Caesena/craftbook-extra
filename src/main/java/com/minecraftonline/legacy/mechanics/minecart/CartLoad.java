/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.TernaryState;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;

@Module(id = "cartload", name = "CartLoad", onEnable="onInitialize", onDisable="onDisable")
public class CartLoad extends SpongeCartBlockMechanic {
    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartload", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.", getDefaultFilter(), TypeToken.of(SpongeBlockFilter.class));

    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {
        if (isActive(blocks) == TernaryState.FALSE || minor) {
            return false;
        }
        if (!minecart.getPassengers().isEmpty()) {
            return false;
        }
        Collection<Entity> nearbyEntities = blocks.getBase().getExtent().getNearbyEntities(blocks.getRail().getPosition(), 3);
        nearbyEntities.stream()
                .filter(entity -> entity.getType().equals(EntityTypes.PLAYER))
                .filter(entity -> entity.get(Keys.GAME_MODE).filter(mode -> mode != GameModes.SPECTATOR).isPresent()) // Exclude spectators.
                .findAny()
                .ifPresent(minecart::addPassenger);
        return false;
    }

    @Override
    public void onInitialize() {
        allowedBlocks.load(config);
        createPermissions.register();
    }

    public SpongeBlockFilter getDefaultFilter() {
        return new SpongeBlockFilter(BlockState.builder().blockType(BlockTypes.WOOL).build().with(Keys.DYE_COLOR, DyeColors.CYAN).get());
    }

    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public SpongeBlockFilter getBlockFilter() {
        return allowedBlocks.getValue();
    }

    @Override
    public String[] getValidSigns() {
        return new String[0];
    }
}
