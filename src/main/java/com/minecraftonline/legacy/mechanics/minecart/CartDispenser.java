/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.legacy.mechanics.minecart.vehiclewrapper.BoatIdentifier;
import com.minecraftonline.legacy.mechanics.minecart.vehiclewrapper.ChestMinecartIdentifier;
import com.minecraftonline.legacy.mechanics.minecart.vehiclewrapper.FurnaceMinecartIdentifier;
import com.minecraftonline.legacy.mechanics.minecart.vehiclewrapper.HopperMinecartIdentifier;
import com.minecraftonline.legacy.mechanics.minecart.vehiclewrapper.MultiIdentifier;
import com.minecraftonline.legacy.mechanics.minecart.vehiclewrapper.RideableMinecartIdentifier;
import com.minecraftonline.legacy.mechanics.minecart.vehiclewrapper.VehicleIdentifier;
import com.minecraftonline.mechanic.SignMechanicCacheManager;
import com.minecraftonline.util.CartUtil;
import com.minecraftonline.util.ChangeBlockPostListenerManager;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeSignMechanic;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import com.sk89q.craftbook.sponge.util.data.mutable.LastPowerData;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.block.tileentity.TileEntityTypes;
import org.spongepowered.api.block.tileentity.carrier.Chest;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.RailDirection;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.filter.Getter;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.util.Tuple;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Module(id = "cartdispenser", name = "CartDispenser", onEnable="onInitialize", onDisable="onDisable")
public class CartDispenser extends SpongeSignMechanic {

    private final Map<Location<World>, RedstoneListener> listeners = new HashMap<>();

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartdispenser", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<Double> launchSpeed = new ConfigValue<>("launch-speed", "Speed to launch minecart at", 1.0, TypeToken.of(Double.class));

    private MultiIdentifier multiIdentifier;

    @Override
    public void onInitialize() throws CraftBookException {
        multiIdentifier = new MultiIdentifier(Arrays.asList(
                new RideableMinecartIdentifier(),
                new ChestMinecartIdentifier(),
                new HopperMinecartIdentifier(),
                new FurnaceMinecartIdentifier(),
                new BoatIdentifier()));

        launchSpeed.load(config);
        createPermissions.register();

        cache.registerCache(this, (loc, signData) -> {
            if (!isValidNoCache(loc, signData)) {
                return null;
            }
            VehicleIdentifier identifier = multiIdentifier.fromString(SignUtil.getTextRaw(signData, 2)).orElse(null);
            RedstoneListener redstoneListener = new RedstoneListener(loc);
            listeners.put(loc, redstoneListener);
            CraftBookPlugin.spongeInst().getChangeBlockPostListenerManager().register(loc.getExtent(), createInputLocations(loc), redstoneListener);
            return new CartDispenserCache(signData.lines().get(), identifier);
        });
    }

    private final Vector3i UP_TWICE = Vector3i.from(0, 2, 0);

    public Collection<Vector3i> createInputLocations(Location<World> signLoc) {
        Vector3i pos = signLoc.getBlockPosition();

        if (signLoc.getBlockType() == BlockTypes.WALL_SIGN) {
            final Direction direction = signLoc.require(Keys.DIRECTION);
            pos = pos.add(direction.asBlockOffset());
        }

        final Vector3i posX = pos.add(1, 0, 0);
        final Vector3i negX = pos.add(-1, 0, 0);
        final Vector3i posZ = pos.add(0, 0, 1);
        final Vector3i negZ = pos.add(0, 0, -1);

        List<Vector3i> list = new ArrayList<>(12);

        list.add(posX);
        list.add(negX);
        list.add(posZ);
        list.add(negZ);

        list.add(posX.add(Vector3i.UP));
        list.add(negX.add(Vector3i.UP));
        list.add(posZ.add(Vector3i.UP));
        list.add(negZ.add(Vector3i.UP));

        BlockType blockType = signLoc.getBlockRelative(Direction.UP).getBlockType();
        if (blockType != BlockTypes.CHEST && blockType != BlockTypes.TRAPPED_CHEST) {
            list.add(posX.add(UP_TWICE));
            list.add(negX.add(UP_TWICE));
            list.add(posZ.add(UP_TWICE));
            list.add(negZ.add(UP_TWICE));
        }
        return list;
    }

    public class RedstoneListener implements ChangeBlockPostListenerManager.ChangeBlockPostListener {

        private final Location<World> loc;

        public RedstoneListener(Location<World> loc) {
            this.loc = loc;
        }

        @Override
        public void onChange(Transaction<BlockSnapshot> transaction, Cause cause) {
            Location<World> baseLocation = transaction.getFinal().getLocation().get();
            BlockState endState = transaction.getFinal().getExtendedState();

            Location<World> heightAdjustedLoc = this.loc.getExtent().getLocation(new Vector3i(loc.getX(), baseLocation.getY(), loc.getZ()));

            Tuple<Boolean, Set<Location<World>>> endTuple = ICSocket.getPoweredInState(endState, baseLocation);

            boolean powered = endTuple != null && endTuple.getFirst() && endTuple.getSecond().contains(heightAdjustedLoc);

            CartDispenserCache cachedData = cache.getCached(CartDispenser.this, this.loc);

            if (cachedData == null) {
                return;
            }
            boolean wasPowered = this.loc.get(CraftBookKeys.LAST_POWER).orElse(0) > 0;

            if (powered != wasPowered) {
                updateState(this.loc, cachedData, powered);
            }

            int powerLevelFake = powered ? 15 : 0; // TODO: get real power level?
            this.loc.offer(new LastPowerData(powerLevelFake));
        }
    }

    public class CartDispenserCache extends SignMechanicCacheManager.CachedData {
        @Nullable
        public final VehicleIdentifier vehicleIdentifier;

        public final boolean push;

        public CartDispenserCache(List<Text> lines, @Nullable VehicleIdentifier vehicleIdentifier) {
            super(lines);
            this.vehicleIdentifier = vehicleIdentifier;
            this.push = getLinePlain(2).equalsIgnoreCase("push") || getLinePlain(3).equalsIgnoreCase("push");
        }

        @Override
        public void onUnload(Location<World> loc) {
            super.onUnload(loc);
            RedstoneListener redstoneListener = listeners.remove(loc);
            if (redstoneListener != null) {
                CraftBookPlugin.spongeInst().getChangeBlockPostListenerManager().unregisterAll(redstoneListener);
            }
        }
    }

    public void impact(Minecart minecart, Location<World> chestLoc) {
        // TODO: move this out of cartdispenser
        VehicleIdentifier identifier = multiIdentifier.fromEntity(minecart);
        if (identifier == null) {
            return;
        }

        ItemStack item = identifier.createItem(minecart);
        Chest chest = (Chest)chestLoc.getTileEntity().get();
        Inventory inv = chest.getDoubleChestInventory().orElse(chest.getInventory());
        minecart.get(Keys.DISPLAY_NAME).ifPresent(name -> item.offer(Keys.DISPLAY_NAME, name));
        minecart.remove();
        if (!inv.canFit(item)) {
            Item itemEntity = (Item) minecart.getLocation().createEntity(EntityTypes.ITEM);
            itemEntity.offer(Keys.REPRESENTED_ITEM, item.createSnapshot());
            minecart.getLocation().spawnEntity(itemEntity);
            return;
        }
        inv.offer(item);
    }

    public SpongeBlockFilter getBlockFilter() {
        return new SpongeBlockFilter(BlockTypes.CHEST);
    }

    public String[] getValidSigns() {
        return new String[] {"[Dispenser]"};
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        String line4 = signData.lines().get(3).toPlain();
        if (!(line4.isEmpty() || line4.equalsIgnoreCase("push"))) {
            if (player != null) {
                player.sendMessage(Text.of(TextColors.RED, "line 4 must be blank or push"));
            }
            return false;
        }
        cache.mechanicCreated(this, location, signData);
        return true;
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    // Uses sign's location, uses cache
    @Override
    public boolean isValid(Location<World> loc) {
        return cache.getCached(this, loc) != null;
    }

    public boolean isValidNoCache(Location<World> loc, SignData signData) {
        return isSignValid(loc, signData);
    }

    public boolean isSignValid(Location<World> loc, SignData signData) {
        Optional<TileEntity> tileEntity = loc.getTileEntity();
        if (!tileEntity.isPresent() || tileEntity.get().getType() != TileEntityTypes.SIGN)
            return false;
        String signLine = SignUtil.getTextRaw(signData, 1);
        for (String validLines : getValidSigns()) {
            if (signLine.equalsIgnoreCase(validLines)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Listens for a minecart thats about to hit a chest.
     */

    @Listener
    public void onVehicleMove(MoveEntityEvent event, @Getter("getTargetEntity") Minecart minecart) {
        Vector3d shift = event.getToTransform().getPosition().sub(event.getFromTransform().getPosition());
        Direction dir;
        if (shift.getX() != 0) {
            if (shift.getX() > 0) {
                dir = Direction.EAST;
            }
            else {
                dir = Direction.WEST;
            }
        }
        else if (shift.getZ() != 0) {
            if (shift.getZ() > 0) {
                dir = Direction.SOUTH;
            }
            else {
                dir = Direction.NORTH;
            }
        }
        else {
            return;
        }
        Location<World> checkLoc = event.getToTransform().getLocation().add(dir.asOffset().mul(0.5));
        if (checkLoc.getBlock().getType() == BlockTypes.CHEST) {
            BlockState blockState = minecart.getLocation().getBlock();
            Optional<RailDirection> railDirection = blockState.get(Keys.RAIL_DIRECTION);
            if (!railDirection.isPresent()) {
                return;
            }
            Vector3i relative = checkLoc.getBlockPosition().sub(minecart.getLocation().getBlockPosition());

            Direction expectedDir = Direction.getClosest(Vector3d.from(relative.getX(), 0, relative.getZ()));
            Direction[] railDirections = CartUtil.getRailDirections(railDirection.get());

            boolean found = false;

            for (Direction d : railDirections) {
                if (d == expectedDir) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                return;
            }

            impact(minecart, checkLoc);
        }

    }

    public static boolean isMinor(Transform<World> from, Transform<World> to) {
        // If they are within .1 of eachother, count as minor
        double oldX = Math.floor(from.getPosition().getX() * 10);
        double newX = Math.floor(to.getPosition().getX() * 10);
        double oldZ = Math.floor(from.getPosition().getZ() * 10);
        double newZ = Math.floor(to.getPosition().getZ() * 10);
        return oldX == newX && oldZ == newZ;
    }

    /**
     * Largely take from SimplePowerable. This part checks for when the
     * Mechanic is powered by redstone
     */

    public static final Location[] EMPTY_LOCATION_ARRAY = new Location[0];

    @Nullable
    public Location<World> getSignFromChest(Location<World> loc) {
        Location<World> chestLoc = loc.getRelative(Direction.DOWN);
        if (chestLoc.getBlockType() == BlockTypes.STANDING_SIGN) {
            return chestLoc;
        }
        chestLoc = chestLoc.getRelative(Direction.DOWN);
        if (chestLoc.getBlockType() == BlockTypes.STANDING_SIGN) {
            return chestLoc;
        }
        return null;
    }

    public Optional<Chest> getChest(Location<World> loc) {
        return loc.getTileEntity().filter(tileEntity -> tileEntity instanceof Chest).map(tileEntity -> (Chest)tileEntity);
    }

    public void updateState(Location<World> signLoc, CartDispenserCache cartDispenserCache, boolean powered) {
        if (!powered)
            return;

        Location<World> chestLoc = signLoc.getRelative(Direction.UP);
        Optional<Chest> optChest = getChest(chestLoc);
        if (!optChest.isPresent()) {
            chestLoc = chestLoc.getRelative(Direction.UP);
            optChest = getChest(chestLoc);
            if (!optChest.isPresent()) {
                return;
            }
        }

        Chest chest = optChest.get();

        Inventory inv = chest.getDoubleChestInventory().orElse(chest.getInventory());

        String line3 = cartDispenserCache.getLinePlain(2);

        Optional<VehicleIdentifier> optVehicleIdentifier = multiIdentifier.fromString(line3);

        Inventory minecartInv = inv.query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(
                optVehicleIdentifier.map(VehicleIdentifier::getItemPredicate).orElse(multiIdentifier.getItemPredicate())
        ));

        Optional<ItemStack> item = minecartInv.peek(1);
        if (!item.isPresent() || item.get().isEmpty()) {
            return;
        }


        VehicleIdentifier vehicleIdentifier = optVehicleIdentifier.orElse(multiIdentifier.fromItemStack(item.get()));
        Location<World> spawnLoc = vehicleIdentifier.findValidSpawnLocation(chestLoc, signLoc.require(Keys.DIRECTION).getOpposite(), 3);
        if (spawnLoc == null) {
            return;
        }
        minecartInv.poll(1);
        Entity entity = vehicleIdentifier.createEntity(spawnLoc.getExtent(), spawnLoc.getPosition(), item.get());

        net.minecraft.entity.Entity nmsEntity = ((net.minecraft.entity.Entity) entity);
        net.minecraft.item.ItemStack nmsItem = (net.minecraft.item.ItemStack) (Object) item.get();

        if (nmsItem.hasDisplayName()) {
            nmsEntity.setCustomNameTag(nmsItem.getDisplayName());
        }

        spawnLoc.spawnEntity(entity);

        if (cartDispenserCache.push) {
            entity.setVelocity(signLoc.require(Keys.DIRECTION).getOpposite().asOffset().mul(launchSpeed.getValue()));
        }
    }
}
