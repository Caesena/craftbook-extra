/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.BlockFilter;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.TernaryState;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.List;

@Module(id = "cartbooster", name = "CartBooster", onEnable="onInitialize", onDisable="onDisable")
public class CartBooster extends SpongeCartBlockMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartbooster", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> maxBoost = new ConfigValue<>("max-boost-material", "maximum speed boost",
            new SpongeBlockFilter(BlockTypes.GOLD_BLOCK), TypeToken.of(SpongeBlockFilter.class));

    private final ConfigValue<SpongeBlockFilter> boost25 = new ConfigValue<>("25x-boost-material", "25% boost (125% resultant speed)",
            new SpongeBlockFilter(BlockTypes.GOLD_ORE), TypeToken.of(SpongeBlockFilter.class));

    private final ConfigValue<SpongeBlockFilter> slow50 = new ConfigValue<>("50x-slowdown-material", "Sets the block that is the base of the 50x slower block.",
            new SpongeBlockFilter(BlockTypes.SOUL_SAND), TypeToken.of(SpongeBlockFilter.class));

    private final ConfigValue<SpongeBlockFilter> slow20 = new ConfigValue<>("20x-slowdown-material", "Sets the block that is the base of the 20x slower block.",
            new SpongeBlockFilter(BlockTypes.GRAVEL), TypeToken.of(SpongeBlockFilter.class));

    private BlockFilter<BlockState> allowedBlocks;
    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        maxBoost.load(config);
        boost25.load(config);
        slow50.load(config);
        slow20.load(config);

        allowedBlocks = new BlockFilter<BlockState>("") {
            @Override
            public List<BlockState> getApplicableBlocks(){
                List<BlockState> blocks = new ArrayList<>();

                blocks.addAll(maxBoost.getValue().getApplicableBlocks());
                blocks.addAll(boost25.getValue().getApplicableBlocks());
                blocks.addAll(slow50.getValue().getApplicableBlocks());
                blocks.addAll(slow20.getValue().getApplicableBlocks());

                return blocks;
            }


        };

        createPermissions.register();

    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {

        if (isActive(blocks) == TernaryState.FALSE || minor) {
            return false;
        }


        double multiplier;

       BlockState blockState =  blocks.getBase().getBlock();

       if (BlockUtil.doesStatePassFilter(maxBoost.getValue(), blockState)) {
           multiplier = 1000;

       }
       else if (BlockUtil.doesStatePassFilter(boost25.getValue(), blockState)) {
           multiplier = 1.250;

       }
       else if (BlockUtil.doesStatePassFilter(slow20.getValue(), blockState)) {
           multiplier = 0.80;

       }
       else if (BlockUtil.doesStatePassFilter(slow50.getValue(), blockState)) {
           multiplier = 0.50;

       }
       else
           throw new IllegalStateException("Block does not match any of the booster blocks.");

        Vector3d velocity = minecart.getVelocity();

        Vector3d newVelocity =  velocity.mul(multiplier);

        minecart.setVelocity(newVelocity);

        return false;
    }

    @Override
    public BlockFilter<BlockState> getBlockFilter() {
        return allowedBlocks;
    }

    @Override
    public String[] getValidSigns() {
        return new String[0];
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public boolean requiresSign() {
        return false;
    }
}
