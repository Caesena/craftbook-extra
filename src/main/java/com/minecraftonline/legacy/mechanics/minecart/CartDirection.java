/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.util.CartUtil;
import com.sk89q.craftbook.core.util.BlockFilter;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Module(id = "cartdirection", name = "CartDirection", onEnable="onInitialize", onDisable="onDisable")
public class CartDirection extends SpongeCartBlockMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartdirection", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.",
            new SpongeBlockFilter(BlockState.builder().blockType(BlockTypes.WOOL).add(Keys.DYE_COLOR, DyeColors.RED).build()), TypeToken.of(SpongeBlockFilter.class));

    private CommandMapping cbgoMapping = null;

    private final Set<CBGoSelection> selectedDirectionMap = new HashSet<>();

    public static class CBGoSelection {
        private final Player player;
        private final Vector3i pos;
        private final World world;


        public CBGoSelection(Player player, Vector3i pos, World world) {
            this.player = player;
            this.pos = pos;
            this.world = world;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof CBGoSelection)) return false;
            CBGoSelection that = (CBGoSelection) o;
            return player.equals(that.player) && pos.equals(that.pos) && world.equals(that.world);
        }

        @Override
        public int hashCode() {
            return Objects.hash(player, pos, world);
        }
    }

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        allowedBlocks.load(config);

        CommandSpec cbGo = CommandSpec.builder()
                .executor((src, args) -> {
                    if (!(src instanceof Player)) {
                        throw new CommandException(Text.of("You must be a player to use this command!"));
                    }
                    final Player player = (Player) src;
                    final Entity baseVehicle = player.getBaseVehicle();
                    if (!(baseVehicle instanceof Minecart)) {
                        throw new CommandException(Text.of("You must be in a minecart to use this command!"));
                    }
                    final Minecart minecart = (Minecart) baseVehicle;
                    final Location<World> loc = minecart.getLocation();
                    final CartMechanismBlocks blocks = CartMechanismBlocks.findByRail(loc)
                            .orElseThrow(() -> new CommandException(Text.of("You must be on a rail to use this command!")));

                    if (!isValid(blocks.getBase())) {
                        throw new CommandException(Text.of("You must be on a CartDirection block to use this command!"));
                    }
                    final double yaw = player.getRotation().getY();
                    final Direction dir = LocationUtil.getDirectionFromYaw(yaw);
                    final Direction[] dirs = CartUtil.getRailDirections(blocks.getRail().require(Keys.RAIL_DIRECTION));
                    for (Direction d : dirs) {
                        if (d == dir) {
                            // Found matching direction.
                            minecart.setVelocity(dir.asOffset().mul(10));
                            player.sendMessage(Text.of(TextColors.GOLD, "Going " + dir.name()));
                            final CBGoSelection selection = new CBGoSelection(player, baseVehicle.getLocation().getBlockPosition(), baseVehicle.getLocation().getExtent());
                            selectedDirectionMap.add(selection);

                            Sponge.getScheduler().createTaskBuilder()
                                    .delayTicks(10)
                                    .execute(() -> selectedDirectionMap.remove(selection))
                                    .submit(CraftBookPlugin.spongeInst());
                            return CommandResult.success();
                        }
                    }
                    throw new CommandException(Text.of("You cannot go in the " + dir.name() + " direction, as the rail does not go that way!"));
                })
                .build();

        cbgoMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), cbGo, "cbgo").orElse(null);
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {
        Vector3d railCentre = blocks.getRail().getBlockPosition().toDouble().add(0.5, 0, 0.5);
        final double acceptableDistance = 0.15;
        final double distance = from.getPosition().distanceSquared(railCentre);
        if (distance > acceptableDistance) {
            return false;
        }
        // Make sure we don't cancel/stop until we are on the block.
        minecart.setVelocity(Vector3d.ZERO);
        Optional<Player> player = minecart.getPassengers().stream()
                .filter(e -> e instanceof Player)
                .map(e -> (Player) e)
                .findAny();
        if (!player.isPresent()) {
            return false;
        }
        if (selectedDirectionMap.contains(new CBGoSelection(player.get(), minecart.getLocation().getBlockPosition(), minecart.getWorld()))) {
            return false;
        }
        player.get().sendMessage(Text.of(TextColors.GOLD, "Stopped at a Cart Direction block. Use /cbgo to pick a direction."));
        return true;
    }

    @Override
    public void onDisable() {
        super.onDisable();

        if (cbgoMapping != null) {
            Sponge.getCommandManager().removeMapping(cbgoMapping);
        }
    }

    @Override
    public String[] getValidSigns() {
        return new String[0];
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public BlockFilter<BlockState> getBlockFilter() {
        return allowedBlocks.getValue();
    }

    @Override
    public boolean requiresSign() {
        return false;
    }
}
