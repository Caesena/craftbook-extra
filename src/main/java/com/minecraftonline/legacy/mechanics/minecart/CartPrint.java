/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.TernaryState;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.event.block.tileentity.ChangeSignEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.regex.Pattern;

@Module(id = "cartprint", name = "CartPrint", onEnable="onInitialize", onDisable="onDisable")
public class CartPrint extends SpongeCartBlockMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartprint", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        createPermissions.register();
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {

        if (minor || isActive(blocks) == TernaryState.FALSE) {
            return false;
        }

        if (!(blocks.hasSign() && SignUtil.getTextRaw(blocks.getSign().getTileEntity().map(sign -> (Sign) sign).get(), 0).equalsIgnoreCase("[Print]")
                || minecart.getPassengers().isEmpty())) {
            return false;
        }

        Sign sign = blocks.getSign().getTileEntity().map(tile -> (Sign) tile).get();
        StringBuilder messageBuilder = new StringBuilder();

        while (sign != null) {
            for (int line = messageBuilder.length() == 0 ? 1 : 0; line < 4; line ++) {
                messageBuilder.append(SignUtil.getTextRaw(sign, line));
            }
            Location<World> below = sign.getLocation().getRelative(Direction.DOWN);
            if (SignUtil.isSign(below)) {
                sign = below.getTileEntity().map(tile -> (Sign) tile).get();
            } else {
                sign = null;
            }
        }

        String message = messageBuilder.toString();
        Arrays.stream(message.split(Pattern.quote("\\n"))).forEach(messageLine -> {
            Text messageText = Text.of(messageLine);

            minecart.getPassengers().stream()
                    .filter(entity -> entity instanceof Player)
                    .map(entity -> (Player) entity)
                    .forEach(player -> player.sendMessage(messageText));
        });
        return false;
    }

    @Override
    public SpongeBlockFilter getBlockFilter() {
        return null;
    }

    @Override
    public String[] getValidSigns() {
        return new String[] {
                "[Print]"
        };
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public boolean requiresSign() { return true; }

    // Change to check the first line for [Print]
    @Override
    public void onSignChange(ChangeSignEvent event, @First Player player) {
        SignData signData = event.getText();
        for(String line : getValidSigns()) {
            if(SignUtil.getTextRaw(signData, 0).equalsIgnoreCase(line)) {
                if(!getCreatePermission().hasPermission(player)) {
                    player.sendMessage(Text.of(TextColors.RED, "You do not have permission to create this mechanic!"));
                    event.setCancelled(true);
                    return;
                } else {
                    signData.setElement(1, Text.of(line));
                    if (!verifyLines(event.getTargetTile().getLocation(), signData, player)) {
                        event.setCancelled(true);
                        return;
                    }
                }

                player.sendMessage(Text.of(TextColors.GOLD, "Created " + getName() + '!'));
                break;
            }
        }
    }

    // This sign has no blockfilter
    @Override
    public boolean isValid(CartMechanismBlocks blocks) { return blocks.hasSign(); }
}
