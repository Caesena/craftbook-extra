/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.util.entity.EntityParsingUtil;
import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.PermissionUtil;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.data.type.RailDirection;
import org.spongepowered.api.data.type.RailDirections;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.ChestMinecart;
import org.spongepowered.api.entity.vehicle.minecart.HopperMinecart;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Direction;

import java.util.*;

// @author tyhdefu

public class CartSorting {

    enum RelativeDirection {
        LEFT,
        RIGHT,
        UP,
        DOWN,
        NONE
    }

    public static boolean match(Minecart minecart, String filter) {
        return match(minecart, filter, false);
    }

    public static RelativeDirection sort(String leftfilter, String rightfilter, Minecart minecart)
    {
        if (match(minecart, leftfilter))
            return RelativeDirection.LEFT;
        else if (match(minecart, rightfilter))
            return RelativeDirection.RIGHT;
        else
            return RelativeDirection.NONE;
    }

    public static RelativeDirection cartLiftSort(String upfilter, String downfilter, Minecart minecart) {
        if (match(minecart, upfilter))
            return RelativeDirection.UP;
        else if (match(minecart, downfilter))
            return RelativeDirection.DOWN;
        else
            return RelativeDirection.NONE;
    }

    /**
     * Minecart should only be null if isValidityCheck is true.
     * @param minecart to check
     * @param filter to filter by
     * @param isValidityCheck if we are checking that this is a valid filter
     * @return if matched or if we're checking validity, if its valid.
     */
    public static boolean match(Minecart minecart, String filter, boolean isValidityCheck) {
        String[] splitfilter = filter.split(":", 2);
        switch (splitfilter[0].toLowerCase())
        {
            case "all":
                return true;
            case "":
            case "none":
                return isValidityCheck;
            case "unoccupied":
            case "empty":
                return isValidityCheck || minecart.getPassengers().isEmpty();
            case "storage":
                return isValidityCheck || minecart.getType().equals(EntityTypes.CHESTED_MINECART);
            case "powered":
                return isValidityCheck || minecart.getType().equals(EntityTypes.FURNACE_MINECART);
            case "hopper":
                return isValidityCheck || minecart.getType().equals(EntityTypes.HOPPER_MINECART);
            case "tnt":
                return isValidityCheck || minecart.getType().equals(EntityTypes.TNT_MINECART);
            case "minecart":
                return isValidityCheck || minecart.getType().equals(EntityTypes.RIDEABLE_MINECART);
            case "occupied":
            case "full":
                return isValidityCheck || !minecart.getPassengers().isEmpty();
            case "animal": {
                return isValidityCheck || hasAnimalRider(minecart);
            }
            case "mob": {
                return isValidityCheck || hasMobRider(minecart);
            }
            case "player": {
                if (isValidityCheck) return true;
                Optional<Entity> rider = getFirstRider(minecart);
                return rider.isPresent() && rider.get().getType().equals(EntityTypes.PLAYER);
            }
            case "nostop": {
                if (isValidityCheck) return true;
                List<Entity> passengers = minecart.getPassengers();
                if (passengers.size() == 0) {
                    return false;
                }
                Entity player = passengers.get(0);
                if (!(player instanceof Player)) {
                    return false;
                }
                return Station.currentDestinations.get(player.getUniqueId()) == null;
            }
            case "ctns": {
                return isValidityCheck || (minecart.getType().equals(EntityTypes.CHESTED_MINECART) || minecart.getType().equals(EntityTypes.HOPPER_MINECART))
                        && !isStorageMinecartEmpty(minecart);
            }
            case "!ctns": {
                return isValidityCheck || (minecart.getType().equals(EntityTypes.CHESTED_MINECART) || minecart.getType().equals(EntityTypes.HOPPER_MINECART))
                        && isStorageMinecartEmpty(minecart);
            }
            case "held":
                if (isValidityCheck) return splitfilter.length > 1 && isItemParamsValid(splitfilter[1]);
                return isHeld(minecart, splitfilter[1]);
            case "inv": {
                if (isValidityCheck) return splitfilter.length > 1 && isItemParamsValid(splitfilter[1]);
                if (minecart.getPassengers().size() == 0 || !minecart.getPassengers().get(0).getType().equals(EntityTypes.PLAYER))
                    return false;
                return hasInInv(((Player)minecart.getPassengers().get(0)).getInventory(), splitfilter[1]);
            }
            case "sci":
                if (isValidityCheck) return splitfilter.length > 1 && isItemParamsValid(splitfilter[1]);
                return hasInFirstSlot(minecart, splitfilter[1]);
            case "sci+": {
                if (isValidityCheck) return splitfilter.length > 1 && isItemParamsValid(splitfilter[1]);
                Inventory inv;
                if (minecart.getType().equals(EntityTypes.CHESTED_MINECART))
                    inv = ((ChestMinecart) minecart).getInventory();
                else if (minecart.getType().equals(EntityTypes.HOPPER_MINECART))
                    inv = ((HopperMinecart) minecart).getInventory();
                else
                    break;
                return hasInInv(inv, splitfilter[1]);
            }
            case "ply": {
                if (isValidityCheck) return splitfilter.length == 2;
                if (minecart.getPassengers().size() == 0)
                    return false;
                Entity rider = minecart.getPassengers().get(0);
                if (!rider.getType().equals(EntityTypes.PLAYER))
                    return false;
                final String name = ((Player) rider).getName();
                if (filter.startsWith("ply:!"))
                    return !name.equals(splitfilter[1].substring(1));
                else
                    return ((Player) rider).getName().equals(splitfilter[1]);
            }
            case "plym": {
                if (isValidityCheck) return splitfilter.length == 2;
                if (minecart.getPassengers().size() == 0)
                    return false;
                Entity rider = minecart.getPassengers().get(0);
                if (!rider.getType().equals(EntityTypes.PLAYER))
                    return false;
                final String name = ((Player) rider).getName();
                if (filter.startsWith("plym:!"))
                    return !name.contains(splitfilter[1].substring(1));
                else
                    return name.contains(splitfilter[1]);
            }
            case "group": {
                if (isValidityCheck) return splitfilter.length == 2;
                Optional<Entity> rider = getFirstRider(minecart);
                if (!rider.isPresent() || !rider.get().getType().equals(EntityTypes.PLAYER))
                    return false;
                if (splitfilter[1].startsWith("!"))
                    return !PermissionUtil.isInGroup((Player) rider.get(), splitfilter[1].substring(1).toLowerCase());
                else
                    return PermissionUtil.isInGroup((Player) rider.get(), splitfilter[1].toLowerCase());
            }
            case "cart": {
                if (isValidityCheck) return splitfilter.length == 2;

                final boolean negate = splitfilter[1].startsWith("!");
                if (negate) {
                    splitfilter[1] = splitfilter[1].substring(1);
                }
                return minecart.get(Keys.DISPLAY_NAME)
                        .map(name -> name.toPlain().equals(splitfilter[1]) ^ negate)
                        .orElse(false);
            }
            case "cartm": {
                if (isValidityCheck) return splitfilter.length == 2;

                final boolean negate = splitfilter[1].startsWith("!");
                if (negate) {
                    splitfilter[1] = splitfilter[1].substring(1);
                }

                return minecart.get(Keys.DISPLAY_NAME)
                        .map(name -> name.toPlain().contains(splitfilter[1]) ^ negate)
                        .orElse(false);
            }
        }
        if (filter.startsWith("#")) {
            //Station
            if (isValidityCheck)
                return filter.length() > 1;
            else {
                if (minecart.getPassengers().size() == 0)
                    return false;
                Entity rider = minecart.getPassengers().get(0);
                if (!rider.getType().equals(EntityTypes.PLAYER))
                    return false;
                String curDestination = Station.currentDestinations.get(rider.getUniqueId());
                return curDestination != null && matchGlobStation(curDestination, filter.substring(1).toLowerCase());
            }
        }
        // If no decision has been made return false
        return false;
    }

    public static boolean matchGlobStation(String station, String glob) {
        int globCharPos = 0;
        final char[] stationChars = station.toLowerCase().toCharArray();
        final char[] globChars = glob.replaceAll("[*]+", "*").toLowerCase().toCharArray();

        for (int i = 0; i < stationChars.length; i++) {
            if (globCharPos == globChars.length) {
                return false;
            }
            char globChar = globChars[globCharPos];
            if (globChar != '*') {
                if (globChar != stationChars[i]) {
                    return false;
                }
                globCharPos++;
            }
            else {
                // *
                if (globCharPos == globChars.length - 1 || i == stationChars.length - 1) {
                    return true; // Doesn't matter where we are, anything will match.
                }
                final char nextChar = globChars[globCharPos + 1];
                if (nextChar == stationChars[i + 1]) {
                    // Stop wildcard matching and begin normal matching.
                    globCharPos++;
                }
                else {
                    return true;
                }
            }
        }
        if (globCharPos == globChars.length - 1) {
            return true; // Finished matching the glob.
        }
        // So we're got nothing left to match, so if we have any remaining glob pattern that isn't *, it doesn't match.
        for (; globCharPos < globChars.length; globCharPos++) {
            if (globChars[globCharPos] != '*') {
                return false;
            }
        }
        return true;
    }

    public static boolean hasMobRider(Minecart minecart) {
        Optional<Entity> rider = getFirstRider(minecart);
        if (!rider.isPresent())
            return false;
        EntityType type = rider.get().getType();
        for (EntityType mobtype : EntityParsingUtil.getMobs()) {
            if (type.equals(mobtype))
                return true;
        }
        return false;
    }

    public static boolean hasAnimalRider(Minecart minecart) {
        Optional<Entity> rider = getFirstRider(minecart);
        if (!rider.isPresent())
            return false;
        EntityType type = rider.get().getType();
        for (EntityType animaltype : EntityParsingUtil.getAnimals()) {
            if (type.equals(animaltype))
                return true;
        }
        return false;
    }

    public static boolean isStorageMinecartEmpty(Minecart minecart) {
        if (minecart.getType().equals(EntityTypes.CHESTED_MINECART)){
            ChestMinecart chestMinecart = (ChestMinecart) minecart;
            return chestMinecart.getInventory().totalItems() == 0;
        }
        else if (minecart.getType().equals(EntityTypes.HOPPER_MINECART)) {
            HopperMinecart hopperMinecart = (HopperMinecart) minecart;
            return hopperMinecart.getInventory().totalItems() == 0;
        }
        return false;
    }

    public static boolean isHeld(Minecart minecart, String itemfilter) {
        itemfilter = itemfilter.toLowerCase();
        if (minecart.getPassengers().size() == 0 || !minecart.getPassengers().get(0).getType().equals(EntityTypes.PLAYER))
            return false;
        Player rider = (Player) minecart.getPassengers().get(0);

        Optional<ItemStack> helditem = rider.getItemInHand(HandTypes.MAIN_HAND);
        String itemparams;

        boolean isnotitem = false;

        if (itemfilter.startsWith("!")) {
            itemparams = itemfilter.substring(1);
            isnotitem = true;
        }
        else {
            itemparams = itemfilter;
        }

        if (itemparams.equals("none")) {
            return helditem.isPresent() == isnotitem;
        }

        String[] itemvalues = parseItemParams(itemparams);

        String itemid = itemvalues[0];
        int amount = itemvalues[1].length() != 0 ? Integer.parseInt(itemvalues[1]) : 0;
        int dmgval = itemvalues[2].length() != 0 ? Integer.parseInt(itemvalues[2]) : 0;

        ItemStack itemStack = IDUtil.getItemStackFromAnyIDAndDamage(itemid, dmgval).get();

        if (helditem.get().getType() == itemStack.getType()
                && helditem.get().toContainer().get(DataQuery.of("UnsafeDamage")).get().equals(dmgval)
                && helditem.get().getQuantity() >= amount)
            return !isnotitem;
        else
            return isnotitem;
    }

    public static boolean hasInInv(Inventory inv, String itemfilter) {
        itemfilter = itemfilter.toLowerCase();
        String itemparams;

        boolean isnotitem = false;
        if (itemfilter.startsWith("!")) {
            isnotitem = true;
            itemparams = itemfilter.substring(1);
        }
        else {
            itemparams = itemfilter;
        }

        if (itemparams.equals("none")) {
            return isnotitem == (inv.totalItems() == 0);
        }

        if (inv.totalItems() == 0) {
            return isnotitem;
        }

        String[] itemvalues = parseItemParams(itemparams);

        String itemid = itemvalues[0];
        int amount = itemvalues[1].length() != 0 ? Integer.parseInt(itemvalues[1]) : 1;
        int dmgval = itemvalues[2].length() != 0 ? Integer.parseInt(itemvalues[2]) : 0;

        ItemStack itemStack = IDUtil.getItemStackFromAnyIDAndDamage(itemid, dmgval).get();
        itemStack.setQuantity(amount);

        if (inv.contains(itemStack))
            return !isnotitem;
        else
            return isnotitem;
    }

    public static boolean hasInFirstSlot(Minecart minecart, String itemfilter) {
        Inventory inv;
        
        Inventory firstslot = null;

        if (minecart.getType().equals(EntityTypes.CHESTED_MINECART))
            inv = ((ChestMinecart) minecart).getInventory();
        else if (minecart.getType().equals(EntityTypes.HOPPER_MINECART))
            inv = ((HopperMinecart) minecart).getInventory();
        else
            return false;

        for (Inventory slot : inv) {
            firstslot = slot;
            break;
        }
        
        if (firstslot == null)
            return false;
        
        return hasInInv(firstslot, itemfilter);
    }

    public static Optional<Entity> getFirstRider(Minecart minecart) {
        if (minecart.getPassengers().size() == 0)
            return Optional.empty();
        return Optional.of(minecart.getPassengers().get(0));
    }

    public static String[] parseItemParams(String itemparams) {
        String itemid = itemparams.replaceAll("[:|@](.*)", "");
        String amount = "", dmgval = "";
        if (itemparams.contains(":")) {
            amount = itemparams.replaceAll(".*:", "").replaceAll("@.*", "");
        }
        if (itemparams.contains("@")) {
            dmgval = itemparams.replaceAll(".*@", "");
        }
        return new String[]{itemid, amount, dmgval};
    }

    public static boolean isItemParamsValid(String itemparams) {
        itemparams = itemparams.toLowerCase();
        if (itemparams.startsWith("!"))
            itemparams = itemparams.substring(1);
        try
        {
            if (itemparams.length() == 0)
                return false;
            if (!IDUtil.getItemTypeFromAnyID(itemparams.replaceAll("[:|@](.*)", "")).isPresent())
                return false;

            if (itemparams.contains(":")) {
                Integer.parseInt(itemparams.replaceAll(".*:", "").replaceAll("@.*", ""));
            }
            if (itemparams.contains("@")) {
                Integer.parseInt(itemparams.replaceAll(".*@", ""));
            }
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    public static RailDirection getRailDirection(Direction direction1, Direction direction2) {
        if ((direction1.equals(Direction.NORTH) && direction2.equals(Direction.SOUTH))
        || direction2.equals(Direction.NORTH) && direction1.equals(Direction.SOUTH))
            return RailDirections.NORTH_SOUTH;
        if ((direction1.equals(Direction.EAST) && direction2.equals(Direction.WEST))
                || direction2.equals(Direction.EAST) && direction1.equals(Direction.WEST))
            return RailDirections.EAST_WEST;
        if ((direction1.equals(Direction.NORTH) && direction2.equals(Direction.EAST))
                || direction2.equals(Direction.NORTH) && direction1.equals(Direction.EAST))
            return RailDirections.NORTH_EAST;
        if ((direction1.equals(Direction.NORTH) && direction2.equals(Direction.WEST))
                || direction2.equals(Direction.NORTH) && direction1.equals(Direction.WEST))
            return RailDirections.NORTH_WEST;
        if ((direction1.equals(Direction.SOUTH) && direction2.equals(Direction.EAST))
                || direction2.equals(Direction.SOUTH) && direction1.equals(Direction.EAST))
            return RailDirections.SOUTH_EAST;
        if ((direction1.equals(Direction.SOUTH) && direction2.equals(Direction.WEST))
                || direction2.equals(Direction.SOUTH) && direction1.equals(Direction.WEST))
            return RailDirections.SOUTH_WEST;
        return null;
    }

    public static Set<Vector3i> possiblesignlocs = new HashSet<>(Arrays.asList(
            new Vector3i(0,-1,0),
            new Vector3i(1,0,0),
            new Vector3i(-1,0,0),
            new Vector3i(0,0,1),
            new Vector3i(0,0,-1)
    ));
}
