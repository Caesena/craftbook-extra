/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.mechanic.SignMechanicCacheManager;
import com.minecraftonline.util.ChangeBlockPostListenerManager;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import com.sk89q.craftbook.sponge.util.data.mutable.LastPowerData;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.util.Tuple;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

@Module(id="station", name = "Station", onEnable = "onInitialize", onDisable = "onDisable")
public class Station extends SpongeCartBlockMechanic {

    private final Map<Location<World>, RedstoneListener> listeners = new HashMap<>();

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.station", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.",
            new SpongeBlockFilter(BlockTypes.OBSIDIAN), TypeToken.of(SpongeBlockFilter.class));

    private CommandMapping stationCommandMapping;

    static Map<UUID, String> currentDestinations = new HashMap<>();

    public class RedstoneListener implements ChangeBlockPostListenerManager.ChangeBlockPostListener {

        private final Location<World> loc;

        public RedstoneListener(Location<World> loc) {
            this.loc = loc;
        }

        @Override
        public void onChange(Transaction<BlockSnapshot> transaction, Cause cause) {
            Location<World> baseLocation = transaction.getFinal().getLocation().get();
            BlockState endState = transaction.getFinal().getExtendedState();

            Location<World> heightAdjustedLoc = this.loc.getExtent().getLocation(new Vector3i(loc.getX(), baseLocation.getY(), loc.getZ()));

            Tuple<Boolean, Set<Location<World>>> endTuple = ICSocket.getPoweredInState(endState, baseLocation);

            boolean powered = endTuple != null && endTuple.getFirst() && endTuple.getSecond().contains(heightAdjustedLoc);

            SignMechanicCacheManager.CachedData cachedData = cache.getCached(Station.this, this.loc);

            if (cachedData == null) {
                return;
            }

            if (powered) {
                Collection<Entity> minecarts = this.loc.getExtent().getNearbyEntities(this.loc.add(0.5,2.5,0.5).getPosition(), 0.5);

                Stream<Entity> filteredStream = minecarts.stream().filter(entity -> entity.getType().equals(EntityTypes.RIDEABLE_MINECART)
                        || entity.getType().equals(EntityTypes.CHESTED_MINECART)
                        || entity.getType().equals(EntityTypes.HOPPER_MINECART)
                        || entity.getType().equals(EntityTypes.TNT_MINECART)
                        || entity.getType().equals(EntityTypes.FURNACE_MINECART));
                Direction signFacingDir = this.loc.require(Keys.DIRECTION);
                filteredStream.forEach(entity -> pushMinecart(signFacingDir, (Minecart) entity));
            }

            int powerLevelFake = powered ? 15 : 0; // TODO: get real power level?
            this.loc.offer(new LastPowerData(powerLevelFake));
        }
    }

    @Override
    public void onInitialize() {
        allowedBlocks.load(config);

        createPermissions.register();

        CommandSpec stationCommandSpec = CommandSpec.builder()
                .description(Text.of("Station Command"))
                .permission("craftbook.station")
                .arguments(GenericArguments.string(Text.of("station")))
                .executor(new StationCommand())
                .build();
        stationCommandMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), stationCommandSpec, "st", "station").orElse(null);

        SignMechanicCacheManager.getInstance().registerCache(this, ((loc, signData) -> {
            //if (!isValidNoCache(location)) {
            //    return null;
            //}
            boolean valid = false;
            for(String line : getValidSigns()) {
                if (SignUtil.getTextRaw(signData, 1).equalsIgnoreCase(line)) {
                    valid = true;
                    break;
                }
            }
            if (!valid) {
                return null;
            }
            RedstoneListener redstoneListener = new RedstoneListener(loc);
            listeners.put(loc, redstoneListener);
            CraftBookPlugin.spongeInst().getChangeBlockPostListenerManager().register(loc.getExtent(), createInputLocations(loc), redstoneListener);
            return new StationCachedData(signData.lines().get());
        }));
    }

    public class StationCachedData extends SignMechanicCacheManager.CachedData {
        protected StationCachedData(List<Text> lines) {
            super(lines);
        }

        @Override
        public void onUnload(Location<World> loc) {
            super.onUnload(loc);
            RedstoneListener redstoneListener = listeners.remove(loc);
            if (redstoneListener != null) {
                CraftBookPlugin.spongeInst().getChangeBlockPostListenerManager().unregisterAll(redstoneListener);
            }
        }
    }

    private final Vector3i UP_TWICE = Vector3i.from(0, 2, 0);

    public Collection<Vector3i> createInputLocations(Location<World> signLoc) {
        Vector3i pos = signLoc.getBlockPosition();

        if (signLoc.getBlockType() == BlockTypes.WALL_SIGN) {
            final Direction direction = signLoc.require(Keys.DIRECTION);
            pos = pos.add(direction.asBlockOffset());
        }

        final Vector3i posX = pos.add(1, 0, 0);
        final Vector3i negX = pos.add(-1, 0, 0);
        final Vector3i posZ = pos.add(0, 0, 1);
        final Vector3i negZ = pos.add(0, 0, -1);

        return Arrays.asList(
                posX,
                negX,
                posZ,
                negZ,

                posX.add(Vector3i.UP),
                negX.add(Vector3i.UP),
                posZ.add(Vector3i.UP),
                negZ.add(Vector3i.UP),

                posX.add(UP_TWICE),
                negX.add(UP_TWICE),
                posZ.add(UP_TWICE),
                negZ.add(UP_TWICE)
        );
    }

    @Override
    public void onDisable() {
        if (stationCommandMapping != null) {
            Sponge.getCommandManager().removeMapping(stationCommandMapping);
        }
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {
        SignMechanicCacheManager.CachedData cachedData = cache.getCached(this, blocks.getSign());
        if (cachedData == null) {
            return false;
        }
        if (!isValidNoCache(blocks.getSign())) {
            return false;
        }

        String line3 = cachedData.getLinePlain(2);
        if (line3.startsWith("#")) {
            String stationName = line3.substring(1).toLowerCase();

            Optional<Player> ridingPlayer = minecart.getPassengers().stream().filter(entity -> entity instanceof Player)
                    .map(entity -> (Player) entity)
                    .findFirst();

            if (!ridingPlayer.isPresent()) {
                return false;
            }
            if (!stationName.equals(currentDestinations.get(ridingPlayer.get().getUniqueId()))) {
                return false; // The player is not stopping here
            }
        }

        int power = BlockUtil.getBlockPowerLevel(blocks.getBase()).orElse(0);

        if (power > 0) {
            pushMinecart(blocks.getSign().require(Keys.DIRECTION), minecart);
        }
        else {
            Vector3d railCentre = blocks.getRail().getBlockPosition().toDouble().add(0.5, 0, 0.5);
            final double acceptableDistance = 0.1;
            final double distance = from.getPosition().distanceSquared(railCentre);
            if (distance > acceptableDistance) {
                return false;
            }
            // Make sure we don't cancel/stop until we are on the block.
            minecart.setVelocity(new Vector3d(0,0,0));
            return true;
        }
        return false;
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        if (SignUtil.getTextRaw(signData, 0).length() != 0
        || SignUtil.getTextRaw(signData, 3).length() != 0) {
            if (player != null)
                player.sendMessage(Text.of(TextColors.RED, "Only line 2/3 can have text on it"));
            return false;
        }
        SignMechanicCacheManager.getInstance().mechanicCreated(this, location, signData);
        return true;
    }

    @Override
    public SpongeBlockFilter getBlockFilter() {
        return allowedBlocks.getValue();
    }

    @Override
    public String[] getValidSigns() {
        return new String[] { "[Station]" };
    }

    @Override
    public boolean requiresSign(){return true;}

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    public static class StationCommand implements CommandExecutor {

        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            if (!(src instanceof Player))
                throw new CommandException(Text.of("A player must call this command"));

            String stationname = args.<String>getOne(Text.of("station")).orElseThrow(() -> new CommandException(Text.of("Station parameter not given")));

            Player player = (Player) src;

            if (stationname.equals("help")) {
                src.sendMessage(Text.of(TextColors.GREEN, "Set destination using /st (stationname). This makes you go the correct direction."));
                return CommandResult.success();
            }

            //if (!player.getBaseVehicle().getType().equals(EntityTypes.RIDEABLE_MINECART))
            //    throw new CommandException(Text.of(TextColors.GREEN, "You must be in a minecart to use this command"));

            Station.currentDestinations.put(player.getUniqueId(), stationname.toLowerCase());
            player.sendMessage(Text.of(TextColors.GREEN, "Set destination to: " + stationname));
            player.sendMessage(Text.of(TextColors.GREEN, "You will have to reset this if you logout/exit/server restarts!"));
            return CommandResult.success();
        }
    }

    /*@Listener
    public void onMinecartExit(RideEntityEvent.Dismount event, @First Player player) {
        if (!event.getTargetEntity().getType().equals(EntityTypes.RIDEABLE_MINECART))
            return;
        if (Station.currentDestinations.remove(player.getUniqueId()) != null) {
            // something was removed
            player.sendMessage(Text.of(TextColors.GREEN, "Destination has been removed."));
        }
    }*/

    public static Set<Vector3i> allowedSignLocs = new HashSet<>();

    static {
        allowedSignLocs.add(new Vector3i(1,0,0));
        allowedSignLocs.add(new Vector3i(-1,0,0));
        allowedSignLocs.add(new Vector3i(1,0,0));
        allowedSignLocs.add(new Vector3i(0,0,-1));
        allowedSignLocs.add(new Vector3i(0,-1,0));
        allowedSignLocs.add(new Vector3i(0,-2,0));
    }

    public void pushMinecart(Direction signDirection, Minecart minecart) {
        minecart.setVelocity(signDirection.getOpposite().asOffset().mul(minecart.getPotentialMaxSpeed()));
    }

    @Override
    public boolean isValid(Location<World> location) {
        return SignMechanicCacheManager.getInstance().getCached(this, location) != null;
    }

    public boolean isValidNoCache(Location<World> location) {
        return super.isValid(location);
    }
}
