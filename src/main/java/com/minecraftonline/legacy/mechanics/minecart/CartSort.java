/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.TernaryState;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.RailDirection;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;

// @author tyhdefu

@Module(id = "cartsort", name = "CartSort", onEnable="onInitialize", onDisable="onDisable")
public class CartSort extends SpongeCartBlockMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartsort", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.",
            new SpongeBlockFilter(BlockTypes.NETHERRACK), TypeToken.of(SpongeBlockFilter.class));

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        allowedBlocks.load(config);

        createPermissions.register();
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {

        if (minor || isActive(blocks) == TernaryState.FALSE) {
            return false;
        }

        if (!blocks.hasSign())
            return false;

        Sign sign = (Sign) blocks.getSign().getTileEntity().get();

        if (!sign.lines().get(1).toPlain().equals("[Sort]"))
            return false;

        Direction signfacing = blocks.getSign().getBlock().get(Keys.DIRECTION).get();

        Location<World> junctionrail = blocks.getSign().add(signfacing.getOpposite().asBlockOffset()).add(0, 2, 0);

        if (!junctionrail.getBlockType().equals(BlockTypes.RAIL))
            return false;

        CartSorting.RelativeDirection direction = CartSorting.sort(SignUtil.getTextRaw(sign, 2), SignUtil.getTextRaw(sign, 3), minecart);

        Direction newDirection = null;

        if (direction.equals(CartSorting.RelativeDirection.LEFT)) {
            newDirection = SignUtil.getCounterClockWise(signfacing.getOpposite());
        }
        else if (direction.equals(CartSorting.RelativeDirection.RIGHT)){
            newDirection = SignUtil.getClockWise(signfacing.getOpposite());
        }
        else if (direction.equals(CartSorting.RelativeDirection.NONE)) {
            newDirection = signfacing.getOpposite();
        }
        if (newDirection == null)
            return false;

        RailDirection raildirection = CartSorting.getRailDirection(signfacing, newDirection);

        if (raildirection == null) {
            return false;
        }

        junctionrail.offer(Keys.RAIL_DIRECTION, raildirection);
        return false;
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        if (!location.getTileEntity().isPresent())
            return false;
        Direction direction = location.getBlock().get(Keys.DIRECTION).get();
        if (!(direction.equals(Direction.NORTH)
        || direction.equals(Direction.EAST)
        || direction.equals(Direction.SOUTH)
        || direction.equals(Direction.WEST))) {
            if (player != null) {
                player.sendMessage(Text.of(TextColors.RED, "Cannot place Sort Sign at this a non 90 degree angle"));
                return false;
            }
        }
        return CartSorting.match(null, SignUtil.getTextRaw(signData, 2), true) && CartSorting.match(null, SignUtil.getTextRaw(signData, 3), true);
    }

    @Override
    public SpongeBlockFilter getBlockFilter() {
        return allowedBlocks.getValue();
    }

    @Override
    public String[] getValidSigns() {
        return new String[]{
                "[Sort]"
        };
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public boolean requiresSign() {
        return true;
    }
}
