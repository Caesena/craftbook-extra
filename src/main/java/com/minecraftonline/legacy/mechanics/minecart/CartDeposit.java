/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.legacy.blockbags.MultiNearbyChestBlockBag;
import com.minecraftonline.legacy.blockbags.NearbyChestBlockBag;
import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicate;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.TernaryState;
import com.sk89q.craftbook.sponge.mechanics.blockbags.inventory.InventoryBlockBag;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.carrier.Chest;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.ChestMinecart;
import org.spongepowered.api.entity.vehicle.minecart.HopperMinecart;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;

// @author tyhdefu

@Module(id = "cartdeposit", name = "CartDeposit", onEnable="onInitialize", onDisable="onDisable")
public class CartDeposit extends SpongeCartBlockMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartdeposit", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.",
            new SpongeBlockFilter(BlockTypes.IRON_ORE), TypeToken.of(SpongeBlockFilter.class));

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        allowedBlocks.load(config);

        createPermissions.register();
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        return CartCollect.verifyItemLine(SignUtil.getTextRaw(signData, 2), player);
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {

        if (minor) {
            return false;
        }

        if (!blocks.hasSign()) {
            return false;
        }
        Sign sign = (Sign) blocks.getSign().getTileEntity().get();
        if (!isMechanicSign(sign.getSignData())) {
            return false;
        }

        Inventory minecartinv;

        if (minecart.getType().equals(EntityTypes.CHESTED_MINECART))
            minecartinv = ((ChestMinecart) minecart).getInventory();
        else if (minecart.getType().equals(EntityTypes.HOPPER_MINECART))
            minecartinv = ((HopperMinecart) minecart).getInventory();
        else
            return false;

        MultiNearbyChestBlockBag bag = CartCollect.getBlockBag(blocks.getRail());

        if (bag == null)
            return false;

        ItemPredicate itemPredicate;

        Integer amount = null;
        if (sign.lines().get(2).toPlain().length() != 0)
        {
            final String line3 = sign.lines().get(2).toPlain();
            if (line3.contains(":")) {
                amount = Integer.parseInt(line3.split(":")[1]);
            }

            itemPredicate = CartCollect.readItemPredicate(line3);
        }
        else {
            itemPredicate = ItemPredicate.ANY;
        }

        for (Inventory slot : bag.slots()) {
            if (!slot.peek().isPresent())
                continue;

            ItemStack itemStack;

            if (amount == null) {
                itemStack = slot.peek().get();
            }
            else {
                itemStack = slot.peek(amount).get();
            }

            if (!itemPredicate.test(itemStack)) {
                continue;
            }

            if (!minecartinv.canFit(itemStack)) {
                continue;
            }

            if (amount == null) {
                slot.poll();
            }
            else {
                slot.poll(amount);
                amount -= itemStack.getQuantity();
            }

            minecartinv.offer(itemStack);

            if (amount != null && amount <= 0)
                break;
        }
        return false;
    }

    @Override
    public String[] getValidSigns() {
        return new String[] {
                "[Deposit]"
        };
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public SpongeBlockFilter getBlockFilter() {
        return allowedBlocks.getValue();
    }

    @Override
    public boolean requiresSign() { return true; }
}
