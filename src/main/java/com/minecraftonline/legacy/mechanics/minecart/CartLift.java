/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.util.CartUtil;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.TernaryState;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

// @author tyhdefu

@Module(id = "cartlift", name = "CartLift", onEnable="onInitialize", onDisable="onDisable")
public class CartLift extends SpongeCartBlockMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartlift", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.",
            new SpongeBlockFilter(BlockTypes.WOOL.getDefaultState().with(Keys.DYE_COLOR, DyeColors.ORANGE).get()), TypeToken.of(SpongeBlockFilter.class));

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        allowedBlocks.load(config);

        createPermissions.register();
    }

    public ConfigValue<SpongeBlockFilter> getAllowedBlocks() {
        return allowedBlocks;
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {

        if (minor || !blocks.hasSign()) {
            return false;
        }

        Sign sign = (Sign) blocks.getSign().getTileEntity().get();

        if (!SignUtil.getTextRaw(sign,1).equals("[CartLift]"))
            return false;

        CartSorting.RelativeDirection direction = CartSorting.cartLiftSort(SignUtil.getTextRaw(sign, 2), SignUtil.getTextRaw(sign, 3), minecart);

        if (direction.equals(CartSorting.RelativeDirection.NONE))
            return false;
        Optional<CartDestination> targetLift = this.findCartLift(blocks.getBase(), direction);
        if (!targetLift.isPresent())
            return false; // no lift found

        // Rounds location - stops recursive effect when moving a block down
        CartDestination cartDestination = targetLift.get();
        cartDestination.teleportCart(minecart);
        return false;
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        if (!location.getTileEntity().isPresent())
            return false;
        return CartSorting.match(null, SignUtil.getTextRaw(signData, 2), true) && CartSorting.match(null, SignUtil.getTextRaw(signData, 3), true);
    }

    @Override
    public SpongeBlockFilter getBlockFilter() { return allowedBlocks.getValue(); }

    @Override
    public String[] getValidSigns() {
        return new String[]{
                "[CartLift]"
        };
    }

    public static class CartDestination {

        private final Location<World> loc;
        private final Direction direction;

        private CartDestination(final Location<World> loc, final Direction direction) {
            this.loc = loc;
            this.direction = direction;
        }

        @Nullable
        public static Direction getDefaultDirection(Location<World> railLoc) {
            Direction[] dirs = CartUtil.getRailDirections(railLoc.require(Keys.RAIL_DIRECTION));
            for (Direction direction : DIRECTIONS_IN_PRIORITY_ORDER) {
                for (Direction railDirection : dirs) {
                    if (CartUtil.isRail(railLoc.getBlockRelative(direction))
                            && direction == railDirection) {
                        return direction;
                    }
                }
            }
            return null;
        }


        public void teleportCart(Minecart minecart) {
            double speed = minecart.getVelocity().length();
            // Stop stack overflow.
            Location<World> tpLoc = new Location<>(loc.getExtent(), loc.add(0, 1, 0).getBlockPosition())
                    .add(direction.asOffset().div(2));
            minecart.setLocation(tpLoc);
            minecart.setVelocity(direction.asOffset().mul(speed));
        }

        public static Direction[] DIRECTIONS_IN_PRIORITY_ORDER = new Direction[] {Direction.WEST, Direction.SOUTH, Direction.EAST, Direction.NORTH};

        public static CartDestination fromLocation(final Location<World> loc, final Predicate<Location<World>> isMechanicSign) {
            Location<World> below = loc.getBlockRelative(Direction.DOWN);
            Direction dir = null;
            if (SignUtil.isSign(below)) {
                dir = below.require(Keys.DIRECTION).getOpposite();
            }
            else {
                List<Location<World>> signs = SignUtil.getAttachedSigns(loc);
                Optional<Location<World>> optSign = signs.stream().filter(isMechanicSign).findAny();
                if (optSign.isPresent()) {
                    dir = optSign.get().require(Keys.DIRECTION).getOpposite();
                }
                else {
                    dir = getDefaultDirection(loc.getBlockRelative(Direction.UP));
                    if (dir == null) {
                        dir = Direction.NONE;
                    }
                }
            }

            return new CartDestination(loc, dir);
        }

    }

    public Optional<CartDestination> findCartLift(Location<World> location, CartSorting.RelativeDirection direction) {
        if (direction.equals(CartSorting.RelativeDirection.UP)
                || direction.equals(CartSorting.RelativeDirection.DOWN)) {
            int y = location.getBlockY();
            int dir = direction.equals(CartSorting.RelativeDirection.UP) ? 1 : -1;
            int dy = dir;
            for (; (y + dy) >= 0 && (y + dy) <= CraftBookPlugin.MINECRAFT_WORLD_HEIGHT_MAX; dy += dir) {
                Location<World> loc = location.add(0, dy, 0);
                BlockState block = loc.getBlock();

                if (!(BlockUtil.doesStatePassFilter(this.getBlockFilter(), block)))
                    continue;
                if (!(CartUtil.isRail(loc.add(0,1,0))))
                    continue;
                return Optional.of(CartDestination.fromLocation(loc, this::isValid));
            }
        }
        return Optional.empty();
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public boolean requiresSign() {
        return true;
    }
}