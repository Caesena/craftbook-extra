/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics.minecart;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.legacy.blockbags.MultiNearbyChestBlockBag;
import com.minecraftonline.legacy.blockbags.NearbyChestBlockBag;
import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.parsing.block.predicate.BlockPredicate;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicate;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.TernaryState;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager;
import com.sk89q.craftbook.sponge.mechanics.blockbags.inventory.BlockInventoryBlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.inventory.InventoryBlockBag;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMechanismBlocks;
import com.sk89q.craftbook.sponge.mechanics.minecart.block.SpongeCartBlockMechanic;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.block.tileentity.carrier.Chest;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.minecart.ChestMinecart;
import org.spongepowered.api.entity.vehicle.minecart.HopperMinecart;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperation;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

// @author tyhdefu

@Module(id = "cartcollect", name = "CartCollect", onEnable = "onInitialize", onDisable = "onDisable")
public class CartCollect extends SpongeCartBlockMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.cartcollect", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    private final ConfigValue<SpongeBlockFilter> allowedBlocks = new ConfigValue<>("material", "The block that this mechanic requires.",
            new SpongeBlockFilter(BlockTypes.IRON_ORE), TypeToken.of(SpongeBlockFilter.class));

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        allowedBlocks.load(config);

        createPermissions.register();
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        return verifyItemLine(SignUtil.getTextRaw(signData, 2), player);
    }

    public static boolean verifyItemLine(final String line, Player player) {
        if (line.length() == 0)
            return true;
        String[] itemparams = line.split("@", 2);

        // Allow empty itemId.
        final String itemId = itemparams[0].split(":")[0];
        if (!itemId.isEmpty() && !IDUtil.getItemTypeFromAnyID(itemId).isPresent()) {
            if (player != null) {
                player.sendMessage(Text.of(TextColors.RED, "Invalid item id provided (id@damage:amount)"));
            }
            return false;
        }

        if (itemparams.length == 2) {
            try {
                Integer.parseInt(itemparams[1].split(":")[0]);
            } catch (NumberFormatException e) {
                if (player != null) {
                    player.sendMessage(Text.of(TextColors.RED, "Invalid item damage value provided (id@damage:amount)"));
                }
                return false;
            }
        }

        if (line.contains(":")) {
            try {
                Integer.parseInt(line.split(":")[1]);
            } catch (NumberFormatException e) {
                if (player != null) {
                    player.sendMessage(Text.of(TextColors.RED, "Invalid amount provided (id@damage:amount)"));
                }
                return false;
            }
        }
        return true;
    }

    public static ItemPredicate readItemPredicate(final String line) {
        String itemid = line.split("@")[0].split(":")[0];
        ItemPredicate.Builder builder = ItemPredicate.builder();
        if (!itemid.isEmpty()) {
            builder.itemType(IDUtil.getItemTypeFromAnyID(itemid).get());
        }
        if (line.contains("@")) {
            int dmgval = Integer.parseInt(line.split("@")[1].split(":")[0]);
            builder.damage(dmgval);
        }
        return builder.build();
    }

    /**
     * Adds all storage blocks in a 5x5x2 cuboid centered around rail location,
     * as well as those located 2 and 3 blocks directly above to be used as storage space.
     * Used for [Deposit]/[Collect].
     *
     * @param base, base block of the cart deposit/cart collect
     * @return true, if at least one storage block was added, false if no suitable block was found
     */
    public static MultiNearbyChestBlockBag getBlockBag(Location<World> base) {
        MultiNearbyChestBlockBag multiNearbyChestBlockBag = new MultiNearbyChestBlockBag();
        Set<Location<World>> skipLocations = new HashSet<>();
        for (int x = -2; x <= 2; x++) {
            for (int z = -2; z <= 2; z++) {
                for (int y = -1; y <= 0; y++) {
                    Location<World> chestLoc = base.add(x, y, z);
                    if (skipLocations.contains(chestLoc) || !chestLoc.hasTileEntity()) {
                        continue;
                    }
                    Optional<Chest> chest = chestLoc.getTileEntity().filter(te -> te instanceof Chest)
                            .map(te -> (Chest) te);
                    if (!chest.isPresent()) {
                        continue;
                    }
                    NearbyChestBlockBag bag = new NearbyChestBlockBag(chest.get().getDoubleChestInventory().orElse(chest.get().getInventory()), chestLoc);
                    multiNearbyChestBlockBag.addBlockBag(bag);
                    chest.get().getConnectedChests().forEach(c -> skipLocations.add(c.getLocation()));
                }
            }
        }

        NearbyChestBlockBag.findBlockBagAt(base.add(0, 2, 0)).ifPresent(multiNearbyChestBlockBag::addBlockBag);
        NearbyChestBlockBag.findBlockBagAt(base.add(0, 3, 0)).ifPresent(multiNearbyChestBlockBag::addBlockBag);

        return multiNearbyChestBlockBag.isEmpty() ? null : multiNearbyChestBlockBag;
    }

    @Override
    public boolean impact(Minecart minecart, CartMechanismBlocks blocks, boolean minor, Location<World> from) {

        if (minor) {
            return false;
        }

        if (!blocks.hasSign()) {
            return false;
        }
        Sign sign = (Sign) blocks.getSign().getTileEntity().get();
        if (!isMechanicSign(sign.getSignData())) {
            return false;
        }

        Inventory minecartinv;

        if (minecart.getType().equals(EntityTypes.CHESTED_MINECART))
            minecartinv = ((ChestMinecart) minecart).getInventory();
        else if (minecart.getType().equals(EntityTypes.HOPPER_MINECART))
            minecartinv = ((HopperMinecart) minecart).getInventory();
        else
            return false;

        MultiNearbyChestBlockBag bag = getBlockBag(blocks.getRail());

        if (bag == null)
            return false;

        Inventory sourceInventory = minecartinv;

        Integer amount = null;
        if (sign.lines().get(2).toPlain().length() != 0) {
            String line3 = sign.lines().get(2).toPlain();

            if (line3.contains(":")) {
                amount = Integer.parseInt(line3.split(":")[1]);
            }

            ItemPredicate itemPredicate = readItemPredicate(line3);
            sourceInventory = minecartinv.query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(itemPredicate));
        }

        for (Inventory slot : sourceInventory.slots()) {
            if (!slot.peek().isPresent())
                continue;
            ItemStack itemStack;

            if (amount != null) {
                itemStack = slot.poll(amount).get();
                amount -= itemStack.getQuantity();
            } else {
                itemStack = slot.poll().get();
            }
            ItemStack unfittable = bag.add(itemStack);
            minecartinv.offer(unfittable); // Return teh items.
            if (amount != null && amount <= 0) {
                break;
            }
        }
        return false;
    }

    @Override
    public String[] getValidSigns() {
        return new String[]{
                "[Collect]"
        };
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public SpongeBlockFilter getBlockFilter() {
        return allowedBlocks.getValue();
    }

    @Override
    public boolean requiresSign() {
        return true;
    }
}
