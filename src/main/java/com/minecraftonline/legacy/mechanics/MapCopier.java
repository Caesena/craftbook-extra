/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics;

import com.flowpowered.math.vector.Vector3d;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.legacy.MapStorageMixinInterface;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeSignMechanic;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import net.minecraft.item.ItemMap;
import net.minecraft.world.storage.MapStorage;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Optional;

@Module(id = "mapcopier", name = "MapCopier", onEnable="onInitialize", onDisable="onDisable")
public class MapCopier extends SpongeSignMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.mapcopier", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    @Listener
    public void onBlockInteract(InteractBlockEvent.Secondary.MainHand event) {
        if (!(event.getSource() instanceof Player && event.getTargetBlock().getState().getType().equals(BlockTypes.BOOKSHELF)))
            return;
        Player player = ((Player) event.getSource());
        if (!player.getItemInHand(HandTypes.MAIN_HAND).isPresent())
            return;
        ItemStack helditem = player.getItemInHand(HandTypes.MAIN_HAND).get();
        if (helditem.getType().equals(ItemTypes.MAP) || helditem.getType().equals(ItemTypes.FILLED_MAP)) {
            getMapCopierSign(event.getTargetBlock().getLocation().get()).ifPresent(sign -> {
                event.setCancelled(true);
                ItemStack newmap = ItemStack.of(ItemTypes.FILLED_MAP);
                newmap = ItemStack.builder().fromContainer(
                        newmap.toContainer().set(DataQuery.of("UnsafeDamage"), Integer.parseInt(sign.lines().get(0).toPlain())))
                        .build();
                if (!player.getInventory().canFit(newmap)) {
                    player.sendMessage(Text.of("You cannot fit the new map"));
                    return;
                }
                helditem.setQuantity(helditem.getQuantity() - 1);
                player.getInventory().offer(newmap);
            });
        }
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        if (!location.getBlockType().equals(BlockTypes.WALL_SIGN)) {
            if (player != null) {
                player.sendMessage(Text.of(TextColors.RED, "MapCopier must be a wall sign!"));
            }
            return false;
        }
        ItemMap i;
        if (!location.add(location.require(Keys.DIRECTION).getOpposite().asBlockOffset()).getBlockType().equals(BlockTypes.BOOKSHELF)) {
            if (player != null)
                player.sendMessage(Text.of(TextColors.RED, "MapCopier must be placed on a bookshelf!"));
            return false;
        }
        MapStorage mapStorage = ((net.minecraft.world.World) location.getExtent()).getMapStorage(); // Map Storage may be null
        MapStorageMixinInterface mapStrgMixinInterface = (MapStorageMixinInterface) mapStorage;
        short highestMap = mapStrgMixinInterface.getHighestMapId();
        try {
            int mapNumber = Integer.parseInt(signData.lines().get(0).toPlain());
            if (mapNumber < 0) {
                if (player != null)
                    player.sendMessage(Text.of("map number must be a positive number"));
                return false;
            }
            if (mapNumber > highestMap) {
                if (player != null)
                    player.sendMessage(Text.of(TextColors.RED, "That map number does not exist"));
                return false;
            }
            /*
            if (mapNumber > Short.MAX_VALUE) {
                if (player != null)
                    player.sendMessage(Text.of("eh, no amounts over 16 bit int limit eh? you lose maps like this"));
                return false;
            }*/
        }
        catch (NumberFormatException e) {
            if (player != null)
                player.sendMessage(Text.of("First line must be a map number"));
            return false;
        }
        return true;
    }

    public Optional<Sign> getMapCopierSign(Location<World> loc) {
        for (Vector3d dir : adjacents) {
            if (loc.add(dir).getBlockType().equals(BlockTypes.WALL_SIGN)) {
                Sign sign = (Sign) loc.add(dir).getTileEntity().get();
                if (isMechanicSign(sign.getSignData()))
                    return Optional.of(sign);
            }
        }
        return Optional.empty();
    }

    static Vector3d[] adjacents = new Vector3d[]{
            new Vector3d(1,0,0),
            new Vector3d(-1,0,0),
            new Vector3d(0,0,1),
            new Vector3d(0,0,-1)
    };

    @Override
    public String[] getValidSigns() {
        return new String[] {"[Map]"};
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }
}
