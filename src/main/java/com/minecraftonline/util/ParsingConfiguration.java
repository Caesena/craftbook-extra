/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import com.google.common.collect.ImmutableMap;
import com.minecraftonline.util.parsing.Parameter;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ParsingConfiguration {

    private final Map<LineParser, LineParserInfo> parsers;

    private ParsingConfiguration(Map<LineParser, LineParserInfo> parsers) {
        this.parsers = parsers;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static ParsingConfiguration blank() {
        return new ParsingConfiguration(Collections.emptyMap());
    }

    public void parseAll(List<Text> lines, @Nullable Subject subject) throws InvalidICException {
        for (Map.Entry<LineParser, LineParserInfo> entry : parsers.entrySet()) {

            LineParser parser = entry.getKey();
            LineParserInfo info = entry.getValue();
            String s = info.getToParse(lines);
            try {
                parser.parse(s, subject);
            } catch (Parameter.ParsingException e) {
                Text.Builder textBuilder = Text.builder();
                textBuilder.append(Text.of("Failed to parse "));
                textBuilder.append(Text.of(info.getLinesDescription()));

                textBuilder.append(Text.of(" '" + s + "'"));

                if (parser.getHelp() != null) {
                    textBuilder.append(Text.of(" ")).append(parser.getHelp());
                }
                textBuilder.append(Text.of(" ")).append(e.getText());
                throw new InvalidICException(textBuilder.build(), e);
            }
        }
    }

    public Map<LineParser, LineParserInfo> getParsers() {
        return ImmutableMap.copyOf(parsers);
    }

    public interface LineParser {
        /**
         * Parses a single line
         * @param line Line to parse in plain text.
         * @param cause Subject cause. May be null. Useful for permission checks.
         *
         * @throws InvalidICException If parsing failed.
         * @throws Parameter.ParsingException If parsing failed.
         */
        void parse(String line, @Nullable Subject cause) throws InvalidICException, Parameter.ParsingException;

        default Text getHelp() {
            return null;
        }
    }

    public static class Builder {

        private final Map<LineParser, LineParserInfo> parsers = new LinkedHashMap<>();

        /**
         * Adds a parser to this configuration. These parsers are
         * guaranteed to called in order that they were added to
         * the builder, the line numbers are irrelevant for
         * parsing order.
         *
         * @param lineParser LineParser parser to add.
         * @param line Lines that this parser will parse.
         * @return This builder, for chaining
         */
        public Builder lineParser(int line, LineParser lineParser) {
            this.parsers.put(lineParser, new SingleLineParserInfo(line));
            return this;
        }

        /**
         * Adds a parser to this configuration.
         * @param lineParser LineParser parser to add.
         * @param lines Lines that this parser will parse, appended together in the given order.
         * @return This builder, for chaining
         */
        public Builder multiLineParser(LineParser lineParser, int... lines) {
            this.parsers.put(lineParser, new MultiLineParserInfo(lines));
            return this;
        }

        public ParsingConfiguration build() {
            return new ParsingConfiguration(Collections.unmodifiableMap(new LinkedHashMap<>(parsers)));
        }
    }

    public interface LineParserInfo {
        String getToParse(List<Text> lines);

        String getLinesDescription();

        boolean includesTopLine();
    }

    public static class SingleLineParserInfo implements LineParserInfo {

        public final int line;

        public SingleLineParserInfo(int line) {
            this.line = line;
        }

        @Override
        public String getToParse(List<Text> lines) {
            return lines.get(line).toPlain();
        }

        @Override
        public String getLinesDescription() {
            return "line " + (line + 1);
        }

        @Override
        public boolean includesTopLine() {
            return line == 0;
        }

        public int getLine() {
            return line;
        }
    }

    public static class MultiLineParserInfo implements LineParserInfo {

        public final int[] lines;

        public MultiLineParserInfo(int[] lines) {
            this.lines = lines;
        }

        @Override
        public String getToParse(List<Text> lines) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int line : this.lines) {
                stringBuilder.append(lines.get(line).toPlain());
            }
            return stringBuilder.toString();
        }

        @Override
        public String getLinesDescription() {
            return "lines " + Arrays.toString(Arrays.stream(lines).map(i -> i + 1).toArray());
        }

        @Override
        public boolean includesTopLine() {
            for (int line : lines) {
                if (line == 0) {
                    return true;
                }
            }
            return false;
        }

        public int[] getLines() {
            return lines;
        }
    }
}
