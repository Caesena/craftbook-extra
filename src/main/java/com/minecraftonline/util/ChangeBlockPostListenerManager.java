/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class ChangeBlockPostListenerManager {

    private final Map<UUID, WorldListenerManager> worldListenerMap = new HashMap<>();
    private final Map<ChangeBlockPostListener, Multimap<UUID, Vector3i>> listenerSourceMap = new HashMap<>();

    public interface ChangeBlockPostListener {

        void onChange(Transaction<BlockSnapshot> transaction, Cause cause);
    }

    public static class WorldListenerManager {
        private final Multimap<Vector3i, ChangeBlockPostListener> listenerMap = HashMultimap.create();

        public void registerAll(Iterable<Vector3i> addLocations, ChangeBlockPostListener listener) {
            for (Vector3i vector3i : addLocations) {
                listenerMap.put(vector3i, listener);
            }
        }

        public void unregisterAll(Iterable<Vector3i> removeLocations, ChangeBlockPostListener listener) {
            for (Vector3i vector3i : removeLocations) {
                listenerMap.remove(vector3i, listener);
            }
        }

        public Collection<ChangeBlockPostListener> getListeners(Vector3i changed) {
            return listenerMap.get(changed);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof WorldListenerManager)) return false;
            WorldListenerManager that = (WorldListenerManager) o;
            return Objects.equals(listenerMap, that.listenerMap);
        }

        @Override
        public int hashCode() {
            return Objects.hash(listenerMap);
        }
    }

    public void register(World world, Iterable<Vector3i> locations, ChangeBlockPostListener changeBlockPostListener) {
        UUID uuid = world.getUniqueId();
        Multimap<UUID, Vector3i> map = this.listenerSourceMap.computeIfAbsent(changeBlockPostListener, (k) -> HashMultimap.create());

        map.putAll(uuid, locations);

        WorldListenerManager worldListenerManager = this.worldListenerMap.computeIfAbsent(uuid, (k) -> new WorldListenerManager());
        worldListenerManager.registerAll(locations, changeBlockPostListener);
    }

    public void unregister(World world, Collection<Vector3i> locations, ChangeBlockPostListener changeBlockPostListener) {
        Multimap<UUID, Vector3i> map = this.listenerSourceMap.remove(changeBlockPostListener);
        if (map == null) {
            return;
        }
        map.get(world.getUniqueId()).removeAll(locations);
        WorldListenerManager worldListenerManager = this.worldListenerMap.get(world.getUniqueId());
        if (worldListenerManager == null) {
            CraftBookPlugin.spongeInst().getLogger().error("Found no world in listenersSourceMap but found a world entry for worldListenerMap");
            return;
        }
        worldListenerManager.unregisterAll(locations, changeBlockPostListener);
    }

    public void unregisterAll(ChangeBlockPostListener changeBlockPostListener) {
        Multimap<UUID, Vector3i> map = this.listenerSourceMap.remove(changeBlockPostListener);
        if (map == null) {
            return;
        }
        for (Map.Entry<UUID, Collection<Vector3i>> entry : map.asMap().entrySet()) {
            WorldListenerManager worldListenerManager = worldListenerMap.get(entry.getKey());
            if (worldListenerManager != null) {
                worldListenerManager.unregisterAll(entry.getValue(), changeBlockPostListener);
            }
            else {
                CraftBookPlugin.spongeInst().getLogger().error("Expected there to be an entry for the world " + entry.getKey() + " in the registered listeners map, as there was one in the source map.");
            }
        }
    }

    @Listener
    public void onChangeBlock(ChangeBlockEvent.Post event) {
        event.getTransactions().forEach(blockSnapshotTransaction -> {
            Location<World> baseLocation = blockSnapshotTransaction.getFinal().getLocation().get();
            World world = baseLocation.getExtent();
            WorldListenerManager worldListenerManager = worldListenerMap.get(world.getUniqueId());
            if (worldListenerManager == null) {
                return;
            }
            Vector3i changed = baseLocation.getBlockPosition();
            worldListenerManager.getListeners(changed).forEach(listener -> listener.onChange(blockSnapshotTransaction, event.getCause()));
        });
    }

    public int getSourceMapSize() {
        return listenerSourceMap.size();
    }
}
