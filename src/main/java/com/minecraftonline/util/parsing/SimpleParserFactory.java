/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing;

import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.lineparser.ParserFactory;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;

import java.util.function.Consumer;

public class SimpleParserFactory<T> implements ParserFactory<T> {

    private final Parser<T> parser;
    @Nullable
    private final T defaultValue;

    public SimpleParserFactory(Parser<T> parser, @Nullable T defaultValue) {
        this.parser = parser;
        this.defaultValue = defaultValue;
    }

    public static <T> SimpleParserFactory<T> of(Parser<T> parser) {
        return new SimpleParserFactory<>(parser, null);
    }

    public static <T> SimpleParserFactory<T> of(Parser<T> parser, T defaultValue) {
        return new SimpleParserFactory<>(parser, defaultValue);
    }

    @Override
    public T parse(String line, @Nullable Subject subject, IC ic) throws Parameter.ParsingException, InvalidICException {
        return parser.parse(line);
    }

    @Override
    public @Nullable T getDefaultValue() {
        return defaultValue;
    }

    @Override
    public Text getHelp() {
        if (defaultValue != null) {

            if (parser.getHelp() == null) {
                return Text.of("Defaults to " + defaultValue);
            }
            return Text.builder().append(parser.getHelp()).append(Text.of(" Defaults to " + defaultValue)).build();
        }
        return parser.getHelp();
    }

    @Override
    public ParsingConfiguration.LineParser createBinder(IC ic, Consumer<T> binder) {
        return new ParsingConfiguration.LineParser() {
            @Override
            public void parse(String line, @Nullable Subject cause) throws InvalidICException, Parameter.ParsingException {
                T value = SimpleParserFactory.this.parse(line, cause, ic);
                if (value == null && defaultValue != null) {
                    binder.accept(defaultValue);
                    return;
                }
                binder.accept(value);
            }

            @Override
            public Text getHelp() {
                return SimpleParserFactory.this.getHelp();
            }
        };
    }
}
