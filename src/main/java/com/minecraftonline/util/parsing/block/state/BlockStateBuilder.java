/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.block.state;

import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.parsing.block.AbstractBlockBuilder;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.world.World;

public class BlockStateBuilder extends AbstractBlockBuilder<BlockStateBuilder, BlockState> {
    @Override
    public BlockState build() {
        if (blockType == null) {
            throw new IllegalStateException("BlockState builder needs to have a blocktype!");
        }
        if (this.damage == null) {
            this.damage = 0;
        }
        if (this.damage != 0) {
            return IDUtil.createBlockStateWithDamage(this.blockType, this.damage);
        }
        else {
            return BlockState.builder().blockType(this.blockType).build();
        }
    }

    @Override
    protected BlockStateBuilder getThis() {
        return this;
    }
}
