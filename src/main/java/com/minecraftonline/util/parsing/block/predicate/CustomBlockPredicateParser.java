/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.block.predicate;

import com.minecraftonline.util.parsing.AbstractParser;
import com.minecraftonline.util.parsing.ParameterBundle;
import com.minecraftonline.util.parsing.block.BlockParameters;
import com.minecraftonline.util.parsing.rangeparser.StringRangeExtractor;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.text.Text;

import java.util.Map;

public class CustomBlockPredicateParser extends AbstractParser<BlockPredicate, String, BlockPredicate.Builder, BlockParameters> implements BlockPredicateParser {

    public CustomBlockPredicateParser(Map<StringRangeExtractor, ParameterBundle<BlockParameters, String>> map, Map<BlockParameters, String> valueMap, Map<String, BlockPredicate> exceptionMap, @Nullable Text helpMessage) {
        super(map, valueMap, exceptionMap, helpMessage);
    }

    @Override
    public BlockPredicate.Builder createBuilder() {
        return new BlockPredicate.Builder();
    }

    @Override
    public String readValue(String s) {
        return s;
    }
}
