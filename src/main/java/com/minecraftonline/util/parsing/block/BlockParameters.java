/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.block;

import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.parsing.Parameter;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.api.block.BlockType;

public enum BlockParameters implements Parameter<String, AbstractBlockBuilder<?, ?>> {

    BLOCK_TYPE() {
        @Override
        public void apply(@NotNull String value, @NotNull AbstractBlockBuilder<?, ?> builder) throws ParsingException {

            BlockType blockType = IDUtil.getBlockTypeFromAnyID(value)
                    .orElseThrow(() -> new ParsingException("'" + value + "' is not a valid block type!"));
            builder.blockType(blockType);
        }
    },

    DAMAGE() {
        @Override
        public void apply(@NotNull String value, @NotNull AbstractBlockBuilder<?, ?> builder) throws ParsingException {
            try {
                int damage = Integer.parseInt(value);
                builder.damage(damage);
            } catch (NumberFormatException e) {
                throw new ParsingException("Invalid damage '" + value + "'. Must be a number");
            }
        }
    }
}
