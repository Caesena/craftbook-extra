/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.block.predicate;

import com.minecraftonline.util.parsing.block.AbstractBlockBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class BlockPredicate implements Predicate<BlockState> {

    private final BlockType blockType;
    private final Integer damage;
    private final boolean inverted;

    private BlockPredicate(BlockType blockType, Integer damage, boolean inverted) {
        this.blockType = blockType;
        this.damage = damage;
        this.inverted = inverted;
    }

    public static BlockPredicate.Builder builder() {
        return new Builder();
    }

    @Override
    public boolean test(BlockState blockState) {
        return ((blockType == null || blockType == blockState.getType())
                && (damage == null || damage == ((Block) blockState.getType()).getMetaFromState((IBlockState) blockState))) ^ inverted;
    }

    private static boolean matchesOrNull(@Nullable Object our, @Nonnull Object other) {
        return our == null || other.equals(our);
    }

    private static boolean matchesOrNull(@Nullable Object our, Supplier<Object> otherSupplier) {
        return our == null || otherSupplier.get().equals(our);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BlockPredicate)) return false;
        BlockPredicate that = (BlockPredicate) o;
        return inverted == that.inverted && Objects.equals(blockType, that.blockType) && Objects.equals(damage, that.damage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(blockType, damage, inverted);
    }

    @Override
    public String toString() {
        return "BlockPredicate{" +
                "blockType=" + blockType +
                ", damage=" + damage +
                ", inverted=" + inverted +
                '}';
    }

    public static class Builder extends AbstractBlockBuilder<Builder, BlockPredicate> {

        protected boolean inverted = false;

        public Builder negated() {
            inverted = !inverted;
            return getThis();
        }

        @Override
        public BlockPredicate build() {
            return new BlockPredicate(blockType, damage, inverted);
        }

        @Override
        protected Builder getThis() {
            return this;
        }
    }
}
