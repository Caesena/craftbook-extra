/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing;

import com.minecraftonline.util.parsing.rangeparser.StringRangeExtractor;
import org.intellij.lang.annotations.Language;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.text.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractParameterParserBuilder<PI, RB extends ParameterBuilder<O>, P extends Parameter<PI, ? super RB>, O, PAR extends Parser<O>>
        implements ParameterParserBuilder<PI, RB, P, O, PAR>{

    // Expose to createParser()
    protected final Map<StringRangeExtractor, ParameterBundle<P, PI>> dataMap = new LinkedHashMap<>();
    protected final Map<String, O> exceptionMap = new LinkedHashMap<>();
    protected Text helpMessage = null;

    private final List<String> delimiters = new ArrayList<>();

    // Pushed and reset every time there is a new "section"
    private ParameterBundle<P, PI> curBundle = null;
    private boolean curOptional = false;

    protected final Map<P, PI> valueMap = new HashMap<>();

    @Override
    public AbstractParameterParserBuilder<PI, RB, P, O, PAR> parameter(P parameter) {
        if (curBundle != null) {
            throw new IllegalStateException("You must use delimiter() after the first set of range modifiers if you wish to have multiple 'groups'");
        }
        curBundle = new ParameterBundle<>(Collections.singletonList(parameter));
        return this;
    }

    @Override
    public AbstractParameterParserBuilder<PI, RB, P, O, PAR> parameters(String delimiter, Collection<P> parameters) {
        if (curBundle != null) {
            throw new IllegalStateException("You must use delimiter() after the first set of range modifiers if you wish to have multiple 'groups'");
        }
        curBundle = new ParameterBundle<>(delimiter, new ArrayList<>(parameters));
        return this;
    }

    @Override
    public AbstractParameterParserBuilder<PI, RB, P, O, PAR> optional(P parameter) {
        return optional(parameter, null);
    }

    @Override
    public AbstractParameterParserBuilder<PI, RB, P, O, PAR> optional(P parameter, @Nullable PI defaultValue) {
        if (curBundle == null) {
            curBundle = new ParameterBundle<>(null, Collections.singletonList(parameter), true, defaultValue);
        }
        else {
            List<P> modifiers = new ArrayList<>(curBundle.getParameters());
            modifiers.add(parameter);
            curBundle = new ParameterBundle<>(curBundle.getDelimiter(), modifiers, true, defaultValue);
        }
        curOptional = true;

        return this;
    }

    @Override
    public AbstractParameterParserBuilder<PI, RB, P, O, PAR> optional() {
        curOptional = true;
        return this;
    }

    @Override
    public ParameterParserBuilder<PI, RB, P, O, PAR> supplyBlockDefault(P param, PI value) {
        if (!curOptional) {
            throw new IllegalStateException("Must be used within an optional block! Call optional() first!");
        }
        if (curBundle == null) {
            throw new IllegalStateException("You must have a parameter block defined before assigning defaults!");
        }
        curBundle.getBlockDefaults().put(param, value);
        return this;
    }

    @Override
    public AbstractParameterParserBuilder<PI, RB, P, O, PAR> helpMessage(Text helpMessage) {
        this.helpMessage = helpMessage;
        return this;
    }

    @Override
    public AbstractParameterParserBuilder<PI, RB, P, O, PAR> delimiter(String delimiter) {
        if (delimiter == null) throw new IllegalArgumentException("Delimiter cannot be null!");

        return pushAllWithDelimiter(delimiter);
    }

    protected AbstractParameterParserBuilder<PI, RB, P, O, PAR> pushAllWithDelimiter(@Language("RegExp") @javax.annotation.Nullable String delimiter) {
        if (curBundle == null) {
            throw new IllegalStateException("Cannot call push with delimiter before setting the batch of parameters. Are there no parameters in the last block/at all?");
        }
        String lastDelimiter = getLastUsedDelimiter();
        int index;
        if (lastDelimiter != null) {
            index = (int) delimiters.stream().filter(del -> del.matches(lastDelimiter)).count();
        }
        else {
            index = 0;
        }

        StringRangeExtractor stringRangeExtractor = new StringRangeExtractor(lastDelimiter, delimiter, index, curOptional);
        dataMap.put(stringRangeExtractor, curBundle);
        if (delimiter != null) {
            delimiters.add(delimiter);
        }
        curBundle = null;
        curOptional = false;
        return this;
    }

    @Override
    public ParameterParserBuilder<PI, RB, P, O, PAR> supplyParameter(P parameter, PI value) {
        valueMap.put(parameter, value);
        return this;
    }

    @Override
    public ParameterParserBuilder<PI, RB, P, O, PAR> exception(String s, O value) {
        this.exceptionMap.put(s, value);
        return this;
    }

    @Override
    public PAR build() {
        pushAllWithDelimiter(null);
        return createParser();
    }

    protected abstract PAR createParser();

    private String getLastUsedDelimiter() {
        if (delimiters.size() == 0) {
            return null;
        }
        return delimiters.get(delimiters.size() - 1);
    }
}
