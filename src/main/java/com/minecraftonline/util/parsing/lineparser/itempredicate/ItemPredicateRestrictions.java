/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser.itempredicate;

import com.minecraftonline.legacy.ics.chips.world.entity.Planter;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicate;
import com.minecraftonline.util.parsing.lineparser.Restriction;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;

public class ItemPredicateRestrictions {

    interface ItemPredicateRestriction extends Restriction<ItemPredicate> {}

    public static final ItemPredicateRestriction PLANTABLES = new ItemPredicateRestriction() {
        @Override
        public void apply(ItemPredicate target) throws InvalidICException {
            ItemType itemType = target.getItemType();

            if (!Planter.itemBlockMap.containsKey(itemType)
                    && !(itemType == ItemTypes.DYE && target.getDamage() == 3)) {
                throw new InvalidICException("Invalid item filter specified. Must be a plant, and if its dye, must be damage value 3 (cocoa beans)!");
            }
        }

        @Override
        public String getDescription() {
            return "Plantable item";
        }
    };
}
