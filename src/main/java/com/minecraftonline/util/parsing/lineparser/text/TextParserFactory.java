/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser.text;

import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.Parser;
import com.minecraftonline.util.parsing.lineparser.AbstractLineParserFactoryBuilder;
import com.minecraftonline.util.parsing.lineparser.LineParserFactory;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

public class TextParserFactory extends LineParserFactory<Text, Parser<Text>> {

    protected TextParserFactory(AbstractLineParserFactoryBuilder.State<Text, Parser<Text>> state) {
        super(state);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends AbstractLineParserFactoryBuilder<Builder, Text, Parser<Text>> {

        @Override
        public TextParserFactory build() {
            return new TextParserFactory(state);
        }

        @Override
        protected Builder getThis() {
            return this;
        }
    }

    public static class FormattingCodeTextParser implements Parser<Text> {

        private final Text helpMessage;
        private final boolean allowEmpty;

        public FormattingCodeTextParser(Text helpMessage, boolean allowEmpty) {
            this.helpMessage = helpMessage;
            this.allowEmpty = allowEmpty;
        }

        @Override
        public @Nullable Text parse(String s) throws Parameter.ParsingException {
            Text text = TextSerializers.FORMATTING_CODE.deserialize(s);
            if (!allowEmpty && text.isEmpty()) {
                throw new Parameter.ParsingException("Cannot be empty!");
            }
            return text;
        }

        @Override
        public @Nullable Text getHelp() {
            return helpMessage;
        }
    }
}
