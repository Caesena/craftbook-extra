/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser.generic.enums;

import com.minecraftonline.util.CraftBookPermissions;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.lineparser.ParserFactory;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class EnumParserFactory<E extends Enum<E>> implements ParserFactory<E> {

    private static final int MAX_ENUM_DISPLAY_COUNT = 5;

    private final Class<E> enumClass;
    private final EnumSet<E> set;
    private final boolean whitelist;
    private final EnumSet<E> adminOnly;
    private final boolean caseInsensitive;
    private final E defaultValue;

    protected EnumParserFactory(Class<E> enumClass, EnumSet<E> set, boolean whitelist, EnumSet<E> adminOnly, boolean caseInsensitive, @Nullable E defaultValue) {
        this.enumClass = enumClass;
        this.set = set;
        this.whitelist = whitelist;
        this.adminOnly = adminOnly;
        this.caseInsensitive = caseInsensitive;
        this.defaultValue = defaultValue;
    }

    public static <E extends Enum<E>> Builder<E> builderFor(Class<E> enumClass) {
        return new Builder<>(enumClass);
    }

    public static class Builder<E extends Enum<E>> {

        private final Class<E> enumClass;
        private final EnumSet<E> set;
        private boolean whitelist = true;

        private final EnumSet<E> adminOnly;
        private E defaultValue = null;
        private boolean caseInsensitive = false;

        public Builder(Class<E> enumClass) {
            this.enumClass = enumClass;
            set = EnumSet.noneOf(enumClass);
            adminOnly = EnumSet.noneOf(enumClass);
        }

        @SafeVarargs
        public final Builder<E> whitelist(E... whitelistedEnums) {
            this.whitelist = true;
            this.set.addAll(Arrays.asList(whitelistedEnums));
            return this;
        }

        @SafeVarargs
        public final Builder<E> blacklist(E... blacklistedEnums) {
            this.whitelist = false;
            this.set.addAll(Arrays.asList(blacklistedEnums));
            return this;
        }

        public Builder<E> caseInsensitive() {
            return caseInsensitive(true);
        }

        public Builder<E> caseInsensitive(boolean caseInsensitive) {
            this.caseInsensitive = caseInsensitive;
            return this;
        }


        @SafeVarargs
        public final Builder<E> adminOnly(E... adminOnlyEnums) {
            adminOnly.addAll(Arrays.asList(adminOnlyEnums));
            return this;
        }

        public Builder<E> defaultValue(E defaultValue) {
            this.defaultValue = defaultValue;
            return this;
        }


        public EnumParserFactory<E> build() {
            return new EnumParserFactory<>(enumClass, set, whitelist, adminOnly, caseInsensitive, defaultValue);
        }
    }

    @Override
    public E parse(String line, @Nullable Subject subject, IC ic) throws Parameter.ParsingException {
        try {
            if (caseInsensitive) {
                line = line.toUpperCase();
            }
            E enumValue = Enum.valueOf(enumClass, line);
            if (set.contains(enumValue)) {
                return enumValue;
            }
            // Could be an admin only option.
            if (!adminOnly.contains(enumValue)
                    || (subject == null || !subject.hasPermission(CraftBookPermissions.UNLIMITED))
                    || !ic.getBlock().get(CraftBookKeys.PLACED_BY_ADMIN).orElse(false)) {
                throw new Parameter.ParsingException(makeInformativeErrorMessage());
            }

            return enumValue;
        } catch (IllegalArgumentException e) {
            throw new Parameter.ParsingException(makeInformativeErrorMessage());
        }
    }

    private String makeInformativeErrorMessage() {
        String msg = "Invalid option. Must be a " + enumClass.getSimpleName();
        if (whitelist && set.size() <= MAX_ENUM_DISPLAY_COUNT) {
            return msg + " (" + set.stream().map(i -> i.name().toUpperCase()).collect(Collectors.joining(", ")) + ")";
        }
        return msg;
    }

    @Override
    public E getDefaultValue() {
        return defaultValue;
    }

    @Override
    public Text getHelp() {
        Text.Builder helpBuilder = Text.builder(enumClass.getSimpleName());
        if (whitelist && set.size() <= MAX_ENUM_DISPLAY_COUNT) {
            helpBuilder.append(Text.of(" (" + set.stream().map(i -> i.name().toUpperCase()).collect(Collectors.joining(", ")) + ")"));
        }
        return helpBuilder.build();
    }

    public ParsingConfiguration.LineParser createBinder(IC ic, Consumer<E> binder) {
        return new ParsingConfiguration.LineParser() {
            @Override
            public void parse(String line, @Nullable Subject cause) throws Parameter.ParsingException {
                binder.accept(EnumParserFactory.this.parse(line, cause, ic));
            }

            @Override
            public Text getHelp() {
                return Text.of(enumClass.getSimpleName());
            }
        };
    }
}
