/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser;

import com.minecraftonline.util.parsing.Parser;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractLineParserFactoryBuilder<THIS, T, P extends Parser<T>> implements LineParserFactory.Builder<THIS, T, P> {

    public static class State<T, P> implements Cloneable {
        protected P parser;
        protected final List<Restriction<T>> restrictions = new ArrayList<>();
        protected final List<Limitation<T>> limitations = new ArrayList<>();
        protected T defaultValue;
        protected boolean hasDefault = false;

        public State() {}

        @Override
        protected State<T, P> clone() throws CloneNotSupportedException {
            State<T, P> state = new State<>();
            state.parser = this.parser;
            state.restrictions.addAll(restrictions);
            state.limitations.addAll(limitations);
            state.defaultValue = defaultValue;
            state.hasDefault = hasDefault;
            return state;
        }
    }

    protected State<T, P> state = new State<>();

    @Override
    public THIS parser(P rangeParser) {
        this.state.parser = rangeParser;
        return getThis();
    }

    @Override
    public THIS restriction(Restriction<T> restriction) {
        this.state.restrictions.add(restriction);
        return getThis();
    }

    @Override
    public THIS limitation(Limitation<T> limitation) {
        this.state.limitations.add(limitation);
        return getThis();
    }

    @Override
    public THIS withDefault(T defaultValue) {
        this.state.defaultValue = defaultValue;
        this.state.hasDefault = true;
        return getThis();
    }

    public abstract LineParserFactory<T, P> build();

    protected abstract THIS getThis();
}
