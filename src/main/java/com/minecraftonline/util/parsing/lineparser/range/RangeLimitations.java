/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser.range;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.util.parsing.lineparser.Limitation;
import com.minecraftonline.util.parsing.rangeparser.RelativeRange;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.service.permission.Subject;

import java.util.function.Supplier;

public class RangeLimitations {
    public interface RangeLimitation extends Limitation<RelativeRange> {}

    public static RangeLimitation limitRange(int max) {
        return new RangeLimitation() {
            @Override
            public RelativeRange apply(RelativeRange target, @Nullable Subject subject) throws InvalidICException {
                if (target.getRange() > max) {
                    return RelativeRange.builder().from(target).range(max).build();
                }
                return target;
            }

            @Override
            public String getDescription() {
                return "Limits the range to " + max;
            }
        };
    }

    public static RangeLimitation maxOffsetInSingleDirection(double maxOffset) {
        return new RangeLimitation() {
            @Override
            public RelativeRange apply(RelativeRange target, @Nullable Subject subject) throws InvalidICException {
                Vector3d vector3d = target.getXyzOffset();
                Vector3d absVec = vector3d.abs();
                if (absVec.getX() > maxOffset || absVec.getY() > maxOffset || absVec.getZ() > maxOffset) {
                    return RelativeRange.builder()
                            .offset(new Vector3d(
                                    limit(vector3d.getX(), maxOffset),
                                    limit(vector3d.getY(), maxOffset),
                                    limit(vector3d.getZ(), maxOffset)
                                    )
                            )
                            .build();
                }
                return target;
            }

            @Override
            public String getDescription() {
                return "Limits the max offset to " + maxOffset;
            }
        };
    }

    public static RangeLimitation maxWidthOrLength(Supplier<Double> maxSupplier) {
        return new RangeLimitation() {
            @Override
            public RelativeRange apply(RelativeRange target, @Nullable Subject subject) {
                double max = maxSupplier.get();
                RelativeRange.Builder newRange = null;
                if (target.getLength() > max) {
                    newRange = RelativeRange.builder().from(target).length(max);
                }
                if (target.getWidth() > max) {
                    if (newRange == null) {
                        return RelativeRange.builder().from(target).width(max).build();
                    }
                    newRange.width(max);
                }
                if (newRange != null) {
                    return newRange.build();
                }
                return target;
            }

            @Override
            public String getDescription() {
                return "Maximum width and length to " + maxSupplier.get() + " (Configurable)";
            }
        };
    }

    public static RangeLimitation maxWidth(Supplier<Double> maxSupplier) {
        return new RangeLimitation() {
            @Override
            public RelativeRange apply(RelativeRange target, @Nullable Subject subject) {
                double max = maxSupplier.get();
                if (target.getWidth() > max) {
                    return RelativeRange.builder().from(target).width(max).build();
                }
                return target;
            }

            @Override
            public String getDescription() {
                return "Maximum width " + maxSupplier.get() + " (Configurable)";
            }
        };
    }

    public static RangeLimitation maxLength(Supplier<Double> maxSupplier) {
        return new RangeLimitation() {
            @Override
            public RelativeRange apply(RelativeRange target, @Nullable Subject subject) {
                double max = maxSupplier.get();
                if (target.getLength() > max) {
                    return RelativeRange.builder().from(target).length(max).build();
                }
                return target;
            }

            @Override
            public String getDescription() {
                return "Maximum length to " + maxSupplier.get() + " (Configurable)";
            }
        };
    }

    public static RangeLimitation maxHeight(Supplier<Double> maxSupplier) {
        return new RangeLimitation() {
            @Override
            public RelativeRange apply(RelativeRange target, @Nullable Subject subject) {
                double max = maxSupplier.get();
                if (target.getHeight() > max) {
                    return RelativeRange.builder().from(target).width(max).build();
                }
                return target;
            }

            @Override
            public String getDescription() {
                return "Maximum height " + maxSupplier.get() + " (Configurable)";
            }
        };
    }

    private static double limit(double i, double max) {
        if (i < 0) {
            return Math.max(i, -max);
        }
        else {
            return Math.min(i, max);
        }
    }
}
