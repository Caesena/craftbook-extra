/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser.generic;

import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.lineparser.ParserFactory;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.intellij.lang.annotations.Language;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;

import java.util.function.BiConsumer;

public class SimpleDualParserFactory<A,B>  {

    private final ParserFactory<A> factoryA;
    private final ParserFactory<B> factoryB;
    private final String delimiter;
    private final boolean lastOptional;

    public SimpleDualParserFactory(
            ParserFactory<A> factoryA,
            ParserFactory<B> factoryB,
            @Language("RegExp") String delimiter,
            boolean lastOptional
    ) {
        this.factoryA = factoryA;
        this.factoryB = factoryB;
        this.delimiter = delimiter;
        this.lastOptional = lastOptional;
    }

    public SimpleDualParserFactory(
            ParserFactory<A> factoryA,
            ParserFactory<B> factoryB,
            @Language("RegExp") String delimiter
    ) {
        this(factoryA, factoryB, delimiter, false);
    }

    public ParsingConfiguration.LineParser createBinder(IC ic, BiConsumer<A,B> binder) {
        return new ParsingConfiguration.LineParser() {
            @Override
            public void parse(String line, @Nullable Subject cause) throws InvalidICException, Parameter.ParsingException {
                String[] split = line.split(delimiter, 2);
                final A a;
                final B b;
                if (split.length == 2) {
                    a = factoryA.parse(split[0], cause, ic);
                    b = factoryB.parse(split[1], cause, ic);
                }
                else if (lastOptional && split.length == 1) {
                    a = factoryA.parse(line, cause, ic);
                    b = factoryB.getDefaultValue();
                }
                else {
                    throw new Parameter.ParsingException("Not enough parameters! (Split by '" + delimiter + "') Expected " + (lastOptional ? "1 or 2" : "2"));
                }

                binder.accept(a, b);
            }

            @Override
            public Text getHelp() {
                Text help1 = factoryA.getHelp();
                Text help2 = factoryB.getHelp();

                return Text.builder().append(help1 == null ? Text.of("N/A") : help1)
                        .append(Text.of(delimiter))
                        .append(help2 == null ? Text.of("N/A") : help2)
                        .build();
            }
        };
    }
}
