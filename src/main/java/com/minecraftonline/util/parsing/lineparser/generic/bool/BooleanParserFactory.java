/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser.generic.bool;

import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.lineparser.ParserFactory;
import com.sk89q.craftbook.sponge.IC;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class BooleanParserFactory implements ParserFactory<Boolean> {

    private final Set<String> set;
    private final boolean negated;
    private final boolean defaultValue;
    private final boolean ignoreCase;
    @Nullable
    private final Text helpMessage;

    public BooleanParserFactory(Set<String> set, boolean negated, boolean defaultValue, boolean ignoreCase, @Nullable Text helpMessage) {
        this.set = set;
        this.negated = negated;
        this.defaultValue = defaultValue;
        this.ignoreCase = ignoreCase;
        this.helpMessage = helpMessage;
    }

    @Override
    public Boolean parse(String line, @Nullable Subject subject, IC ic) {
        if (line.isEmpty()) {
            return defaultValue;
        }
        if (!ignoreCase) {
            boolean a = set.contains(line);
            return a ^ negated;
        }
        for (String s : set) {
            if (line.equalsIgnoreCase(s)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean getDefaultValue() {
        return defaultValue;
    }

    @Override
    public Text getHelp() {
        if (helpMessage != null) {
            return helpMessage;
        }
        return Text.of("True/False - " + !negated + " if equal to any of: " + String.join(", ", set));
    }

    @Override
    public ParsingConfiguration.LineParser createBinder(IC ic, Consumer<Boolean> binder) {
        return new ParsingConfiguration.LineParser() {
            @Override
            public void parse(String line, @Nullable Subject cause) {
                binder.accept(BooleanParserFactory.this.parse(line, cause, ic));
            }

            @Override
            public Text getHelp() {
                return BooleanParserFactory.this.getHelp();
            }
        };
    }

    public static BooleanParserFactory.Builder builder() {
        return new BooleanParserFactory.Builder();
    }

    public static class Builder {

        private boolean negated = false;
        private boolean defaultValue = false;
        private boolean ignoreCase = false;
        private final Set<String> set = new HashSet<>();
        private Text helpMessage;

        public Builder negated() {
            negated = !negated;
            return this;
        }

        public Builder ignoreCase() {
            ignoreCase = true;
            return this;
        }

        public Builder defaultValue(boolean defaultValue) {
            this.defaultValue = defaultValue;
            return this;
        }

        public Builder string(String... s) {
            set.addAll(Arrays.asList(s));
            return this;
        }

        public Builder helpMessage(Text helpMessage) {
            this.helpMessage = helpMessage;
            return this;
        }

        public BooleanParserFactory build() {
            return new BooleanParserFactory(set, negated, defaultValue, ignoreCase, helpMessage);
        }
    }

}
