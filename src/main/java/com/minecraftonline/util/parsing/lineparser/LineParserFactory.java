/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser;

import com.google.common.collect.ImmutableList;
import com.minecraftonline.data.PlacedByAdminData;
import com.minecraftonline.util.CraftBookPermissions;
import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.Parser;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.util.ParsingConfiguration;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import javax.annotation.Nullable;
import java.util.List;
import java.util.function.Consumer;

public class LineParserFactory<T, P extends Parser<T>> implements ParserFactory<T> {

    private final P parser;
    private final List<Restriction<T>> restrictions;
    private final List<Limitation<T>> limitations;
    private final T defaultValue;
    private final boolean hasDefault;

    protected LineParserFactory(AbstractLineParserFactoryBuilder.State<T, P> state) {
        this.parser = state.parser;
        this.restrictions = state.restrictions;
        this.limitations = state.limitations;
        this.defaultValue = state.defaultValue;
        this.hasDefault = state.hasDefault;
    }

    public interface Builder<THIS, T, P extends Parser<T>> {

        THIS parser(P parser);

        THIS withDefault(T defaultValue);

        THIS restriction(Restriction<T> restriction);

        THIS limitation(Limitation<T> restriction);

        LineParserFactory<T, P> build();
    }

    public List<Limitation<T>> getLimitations() {
        return ImmutableList.copyOf(limitations);
    }

    public List<Restriction<T>> getRestrictions() {
        return ImmutableList.copyOf(restrictions);
    }

    public P getParser() {
        return parser;
    }

    @Override
    public T getDefaultValue() {
        return defaultValue;
    }

    @Override
    public T parse(String line, @Nullable Subject subject, IC ic) throws Parameter.ParsingException, InvalidICException {
        T value = parser.parse(line);
        if (value == null) {
            if (!hasDefault) {
                throw new Parameter.ParsingException("Must not be blank!");
            }
            return defaultValue;
        }
        for (Restriction<T> restriction : restrictions) {
            restriction.apply(value);
        }
        if (!limitations.isEmpty()) {
            for (Limitation<T> limitation : limitations) {
                T newValue = limitation.apply(value, null);
                if (newValue != value) {
                    if ((subject != null && subject.hasPermission(CraftBookPermissions.UNLIMITED))
                    || ic.getBlock().get(CraftBookKeys.PLACED_BY_ADMIN).orElse(false)) {
                        if (subject != null) {
                            subject.getCommandSource().ifPresent(src -> src.sendMessage(Text.of(TextColors.GREEN, "You use your admin powers to create a more powerful IC")));
                            ic.getBlock().offer(new PlacedByAdminData(true));
                            ic.setAdminAppearanceOnLoad();
                        }
                        break;
                    }
                    else if (subject != null) {
                        throw new InvalidICException(Text.of(TextColors.RED, "Exceeded limitation: " + limitation.getDescription()));
                    }
                    value = newValue; // Apply limitation.
                }
            }
        }
        return value;
    }

    @Override
    public Text getHelp() {
        return parser.getHelp();
    }

    @Override
    public ParsingConfiguration.LineParser createBinder(IC ic, Consumer<T> binder) {
        return new FactoryLineParser<T>(this, ic, binder);
    }

    public static class FactoryLineParser<T> implements ParsingConfiguration.LineParser {
        private final LineParserFactory<T, ?> factoryRef;
        private final IC ic;
        private final Consumer<T> binder;

        public FactoryLineParser(LineParserFactory<T, ?> factoryRef, IC ic, Consumer<T> binder) {
            this.factoryRef = factoryRef;
            this.ic = ic;
            this.binder = binder;
        }

        @Override
        public void parse(String line, @org.jetbrains.annotations.Nullable Subject cause) throws InvalidICException, Parameter.ParsingException {
            T range = factoryRef.parse(line, cause, ic);
            if (range == null) {
                range = factoryRef.defaultValue;
            }
            binder.accept(range);
        }

        @Override
        public Text getHelp() {
            return factoryRef.parser.getHelp();
        }

        public LineParserFactory<T, ?> getFactoryRef() {
            return factoryRef;
        }
    }
}
