/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser.itempredicate;

import com.minecraftonline.util.parsing.item.predicate.ItemPredicate;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicateParser;
import com.minecraftonline.util.parsing.lineparser.AbstractLineParserFactoryBuilder;
import com.minecraftonline.util.parsing.lineparser.LineParserFactory;

public class ItemPredicateParserFactory extends LineParserFactory<ItemPredicate, ItemPredicateParser> {

    protected ItemPredicateParserFactory(AbstractLineParserFactoryBuilder.State<ItemPredicate, ItemPredicateParser> state) {
        super(state);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends AbstractLineParserFactoryBuilder<Builder, ItemPredicate, ItemPredicateParser> {

        @Override
        public ItemPredicateParserFactory build() {
            return new ItemPredicateParserFactory(state);
        }

        @Override
        protected Builder getThis() {
            return this;
        }
    }
}
