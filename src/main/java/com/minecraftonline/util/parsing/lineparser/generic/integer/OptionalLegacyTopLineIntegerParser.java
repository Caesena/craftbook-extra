/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser.generic.integer;

import com.minecraftonline.util.parsing.Parameter;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.text.Text;

import java.util.function.Supplier;

public class OptionalLegacyTopLineIntegerParser extends OptionalIntegerParser {

    private final Supplier<String> icTitle;

    public OptionalLegacyTopLineIntegerParser(Text helpMessage, Supplier<String> icTitle) {
        super(helpMessage);
        this.icTitle = icTitle;
    }

    @Override
    public @Nullable Integer parse(String s) throws Parameter.ParsingException {
        try {
            return super.parse(s);
        } catch (Parameter.ParsingException e) {
            String title = icTitle.get();
            if (s.length() >= title.length()) {
                try {
                    return Integer.parseInt(s.substring(title.length()));
                } catch (NumberFormatException e2) {
                    throw e;
                }
            }
            throw e;
        }
    }
}
