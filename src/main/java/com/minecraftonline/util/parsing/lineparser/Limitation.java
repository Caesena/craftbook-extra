/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser;

import com.minecraftonline.util.CraftBookPermissions;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.spongepowered.api.service.permission.Subject;

import javax.annotation.Nullable;

/**
 * A limitation to a certain target.
 * Instead of always throwing InvalidICExceptions,
 * this limitation is encouraged to mostly return
 * the a copy of the given target, fitted into
 * the required parameters.
 * @param <T> Target type.
 *
 * @see Restriction
 */
public interface Limitation<T> {

    /**
     * Applies this limitation to a given target
     * @param target Target to apply this limitation to.
     * @param subject Subject responsible for this. May be null
     * @return Target, either a changed copy of the target argument
     *      or the target argument if the target does not need
     *      limiting in any way.
     * @throws InvalidICException If the target is beyond any reasonable
     *      limitation such that it is no longer a gameplay reason for
     *      restricting this, but rather a functionality reason.
     */
    T apply(T target, @Nullable Subject subject) throws InvalidICException;

    @Nullable
    default String getDescription() {
        return null;
    }
}
