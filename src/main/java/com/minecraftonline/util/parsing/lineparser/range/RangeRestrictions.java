/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser.range;

import com.minecraftonline.util.parsing.lineparser.Restriction;
import com.minecraftonline.util.parsing.rangeparser.RelativeRange;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class RangeRestrictions {

    public interface RangeRestriction extends Restriction<RelativeRange> {}

    public static RangeRestriction rangeRestriction(double minRange, double maxRange) {
        return new RangeRestriction() {
            @Override
            public void apply(RelativeRange target) throws InvalidICException {
                if (minRange > target.getRange() || target.getRange() > maxRange) throw new InvalidICException("Range must be between " + minRange + " and " + maxRange);
            }

            @Override
            public String getDescription() {
                return "Range between " + minRange + " to " + maxRange;
            }
        };
    }

    public static RangeRestriction minRange(double minRange) {
        return new RangeRestriction() {
            @Override
            public void apply(RelativeRange target) throws InvalidICException {
                if (minRange > target.getRange()) throw new InvalidICException("Range must be at least: " + minRange);
            }

            @Override
            public String getDescription() {
                return "Restricts the minimum range to " + minRange;
            }
        };
    }

    public static RangeRestriction maxRange(double maxRange) {
        return new RangeRestriction() {
            @Override
            public void apply(RelativeRange target) throws InvalidICException {
                if (target.getRange() > maxRange) throw new InvalidICException("Range must be at most: " + maxRange);
            }

            @Override
            public String getDescription() {
                return "Maximum range to " + maxRange;
            }
        };
    }

    public static RangeRestriction maxOffsetInSingleDirection(double maxOffset) {
        return new RangeRestriction() {
            @Override
            public void apply(RelativeRange target) throws InvalidICException {
                if (target.hasXYOrZOffsetLargerThan(maxOffset)) throw new InvalidICException("Max offset in a single direction is greater than " + maxOffset);
            }

            @Override
            public String getDescription() {
                return "Maximum XYZ offset to " + maxOffset;
            }
        };
    }

    public static RangeRestriction maxWidth(Supplier<Double> maxSupplier) {
        return new RangeRestriction() {
            @Override
            public void apply(RelativeRange target) throws InvalidICException {
                double max = maxSupplier.get();
                if (target.getWidth() > max) throw new InvalidICException("Max width was exceeded. Width must be max " + max);
            }

            @Override
            public String getDescription() {
                return "Maximum width " + maxSupplier.get();
            }
        };
    }

    public static RangeRestriction maxLength(Supplier<Double> maxSupplier) {
        return new RangeRestriction() {
            @Override
            public void apply(RelativeRange target) throws InvalidICException {
                double max = maxSupplier.get();
                if (target.getLength() > max) throw new InvalidICException("Max length was exceeded. Length must be max " + max);
            }

            @Override
            public String getDescription() {
                return "Maximum length " + maxSupplier.get();
            }
        };
    }

    public static RangeRestriction maxHeight(Supplier<Double> maxSupplier) {
        return new RangeRestriction() {
            @Override
            public void apply(RelativeRange target) throws InvalidICException {
                double max = maxSupplier.get();
                if (target.getHeight() > max) throw new InvalidICException("Max height was exceeded. Height must be max " + max);
            }

            @Override
            public String getDescription() {
                return "Maximum height " + maxSupplier.get();
            }
        };
    }

    public static RangeRestriction maxWidthOrLength(double max) {
        return new RangeRestriction() {
            @Override
            public void apply(RelativeRange target) throws InvalidICException {
                if (target.getLength() > max || target.getWidth() > max) throw new InvalidICException("Max width/length was exceeded. Width/Length must be max " + max);
            }

            @Override
            public String getDescription() {
                return "Maximum width and length to " + max;
            }
        };
    }

    public static RangeRestriction maxWidthOrLength(Supplier<Double> maxSupplier) {
        return new RangeRestriction() {
            @Override
            public void apply(RelativeRange target) throws InvalidICException {
                double max = maxSupplier.get();
                if (target.getLength() > max || target.getWidth() > max) throw new InvalidICException("Max width/length was exceeded. Width/Length must be max " + max);
            }

            @Override
            public String getDescription() {
                return "Maximum width and length to " + maxSupplier.get() + " (Configurable)";
            }
        };
    }

    public static RangeRestriction maxWidthOrHeight(Supplier<Double> maxSupplier) {
        return new RangeRestriction() {
            @Override
            public void apply(RelativeRange target) throws InvalidICException {
                double max = maxSupplier.get();
                if (target.getHeight() > max || target.getWidth() > max) throw new InvalidICException("Max width/height was exceeded. Width/Height must be max " + max);
            }

            @Override
            public String getDescription() {
                return "Maximum width and height to " + maxSupplier.get() + " (Configurable)";
            }
        };
    }

    public static RangeRestriction maxWidthLengthAndHeight(Supplier<Double> maxSupplier) {
        return new RangeRestriction() {
            @Override
            public void apply(RelativeRange target) throws InvalidICException {
                double max = maxSupplier.get();
                if (target.getLength() > max || target.getWidth() > max || target.getHeight() > max) throw new InvalidICException("Max width/length/height was exceeded. Width/Length/Height must be max " + max);
            }

            @Override
            public String getDescription() {
                return "Maximum width, length and height to " + maxSupplier.get() + " (Configurable)";
            }
        };
    }
}
