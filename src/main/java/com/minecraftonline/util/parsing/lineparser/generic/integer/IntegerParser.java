/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.lineparser.generic.integer;

import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.Parser;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.text.Text;

// Can't be bothered to make builders for something as simple as this.
public class IntegerParser implements Parser<Integer> {

    private final Text helpMessage;

    public IntegerParser(Text helpMessage) {
        this.helpMessage = helpMessage;
    }

    @Override
    public @Nullable Integer parse(String s) throws Parameter.ParsingException {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            throw new Parameter.ParsingException("Expected number, got '" + s + "'");
        }
    }

    @Override
    public @Nullable Text getHelp() {
        return helpMessage;
    }
}
