/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing;

import org.spongepowered.api.text.Text;

import javax.annotation.Nonnull;

public interface Parameter<I, B extends ParameterBuilder<?>> {

    class ParsingException extends Exception {
        private final Text text;
        public ParsingException(Text msg) {
            super(msg.toPlain());
            this.text = msg;
        }

        public ParsingException(String msg) {
            this(Text.of(msg));
        }

        public Text getText() {
            return text;
        }
    }

    /**
     * Applies a value to the given builder.
     * @param value Value to apply.
     * @param builder Builder to apply the value to
     */
    void apply(@Nonnull I value, @Nonnull B builder) throws ParsingException;
}
