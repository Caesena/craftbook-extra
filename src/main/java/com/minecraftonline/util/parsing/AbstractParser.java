/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing;

import com.minecraftonline.util.parsing.rangeparser.StringRangeExtractor;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;

import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractParser<O, PI, B extends ParameterBuilder<O>, P extends Parameter<PI, ? super B>> implements Parser<O> {

    private final Map<StringRangeExtractor, ParameterBundle<P, PI>> map;
    private final Map<P, PI> valueMap;
    private final Map<String, O> exceptionMap;
    private final Text helpMessage;

    public AbstractParser(Map<StringRangeExtractor, ParameterBundle<P, PI>> map, Map<P, PI> valueMap, Map<String, O> exceptionMap, @Nullable Text helpMessage) {
        this.map = map;
        this.valueMap = valueMap;
        this.exceptionMap = exceptionMap;
        this.helpMessage = helpMessage;
    }

    public abstract B createBuilder();

    public abstract PI readValue(String s) throws Parameter.ParsingException;

    @Nullable
    @Override
    public O parse(String s) throws Parameter.ParsingException {
        final O possibleException = exceptionMap.get(s);
        if (possibleException != null) {
            return possibleException;
        }
        boolean anyValue = false;
        B builder = createBuilder();
        int blockNum = 0;
        for (Map.Entry<StringRangeExtractor, ParameterBundle<P, PI>> entry : map.entrySet()) {
            blockNum++;
            String part;
            try {
                part = entry.getKey().extract(s);
            } catch (StringRangeExtractor.MissingParameterException e) {
                throw new Parameter.ParsingException("Missing non-optional parameter block " + entry.getValue().getParameters());
            }

            ParameterBundle<P, PI> bundle = entry.getValue();
            int rangeModifierAmt = bundle.getParameters().size();

            if (part == null) {
                // Apply block defaults
                for (Map.Entry<P, PI> entry2 : bundle.getBlockDefaults().entrySet()) {
                    entry2.getKey().apply(entry2.getValue(), builder);
                    anyValue = true;
                }
                if (bundle.isLastOptional() && rangeModifierAmt == 1 && bundle.getLastDefaultValue() != null) {
                    // Apply Default value if available.
                    try {
                        P param = bundle.getParameters().get(bundle.getParameters().size() - 1);
                        param.apply(bundle.getLastDefaultValue(), builder);
                        anyValue = true;
                    } catch (Parameter.ParsingException e) {
                        throw new IllegalStateException("Default value for parameter was unparsable!");
                    }
                }
                continue; // Optional, and didn't exist.
            }

            String[] rangeModifierParts;
            if (bundle.getDelimiter() != null) {
                rangeModifierParts = part.split(bundle.getDelimiter(), rangeModifierAmt);
            }
            else {
                rangeModifierParts = new String[] {part};
            }

            if (rangeModifierParts.length == 1 && rangeModifierParts[0].isEmpty()) {
                rangeModifierParts = new String[0];
                // Make it 0 length, because it pretty much is.
                // This way our default handling will handle it easily.
            }

            if (rangeModifierParts.length != rangeModifierAmt) {
                if (bundle.isLastOptional() && --rangeModifierAmt == rangeModifierParts.length) {
                    // Apply Default value if available.
                    if (bundle.getLastDefaultValue() != null) {
                        try {
                            P param = bundle.getParameters().get(bundle.getParameters().size() - 1);
                            param.apply(bundle.getLastDefaultValue(), builder);
                            anyValue = true;
                        } catch (Parameter.ParsingException e) {
                            throw new IllegalStateException("Default value for parameter was unparsable!");
                        }
                    }
                }
                else {
                    if (part.isEmpty()) {
                        throw new Parameter.ParsingException("Cannot be blank!");
                    }
                    throw new Parameter.ParsingException("Invalid. Not enough of the delimiter '" + bundle.getDelimiter() + "' provided in section: '" + part + "' (for parameters " + entry.getValue().getParameters() + ")");
                }
            }
            for (int i = 0; i < rangeModifierAmt; i++) {
                String str = rangeModifierParts[i];
                P param = bundle.getParameters().get(i);
                try {
                    PI value = readValue(str);
                    param.apply(value, builder);
                    anyValue = true;
                } catch (Parameter.ParsingException e) {
                    Text.Builder textBuilder = Text.builder("Error parsing parameter ");
                    Text params;
                    if (bundle.getDelimiter() != null) {
                        params = Text.of(bundle.getParameters().stream().map(Object::toString).collect(Collectors.joining(bundle.getDelimiter())));
                    }
                    else {
                        params = Text.of(bundle.getParameters().get(0));
                    }
                    Text txt = Text.builder(param.toString() + " ")
                            .color(TextColors.LIGHT_PURPLE)
                            .onHover(TextActions.showText(Text.joinWith(Text.NEW_LINE,
                                    Text.of(TextColors.GRAY, "In block " + i + " " , TextColors.AQUA, params),
                                    Text.of(TextColors.GRAY, "Parameter ", TextColors.AQUA, i)
                            )))
                            .build();
                    textBuilder.append(
                            txt
                    );
                    textBuilder.append(e.getText()).build();
                    throw new Parameter.ParsingException(textBuilder.build());
                }
            }
        }
        if (!anyValue) {
            return null;
        }
        for (Map.Entry<P, PI> entry : valueMap.entrySet()) {
            entry.getKey().apply(entry.getValue(), builder);
        }
        return builder.build();
    }

    @Nullable
    @Override
    public Text getHelp() {
        return helpMessage;
    }
}
