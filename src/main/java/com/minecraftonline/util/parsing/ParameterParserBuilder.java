/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing;

import org.intellij.lang.annotations.Language;
import org.spongepowered.api.text.Text;

import javax.annotation.Nullable;
import java.util.Collection;

public interface ParameterParserBuilder<PI, RB extends ParameterBuilder<O>, P extends Parameter<PI, ? super RB>, O, PAR extends Parser<O>> {
    /**
     * Adds a single parameter.
     * This cannot be proceeded by any other parameter adding methods,
     * {@link #delimiter(String)} must be used, or no other parameters added.
     *
     * @param parameter Parameter
     * @return This builder, for chaining.
     */
    ParameterParserBuilder<PI, RB, P, O, PAR> parameter(P parameter);

    /**
     * Adds multiple parameters at once, all split by the same
     * regex delimiter.
     *
     * For example delimiter ':' would mean parameters would be parsed like
     * P1:P2:P3 etc.
     *
     * @param delimiter Regex delimiter to put between the parameters.
     * @param parameters List of parameters, in order to be parsed.
     * @return This builder, for chaining.
     */
    ParameterParserBuilder<PI, RB, P, O, PAR> parameters(@Language("RegExp") String delimiter, Collection<P> parameters);

    /**
     * Adds a single optional parameter.
     * This may be used after a group of parameters were added with a delimiter.
     * For example {@link #parameters(String, Collection)}.
     *
     * This cannot be proceeded by any other parameter adding methods,
     * {@link #delimiter(String)} must be used, or no other parameters added.
     *
     * @param parameter optional Parameter to append
     * @return This builder, for chaining.
     */
    ParameterParserBuilder<PI, RB, P, O, PAR> optional(P parameter);

    /**
     * Same as {@link #optional(Parameter)}, but has the option for a
     * default value to be applied to the Result Builder <RB>, if the parameter is not present
     * @param parameter Parameter to add, and supply a default value for.
     * @param defaultValue Default value to use if not available. If null, no value is applied.
     * @return This builder, for chaining
     */
    ParameterParserBuilder<PI, RB, P, O, PAR> optional(P parameter, @Nullable PI defaultValue);

    /**
     * Makes the entire last "section" optional. This means anything
     * up to last call of {@link #delimiter(String)} is optional, or
     * if {@link #delimiter(String)} was never called, makes everything optional.
     *
     * Fully compatible with the use of {@link #optional(Parameter)} )} and {@link #optional(Parameter, Object)},
     * as these make different things optional.
     *
     * As an example, take P1:P2:P3/[P4:P5[:P6]]
     * Where being surrounded by square brackets means optional. This format could be built
     * by using {@link #parameters(String, Collection)} with P1, P2, P3 and delimiter ':'
     * then by using {@link #delimiter(String)} to create a new "block", with delimiter '/'
     * then using {@link #parameters(String, Collection)} with P4 and P5, delimiter ':',
     * then by using {@link #optional(Parameter)},
     * then using this method to make the last "block" optional.
     *
     * @return This builder, for chaining.
     */
    ParameterParserBuilder<PI, RB, P, O, PAR> optional();

    /**
     * For use in combination with a optional block ({@link #optional(Parameter, Object)}.
     * Must be called in an optional block, or will error.
     *
     * @param param Parameter to give a default value
     * @param value Value
     * @return This builder, for chaining.
     */
    ParameterParserBuilder<PI, RB, P, O, PAR> supplyBlockDefault(P param, PI value);

    /**
     * Adds a help message to assist users on how to correctly format their
     * input string.
     * @param helpMessage Help message.
     * @return This builder, for chaining.
     */
    ParameterParserBuilder<PI, RB, P, O, PAR> helpMessage(Text helpMessage);

    /**
     * Completes the current block after adding some parameters.
     * This must be followed by more parameters.
     * If you don't wish to add any more paramters, that means that
     * you have finished, so call {@link #build()}
     *
     * For example P1:P2/P3:P4 can be created by using
     * {@link #parameters(String, Collection)} with P1 and P2, delimiter '@',
     * then using this method, with delimiter '/' to split the blocks.
     *
     * @param delimiter Regex delimiter to separate the new block from the last one.
     * @return This builder, for chaining.
     */
    ParameterParserBuilder<PI, RB, P, O, PAR> delimiter(@Language("RegExp") String delimiter);

    /**
     * Supplies a constant, that will always be applied to the builder.
     *
     * @param parameter
     * @param value
     * @return
     */
    ParameterParserBuilder<PI, RB, P, O, PAR> supplyParameter(P parameter, PI value);

    /**
     * Adds an exception, such that if the input string is
     * equal to the string parameter, value is returned.
     * @param s String value to equal.
     * @param value Value to return if equal
     * @return This builder, for chaining.
     */
    ParameterParserBuilder<PI, RB, P, O, PAR> exception(final String s, O value);

    /**
     * Builds the current Parameters into a parser.
     * At least 1 parameter must be provided in the last block.
     * Therefore if {@link #delimiter(String)} is called, it must be followed by
     * parameters, or this will throw an exception.
     * Additionally, if no parameters are ever specified, an exception will also be thrown.
     *
     * @return A built parsing, using all the information from the builder.
     */
    PAR build();
}
