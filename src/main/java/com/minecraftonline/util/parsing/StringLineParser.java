/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing;

import com.minecraftonline.util.ParsingConfiguration;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;

import java.util.function.Consumer;

public class StringLineParser implements ParsingConfiguration.LineParser {

    @Nullable
    private final Text helpMessage;
    private final Consumer<String> binder;
    private final boolean allowEmpty;

    public StringLineParser(@Nullable Text helpMessage, Consumer<String> binder, boolean allowEmpty) {
        this.helpMessage = helpMessage;
        this.binder = binder;
        this.allowEmpty = allowEmpty;
    }

    public static StringLineParser of(Consumer<String> binder) {
        return new StringLineParser(null, binder, true);
    }

    public static StringLineParser of(Text text, Consumer<String> binder) {
        return new StringLineParser(text, binder, true);
    }

    public static StringLineParser ofNotBlank(Text text, Consumer<String> binder) {
        return new StringLineParser(text, binder, false);
    }

    @Override
    public void parse(String line, @Nullable Subject cause) throws InvalidICException, Parameter.ParsingException {
        if (line.isEmpty() && !allowEmpty) {
            throw new Parameter.ParsingException("Cannot be blank!");
        }
        binder.accept(line);
    }

    @Override
    public Text getHelp() {
        return helpMessage;
    }
}
