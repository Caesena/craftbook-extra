/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.item.predicate;

import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.parsing.item.AbstractItemBuilder;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.function.Predicate;

public class ItemPredicate implements Predicate<ItemStack> {

    private final ItemType itemType;
    private final Integer damage;
    private final boolean inverted;

    public ItemPredicate(@Nullable ItemType itemType, @Nullable Integer damage, boolean inverted) {
        this.itemType = itemType;
        this.damage = damage;
        this.inverted = inverted;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final ItemPredicate ANY = new ItemPredicate(null, null, false);

    @Override
    public boolean test(ItemStack itemStack) {
        return (matchesOrNull(itemType, itemStack.getType())
                && matchesOrNull(damage, IDUtil.getDamageFromItemStack(itemStack))) ^ inverted;
    }

    public boolean test(Item item) {
        return (matchesOrNull(itemType, item.getItemType())
                && matchesOrNull(damage, IDUtil.getDamagefromItem(item))) ^ inverted;
    }

    private static boolean matchesOrNull(@Nullable Object our, @Nonnull Object other) {
        return our == null || other.equals(our);
    }

    public ItemType getItemType() {
        return itemType;
    }

    public Integer getDamage() {
        return damage;
    }

    public static class Builder extends AbstractItemBuilder<Builder, ItemPredicate> {

        private boolean inverted = false;

        public Builder negated() {
            inverted = true;
            return this;
        }

        @Override
        public ItemPredicate build() {
            return new ItemPredicate(
                    itemType,
                    damage,
                    inverted
            );
        }

        @Override
        protected Builder getThis() {
            return this;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ItemPredicate)) return false;
        ItemPredicate that = (ItemPredicate) o;
        return Objects.equals(itemType, that.itemType) && Objects.equals(damage, that.damage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemType, damage);
    }

    @Override
    public String toString() {
        return "ItemPredicate{" +
                "itemType=" + itemType +
                ", damage=" + damage +
                '}';
    }
}
