/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.item;

import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.parsing.Parameter;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.api.item.ItemType;

public enum ItemParameters implements Parameter<String, AbstractItemBuilder<?, ?>> {

    ITEM_TYPE {
        @Override
        public void apply(@NotNull String value, @NotNull AbstractItemBuilder<?, ?> builder) throws ParsingException {
            final ItemType itemType = IDUtil.getItemTypeFromAnyID(value)
                    .orElseThrow(() -> new ParsingException("Invalid item type string: " + value));
            builder.itemType(itemType);
        }
    },

    DAMAGE {
        @Override
        public void apply(@NotNull String value, @NotNull AbstractItemBuilder<?, ?> builder) throws ParsingException {
            try {
                int dmg = Integer.parseInt(value);
                builder.damage(dmg);
            } catch (NumberFormatException e) {
                throw new ParsingException("Damage must be a number, was: " + value);
            }
        }
    },

    ;
}
