/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.vector3i;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.Parser;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.text.Text;

public class XYZOffsetParser implements Parser<Vector3i> {

    private final String[] INT_X_Y_Z_TRANSLATOR = {"X", "Y", "Z"};

    private final boolean allowBlank;

    public XYZOffsetParser(boolean allowBlank) {
        this.allowBlank = allowBlank;
    }

    @Override
    public @Nullable Vector3i parse(String s) throws Parameter.ParsingException {
        if (s.isEmpty()) {
            if (allowBlank) {
                return null;
            }
            throw new Parameter.ParsingException("X:Y:Z Offset cannot be blank!");
        }
        String[] splitString = s.split(":", 3);
        if (splitString.length != 3) {
            throw new Parameter.ParsingException("Not enough colons in X:Y:Z Offset");
        }
        int i = 0;
        int x,y,z = 0;
        try {
            x = Integer.parseInt(splitString[i++]);
            y = Integer.parseInt(splitString[i++]);
            z = Integer.parseInt(splitString[i]);
        } catch (NumberFormatException e) {
            throw new Parameter.ParsingException("The " + INT_X_Y_Z_TRANSLATOR[i] + " offset provided was not a number '" + splitString[i]);
        }
        return Vector3i.from(x,y,z);
    }

    @Override
    public @Nullable Text getHelp() {
        return Text.of("X:Y:Z Offset");
    }
}
