/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.vector3i;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.util.parsing.Parameter;
import com.minecraftonline.util.parsing.Parser;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.text.Text;

public class BasicOffsetParser implements Parser<Vector3i> {

    private BasicOffsetParser() {}

    public static final BasicOffsetParser INSTANCE = new BasicOffsetParser();

    @Override
    public @Nullable Vector3i parse(String s) throws Parameter.ParsingException {
        if (s.length() < 3) {
            throw new Parameter.ParsingException("Offset must be at least 3 characters e.g (X+5)");
        }
        final Vector3i offsetDir;
        char xyz = s.charAt(0);
        switch (xyz) {
            case 'x':
            case 'X': {
                offsetDir = Vector3i.UNIT_X;
                break;
            }
            case 'y':
            case 'Y': {
                offsetDir = Vector3i.UNIT_Y;
                break;
            }
            case 'z':
            case 'Z': {
                offsetDir = Vector3i.UNIT_Z;
                break;
            }
            default: throw new Parameter.ParsingException("Invalid first character, should have been x, y or z, was '" + xyz + "'");
        }

        char plusMinus = s.charAt(1);

        if (plusMinus != '+' && plusMinus != '-') {
            throw new Parameter.ParsingException("Invalid second character, must be plus or minus, was '" + plusMinus + "'");
        }

        String amountStr = s.substring(1);
        final int amt;
        try {
            amt = Integer.parseInt(amountStr);
        } catch (NumberFormatException e) {
            throw new Parameter.ParsingException("Expected number, got '" + amountStr + "'");
        }
        return offsetDir.mul(amt);
    }

    @Override
    public @Nullable Text getHelp() {
        return Text.of("(X|Y|Z)(+|-)Distance");
    }
}
