/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.vector3i;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.util.parsing.lineparser.Limitation;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.service.permission.Subject;

public class Vector3iLimitations {
    interface Vector3iLimitation extends Limitation<Vector3i> {}

    public static Vector3iLimitation maxInSingleDirection(int max) {
        return new Vector3iLimitation() {
            @Override
            public Vector3i apply(Vector3i target, @Nullable Subject subject) {
                if (Math.abs(target.getX()) > max
                || Math.abs(target.getY()) > max
                || Math.abs(target.getZ()) > max) {
                    return new Vector3i(
                            limit(target.getX(), max),
                            limit(target.getY(), max),
                            limit(target.getZ(), max)
                    );
                }
                return target;
            }

            @Override
            public String getDescription() {
                return "Max offset of " + max + " in any direction";
            }
        };
    }

    private static int limit(int i, int max) {
        if (i < 0) {
            return Math.max(i, -max);
        }
        else {
            return Math.min(i, max);
        }
    }
}
