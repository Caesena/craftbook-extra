/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.vector3i;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.util.parsing.lineparser.Restriction;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.jetbrains.annotations.Nullable;

public class Vector3iRestrictions {

    interface Vector3iRestriction extends Restriction<Vector3i> {}

    public static Vector3iRestriction maxXZOffset(int max) {
        return new Vector3iRestriction() {
            @Override
            public void apply(Vector3i target) throws InvalidICException {
                if (Math.abs(target.getX()) > max) {
                    throw new InvalidICException("X offset distance must be less than " + max);
                }
                if (Math.abs(target.getZ()) > max) {
                    throw new InvalidICException("Z offset distance must be less than " + max);
                }
            }

            @Override
            public String getDescription() {
                return "Max X/Z offset of " + max;
            }
        };
    }

    public static Vector3iRestriction maxYOffset(int max) {
        return new Vector3iRestriction() {
            @Override
            public void apply(Vector3i target) throws InvalidICException {
                if (Math.abs(target.getY()) > max) {
                    throw new InvalidICException("Y offset distance must be less than " + max);
                }
            }

            @Override
            public String getDescription() {
                return "Max Y offset of +/- " + max;
            }
        };
    }
}
