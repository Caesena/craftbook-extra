/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.rangeparser;

import com.minecraftonline.util.parsing.Parameter;
import org.spongepowered.api.text.Text;

import java.util.Arrays;

public class RangeParsers {

    public static class InvalidRangeException extends Exception {
        public InvalidRangeException(String msg) {
            super(msg);
        }
    }

    // CBWARP
    public static final RangeParser CBWARP_AREA = RangeParser.builder()
            .parameters(":", Arrays.asList(RangeParameters.HEIGHT, RangeParameters.WIDTH, RangeParameters.LENGTH))
            .delimiter("/")
            .parameters(":", Arrays.asList(RangeParameters.X_OFFSET, RangeParameters.Y_OFFSET, RangeParameters.Z_OFFSET))
            .optional()
            .supplyBlockDefault(RangeParameters.Y_OFFSET, 1.0)
            .helpMessage(Text.of("H:W:L[/X:Y:Z offset]"))
            .build();

    // Bridge
    public static final RangeParser WIDTH_LENGTH_AND_Y_OFFSET = RangeParser.builder()
            .parameters(":", Arrays.asList(RangeParameters.WIDTH, RangeParameters.LENGTH))
            .optional(RangeParameters.Y_OFFSET)
            .supplyParameter(RangeParameters.HEIGHT, 1.0)
            .helpMessage(Text.of("W:L[:Y Offset]"))
            .build();

    // Door
    public static final RangeParser WIDTH_HEIGHT_AND_Y_OFFSET = RangeParser.builder()
            .parameters(":", Arrays.asList(RangeParameters.WIDTH, RangeParameters.HEIGHT))
            .optional(RangeParameters.Y_OFFSET)
            .supplyParameter(RangeParameters.LENGTH, 1.0)
            .helpMessage(Text.of("W:H[Y Offset]"))
            .build();

    public static final RangeParser RANGE_AND_OFFSET = RangeParser.builder()
            .optional(RangeParameters.RANGE, 8.0D)
            .delimiter(":")
            .parameters(":", Arrays.asList(RangeParameters.X_OFFSET, RangeParameters.Y_OFFSET, RangeParameters.Z_OFFSET))
            .optional()
            .helpMessage(Text.of("[Range:[X:Y:Z]]"))
            .build();

    public static final RangeParser OPTIONAL_Y_OFFSET = RangeParser.builder()
            .optional(RangeParameters.Y_OFFSET)
            .helpMessage(Text.of("[yOffset]"))
            .build();

    // Area Planter
    public static final RangeParser WIDTH_LENGTH_AND_DEFAULT_1_Y_OFFSET = RangeParser.builder()
            .parameters(":", Arrays.asList(RangeParameters.WIDTH, RangeParameters.LENGTH))
            .optional(RangeParameters.Y_OFFSET, 1.0D)
            .supplyParameter(RangeParameters.HEIGHT, 1.0D)
            .helpMessage(Text.of("width:length[:YOffset (default 1)]"))
            .build();

    public static final RangeParser X_Y_Z_OFFSET = RangeParser.builder()
            .parameters(":", Arrays.asList(RangeParameters.X_OFFSET, RangeParameters.Y_OFFSET, RangeParameters.Z_OFFSET))
            .optional()
            .helpMessage(Text.of("[X:Y:Z Offset]"))
            .build();

    public static final RangeParser OPTIONAL_X_OPTIONAL_Y_OPTIONAL_Z_OFFSET = RangeParser.builder()
            .optional(RangeParameters.X_OFFSET)
            .delimiter(":")
            .optional(RangeParameters.Y_OFFSET)
            .delimiter(":")
            .optional(RangeParameters.Z_OFFSET)
            .helpMessage(Text.of("[X:Y:Z Offset]"))
            .build();

    // Harvester

    public static final RangeParser WIDTH_LENGTH_HEIGHT_Y_OFFSET = RangeParser.builder()
            .parameters(":", Arrays.asList(RangeParameters.WIDTH, RangeParameters.LENGTH, RangeParameters.HEIGHT))
            .delimiter("/")
            .optional(RangeParameters.Y_OFFSET, 1.0)
            .helpMessage(Text.of("Width:Length:Height[/Y Offset]"))
            .build();
}
