/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.rangeparser;

import com.minecraftonline.util.parsing.Parameter;

import javax.annotation.Nullable;

public class StringRangeExtractor {
    private final String start;
    private final String end;
    private final int index;
    private final boolean optional;

    public StringRangeExtractor(String start, String end, int index, boolean optional) {
        this.start = start;
        this.end = end;
        this.index = index;
        this.optional = optional;
    }

    public static class MissingParameterException extends Exception {
        public MissingParameterException(String msg) {
            super(msg);
        }
    }

    @Nullable
    public String extract(String s) throws MissingParameterException {
        if (s.isEmpty()) {
            if (optional) {
                return null;
            }
            throw new MissingParameterException("Missing parameter that was not optional.");
        }
        if (start != null) {
            String[] split = s.split(start, index + 1);
            if (split.length <= index) {
                if (optional) {
                    return null;
                }
                throw new MissingParameterException("Missing parameter that was not optional.");
            }
            s = split[index];
        }
        if (end != null) {
            s = s.split(end, 2)[0];
        }
        return s;
    }
}
