/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.rangeparser;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.util.parsing.ParameterBuilder;
import org.spongepowered.api.util.Direction;

import java.util.Objects;

public class RelativeRange {
    private final double width;
    private final double length;
    private final double height;

    private final double range;

    private final double widthOffset;
    private final double lengthOffset;

    private final Vector3d xyzOffset;

    private RelativeRange(double width, double length, double height, double range,
                          double widthOffset, double lengthOffset,
                          Vector3d xyzOffset) {
        this.width = width;
        this.length = length;
        this.height = height;

        this.range = range;

        this.widthOffset = widthOffset;
        this.lengthOffset = lengthOffset;

        this.xyzOffset = xyzOffset;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements ParameterBuilder<RelativeRange> {
        private double width = 0;
        private double length = 0;
        private double height = 0;

        private double range = 0;

        private double widthOffset = 0;
        private double lengthOffset = 0;

        private Vector3d nonRelativeOffset = Vector3d.ZERO;

        private Builder() {}

        public Builder from(RelativeRange range) {
            this.width = range.getWidth();
            this.length = range.getLength();
            this.height = range.getHeight();

            this.range = range.getRange();

            this.widthOffset = range.getWidthOffset();
            this.lengthOffset = range.getLengthOffset();

            this.nonRelativeOffset = range.getXyzOffset();

            return this;
        }

        public Builder width(final double width) {
            this.width = width;
            return this;
        }

        public Builder length(final double length) {
            this.length = length;
            return this;
        }

        public Builder height(final double height) {
            this.height = height;
            return this;
        }

        public Builder range(double range) {
            this.range = range;
            return this;
        }

        public Builder widthOffset(final double widthOffset) {
            this.widthOffset = widthOffset;
            return this;
        }

        public Builder lengthOffset(final double lengthOffset) {
            this.lengthOffset = lengthOffset;
            return this;
        }

        public Builder xOffset(final double xOffset) {
            this.nonRelativeOffset = Vector3d.from(xOffset, nonRelativeOffset.getY(), nonRelativeOffset.getZ());
            return this;
        }

        public Builder yOffset(final double yOffset) {
            this.nonRelativeOffset = Vector3d.from(nonRelativeOffset.getX(), yOffset, nonRelativeOffset.getZ());
            return this;
        }

        public Builder zOffset(final double zOffset) {
            this.nonRelativeOffset = Vector3d.from(nonRelativeOffset.getX(), nonRelativeOffset.getY(), zOffset);
            return this;
        }

        public Builder offset(final Vector3d offset) {
            this.nonRelativeOffset = offset;
            return this;
        }

        public RelativeRange build() {
            return new RelativeRange(
                    width, length, height, range,
                    widthOffset, lengthOffset,
                    nonRelativeOffset);
        }
    }

    public Vector3d getStart(Vector3d pos, Direction facing) {
        Vector3d lengthDirection = facing.asBlockOffset().toDouble();
        Vector3d widthDirection = new Vector3i(lengthDirection.getZ(), 0, lengthDirection.getX()).toDouble();

        return pos.add(widthDirection.negate().mul(width / 2))
                .add(widthDirection.mul(widthOffset))
                .add(lengthDirection.mul(lengthOffset))
                .add(xyzOffset);
    }

    public Vector3d getEnd(Vector3d pos, Direction facing) {
        Vector3d lengthDirection = facing.asBlockOffset().toDouble();
        Vector3d widthDirection = new Vector3i(lengthDirection.getZ(), 0, lengthDirection.getX()).toDouble();

        return pos.add(widthDirection.mul(width / 2))
                .add(lengthDirection.mul(lengthOffset + length))
                .add(Vector3d.UP.mul(height))
                .add(xyzOffset);
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }

    public double getRange() {
        return range;
    }

    public double getLengthOffset() {
        return lengthOffset;
    }

    public double getWidthOffset() {
        return widthOffset;
    }

    public Vector3d getXyzOffset() {
        return xyzOffset;
    }

    public boolean hasXYOrZOffsetLargerThan(double max) {
        Vector3d absolute = xyzOffset.abs();
        return absolute.getX() > max
                || absolute.getY() > max
                || absolute.getZ() > max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RelativeRange)) return false;
        RelativeRange that = (RelativeRange) o;
        return Double.compare(that.width, width) == 0 && Double.compare(that.length, length) == 0 && Double.compare(that.height, height) == 0 && Double.compare(that.widthOffset, widthOffset) == 0 && Double.compare(that.lengthOffset, lengthOffset) == 0 && xyzOffset.equals(that.xyzOffset);
    }

    @Override
    public int hashCode() {
        return Objects.hash(width, length, height, widthOffset, lengthOffset, xyzOffset);
    }

    @Override
    public String toString() {
        return "RelativeRange{" +
                "width=" + width +
                ", length=" + length +
                ", height=" + height +
                ", widthOffset=" + widthOffset +
                ", lengthOffset=" + lengthOffset +
                ", xyzOffset=" + xyzOffset +
                '}';
    }
}
