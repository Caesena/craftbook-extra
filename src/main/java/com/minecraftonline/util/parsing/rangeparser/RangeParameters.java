/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.rangeparser;

import com.minecraftonline.util.parsing.Parameter;
import org.jetbrains.annotations.NotNull;

import java.util.function.BiConsumer;

public enum RangeParameters implements Parameter<Double, RelativeRange.Builder> {
    WIDTH((width, builder) -> builder.width(width)),
    HEIGHT((height, builder) -> builder.height(height)),
    LENGTH((length, builder) -> builder.length(length)),

    RANGE((range, builder) -> builder.range(range)),

    X_OFFSET((offset, builder) -> builder.xOffset(offset)),
    Y_OFFSET((offset, builder) -> builder.yOffset(offset)),
    Z_OFFSET((offset, builder) -> builder.zOffset(offset)),

    WIDTH_OFFSET((offset, builder) -> builder.widthOffset(offset)),
    LENGTH_OFFSET((offset, builder) -> builder.lengthOffset(offset)),
    ;

    private final BiConsumer<Double, RelativeRange.Builder> modifier;

    RangeParameters(BiConsumer<Double, RelativeRange.Builder> modifier) {
        this.modifier = modifier;
    }

    @Override
    public void apply(@NotNull Double value, RelativeRange.@NotNull Builder builder) {
        modifier.accept(value, builder);
    }
}
