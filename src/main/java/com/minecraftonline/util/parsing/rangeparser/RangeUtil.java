/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.rangeparser;

import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Iterator;

public class RangeUtil {
    public static Iterator<Location<World>> iterateBlocks(World world, Vector3i start, Vector3i end) {
        Vector3i min = start.min(end);
        Vector3i max = start.max(end);

        return new Iterator<Location<World>>() {

            private int x = min.getX();
            private int y = min.getY();
            private int z = min.getZ();

            private boolean finished = false;

            @Override
            public boolean hasNext() {
                return !finished;
            }

            @Override
            public Location<World> next() {
                if (!hasNext()) {
                    throw new IndexOutOfBoundsException();
                }
                Location<World> loc = world.getLocation(x, y, z);
                x++;
                if (x > max.getX()) {
                    x = min.getX();
                    z++;
                    if (z > max.getZ()) {
                        z = min.getZ();
                        y++;
                        if (y > max.getY()) {
                            finished = true;
                        }
                    }
                }

                return loc;
            }
        };
    }
}
