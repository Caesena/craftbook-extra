/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing;

import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParameterBundle<P, V> {
    @Nullable
    private final String delimiter;
    private final List<P> parameters;
    private final boolean lastOptional;
    private final V lastDefaultValue;
    private final Map<P, V> blockDefaults = new HashMap<>(); // The default value for all parameters if this bundle is wholly optionally.

    public ParameterBundle(@Nullable String delimiter, List<P> parameters, boolean lastOptional, V lastDefaultValue) {
        this.delimiter = delimiter;
        this.parameters = parameters;
        this.lastOptional = lastOptional;
        this.lastDefaultValue = lastDefaultValue;
    }

    public ParameterBundle(List<P> parameters) {
        this(null, parameters);
    }

    public ParameterBundle(@Nullable String delimiter, List<P> parameters) {
        this(delimiter, parameters, false, null);
    }

    public List<P> getParameters() {
        return parameters;
    }

    @Nullable
    public String getDelimiter() {
        return delimiter;
    }

    public boolean isLastOptional() {
        return lastOptional;
    }

    public V getLastDefaultValue() {
        return lastDefaultValue;
    }

    public Map<P, V> getBlockDefaults() {
        return blockDefaults;
    }
}
