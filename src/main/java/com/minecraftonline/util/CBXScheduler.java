/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

public class CBXScheduler {
    private final ExecutorService pool;
    private final ExecutorService singleThreadPool;

    private final BlockingQueue<Runnable> syncQueue = new LinkedBlockingQueue<>();
    private List<Runnable> batch = createBatchList();

    public CBXScheduler() {
        final int hardwareThreads = Runtime.getRuntime().availableProcessors();
        int poolThreadCount = Math.max(4, Math.min(2, hardwareThreads / 2));
        pool = Executors.newFixedThreadPool(poolThreadCount);
        this.singleThreadPool = Executors.newSingleThreadExecutor();
    }

    public void scheduleAsync(Runnable runnable) {
        pool.execute(runnable);
    }

    public void scheduleAsync(Collection<Runnable> runnables) {
        pool.execute(() -> runnables.forEach(Runnable::run));
    }

    public Future<?> submitAsync(Runnable runnable) {
        return pool.submit(runnable);
    }

    /**
     * Submit something that should be completed eventually, but is important
     * that multiple of the task are not running at the same time, e.g reading
     * then writing to a file.
     * @param runnable Runnable to run.
     * @return Future for the task.
     */
    public Future<?> submitAsyncSingleThread(Runnable runnable) {
        return this.singleThreadPool.submit(runnable);
    }

    public void scheduleAsyncBatch(Runnable runnable) {
        batch.add(runnable);
    }

    public void executeBatch() {
        if (!batch.isEmpty()) {
            List<Runnable> toRun = batch;
            pool.execute(() -> toRun.forEach(Runnable::run));
            batch = createBatchList();
        }
    }

    public static List<Runnable> createBatchList() {
        return new LinkedList<>();
    }


    public void scheduleSync(Runnable runnable) {
        syncQueue.add(runnable);
    }

    public void tickSync() {
        executeBatch();
        List<Runnable> runnables = new ArrayList<>(syncQueue.size());
        syncQueue.drainTo(runnables);
        runnables.forEach(Runnable::run);
    }
}
