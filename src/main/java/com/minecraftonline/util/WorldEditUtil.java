/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.regions.selector.CuboidRegionSelector;
import com.sk89q.worldedit.regions.selector.SphereRegionSelector;
import com.sk89q.worldedit.sponge.SpongeWorldEdit;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class WorldEditUtil {

    public static Vector toWEVector(Vector3d vec) {
        return new Vector(vec.getX(), vec.getY(), vec.getZ());
    }

    public static Vector toWEVector(Vector3i vec) {
        return new Vector(vec.getX(), vec.getY(), vec.getZ());
    }

    public static CuboidRegionSelector toCuboidSelection(World world, Vector3d min, Vector3d max, boolean endExclusive) {
        if (endExclusive) {
            max = Vector3d.from(max.getX() - 1, max.getY() - 1, max.getZ() -1);
        }
        return toCuboidSelection(world, min, max);
    }

    public static CuboidRegionSelector toCuboidSelection(World world, Vector3d min, Vector3d max) {
        return new CuboidRegionSelector(
                SpongeWorldEdit.inst().getWorld(world),
                toWEVector(min),
                toWEVector(max)
        );
    }

    public static CuboidRegionSelector toCuboidFromPoint(Location<World> loc) {
        Vector weVector = toWEVector(loc.getBlockPosition());
        return new CuboidRegionSelector(
                SpongeWorldEdit.inst().getWorld(loc.getExtent()),
                weVector,
                weVector
        );
    }

    public static SphereRegionSelector toSphereSelection(World world, Vector3d centre, int radius) {
        return new SphereRegionSelector(
                SpongeWorldEdit.inst().getWorld(world),
                toWEVector(centre),
                radius
        );
    }
}
