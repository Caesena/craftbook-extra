/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.minecraftonline.legacy.ThreadSafeReadClassInheritanceMultiMap;
import net.minecraft.util.ClassInheritanceMultiMap;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.api.Platform;
import org.spongepowered.common.SpongeImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReadThreadSafeClassInheritanceMultimap<T> extends ClassInheritanceMultiMap<T> implements ThreadSafeReadClassInheritanceMultiMap {

    private transient final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final Platform.Type platformExecutionType;

    public ReadThreadSafeClassInheritanceMultimap(Class<T> baseClassIn) {
        super(baseClassIn);
        this.platformExecutionType = SpongeImpl.getGame().getPlatform().getExecutionType();
    }

    /* ************************************ *\
     *       NOW PERMITTED OPERATIONS       *
    \* ************************************ */

    @Override
    protected void createLookup(Class<?> clazz) {
        // This is called by constructor, so it may not have been initialized yet..
        // (In which case, only the constructor has access, so we don't need to lock it)
        if (lock != null) {
            lock.writeLock().lock();
        }
        try {
            ALL_KNOWN.add(clazz);

            for (T t : this.values)
            {
                if (clazz.isAssignableFrom(t.getClass()))
                {
                    this.addForClassAsync(t, clazz);
                }
            }

            this.knownKeys.add(clazz);
        } finally {
            if (lock != null) {
                lock.writeLock().unlock();
            }
        }
    }

    /**
     * Avoid Sponge's concurrent check
     * @param value Value
     * @param parentClass Parent Class
     */
    private void addForClassAsync(T value, Class<?> parentClass) {
        List<T> list = this.map.get(parentClass);

        if (list == null)
        {
            this.map.put(parentClass, Lists.newArrayList(value));
        }
        else
        {
            list.add(value);
        }
    }


    @Override
    public <S> List<S> getCopyByClass(Class<S> clazz) {
        try {
            lock.readLock().lock();
            List<S> list =  (List<S>) map.get(initializeClassLookupWhileReadLocked(clazz));
            if (list == null) {
                return Collections.EMPTY_LIST;
            }
            return new ArrayList<>(list);
        } finally {
            lock.readLock().unlock();
        }
    }

    protected Class<?> initializeClassLookupWhileReadLocked(Class<?> clazz) {
        if (clazz.isInterface() || this.baseClass.isAssignableFrom(clazz))
        {
            if (!this.knownKeys.contains(clazz))
            {
                try {
                    lock.readLock().unlock();
                    this.createLookup(clazz);
                } finally {
                    lock.readLock().lock();
                }
            }

            return clazz;
        }
        else
        {
            throw new IllegalArgumentException("Don't know how to search for " + clazz);
        }
    }

    /* ****************************************************************************** *\
     * WRITE OPERATIONS - WRITE LOCK + ENSURE CALLING FROM MAIN THREAD (FROM SPONGE)  *
    \* ****************************************************************************** */

    @Override
    public boolean add(T p_add_1_) {
        lock.writeLock().lock();
        try {
            return super.add(p_add_1_);
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public boolean remove(Object p_remove_1_) {
        lock.writeLock().lock();
        try {
            return super.remove(p_remove_1_);
        } finally {
            lock.writeLock().unlock();
        }
    }

    /* *********************************************** *\
     *  ITERABLE OPERATIONS - READ LOCK + MAIN THREAD  *
    \* *********************************************** */

    public <S> Iterable<S> getByClass(final Class<S> clazz)
    {
        return new Iterable<S>()
        {
            public Iterator<S> iterator()
            {
                try {
                    lock.readLock().lock();
                    List<T> list = map.get(initializeClassLookupWhileReadLocked(clazz));

                    if (list == null)
                    {
                        return Collections.emptyIterator();
                    }
                    else
                    {
                        Iterator<T> iterator = list.iterator();
                        return Iterators.filter(iterator, clazz);
                    }
                } finally {
                    lock.readLock().unlock();
                }
            }
        };
    }

    @NotNull
    @Override
    public Iterator<T> iterator() {
        lock.readLock().lock();
        try {
            // Ok we're not currently sorting this in a different thread, good stuff.
            // Additionally, just make sure no one calls remove, though i don't think anyone ever does
            if (platformExecutionType != Platform.Type.CLIENT && !SpongeImpl.getServer().isCallingFromMinecraftThread()) {
                Thread.dumpStack();
                SpongeImpl.getLogger().error("Call .iterator on entity ClassInheritanceMultiMap asynchronously.\n"
                        + " This is very bad as the map cannot tell when it will be safe to write again.\n"
                        + " Skipping...");
                return Collections.emptyIterator();
            }
            return super.iterator();
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public int size() {
        lock.readLock().lock();
        try {
            if (platformExecutionType != Platform.Type.CLIENT && !SpongeImpl.getServer().isCallingFromMinecraftThread()) {
                Thread.dumpStack();
                SpongeImpl.getLogger().error("Check size of entity ClassInheritanceMultiMap asynchronously.\n"
                        + " This is very bad as it can cause IndexOutOfBoundException's during a server tick. - Could change\n"
                        + " Skipping...");
                return 0;
            }
            return super.size();
        } finally {
            lock.readLock().unlock();
        }
    }
}
