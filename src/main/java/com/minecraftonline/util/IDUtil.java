/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.sponge.SpongeWorld;
import com.sk89q.worldedit.sponge.SpongeWorldEdit;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.trait.BlockTrait;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.TreeType;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.world.World;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Brendan  (doublehelix457)
 */

public class IDUtil {

    /**
     * Used to handle conversion from legacy ids and so forth.
     *
     * Deprecated - prefer getItemTypeFromAnyID, allows string and numeric id
     *
     * @param id Integer that represents a legacy id
     * @return Optional<ItemType> a sponge ItemType or a null value.
     */
    @Deprecated
    public static ItemType getItemFromLegacyID(int id) {
        return SpongeWorldEdit.inst().getAdapter().resolveItem(id);
    }

    /**
    * Used to allow String ids or int ids
    *
    * @param id String that is an integer or String id
    * @return Optional<ItemType> a sponge ItemType or empty option if not found
     */
    public static Optional<ItemType> getItemTypeFromAnyID(String id) {
        try {
            int Intid = Integer.parseInt(id);
            if (Intid == 144) {
                return Optional.of(ItemTypes.SKULL);
            }
            ItemType itemType = SpongeWorldEdit.inst().getAdapter().resolveItem(Intid);
            return itemType == null ? Optional.empty() : Optional.of(itemType);
        }
        catch (NumberFormatException e) {
            return Optional.empty();//Sponge.getRegistry().getType(ItemType.class, id);
        }
    }

    /**
     * Used to get ItemStack with damage
     *
     * @param id String that is int or String
     * @param damage int
     */
    public static Optional<ItemStack> getItemStackFromAnyIDAndDamage(String id, int damage) {
        Optional<ItemType> itemType = getItemTypeFromAnyID(id);
        if (!itemType.isPresent()) return Optional.empty();

        return Optional.of(ItemStack.builder().fromContainer(ItemStack.builder().itemType(itemType.get()).build().toContainer().set(DataQuery.of("UnsafeDamage"), damage)).build());
    }

    /**
     * Gets ItemStack you could place the blockState with.
     * May return an ItemStack with higher than 1 quantity, if it is a double slab.
     * @param state blockstate to get item for
     * @return ItemStack that could be used by a player to place the blockstate OR ItemStack.empty() if no item could be found
     */
    public static ItemStack getItemStackFromBlockState(BlockState state) {
        BlockType blockType = state.getType();
        if (blockType == BlockTypes.DOUBLE_STONE_SLAB) {
            return ItemStack.of(ItemTypes.STONE_SLAB, 2);
        }
        else if (blockType == BlockTypes.DOUBLE_STONE_SLAB2) {
            return ItemStack.of(ItemTypes.STONE_SLAB2, 2);
        }
        else if (blockType == BlockTypes.DOUBLE_WOODEN_SLAB) {
            return ItemStack.of(ItemTypes.WOODEN_SLAB, 2);
        }
        else if (blockType == BlockTypes.PURPUR_DOUBLE_SLAB) {
            return ItemStack.of(ItemTypes.PURPUR_SLAB, 2);
        }
        else if (blockType == BlockTypes.LEAVES || blockType == BlockTypes.LEAVES2) {
            TreeType treeType = state.require(Keys.TREE_TYPE);
            return ItemStack.builder().itemType(blockType.getItem().get()).add(Keys.TREE_TYPE, treeType).build();
        }

        Optional<ItemType> itemType = state.getType().getItem();
        if (!itemType.isPresent()) {
            return ItemStack.empty();
        }
        int itemDamage = getItemDamageFromBlockDamage(state);
        if (itemDamage == -1) {
            return ItemStack.empty();
        }
        return getItemStackWithDamage(itemType.get(), itemDamage);
    }

    /**
     * Used to get ItemStack with damage
     * @param type ItemType
     * @param damage damage
     */
    public static ItemStack getItemStackWithDamage(ItemType type, int damage) {
        return ItemStack.builder().fromContainer(ItemStack.builder().itemType(type).build().toContainer().set(DataQuery.of("UnsafeDamage"), damage)).build();
    }

    /**
     *
     * @param itemStack to multiple
     * @param x multiplier
     * @return List<ItemStack> list of itemStacks of total quantity itemStack.getQuantity()*x
     */
    public static List<ItemStack> multiplyItemStack(ItemStack itemStack, int x) {
        List<ItemStack> list = new ArrayList<>();
        int amount = itemStack.getQuantity() * x;
        int maxStackSize = itemStack.getMaxStackQuantity();
        int fullStacks = Math.floorDiv(amount, maxStackSize);
        int remainder = amount % maxStackSize;
        for (int i = 0; i < fullStacks; i++) {
            ItemStack fullStack = ItemStack.builder().from(itemStack).quantity(maxStackSize).build();
            list.add(fullStack.copy());
        }
        if (remainder != 0) {
            ItemStack remaining = itemStack.copy();
            remaining.setQuantity(remainder);
            list.add(remaining);
        }
        return list;
    }

    /**
     * Gets BlockType from String or int
     *
     * @param id String that is an int or String id
     * @return Optional<BlockType> a sponge BlockType or empty optional if not found
     */
    public static Optional<org.spongepowered.api.block.BlockType> getBlockTypeFromAnyID(String id) {
        try {
            int Intid = Integer.parseInt(id);
            if (Intid == 144) {
                return Optional.of(BlockTypes.SKULL);
            }
            org.spongepowered.api.block.BlockType blockType = SpongeWorldEdit.inst().getAdapter().resolveBlock(Intid);
            return Optional.ofNullable(blockType);
        }
        catch (NumberFormatException e) {
            return Optional.empty();//return Sponge.getRegistry().getType(org.spongepowered.api.block.BlockType.class, id);
        }
    }

    /**
     * Strips all traits other than variant in order to get a item-compatible damage.
     *
     * @param state BlockState to get item damage from
     * @return Same type without rotation, -1 if invalid damage or same damage if not handled
     */
    public static int getItemDamageFromBlockDamage(BlockState state) {
        Optional<BlockTrait<?>> trait = state.getTrait("variant");
        if (!trait.isPresent())
            return getDamageFromBlockState(state);
        Comparable value = state.getTraitValue(trait.get()).get();
        BlockState newState = state.getType().getDefaultState().withTrait(trait.get(), value).get();
        return getDamageFromBlockState(newState);
    }

    public static boolean areItemStacksEqualIgnoringQuantity(ItemStack itemStack1, ItemStack itemStack2) {
        ItemStack stack1 = itemStack1.copy();
        ItemStack stack2 = itemStack2.copy();
        stack1.setQuantity(1);
        stack2.setQuantity(1);
        return stack1.equalTo(stack2);
    }

    /**
     * Get a legacy ID from a block state.
     *
     * @param block
     */
    public static int getLegacyIDFromBlockState(BlockState block){
        return SpongeWorldEdit.inst().getAdapter().resolve(block.getType());
    }

    public static int getLegacyIDFromItemStack(ItemType itemType) {
        return SpongeWorldEdit.inst().getAdapter().resolve(itemType);
    }


    /**
     * Create BlockState with damage value
     *
     * @param blockType
     * @param damage
     * @return BlockState
     */
    public static BlockState createBlockStateWithDamage(BlockType blockType, int damage) {
        int legacyid = SpongeWorldEdit.inst().getAdapter().resolve(blockType);
        return IDUtil.getBlockStateFromLegacyID(legacyid, damage);
    }

    /**
     * Get legacy UnsafeDamage from a blockstate
     *
     * @param block
     */
    public static int getDamageFromBlockState(BlockState block){
        try {
            IBlockState nmsBlock = ((IBlockState) block);
            return nmsBlock.getBlock().getMetaFromState(nmsBlock);
        }catch(Throwable e){
            //block might not be itemizeable, and thus wont have a damage value
            return 0;
        }
    }

    /**
     * Get a Legacy Damage Value from an Item
     *
     * @param item
     */
    public static int getDamagefromItem(Item item) {
        return ((EntityItem) item).getItem().getItemDamage();
    }

    /**
     * Get a Legacy Damage Value from an ItemStack.
     *
     * @param stack
     */
    public static int getDamageFromItemStack(ItemStack stack){
        return ((net.minecraft.item.ItemStack) (Object) stack).getItemDamage();
    }


    public static BlockState getBlockStateFromLegacyID(int id, int damage) {
        Block block = Block.getBlockById(id);
        if(damage > 0){
            return (BlockState) block.getStateFromMeta(damage);
        }
        return (BlockState) block.getDefaultState();
    }

    /**
     * Horribly disgusting way of getting a blockstate from legacy ID and Damage
     * because WorldEdit is too selfish to share its methods.
     *
     * Will be replaced by LegacyMapper
     *
     * @param world
     * @param block
     * @return a BlockState
     */
    protected static BlockState getBlockStateFromBaseBlock(World world, BaseBlock block){
        //the lengths i go to keep everyone happy...
        try {
            SpongeWorld spongeworld = SpongeWorldEdit.inst().getAdapter().getWorld(world);
            Class<?> adapterWorld = SpongeWorldEdit.inst().getAdapter().getWorld(world).getClass();
            Method convertBaseBlock = adapterWorld.getDeclaredMethod("getBlockState", BaseBlock.class);
            convertBaseBlock.setAccessible(true);
            return (BlockState) convertBaseBlock.invoke(spongeworld, block);
        }catch(NoSuchMethodException | IllegalAccessException | InvocationTargetException | ClassCastException ex){
            //oops
        }
        return BlockTypes.AIR.getDefaultState();
    }

    /******************************************************************
     * Utility methods for parsing ID's and items off of signs
     *
     * These methods are generally considered unsafe as they were ported from
     * various Util Classes of CraftBook Extra and can return a null value.
     *****************************************************************/
//TODO Refactor the following methods to return optionals rather than null
    public static int[] getItemInfoFromParts(String[] parts) {
        if (parts == null) return null;

        if (parts.length > 1) {
            return oldGetItemInfoFromParts(parts);
        }
        String[] parts2 = parts[0].split("@", 2);
        if (parts2 == null) return null;
        int[] item = new int[3];

        try {
            item[0] = Integer.parseInt(parts2[0]);
            if (parts2.length > 1)
                item[1] = Integer.parseInt(parts2[1]);
            else
                item[1] = 0;
            item[2] = 1;
        } catch (NumberFormatException e) {
            return null;
        }

        return item;
    }

    public static int[] oldGetItemInfoFromParts(String[] parts) {
        if (parts == null || parts.length < 2)
            return null;

        String[] parts2 = parts[1].split("@", 2);
        int[] item = new int[3];

        try {
            item[0] = Integer.parseInt(parts2[0]);
            item[1] = 0;
            if (parts2.length > 1) {
                item[1] = Integer.parseInt(parts2[1]);
                if (item[1] > 15 || item[1] < 0)
                    return null;
            }

            item[2] = 1;
            if (parts.length > 2) {
                item[2] = Integer.parseInt(parts[2]);
            }
        } catch (NumberFormatException e) {
            return null;
        }

        return item;
    }
}