/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import org.h2.util.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * A Class that represents some number
 * with wildcards e.g ****4.
 * This would be equal to 2004
 * This would be less than 2005
 * This would be greater than 2003.
 * Integers given to {@link #compareTo(Integer)}
 * that are not long enough for the matching wildcards,
 * are padded out with leading 0's.
 */
public class WildcardInteger implements Comparable<Integer> {

    private final char[] chars;
    public static final String REGEX = "^[0-9\\*]*[0-9]+[0-9\\*]*$";

    public WildcardInteger(String string) {
        if (!string.matches(REGEX)) {
            throw new IllegalArgumentException("String given to wildcard integer was invalid! '" + string + "'");
        }
        this.chars = string.toCharArray();
    }

    /**
     * Constructs a WildcardInteger capable of handling
     * integers with the given required length.
     * @param string Wildcard string
     * @param requiredLength Required length.
     */
    public WildcardInteger(String string, int requiredLength) {
        this(StringUtils.pad(string, requiredLength, "0", false));
    }

    /**
     * @throws IllegalArgumentException If the integer's length, in base 10,
     *                                  is greater than the length of the wildcard.
     * @param o An integer to be compared with.
     * @return 0 if the non-wildcards exactly match,
     *         -1 if the non-wildcards are less than the input
     *         1 if the non-wildcards are greater than the input.
     */
    @Override
    public int compareTo(@NotNull Integer o) {
        if (o < 0) {
            return -1;
        }
        String otherString = String.format("%0" + chars.length + "d", o);
        if (otherString.length() > chars.length) {
            throw new IllegalArgumentException("This wildcard integer is only comparable with integers up to " + chars.length + " long, decimal format, but was " + otherString.length());
        }
        char[] otherArray = otherString.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (c == '*') {
                continue;
            }
            if (c != otherArray[i]) {
                return c < otherArray[i] ? -1 : 1;
            }
        }
        return 0;
    }

    public int length() {
        return chars.length;
    }

    @Override
    public String toString() {
        return new String(chars);
    }
}
