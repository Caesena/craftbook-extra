/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import com.flowpowered.math.vector.Vector3i;

/**
 * A smaller version of a vector3i that packs xyz into
 * an integer in order to have 0 collisions on hashcodes.
 */
public class WithinChunkPos {
    private static final int UNSET_HASHCODE = -1;
    private int hashCode = UNSET_HASHCODE;

    // 0-16
    private final byte x;
    // 0-256 (currently). Store as a short to stop pain.
    private final short y;
    // 0-16
    private final byte z;

    public static WithinChunkPos fromBlockPos(Vector3i blockPos) {
        return fromBlockPos(blockPos.getX(), blockPos.getY(), blockPos.getZ());
    }

    public static WithinChunkPos fromBlockPos(int x, int y, int z) {
        return new WithinChunkPos((byte)(x & 15), (short)y, (byte)(z & 15));
    }

    public WithinChunkPos(byte x, short y, byte z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WithinChunkPos)) return false;
        WithinChunkPos chunkPos = (WithinChunkPos) o;
        return x == chunkPos.x && y == chunkPos.y && z == chunkPos.z;
    }

    /**
     * The fun stuff. Combine X, Y and Z.
     * X ranges from 0-15, 4 bits
     * Y ranges from 0-255, 8 bits,
     * Z ranges from 0-15, 4 bits.
     *
     * Bit shifting in order to produce
     * 0000 0000 0000 0000 0000 0000 0000 0000
     * 0000 0000 0000 0000 xxxx yyyy yyyy zzzz
     *
     * This is only uses the bottom 16 bits. HashMap's implementation
     * however, tries to compress our bits into the lower 16! How could they!! leave us alone!!!
     *
     * @return A nice hashcode that uses the bottom 16 bits (for now)
     */
    @Override
    public int hashCode() {
        if (this.hashCode == UNSET_HASHCODE) {
            //this.hashCode = y + x >> 4 + z >> 8;
            this.hashCode = (((x << 8) | y) << 4) | z;
        }
        return this.hashCode;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }

    public String toString(int radix) {
        return "(" + Integer.toString(x, radix) + ", " + Integer.toString(y, radix) + ", " + Integer.toString(z, radix) + ")";
    }
}
