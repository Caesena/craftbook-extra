/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class Tree<T> {
	@Nullable
	private final Tree<T> parent;
	private final List<Tree<T>> children = new ArrayList<>();
	private T value;

	public Tree(@Nonnull T value) {
		this(null, value);
	}

	// Null if Top of tree, has no parent
	private Tree(@Nullable Tree<T> parent, @Nonnull T value) {
		Preconditions.checkNotNull(value);
		this.parent = parent;
		this.value = value;
	}

	/**
	 * Gets the value of this node.
	 * @return The Value of this node.
	 */
	@Nonnull
	public T getValue() {
		return value;
	}

	public void setValue(@Nonnull T value) {
		this.value = value;
	}

	/**
	 * Adds a child to this tree
	 * @param value T to be added as a child
	 * @return Sub-Node of this node
	 */
	public Tree<T> addChild(T value) {
		Tree<T> newNode = new Tree<>(this, value);
		this.children.add(newNode);
		return newNode;
	}

	/**
	 * Gets a immutable list that contains the
	 * children of the tree.
	 *
	 * @return The children of this tree node
	 */
	public ImmutableList<Tree<T>> getChildren() {
		return ImmutableList.copyOf(this.children);
	}

	/**
	 * Gets the parent of this Tree Node if it
	 * is available
	 * @return The parent node or null if it doesn't exist.
	 */
	@Nullable
	public Tree<T> getParent() {
		return this.parent;
	}
}

