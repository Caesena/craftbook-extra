/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import com.flowpowered.math.vector.Vector3i;

public enum BlockFace {
    FRONT(true),
    BACK(true),

    LEFT(true),
    RIGHT(true),

    SIDE(true), // If we don't care or a block has no direction

    TOP,
    BOTTOM,

    IGNORED(true),

    ;

    private final boolean isSide;

    BlockFace() {
        this(false);
    }

    BlockFace(boolean isSide) {
        this.isSide = isSide;
    }

    public boolean isSide() {
        return isSide;
    }

    public static BlockFace fromSimple(Vector3i to, Vector3i from) {
        Vector3i sub = from.sub(to);
        switch (sub.getY()) {
            case 0: return BlockFace.SIDE;
            case 1: return BlockFace.TOP;
            case -1: return BlockFace.BOTTOM;
            default: throw new IllegalStateException("Invalid from position!");
        }
    }
}
