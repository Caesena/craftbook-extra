/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.type.RailDirection;
import org.spongepowered.api.data.type.RailDirections;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class CartUtil {

    public static Direction[] getRailDirections(RailDirection railDirection) {
        if (railDirection == RailDirections.ASCENDING_NORTH
                || railDirection == RailDirections.ASCENDING_SOUTH
                || railDirection == RailDirections.NORTH_SOUTH) {
            return new Direction[] {Direction.NORTH, Direction.SOUTH};
        }
        else if (railDirection == RailDirections.ASCENDING_EAST
                || railDirection == RailDirections.ASCENDING_WEST
                || railDirection == RailDirections.EAST_WEST) {
            return new Direction[] {Direction.EAST, Direction.WEST};
        }
        else if (railDirection == RailDirections.NORTH_EAST) {
            return new Direction[] {Direction.NORTH, Direction.EAST};
        }
        else if (railDirection == RailDirections.NORTH_WEST) {
            return new Direction[] {Direction.NORTH, Direction.WEST};
        }
        else if (railDirection == RailDirections.SOUTH_EAST) {
            return new Direction[] {Direction.SOUTH, Direction.EAST};
        }
        else if (railDirection == RailDirections.SOUTH_WEST) {
            return new Direction[] {Direction.SOUTH, Direction.WEST};
        }
        else {
            throw new IllegalStateException("Missing case for RailDirection.");
        }
    }

    public static boolean isRail(Location<World> loc) {
        return isRail(loc.getBlockType());
    }

    public static boolean isRail(BlockType blockType) {
        return blockType == BlockTypes.RAIL
                || blockType == BlockTypes.GOLDEN_RAIL
                || blockType == BlockTypes.DETECTOR_RAIL
                || blockType == BlockTypes.ACTIVATOR_RAIL;
    }
}
