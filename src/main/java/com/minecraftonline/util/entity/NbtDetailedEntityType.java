/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.entity;

import net.minecraft.nbt.NBTTagCompound;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;

public class NbtDetailedEntityType extends DetailedEntityType {

	private final DetailedEntityType base;
	private final NBTTagCompound nbt;

	public NbtDetailedEntityType(DetailedEntityType base, NBTTagCompound nbt) {
		this.base = base;
		this.nbt = nbt;
	}

	@Override
	public boolean matches(Entity entity) {
		if (!base.matches(entity)) {
			return false;
		}
		if (nbt.isEmpty()) {
			return true;
		}
		net.minecraft.entity.Entity nmsEntity = (net.minecraft.entity.Entity)entity;
		NBTTagCompound compound = nmsEntity.writeToNBT(new NBTTagCompound());
		for (String key : nbt.getKeySet()) {
			if (!nbt.getTag(key).equals(compound.getTag(key))) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isCreatable() {
		return base.isCreatable();
	}

	@Override
	public Optional<Entity> createEntity(Location<World> loc) {
		return base.createEntity(loc)
				.map(entity -> {
					net.minecraft.entity.Entity nmsEntity = ((net.minecraft.entity.Entity)entity);
					NBTTagCompound nbt = nmsEntity.writeToNBT(new NBTTagCompound());
					nbt.merge(this.nbt);
					nmsEntity.readFromNBT(nbt);
					return entity;
				});
	}
}
