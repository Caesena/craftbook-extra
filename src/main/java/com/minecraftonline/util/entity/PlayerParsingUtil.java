/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.entity;

import com.minecraftonline.util.PermissionUtil;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Humanoid;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.util.Tuple;

import javax.annotation.Nullable;
import java.util.Objects;

public class PlayerParsingUtil {

    @Nullable
    public static HumanoidMatcher parse(final String originalString) {
        String s = originalString.toLowerCase();

        if (s.equals("p")
                || s.equals("ply")
                || s.equals("player")) { // Any player.
            return new AnyHumanoid();
        }
        else if (s.startsWith("p:")
                || s.startsWith("ply:")
                || s.startsWith("player:")) { // Specific Player
            Tuple<String, Boolean> param = extractParameter(originalString);
            if (param.getFirst().isEmpty()) {
                return null;
            }
            return new SpecificPlayer(param.getFirst(), param.getSecond());
        }
        else if (s.startsWith("g:")
                || s.startsWith("grp:")
                || s.startsWith("group:")) { // permission group
            Tuple<String, Boolean> param = extractParameter(originalString);
            if (param.getFirst().isEmpty()) {
                return null;
            }
            return new PlayerGroup(param.getFirst(), param.getSecond());
        }
        else if (s.startsWith("m:")
                || s.startsWith("match:")) { // playername contains this string.
            Tuple<String, Boolean> param = extractParameter(originalString);
            if (param.getFirst().isEmpty()) {
                return null;
            }
            return new HumanoidNameMatch(param.getFirst(), param.getSecond());
        }
        return null;
    }

    /**
     * Extracts the parameter from the player matching string.
     *
     * @param s Original string
     * @return Parameter and whether it is negated
     */
    public static Tuple<String, Boolean> extractParameter(String s) {
        String param = s.split(":", 2)[1];
        if (param.startsWith("!")) {
            return Tuple.of(param.substring(1), true);
        }
        return Tuple.of(param, false);
    }

    /**
     * Base class for all humanoid matchers. For a matcher
     * that matches any humanoid, use {@link AnyHumanoid}
     */
    public static abstract class HumanoidMatcher extends DetailedEntityType {

        /**
         * Whether this to invert the specific <b>underlying condition</b>
         * For example, if we're negating, and looking for `tyhdefu`
         * a player called Mechman would set cause this to come on,
         * as well as when both tyhdefu and mechman were present.
         * But not when a zombie was present etc.
         */
        private final boolean negate;

        public HumanoidMatcher() {
            this(false);
        }

        public HumanoidMatcher(boolean negate) {
            this.negate = negate;
        }

        @Override
        public final boolean matches(Entity entity) {
            return (entity instanceof Humanoid) && (testExtra((Humanoid) entity) ^ negate);
        }

        public boolean testExtra(Humanoid humanoid) {
            return true;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            HumanoidMatcher matcher = (HumanoidMatcher) o;
            return negate == matcher.negate;
        }

        protected boolean isNegate() {
            return negate;
        }

        @Override
        public int hashCode() {
            return Objects.hash(negate);
        }
    }

    public static class AnyHumanoid extends HumanoidMatcher {
        public AnyHumanoid() {
            super();
        }

        public AnyHumanoid(boolean negate) {
            super(negate);
        }

        @Override
        public String toString() {
            return "{Any Player (Or human), negate=" + isNegate() + "}";
        }
    }

    public static class SpecificPlayer extends HumanoidMatcher {
        private final String playername;

        public SpecificPlayer(final String playername, final boolean negate) {
            super(negate);
            this.mode = EntityMode.SpecificPlayer;
            this.playername = playername;
        }

        @Override
        public boolean testExtra(Humanoid humanoid) {
            return humanoid.getName().equals(this.playername);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SpecificPlayer that = (SpecificPlayer) o;
            return playername.equals(that.playername);
        }

        @Override
        public int hashCode() {
            return Objects.hash(playername);
        }

        @Override
        public String toString() {
            return "SpecificPlayer{" +
                    "negate=" + isNegate() +
                    ", playername='" + playername + '\'' +
                    '}';
        }
    }

    public static class HumanoidNameMatch extends HumanoidMatcher {
        private final String matchString;

        public HumanoidNameMatch(final String matchString, final boolean negate) {
            super(negate);
            this.matchString = matchString.toLowerCase();
        }

        @Override
        public boolean testExtra(Humanoid humanoid) {
            return humanoid.getName().toLowerCase().contains(this.matchString);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            HumanoidNameMatch that = (HumanoidNameMatch) o;
            return matchString.equals(that.matchString);
        }

        @Override
        public int hashCode() {
            return Objects.hash(matchString);
        }

        @Override
        public String toString() {
            return "HumanoidNameMatch{" +
                    "negate=" + isNegate() +
                    ", matchString='" + matchString + '\'' +
                    '}';
        }
    }

    public static class PlayerGroup extends HumanoidMatcher {
        private final String groupname;

        public PlayerGroup(String groupname, final boolean negate) {
            super(negate);
            this.mode = DetailedEntityType.EntityMode.Group;
            this.groupname = groupname;
        }

        @Override
        public boolean testExtra(Humanoid humanoid) {
            return humanoid instanceof Subject && PermissionUtil.isInGroup((Subject) humanoid, groupname);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PlayerGroup that = (PlayerGroup) o;
            return groupname.equals(that.groupname);
        }

        @Override
        public int hashCode() {
            return Objects.hash(groupname);
        }

        @Override
        public String toString() {
            return "PlayerGroup{" +
                    "negate=" + isNegate() +
                    ", groupname='" + groupname + '\'' +
                    '}';
        }
    }
}
