/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.entity;

import com.minecraftonline.util.PermissionUtil;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.Humanoid;
import org.spongepowered.api.entity.living.animal.Animal;
import org.spongepowered.api.entity.living.monster.Monster;
import org.spongepowered.api.entity.projectile.arrow.Arrow;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;
import java.util.function.Predicate;

/**
 * Has a variety of CraftBook-used settings such as AnyPlayer, specific, group, mob/animal etc.
 * Overloads .equals() for easy checking if it matches the criteria.
 * Returned by EntityUtil.parseEntity();
 * @author tyhdefu (blame me if i did this badly)
 */
public abstract class DetailedEntityType  implements Predicate<Entity> {

    public abstract boolean matches(Entity entity);
    // Creates Entity of the type empty optional if cannot be created
    public Optional<Entity> createEntity(Location<World> loc) {
        return Optional.empty();
    }
    public boolean isCreatable() { return false; }

    protected EntityMode mode;

    public enum EntityMode {
        AnyPlayer,
        SpecificPlayer,
        Group,
        Mob,
        SpecificType,
        SpecificInstance,
        Item
    }

    @Override
    public boolean test(Entity entity) {
        return this.matches(entity);
    }

    public static class Any extends DetailedEntityType {
        @Override
        public boolean matches(Entity entity) {
            return true;
        }
    }

    public static class Mob extends DetailedEntityType {
        private final EntityParsingUtil.MobModes mobmode;

        public Mob(EntityParsingUtil.MobModes mobmode) {
            this.mode = EntityMode.Mob;
            this.mobmode = mobmode;
            if (mobmode.equals(EntityParsingUtil.MobModes.Specific))
                CraftBookPlugin.spongeInst().getLogger().warn("mobmode in EntityUtil.DetailedEntityType.Mob should never be specific, should be a DetailedEntityType.SpecificType if so");
        }

        @Override
        public boolean matches(Entity entity) {
            if (entity == null)
                return false;
            if (mobmode.equals(EntityParsingUtil.MobModes.All)) {
                return entity instanceof Monster || entity instanceof Animal;
            }
            else if (mobmode.equals(EntityParsingUtil.MobModes.Mobs)) {
                return entity instanceof Monster;
            }
            else if (mobmode.equals(EntityParsingUtil.MobModes.Animals)) {
                return entity instanceof Animal;
            }
            return false;
        }
    }

    public static class SpecificType extends DetailedEntityType {
        private final EntityType entityType;

        public SpecificType(EntityType entityType) {
            this.mode = EntityMode.SpecificType;
            this.entityType = entityType;
        }

        @Override
        public boolean matches(Entity entity) {
            return entity != null && entity.getType().equals(entityType);
        }

        @Override
        public Optional<Entity> createEntity(Location<World> loc) {
            return Optional.of(loc.getExtent().createEntity(entityType, loc.getPosition()));
        }

        @Override
        public boolean isCreatable() {
            return true;
        }

        public boolean isMob() {
            return entityType instanceof Monster;
        }

        public boolean isAnimal() {
            return entityType instanceof Animal;
        }
    }

    public static class SpecificInstance extends DetailedEntityType {
        public enum instances {
            Arrow,
            Minecart
        }

        instances instance;

        public SpecificInstance(instances instance) {
            this.mode = EntityMode.SpecificType;
            this.instance = instance;
        }

        @Override
        public boolean matches(Entity entity) {
            if (instance.equals(instances.Arrow))
                return entity instanceof Arrow;
            else if (instance.equals(instances.Minecart))
                return entity instanceof Minecart;
            return false;
        }
    }

    public static class Creature extends DetailedEntityType {

        @Override
        public boolean matches(Entity entity) {
            return entity instanceof Creature;
        }
    }

    public static class Item extends DetailedEntityType {
        private final ItemStack itemStack;

        public Item(ItemStack itemStack) {
            this.mode = EntityMode.Item;
            this.itemStack = itemStack;
        }

        @Override
        public boolean matches(Entity entity) {
            if (!entity.getType().equals(EntityTypes.ITEM))
                return false;
            org.spongepowered.api.entity.Item item = (org.spongepowered.api.entity.Item) entity;
            return item.getItemType().equals(itemStack.getType())
                    && (int) item.item().get().toContainer().get(DataQuery.of("UnsafeDamage")).get()
                    == (int) itemStack.toContainer().get(DataQuery.of("UnsafeDamage")).get();
        }

        @Override
        public Optional<Entity> createEntity(Location<World> loc) {
            Entity entity = loc.getExtent().createEntity(EntityTypes.ITEM, loc.getPosition());
            entity.offer(Keys.REPRESENTED_ITEM, itemStack.createSnapshot());
            return Optional.of(entity);
        }
    }

    public static class Error extends Exception {
        public Error(String msg) {
            super(msg);
        }
    }
}
