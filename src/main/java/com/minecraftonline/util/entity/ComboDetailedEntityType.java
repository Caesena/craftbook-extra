/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.entity;

import com.minecraftonline.util.Tree;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Objects;
import java.util.Optional;


public class ComboDetailedEntityType extends DetailedEntityType {
	private final Tree<DetailedEntityType> entities;

	/**
	 * Creates a ComboDetailedEntityType that looks for an entity with given riders
	 * It will match if an entity even if it has more riders than desired,
	 * i.e Pig with Pig rider will match Pig on Pig on Pig.
	 *
	 * @param entities The entity (and their passengers) to match,
	 *                 with the base entity being the value of the
	 *                 tree, and the children of it being its children,
	 *                 etc, to tree-finity and beyond.
	 */
	public ComboDetailedEntityType(Tree<DetailedEntityType> entities) {
		this.entities = Objects.requireNonNull(entities);
	}

	@Override
	public boolean matches(Entity entity) {
		return this.matches(entity, this.entities);
	}

	private boolean matches(Entity entity, Tree<DetailedEntityType> subTree) {
		if (!subTree.getValue().matches(entity)) {
			return false;
		}
		for (Tree<DetailedEntityType> subNode : subTree.getChildren()) {
			if (entity.getPassengers().stream().noneMatch(e -> matches(e, subNode))) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isCreatable() {
		return this.isCreatable(this.entities);
	}

	private boolean isCreatable(Tree<DetailedEntityType> subTree) {
		if (!subTree.getValue().isCreatable()) {
			return false;
		}
		for (Tree<DetailedEntityType> subNode : subTree.getChildren()) {
			if (!isCreatable(subNode)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Optional<Entity> createEntity(Location<World> loc) {
		if (!isCreatable()) {
			return Optional.empty();
		}
		Entity baseEntity = this.entities.getValue().createEntity(loc).get();

		this.addPassengers(baseEntity, loc, this.entities);
		return Optional.of(baseEntity);
	}

	private void addPassengers(Entity vehicle, Location<World> loc, Tree<DetailedEntityType> subTree) {
		for (Tree<DetailedEntityType> subNode : subTree.getChildren()) {
			Entity entity = subNode.getValue().createEntity(loc).get();
			loc.spawnEntity(entity);
			vehicle.addPassenger(entity);
			addPassengers(entity, loc, subNode);
		}
	}
}
