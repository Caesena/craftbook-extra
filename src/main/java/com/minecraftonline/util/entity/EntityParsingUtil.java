/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.entity;

import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.Tree;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.animal.Horse;
import org.spongepowered.api.entity.living.animal.Pig;
import org.spongepowered.api.entity.living.monster.Creeper;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class EntityParsingUtil {
	private static final Set<EntityType> Animals = new HashSet<>(Arrays.asList(
			EntityTypes.BAT,
			EntityTypes.OCELOT,
			EntityTypes.CHICKEN,
			EntityTypes.COW,
			EntityTypes.DONKEY,
			EntityTypes.HORSE,
			EntityTypes.MUSHROOM_COW,
			EntityTypes.MULE,
			EntityTypes.PARROT,
			EntityTypes.PIG,
			EntityTypes.RABBIT,
			EntityTypes.SHEEP,
			EntityTypes.SKELETON_HORSE,
			EntityTypes.SQUID,
			EntityTypes.VILLAGER,
			EntityTypes.LLAMA,
			EntityTypes.WOLF,
			EntityTypes.IRON_GOLEM));
	private static final Set<EntityType> Mobs = new HashSet<>(Arrays.asList(
			EntityTypes.CAVE_SPIDER,
			EntityTypes.ENDERMAN,
			EntityTypes.SPIDER,
			EntityTypes.PIG_ZOMBIE,
			EntityTypes.BLAZE,
			EntityTypes.CREEPER,
			EntityTypes.ELDER_GUARDIAN,
			EntityTypes.ENDERMITE,
			EntityTypes.EVOCATION_ILLAGER,
			EntityTypes.GHAST,
			EntityTypes.GIANT,
			EntityTypes.GUARDIAN,
			EntityTypes.HUSK,
			EntityTypes.MAGMA_CUBE,
			EntityTypes.SHULKER,
			EntityTypes.SILVERFISH,
			EntityTypes.SKELETON,
			EntityTypes.SLIME,
			EntityTypes.STRAY,
			EntityTypes.VEX,
			EntityTypes.VINDICATION_ILLAGER,
			EntityTypes.WITCH,
			EntityTypes.WITHER,
			EntityTypes.WITHER_SKELETON,
			EntityTypes.ZOMBIE,
			EntityTypes.ZOMBIE_VILLAGER
	));

	/**
	 * Parse entity type string from sign, eg. pig.
	 * call {@link DetailedEntityType#matches(Entity)} on DetailedEntityType with entity to see if it matches.
	 * This is only a basic parsing of ONLY an entity, it will not parse stacked
	 * mobs or nbt, for that see {@link #parseStackedAndNbt(String)}
	 *
	 * Create your own specific type and override .equals for ic-specific types, or add here if used in multiple places.
	 * originalString will be with original capitalisation, so use this if you need to preserve
	 * capitilasation, else just use s for more intuitive comparison
	 *
	 *  @param originalString String
	 *  @return DetailedEntityType
	 *
	 *  @author tyhdefu
	 *  
	 * @see #parseStackedAndNbt(String)
	 */
	public static DetailedEntityType parseEntity(String originalString) throws DetailedEntityType.Error {

		if (originalString == null || originalString.equals(""))
			return null;

		String s = originalString.toLowerCase();

		{
			PlayerParsingUtil.HumanoidMatcher humanoidMatcher = PlayerParsingUtil.parse(originalString);
			if (humanoidMatcher != null) {
				return humanoidMatcher;
			}
		}

		if (s.equals("mob")
				|| s.equals("mobs")) {
			return new DetailedEntityType.Mob(MobModes.Mobs);
		}
		else if (s.equals("animal")
				|| s.equals("animals")) {
			return new DetailedEntityType.Mob(MobModes.Animals);
		}
		else if (s.equals("entityhorse")) {
			return new DetailedEntityType() {
				@Override
				public boolean matches(Entity entity) {
					return entity instanceof Horse;
				}
			};
		}
		else if (s.startsWith("minecart")) {
			if (s.startsWith("minecart:")) {
				int type;
				try {
					type = Integer.parseInt(s.substring(9));
				}
				catch (NumberFormatException e) {throw new DetailedEntityType.Error("Type of minecart should be a number");}
				if (type == 0) {
					return new DetailedEntityType.SpecificType(EntityTypes.RIDEABLE_MINECART);
				}
				else if (type == 1) {
					return new DetailedEntityType.SpecificType(EntityTypes.CHESTED_MINECART);
				}
				else if (type == 2) {
					return new DetailedEntityType.SpecificType(EntityTypes.FURNACE_MINECART);
				}
				else {
					throw new DetailedEntityType.Error("Minecart number must be between 0-2");
				}
			}
			else if (s.equals("minecart")) {
				return new DetailedEntityType.SpecificInstance(DetailedEntityType.SpecificInstance.instances.Minecart);
			}
			else {
				throw new DetailedEntityType.Error("Should be minecart or minecart:#");
			}
		}
		else if (s.equals("boat")) {
			return new DetailedEntityType.SpecificType(EntityTypes.BOAT);
		}
		else if (s.startsWith("item")) {
			if (s.startsWith("item:")) {
				String[] ids = s.substring(5).split("@", 2);
				String id = ids[0];
				Sponge.getServer().getConsole().getMessageChannel().send(Text.of(id + " " + ids.length));
				int dmg = 0;
				if (ids.length == 2) {
					try {
						dmg = Integer.parseInt(ids[1]);
					} catch (NumberFormatException e) {throw new DetailedEntityType.Error("Damage value was not a number");}
				}
				ItemStack itemStack = ItemStack.builder().itemType(IDUtil.getItemTypeFromAnyID(id).orElseThrow(() -> new DetailedEntityType.Error("Invalid item id"))).build();
				if (dmg != 0)
					itemStack = ItemStack.builder().fromContainer(itemStack.toContainer().set(DataQuery.of("UnsafeDamage"), dmg)).build();
				return new DetailedEntityType.Item(itemStack);
			}
			else {
				return new DetailedEntityType.SpecificType(EntityTypes.ITEM);
			}
		}
		else if (s.equals("arrow")) {
			return new DetailedEntityType.SpecificInstance(DetailedEntityType.SpecificInstance.instances.Arrow);
		}
		else if (s.equals("fireball")) {
			return new DetailedEntityType.SpecificType(EntityTypes.FIREBALL);
		}
		else {
			// Extra data, eg. coloured sheep, saddled pig, charged creeper
			if (s.startsWith("sheep@")) {
				int colour = 0;
				try {
					colour = Integer.parseInt(s.substring(6));
				}
				catch (NumberFormatException e) {throw new DetailedEntityType.Error("Invalid colour value for sheep");}
				DyeColor sheepColor = getSheepColor(colour);
				if (sheepColor == null)
					throw new DetailedEntityType.Error("Invalid colour for sheep");

				return new DetailedEntityType() {
					@Override
					public boolean matches(Entity entity) {
						return entity.getType().equals(EntityTypes.SHEEP)
								&& entity.get(Keys.DYE_COLOR).get().equals(sheepColor);
					}

					@Override
					public boolean isCreatable() {return true;}

					@Override
					public Optional<Entity> createEntity(Location<World> loc) {
						Entity entity = loc.getExtent().createEntity(EntityTypes.SHEEP, loc.getBlockPosition());
						entity.offer(Keys.DYE_COLOR, sheepColor);
						return Optional.of(entity);
					}
				};
			}
			else if (s.startsWith("pig@")) {
				int type;
				try {
					type = Integer.parseInt(s.substring(4));
					if (!(type == 0 || type == 1))
						throw new DetailedEntityType.Error("Pig type must be 0 or 1");
				} catch (NumberFormatException e) {throw new DetailedEntityType.Error("Pig type was not a number");}
				if (type == 1) {
					return new DetailedEntityType() {
						@Override
						public boolean matches(Entity entity) {
							return entity.getType().equals(EntityTypes.PIG)
									&& !((Pig)entity).saddled().get();
						}

						@Override
						public boolean isCreatable() {return true;}

						@Override
						public Optional<Entity> createEntity(Location<World> loc) {
							Entity entity = loc.getExtent().createEntity(EntityTypes.PIG, loc.getBlockPosition());
							entity.offer(Keys.PIG_SADDLE, true);
							return Optional.of(entity);
						}
					};
				}
				else {
					return new DetailedEntityType() {
						@Override
						public boolean matches(Entity entity) {
							return entity.getType().equals(EntityTypes.PIG)
									&& !((Pig)entity).saddled().get();
						}
					};
				}
			}
			else if (s.startsWith("creeper@")) {
				int type;
				try {
					type = Integer.parseInt(s.substring(8));
					if (!(type == 0 || type == 1))
						throw new DetailedEntityType.Error("Creeper type must be 0 or 1");
				} catch (NumberFormatException e) {throw new DetailedEntityType.Error("Creeper type was not a number");}
				if (type == 1) {
					return new DetailedEntityType() {
						@Override
						public boolean matches(Entity entity) {
							return entity.getType().equals(EntityTypes.CREEPER)
									&& ((Creeper)entity).charged().get();
						}

						@Override
						public boolean isCreatable() {return true;}

						@Override
						public Optional<Entity> createEntity(Location<World> loc) {
							Entity entity = loc.getExtent().createEntity(EntityTypes.CREEPER, loc.getBlockPosition());
							entity.offer(Keys.CREEPER_CHARGED, true);
							return Optional.of(entity);
						}
					};
				}
				else {
					return new DetailedEntityType() {
						@Override
						public boolean matches(Entity entity) {
							return entity.getType().equals(EntityTypes.CREEPER)
									&& !((Creeper)entity).charged().get();
						}

						@Override
						public boolean isCreatable() {return true;}

						@Override
						public Optional<Entity> createEntity(Location<World> loc) {
							return Optional.of(loc.getExtent().createEntity(EntityTypes.CREEPER, loc.getBlockPosition()));
						}
					};
				}
			}
			EntityType type = Sponge.getRegistry().getType(EntityType.class, s).orElse(null);
			if (type != null) {
				return new DetailedEntityType.SpecificType(type);
			}
			return null;
		}
	}

	public static DetailedEntityType parseStackedAndNbt(String originalString) throws DetailedEntityType.Error {

		if (originalString.isEmpty()) {
			throw new DetailedEntityType.Error("Cannot parse empty string!");
		}

		if (originalString.equalsIgnoreCase("RiddenPig")) {
			Tree<DetailedEntityType> tree = new Tree<>(new DetailedEntityType.SpecificType(EntityTypes.PIG));
			tree.addChild(new PlayerParsingUtil.AnyHumanoid());
			return new ComboDetailedEntityType(tree);
		}
		Tree<DetailedEntityType> passengerTree = null;

		Tree<DetailedEntityType> cursor = null;

		int lastUnprocessed = 0;
		int nbtStarts = 0; // Amount of start nbts, used to keep track of when nbt has really finished.
		SpecialCharacter lastSpecial = null;

		for (int i = 0; i < originalString.length(); i++) {
			char c = originalString.charAt(i);
			SpecialCharacter specialCharacter = SpecialCharacter.getSpecial(c);
			if (specialCharacter == null) {
				lastSpecial = null;
				continue;
			}
			if (i == 0) {
				throw new DetailedEntityType.Error("Invalid demarcation, cannot start with a " + specialCharacter);
			}
			// Cannot repeat demarcation except child
			if (lastSpecial != null && lastSpecial != SpecialCharacter.CHILD && lastSpecial != SpecialCharacter.END_NBT) {
				throw new ComplexDetailedEntityError("Cannot use demarcation '" + specialCharacter + "' after '" + lastSpecial.getChar(), lastUnprocessed);
			}

			if (lastSpecial == SpecialCharacter.END_NBT && specialCharacter == SpecialCharacter.PASSENGER) {
				lastSpecial = specialCharacter;
				lastUnprocessed = i + 1;
				continue; // No entity between these.
			}

			// Don't parse nbt as a mob.
			if (nbtStarts == 0 && specialCharacter != SpecialCharacter.END_NBT) {
				String detailedEntityTypeString = originalString.substring(lastUnprocessed, i);
				DetailedEntityType type = EntityParsingUtil.parseEntity(detailedEntityTypeString);

				if (type == null) {
					throw new ComplexDetailedEntityError("Invalid entity '" + detailedEntityTypeString + "'", lastUnprocessed);
				}

				if (cursor == null) {
					passengerTree = new Tree<>(type);
					cursor = passengerTree;
				}
				else {
					cursor = cursor.addChild(type);
				}
			}

			switch (specialCharacter) {
				case CHILD: {
					cursor = cursor.getParent();
					if (cursor == null) {
						throw new ComplexDetailedEntityError("Too many '" + SpecialCharacter.CHILD.getChar() + "'s, reached the bottom of the stack already!", lastUnprocessed);
					}
					break;

				}
				case START_NBT: {
					nbtStarts++;
					break;
				}
				case END_NBT: {
					if (nbtStarts == 0) {
						throw new ComplexDetailedEntityError("End nbt demarcation found before start demarcation!", i);
					}
					nbtStarts--;
					if (nbtStarts == 0) {
						String nbt = originalString.substring(lastUnprocessed - 1, i + 1); // also include current demarcation marker
						NBTTagCompound nbtTagCompound;
						try {
							nbtTagCompound = JsonToNBT.getTagFromJson(nbt);
						} catch (NBTException e) {
							throw new ComplexDetailedEntityError("Error parsing nbt ('" + nbt + "') " + e.getMessage(), i);
						}
						NbtDetailedEntityType nbtDetailedEntityType = new NbtDetailedEntityType(cursor.getValue(), nbtTagCompound);
						cursor.setValue(nbtDetailedEntityType);
					}
					break;
				}
				case PASSENGER: {
					break; // Basically its what we expecting, default behavior so change nothing.
				}
				default: throw new IllegalStateException("Missing Special Character case!!");
			}
			if (nbtStarts > (specialCharacter == SpecialCharacter.START_NBT ? 1 : 0)) {
				// don't set lastUnprocessed inside nbt.
				continue;
			}
			lastSpecial = specialCharacter;
			lastUnprocessed = i + 1;
		}
		if (lastUnprocessed != originalString.length()) {
			String detailedEntityTypeString = originalString.substring(lastUnprocessed);
			DetailedEntityType type = EntityParsingUtil.parseEntity(detailedEntityTypeString);

			if (type == null) {
				throw new ComplexDetailedEntityError("Invalid entity '" + detailedEntityTypeString + "'", lastUnprocessed);
			}
			if (cursor == null) {
				passengerTree = new Tree<>(type);
			}
			else {
				cursor.addChild(type);
			}
		}
		return new ComboDetailedEntityType(passengerTree);
	}

	public static class ComplexDetailedEntityError extends DetailedEntityType.Error {
		public ComplexDetailedEntityError(String msg, int index) {
			super(msg + " at index " + index);
		}
	}

	/**
	 * Formatting:
	 *
	 * Pig+Cow+Pig\(=)Cow+Slime\(=)Cow+MagmaCube
	 *
	 *           Pig    Slime  MagmaCube
	 *            \        |      /
	 * 			  Cow = Cow = Cow
	 * 			   \    |     /
	 *                 Pig
	 * This needs new rules because before adding multiple on the same level would not be possible,
	 * so stacking up 100 cows on one pig for an ultimate explosion of mobs when the pig was killed would not be possible
	 * Rules:
	 * 	+ goes up and is followed by a riding passenger,
	 * 	\ goes down (pointing down) and moves the position by 1 down
	 * 	= goes is similar to down then up and is followed by the passenger. This is needed since the first entity doesn't have a down.
	 * 	= is optional after a \.
	 */
	public enum SpecialCharacter {
		START_NBT('{'), // Starts the nbt, need an equivalent number of these to end nbt.
		END_NBT('}'), // Ends the nbt, equal number as start to end the nbt

		PASSENGER('+'), // Shows that the next string will be a mob that riders on the current one
		// TODO: find if actually needed SIBLING_PASSENGER("="), // Shows that the next string will be a mob riding the same entity as this one is riding on
		CHILD('\\'); // Moves the "cursor" down a mob, so you can either make a sibling of the current mob or make another "branch" with mobs stacking on that. Can be repeated.

		private final char character;

		SpecialCharacter(char c) {
			this.character = c;
		}

		boolean matches(char c) {
			return this.character == c;
		}

		@Nullable
		public static SpecialCharacter getSpecial(char c) {
			for (SpecialCharacter specialCharacter : values()) {
				if (specialCharacter.matches(c)) {
					return specialCharacter;
				}
			}
			return null;
		}

		public char getChar() {
			return character;
		}

		@Override
		public String toString() {
			return String.valueOf(character);
		}
	}

	/*public static Map<Integer, DyeColor> IntColorMap = new HashMap<>();

	static {
		IntColorMap.put(0, DyeColors.WHITE);
		IntColorMap.put(1, DyeColors.ORANGE);
		IntColorMap.put(2, DyeColors.MAGENTA);
		IntColorMap.put(3, DyeColors.LIGHT_BLUE);
		IntColorMap.put(4, DyeColors.YELLOW);
		IntColorMap.put(5, DyeColors.LIME);
		IntColorMap.put(6, DyeColors.PINK);
		IntColorMap.put(7, DyeColors.GRAY);
		IntColorMap.put(8, DyeColors.SILVER);

		IntColorMap.put(9, DyeColors.CYAN);
		IntColorMap.put(10, DyeColors.PURPLE);
		IntColorMap.put(11, DyeColors.BLUE);
		IntColorMap.put(12, DyeColors.BROWN);
		IntColorMap.put(13, DyeColors.GREEN);
		IntColorMap.put(14, DyeColors.RED);
		IntColorMap.put(15, DyeColors.BLACK);

	}*/

	@Nullable
	public static DyeColor getSheepColor(int color) {
		if (color < 0 || color > 15) {
			return null;
		}
		return (DyeColor) (Object) EnumDyeColor.values()[color];
	}

	public static Set<EntityType> getAnimals() {
		return Animals;
	}

	public static Set<EntityType> getMobs() {
		return Mobs;
	}


	public enum MobModes {
		Specific,
		Mobs,
		Animals,
		All
	}

	public enum PlayerModes {
		All,
		Specific,
		Group
	}
}
