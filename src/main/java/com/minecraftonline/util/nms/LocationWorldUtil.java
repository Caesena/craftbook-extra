/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.nms;

import com.flowpowered.math.vector.Vector3i;
import net.minecraft.block.BlockBasePressurePlate;
import net.minecraft.block.BlockLever;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.common.bridge.world.chunk.ChunkProviderBridge;

public class LocationWorldUtil {

    /**
     * Gets the light level in at a specific location.
     *
     * Searge:
     * (World) The minecraft world.
     * (getLight()) func_175699_k / Get the light level with a specifc BlockPos
     * (BlockPos) A block position Vector.
     * boolean checks neighbors.
     *
     * @param extent
     * @param x
     * @param y
     * @param z
     * @return the light level int
     */
    public static int getLightLevel(World extent, int x, int y, int z){
        return ((net.minecraft.world.World) extent).getLight(new BlockPos(x,y,z), true);
    }

    /**
     * Sets the lever state based on the given boolean
     *
     * Searge:
     * (World) The minecraft world.
     * (BlockPos) Block Position Vector
     * World.getBlockState() func_180495_p / Get a block state
     * (IBlockState) The state of the block
     * Block.getMetaFromState func_176201_c / Convert the BlockState into the correct metadata value
     * Block.getStateFromMeta() func_176203_a / Convert the given metadata into a BlockState for this Block
     * World.setBlockState() func_175656_a / Convenience method to update the block on both the client and server
     */
    //TODO Get rid of this nastiness when sponge gets fixed.
    public static void setLeverSate(Location<World> lever, Location<World> attached, boolean set) {
        net.minecraft.world.World world = (net.minecraft.world.World) lever.getExtent();
        BlockPos leverPos = new BlockPos(lever.getBlockX(),lever.getBlockY(),lever.getBlockZ());
        IBlockState leverState = world.getBlockState(leverPos);

        int data = leverState.getBlock().getMetaFromState(leverState);
        int newData;

        if(!set) newData = data & 0x7;
        else newData = data | 0x8;

        IBlockState newState = leverState.getBlock().getStateFromMeta(newData);

        world.setBlockState(leverPos, newState);
        world.notifyNeighborsOfStateChange(leverPos,newState.getBlock(),true);
        BlockPos attachedPos = new BlockPos(attached.getBlockX(),attached.getBlockY(),attached.getBlockZ());
        world.scheduleUpdate(attachedPos,world.getBlockState(attachedPos).getBlock(),0);
        world.notifyNeighborsOfStateChange(attachedPos,world.getBlockState(attachedPos).getBlock(),true);
    }

    public static void toggleLever(Location<World> loc, boolean playSound) {
        // This code is basically just pulled from onActivated, minus the sounds
        net.minecraft.world.World world = (net.minecraft.world.World) loc.getExtent();

        BlockPos pos = new BlockPos(loc.getX(), loc.getY(), loc.getZ());
        IBlockState state = world.getBlockState(pos);
        BlockLever lever = (BlockLever) state.getBlock();
        state = state.cycleProperty(BlockLever.POWERED);
        world.setBlockState(pos, state, 3);

        if (playSound) {
            float f = state.getValue(BlockLever.POWERED).booleanValue() ? 0.6F : 0.5F;
            world.playSound(null, pos, SoundEvents.BLOCK_LEVER_CLICK, SoundCategory.BLOCKS, 0.3F, f);
        }

        world.notifyNeighborsOfStateChange(pos, state.getBlock(), false);
        EnumFacing enumfacing = state.getValue(BlockLever.FACING).getFacing();
        world.notifyNeighborsOfStateChange(pos.offset(enumfacing.getOpposite()), state.getBlock(), false);
    }

    public static void updatePressurePlate(Location<World> loc) {
        net.minecraft.world.World world = (net.minecraft.world.World) loc.getExtent();
        BlockPos pos = new BlockPos(loc.getX(), loc.getY(), loc.getZ());
        IBlockState state = world.getBlockState(pos);
        BlockBasePressurePlate plate = (BlockBasePressurePlate) state.getBlock();
        world.notifyNeighborsOfStateChange(pos, plate, false);
        world.notifyNeighborsOfStateChange(pos.down(), plate, false);
    }

    public static boolean isChunkQueuedForUnload(World world, Vector3i chunkPos) {
        net.minecraft.world.World mcWorld = (net.minecraft.world.World) world;
        ChunkProviderBridge chunkProviderBridge = (ChunkProviderBridge) mcWorld.getChunkProvider();
        Chunk chunk = chunkProviderBridge.bridge$getLoadedChunkWithoutMarkingActive(chunkPos.getX(), chunkPos.getZ());
        return chunk == null || chunk.unloadQueued;
    }

    public static boolean isAllLoaded(World world, Vector3i start, Vector3i end) {
        final int startChunkX = start.getX() >> 4;
        final int startChunkZ = end.getZ() >> 4;

        final int endChunkX = start.getX() >> 4;
        final int endChunkZ = end.getZ() >> 4;

        ChunkProviderBridge chunkProviderBridge = (ChunkProviderBridge) ((net.minecraft.world.World) world).getChunkProvider();

        for (int cx = startChunkX; cx <= endChunkX; cx++) {
            for (int cz = startChunkZ; cz <= endChunkZ; cz++) {
                Chunk mcChunk = chunkProviderBridge.bridge$getLoadedChunkWithoutMarkingActive(cx, cz);
                if (mcChunk == null || mcChunk.unloadQueued) {
                    return false;
                }
            }
        }
        return true;
    }
}
