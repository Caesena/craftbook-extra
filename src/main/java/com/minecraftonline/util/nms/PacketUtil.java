/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.nms;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketChangeGameState;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.World;

public class PacketUtil {
    /**
     * Sends a Packet to all players online at once.
     *
     * Searge:
     * Packet<?>
     *
     * @param world
     * @param packet
     */

    //Uses Acess Transformer here found in resources 'craftbook_at.cfg'
    public static void sendPacketToAllPlayers(World world, Packet<?> packet){
        for (Player player : world.getPlayers()){
            sendPacketToPlayer(player,packet);
        }
    }

    /**
     * Send a Packet to a player.
     *
     * Searge:
     * (EntityPlayerMP) A Multiplayer player.
     * (connection) field_71135_a / A NetHandlerPlayServer assigned to the specific player
     * (sendPacket()) func_147359_a()/ Send a Packet to the player connection.)
     *
     * @param player
     * @param packet
     */
    public static void sendPacketToPlayer(Player player, Packet<?> packet){
        EntityPlayerMP entityPlayer = (EntityPlayerMP) player;
        entityPlayer.connection.sendPacket(packet);
    }

    /**
     * Use of a builder design keeps the NMS use contained here.
     * As the need arises, more packets can be built here.
     */
    public static class PacketBuilder {

        Packet type;

        public PacketBuilder asGameStateChange(int state, int mode) {
            type = new SPacketChangeGameState(state, mode);
            return this;
        }

        public Packet build() {
            return type;
        }
    }
}
