/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.nms;

import net.minecraft.entity.IProjectile;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.projectile.Projectile;

public class EntityUtil {

    /**
     * Shoot an already existing projectile entity.
     *
     * Searge:
     * (IProjectile) Represents an entity that is a projectile
     * (shoot()) func_70186_c() / Shoot an already existing projectile entity in a specific direction.
     *
     * @param projectile
     * @param x
     * @param y
     * @param z
     * @param speed
     * @param spread
     */
    public static void setThrowableHeading(Projectile projectile, double x, double y, double z, float speed, float spread)
    {
        ((IProjectile) projectile).shoot(x,y,z,speed,spread);
    }

    /**
     * Update an existing entity's position/logic
     *
     * Searge:
     * (Entity) The Entity to force an update upon.
     * (onUpdate()) func_70071_h_ / Update the exsisting entity's position/logic
     *
     * @param entity
     */
    public static void onUpdate(Entity entity){
        ((net.minecraft.entity.Entity) entity).onUpdate();
    }


}
