/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import net.minecraft.inventory.IInventory;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.common.item.inventory.util.ItemStackUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class InventoryUtil {
    /**
     *
     * @param inv to offer to
     * @param itemStack to offer
     * @param cantFitHandler handler for items that dont fit.
     * @return if all items were transferred successfully.
     */
    public static boolean offerInventory(Inventory inv, ItemStack itemStack, Consumer<ItemStack> cantFitHandler) {
        inv.offer(itemStack);
        if (!itemStack.isEmpty()) {
            cantFitHandler.accept(itemStack);
            return false;
        }
        return true;
    }

    /**
     * Uses NMS to avoid sponge wrapping, subject to change / removal.
     * @param spongeInv Inventory to offer to
     * @param stack Stack to offer, this stack will be mutated
     * @return The remaining ItemStack
     */
    public static ItemStack offerInventoryQuick(Inventory spongeInv, ItemStack stack) {
        IInventory inv = (IInventory) spongeInv;
        net.minecraft.item.ItemStack mcStack = (net.minecraft.item.ItemStack) (Object) stack;
        int inventoryStackLimit = inv.getInventoryStackLimit();
        for (int i = 0; i < inv.getSizeInventory(); i++) {
            net.minecraft.item.ItemStack itemStack = inv.getStackInSlot(i);
            if (itemStack.isEmpty()) {
                inv.setInventorySlotContents(i, mcStack);
                return ItemStack.empty();
            }
            int maxStackSize = Math.min(inventoryStackLimit, itemStack.getMaxStackSize());
            int space = maxStackSize - itemStack.getCount();
            if (space > 0 && areItemsEqualIgnoringSize(itemStack, mcStack)) {
                if (space >= mcStack.getCount()) {
                    itemStack.setCount(itemStack.getCount() + mcStack.getCount());
                    return ItemStack.empty();
                }
                itemStack.setCount(maxStackSize);
                mcStack.setCount(mcStack.getCount() - space);
            }
        }
        return stack;
    }

    public static int canFitHowMuch(Inventory inv, ItemStack itemStack) {
        if ((inv.capacity() - inv.size()) > 0 && inv.getMaxStackSize() >= itemStack.getMaxStackQuantity()) {
            return itemStack.getMaxStackQuantity();
        }
        // Ok lets see if theres room in existing slots.
        int maxStackSize = Math.min(inv.getMaxStackSize(), itemStack.getMaxStackQuantity());
        int room = 0;
        for (Inventory slot : inv.slots()) {
            room += maxStackSize - slot.peek().map(ItemStack::getQuantity).orElse(0);
            if (room > itemStack.getMaxStackQuantity()) {
                room = itemStack.getMaxStackQuantity();
                break;
            }
        }
        return room;
    }

    public static boolean areItemsEqualIgnoringSize(net.minecraft.item.ItemStack itemStack1, net.minecraft.item.ItemStack itemStack2) {
        if (itemStack1.isEmpty()) {
            return itemStack2.isEmpty();
        }
        if (itemStack1.getItem() != itemStack2.getItem()) {
            return false;
        }
        if (itemStack1.getItemDamage() != itemStack2.getItemDamage()) {
            return false;
        }
        if (itemStack1.getTagCompound() == null) {
            return itemStack2.getTagCompound() == null;
        }
        return itemStack2.getTagCompound() != null && itemStack1.getTagCompound().equals(itemStack2.getTagCompound());
    }
}
