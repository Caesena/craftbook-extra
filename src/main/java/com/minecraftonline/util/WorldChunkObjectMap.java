/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import com.flowpowered.math.vector.Vector3i;
import com.sk89q.craftbook.sponge.IC;
import net.minecraft.util.math.ChunkPos;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class WorldChunkObjectMap<V> {
    private final Map<UUID, Map<ChunkPos, Map<WithinChunkPos, V>>> map = new HashMap<>();

    @Nullable
    public V get(Location<World> loc) {
        Map<WithinChunkPos, V> map = getChunkMap(loc.getExtent(), loc.getChunkPosition());
        if (map == null) {
            return null;
        }
        return map.get(WithinChunkPos.fromBlockPos(loc.getBlockPosition()));
    }

    @Nullable
    public V put(Location<World> loc, V value) {
        Map<ChunkPos, Map<WithinChunkPos, V>> chunkMap = map.computeIfAbsent(loc.getExtent().getUniqueId(), k -> new HashMap<>());
        Map<WithinChunkPos, V> withinChunkPosMap = chunkMap.computeIfAbsent(new ChunkPos(loc.getBlockX() >> 4, loc.getBlockZ() >> 4), k -> new WithinChunkPosMap<>());
        return withinChunkPosMap.put(WithinChunkPos.fromBlockPos(loc.getBlockPosition()), value);
    }

    @Nullable
    public V remove(Location<World> loc) {
        Map<WithinChunkPos, V> map = getChunkMap(loc);
        return map == null ? null : map.remove(WithinChunkPos.fromBlockPos(loc.getBlockPosition()));
    }

    @Nullable
    public Map<WithinChunkPos, V> getChunkMap(Chunk chunk) {
        return getChunkMap(chunk.getWorld(), chunk.getPosition());
    }

    @Nullable
    public Map<WithinChunkPos, V> getChunkMap(Location<World> loc) {
        return getChunkMap(loc.getExtent(), loc.getChunkPosition());
    }

    @Nullable
    public Map<WithinChunkPos, V> getChunkMap(World world, Vector3i chunkPosition) {
        final Map<ChunkPos, Map<WithinChunkPos, V>> chunkMap = map.get(world.getUniqueId());
        if (chunkMap == null) {
            return null;
        }
        return chunkMap.get(new ChunkPos(chunkPosition.getX(), chunkPosition.getZ()));
    }

    @Nullable
    public Map<WithinChunkPos, V> removeChunk(Chunk chunk) {
        final Map<ChunkPos, Map<WithinChunkPos, V>> chunkMap = map.get(chunk.getWorld().getUniqueId());
        if (chunkMap == null) {
            return null;
        }
        Vector3i chunkPosition = chunk.getPosition();
        return chunkMap.remove(new ChunkPos(chunkPosition.getX(), chunkPosition.getZ()));
    }

    public void clear() {
        this.map.clear();
    }

    public void forEachValue(Consumer<? super V> consumer) {
        this.map.values().forEach(map2 -> map2.values().forEach(map3 -> map3.values().forEach(consumer)));
    }

    public int checkHashCodes() {
        AtomicInteger collisions = new AtomicInteger();
        this.map.values().forEach(map2 -> map2.values().forEach(map3 -> {
            Set<Integer> hashCodes = new HashSet<>();
            map3.keySet().forEach(withinChunkPos -> {
                int hashCode = withinChunkPos.hashCode();
                if (!hashCodes.add(hashCode)) {
                    System.out.println("Hashcode collided! " + hashCode + " on " + withinChunkPos);
                    collisions.getAndIncrement();
                }
            });
        }));
        return collisions.get();
    }

    public Iterator<V> valueIterator() {
        return this.map.values().stream().flatMap(map -> map.values().stream())
                .flatMap(map -> map.values().stream()).iterator();
    }
}
