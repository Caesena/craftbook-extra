/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.tweaker;

import net.minecraft.launchwrapper.LaunchClassLoader;
import org.spongepowered.asm.mixin.MixinEnvironment;
import org.spongepowered.asm.mixin.Mixins;
import org.spongepowered.asm.mixin.extensibility.IRemapper;
import org.spongepowered.common.launch.SpongeLaunch;
import org.spongepowered.lwts.AbstractTestTweaker;
import org.spongepowered.server.launch.VanillaLaunch;
import org.spongepowered.server.launch.transformer.deobf.SrgRemapper;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.spongepowered.asm.mixin.MixinEnvironment.Side.SERVER;

/**
 * Used by LWTS to apply mixins (https://github.com/SpongePowered/LaunchWrapperTestSuite)
 *
 * Our implementation is a blend of SpongeCommon's TestTweaker and SpongeVanilla's VanillaTweaker,
 * and also sticking our plugin mixins in too.
 */
public class MyTestTweaker extends AbstractTestTweaker {

    @Override
    public void injectIntoClassLoader(LaunchClassLoader loader) {
        super.injectIntoClassLoader(loader);

        configureLaunchClassLoader(loader);

        registerAccessTransformer("META-INF/craftbook_at.cfg");

        // TODO: review. We probably need more gradle magic to transfer this file too?
        registerAccessTransformer("META-INF/common_at.cfg");

        SpongeLaunch.initPaths(new File("testRuns"));

        configureMixinEnvironment();

        Mixins.addConfiguration("craftbook.json");

        System.out.println("Mixin Configs: " + Mixins.getConfigs());
    }

    public static void configureMixinEnvironment() {
        VanillaLaunch.getLogger().debug("Initializing Mixin environment...");
        SpongeLaunch.setupMixinEnvironment();

        Mixins.addConfiguration("mixins.vanilla.core.json");
        Mixins.addConfiguration("mixins.vanilla.entityactivation.json");
        Mixins.addConfiguration("mixins.vanilla.chunkio.json");
        Mixins.addConfiguration("mixins.vanilla.optimization.json");

        MixinEnvironment.getDefaultEnvironment().setSide(SERVER);

        // Add our remapper to Mixin's remapper chain
        SrgRemapper remapper = VanillaLaunch.getRemapper();
        if (remapper instanceof IRemapper) {
            MixinEnvironment.getDefaultEnvironment().getRemappers().add((IRemapper) remapper);
        }
    }

    @Override
    public String getLaunchTarget() {
        return "com.minecraftonline.craftbook.integration.IntegrationTestMain";
    }

    public static void configureLaunchClassLoader(LaunchClassLoader loader) {

        SpongeLaunch.addJreExtensionsToClassPath();

        // See TestTweaker in SpongeCommon


        // TODO: figure out why this doesn't like being converted to a list.
        // Logging
        loader.addClassLoaderExclusion("org.slf4j.");
        loader.addClassLoaderExclusion("net.minecrell.terminalconsole.");
        loader.addClassLoaderExclusion("org.jline.");
        loader.addClassLoaderExclusion("com.sun.");
        loader.addClassLoaderExclusion("com.mojang.util.QueueLogAppender");

        // Sponge Launch
        loader.addClassLoaderExclusion("joptsimple.");
        loader.addClassLoaderExclusion("com.google.common.");
        loader.addClassLoaderExclusion("org.spongepowered.common.launch.");
        loader.addClassLoaderExclusion("org.spongepowered.server.launch.");
        loader.addClassLoaderExclusion("org.spongepowered.plugin.");
        loader.addTransformerExclusion("org.spongepowered.common.event.tracking.PhaseTracker");
        loader.addTransformerExclusion("org.spongepowered.common.event.tracking.TrackingUtil");

        // Don't allow transforming libraries
        loader.addTransformerExclusion("com.google.");
        loader.addTransformerExclusion("org.apache.");
        loader.addTransformerExclusion("io.netty.");
        loader.addTransformerExclusion("com.flowpowered.");
        loader.addTransformerExclusion("it.unimi.dsi.fastutil.");
        loader.addTransformerExclusion("com.github.benmanes.caffeine.");
        // Guice
        loader.addTransformerExclusion("org.aopalliance.");
        // Configurate
        loader.addTransformerExclusion("ninja.leaping.configurate.");
        loader.addTransformerExclusion("com.typesafe.config.");
        loader.addTransformerExclusion("org.yaml.snakeyaml.");
        // Database connectors
        loader.addTransformerExclusion("com.zaxxer.hikari.");
        loader.addTransformerExclusion("org.h2.");
        loader.addTransformerExclusion("org.mariadb.");
        loader.addTransformerExclusion("org.sqlite.");
    }

    public static final List<String> classLoaderExclusions = Arrays.asList(
            "org.slf4j.",
            "net.minecrell.terminalconsole.",
            "org.jline.",
            "com.sun.",
            "com.mojang.util.QueueLogAppender",

            "joptsimple.",
            "com.google.common.",
            "org.spongepowered.common.launch.",
            "org.spongepowered.server.launch.",
            "org.spongepowered.plugin.",
            "org.spongepowered.common.event.tracking.PhaseTracker",
            "org.spongepowered.common.event.tracking.TrackingUtil",

            "com.google.",
            "org.apache.",
            "io.netty.",
            "com.flowpowered.",
            "it.unimi.dsi.fastutil.",
            "com.github.benmanes.caffeine.",

            "org.aopalliance.",

            "ninja.leaping.configurate.",
            "com.typesafe.config.",
            "org.yaml.snakeyaml.",

            "com.zaxxer.hikari.",
            "org.h2.",
            "org.mariadb.",
            "org.sqlite."
    );
}
