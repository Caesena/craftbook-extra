/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge;

import com.google.inject.Inject;
import com.me4502.modularframework.ShadedModularFramework;
import com.me4502.modularframework.SpongeModuleController;
import com.me4502.modularframework.exception.ModuleNotInstantiatedException;
import com.me4502.modularframework.module.ModuleWrapper;
import com.me4502.modularframework.module.SpongeModuleWrapper;
import com.minecraftonline.util.CBXScheduler;
import com.minecraftonline.util.ChangeBlockPostListenerManager;
import com.sk89q.craftbook.core.CraftBookAPI;
import com.sk89q.craftbook.core.Mechanic;
import com.sk89q.craftbook.core.st.SelfTriggerManager;
import com.sk89q.craftbook.core.util.RegexUtil;
import com.sk89q.craftbook.core.util.documentation.DocumentationGenerator;
import com.sk89q.craftbook.core.util.documentation.DocumentationProvider;
import com.sk89q.craftbook.sponge.command.AboutCommand;
import com.sk89q.craftbook.sponge.command.ReportCommand;
import com.sk89q.craftbook.sponge.command.docs.GenerateDocsCommand;
import com.sk89q.craftbook.sponge.command.docs.GetDocsCommand;
import com.sk89q.craftbook.sponge.st.SelfTriggeringMechanic;
import com.sk89q.craftbook.sponge.st.SpongeSelfTriggerManager;
import com.sk89q.craftbook.sponge.util.Metrics;
import com.sk89q.craftbook.sponge.util.data.CraftBookData;
import com.sk89q.craftbook.sponge.util.locale.TranslationsManager;
import com.sk89q.craftbook.sponge.util.type.TypeSerializers;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.slf4j.Logger;
import org.spongepowered.api.GameState;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.text.Text;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

@Plugin(id = "craftbook", name = "CraftBook",
        description = "CraftBook adds a number of new mechanics to Minecraft with no client mods required.")
public class CraftBookPlugin extends CraftBookAPI {

    public static final int MINECRAFT_WORLD_HEIGHT_MAX = 255;
    public static final int MINECRAFT_WORLD_HEIGHT_MIN = 0;

    //public static boolean isTestEnviroment = false;

    /* Configuration Data */

    @Inject
    public CraftBookPlugin() {
        setInstance(this);
    }

    @Inject
    @DefaultConfig(sharedRoot = false)
    private File mainConfig;

    @Inject
    @ConfigDir(sharedRoot = true)
    private Path sharedConfigPath;

    @Inject
    private Metrics metrics;

    @Inject
    @DefaultConfig(sharedRoot = false)
    private ConfigurationLoader<CommentedConfigurationNode> configManager;

    @Inject
    public PluginContainer container;

    public SpongeModuleController<CraftBookPlugin> moduleController;

    private SelfTriggerManager selfTriggerManager;

    //private SpongeCommandManager commandManager;

    protected SpongeConfiguration config;

    ConfigurationOptions configurationOptions;

    private final CBXScheduler scheduler = new CBXScheduler();

    private final ChangeBlockPostListenerManager changeBlockPostListenerManager = new ChangeBlockPostListenerManager();

    public static String BUILD_NUMBER = "UNKNOWN";
    public static String GIT_HASH = "UNKNOWN";

    /* Logging */

    @Inject
    private Logger logger;

    @Override
    public Logger getLogger() {
        return logger;
    }

    public CBXScheduler getScheduler() {
        return scheduler;
    }

    public ChangeBlockPostListenerManager getChangeBlockPostListenerManager() {
        return this.changeBlockPostListenerManager;
    }

    @Override
    public String getVersionString() {
        return this.container.getVersion().orElse("UNKNOWN") + '-' + BUILD_NUMBER + '-' + GIT_HASH;
    }

    public PluginContainer getContainer() {
        return this.container;
    }

    @Listener
    public void onPreInitialization(GamePreInitializationEvent event) {
        logger.info("Performing Pre-Initialization");

        // Load build information.
        container.getAsset("build.txt").ifPresent(asset -> {
            try {
                for (String line : asset.readLines()) {
                    if (line.startsWith("hash=")) {
                        if (line.contains("-")) {
                            String[] bits = RegexUtil.MINUS_PATTERN.split(line.substring(5));
                            BUILD_NUMBER = bits[0];
                            GIT_HASH = bits[1];
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        TypeSerializers.registerDefaults();
        CraftBookData.registerData();

        Sponge.getEventManager().registerListeners(this, changeBlockPostListenerManager);

        discoverMechanics();

        loadConfig();

        if(config.dataOnlyMode.getValue()) {
            logger.info("Halting CraftBook Initialization - Data Only Mode! Note: Nothing will work.");
            return;
        }

        //commandManager = new SpongeCommandManager(getContainer());
        //commandManager.enableUnstableAPI("help");


        loadMechanics(event.getState());
    }

    @Listener
    public void onInitialize(GameInitializationEvent event) {
        if(config.dataOnlyMode.getValue()) {
            logger.info("Halting CraftBook Initialization - Data Only Mode! Note: Nothing will work.");
            return;
        }

        logger.info("Starting CraftBook");

        CommandSpec generateDocsCommandSpec = CommandSpec.builder()
                .description(Text.of("Generates Documentation"))
                .permission("craftbook.docs.generate")
                .executor(new GenerateDocsCommand())
                .build();

        CommandSpec getDocsCommandSpec = CommandSpec.builder()
                .description(Text.of("Gets a Link to the Documentation"))
                .permission("craftbook.docs.get")
                .executor(new GetDocsCommand())
                .build();

        CommandSpec docsCommandSpec = CommandSpec.builder()
                .description(Text.of("Docs Base Command"))
                .permission("craftbook.docs")
                .child(generateDocsCommandSpec, "generate", "make", "build")
                .child(getDocsCommandSpec, "get", "help", "link")
                .build();

        CommandSpec aboutCommandSpec = CommandSpec.builder()
                .description(Text.of("CraftBook About Command"))
                .permission("craftbook.about")
                .executor(new AboutCommand())
                .build();

        CommandSpec reportCommandSpec = CommandSpec.builder()
                .description(Text.of("CraftBook Report Command"))
                .permission("craftbook.report")
                .arguments(
                        GenericArguments.flags().permissionFlag("craftbook.report.pastebin", "p").buildWith(GenericArguments.none())
                )
                .executor(new ReportCommand())
                .build();

        CommandSpec craftBookCommandSpec = CommandSpec.builder()
                .description(Text.of("CraftBook Base Command"))
                .permission("craftbook.craftbook")
                .child(docsCommandSpec, "docs", "manual", "man", "documentation", "doc", "help")
                .child(aboutCommandSpec, "about", "version", "ver")
                .child(reportCommandSpec, "report", "dump")
                .build();

        Sponge.getCommandManager().register(this, craftBookCommandSpec, "cb", "craftbook");

        loadMechanics(event.getState());

        saveConfig(); //Do initial save of config.
    }

    @Listener
    public void onServerStarting(GameStartingServerEvent event) {
        if(config.dataOnlyMode.getValue()) {
            logger.info("Halting CraftBook Initialization - Data Only Mode! Note: Nothing will work.");
            return;
        }
        loadMechanics(event.getState());

        Sponge.getScheduler().createTaskBuilder().intervalTicks(1).execute(scheduler::tickSync).submit(this);
    }

    @Listener
    public void onServerStarted(GameStartedServerEvent event) {
        if(config.dataOnlyMode.getValue()) {
            logger.info("Halting CraftBook Initialization - Data Only Mode! Note: Nothing will work.");
            return;
        }
        loadMechanics(event.getState());
    }

    @Listener
    public void onServerReload(GameReloadEvent event) {
        disableMechanics();
        loadConfig();
        for (GameState state : GameState.values()) {
            loadMechanics(state);
        }
    }

    @Listener
    public void onServerStopping(GameStoppingServerEvent event) {
        saveConfig();

        disableMechanics();
    }

    private void loadConfig() {
        config = new SpongeConfiguration(this, mainConfig, configManager);

        configurationOptions = ConfigurationOptions.defaults();

        logger.info("Loading Configuration");

        config.load();
        TranslationsManager.initialize();
    }

    public void saveConfig() {
        config.save();
    }

    @Override
    public void discoverMechanics() {
        logger.info("Enumerating Mechanics");

        moduleController = ShadedModularFramework.registerModuleController(this, Sponge.getGame());
        File configDir = new File(getWorkingDirectory(), "mechanics");
        configDir.mkdir();
        moduleController.setConfigurationDirectory(configDir);
        moduleController.setConfigurationOptions(configurationOptions);
        moduleController.setOverrideConfigurationNode(true);

        //Standard Mechanics
        registerModule("com.sk89q.craftbook.sponge.mechanics.variable.Variables", GameState.PRE_INITIALIZATION);
        registerModule("com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager", GameState.PRE_INITIALIZATION);
        registerModule("com.sk89q.craftbook.sponge.mechanics.BetterPhysics");
        registerModule("com.sk89q.craftbook.sponge.mechanics.BetterPlants");
        registerModule("com.sk89q.craftbook.sponge.mechanics.BounceBlocks");
        registerModule("com.sk89q.craftbook.sponge.mechanics.Chairs");
        registerModule("com.sk89q.craftbook.sponge.mechanics.ChunkAnchor");
        registerModule("com.sk89q.craftbook.sponge.mechanics.CookingPot", GameState.PRE_INITIALIZATION);
        registerModule("com.sk89q.craftbook.sponge.mechanics.CommandSigns");
        registerModule("com.sk89q.craftbook.sponge.mechanics.Elevator");
        registerModule("com.sk89q.craftbook.sponge.mechanics.Snow");
        registerModule("com.sk89q.craftbook.sponge.mechanics.area.Bridge");
        registerModule("com.sk89q.craftbook.sponge.mechanics.area.Door");
        registerModule("com.sk89q.craftbook.sponge.mechanics.area.Gate");
        registerModule("com.sk89q.craftbook.sponge.mechanics.area.complex.ComplexArea");
        registerModule("com.sk89q.craftbook.sponge.mechanics.Bookshelf");
        registerModule("com.sk89q.craftbook.sponge.mechanics.Footprints");
        registerModule("com.sk89q.craftbook.sponge.mechanics.HeadDrops");
        registerModule("com.sk89q.craftbook.sponge.mechanics.HiddenSwitch");
        registerModule("com.sk89q.craftbook.sponge.mechanics.LightStone");
        registerModule("com.sk89q.craftbook.sponge.mechanics.Teleporter");
        registerModule("com.sk89q.craftbook.sponge.mechanics.treelopper.TreeLopper");
        registerModule("com.sk89q.craftbook.sponge.mechanics.PaintingSwitcher");
        registerModule("com.sk89q.craftbook.sponge.mechanics.pipe.Pipes");
        registerModule("com.sk89q.craftbook.sponge.mechanics.LightSwitch");
        registerModule("com.sk89q.craftbook.sponge.mechanics.Marquee");
        registerModule("com.sk89q.craftbook.sponge.mechanics.signcopier.SignCopier");
        registerModule("com.sk89q.craftbook.sponge.mechanics.dispenser.DispenserRecipes");
        registerModule("com.sk89q.craftbook.sponge.mechanics.XPStorer", GameState.PRE_INITIALIZATION);

        //Circuit Mechanics
        registerModule("com.sk89q.craftbook.sponge.mechanics.Ammeter");
        registerModule("com.sk89q.craftbook.sponge.mechanics.ics.ICSocket", GameState.PRE_INITIALIZATION);
        registerModule("com.sk89q.craftbook.sponge.mechanics.powerable.GlowStone");
        registerModule("com.sk89q.craftbook.sponge.mechanics.powerable.Netherrack");
        registerModule("com.sk89q.craftbook.sponge.mechanics.powerable.JackOLantern");
        registerModule("com.sk89q.craftbook.sponge.mechanics.powerable.RedstoneJukebox");

        //Vehicle Mechanics
        //Boat
        registerModule("com.sk89q.craftbook.sponge.mechanics.boat.EmptyDecay");
        registerModule("com.sk89q.craftbook.sponge.mechanics.boat.ExitRemover");
        registerModule("com.sk89q.craftbook.sponge.mechanics.boat.LandBoats");
        registerModule("com.sk89q.craftbook.sponge.mechanics.boat.RemoveEntities");
        registerModule("com.sk89q.craftbook.sponge.mechanics.boat.SpeedModifiers");
        registerModule("com.sk89q.craftbook.sponge.mechanics.boat.WaterPlaceOnly");
        //Minecart
        registerModule("com.sk89q.craftbook.sponge.mechanics.minecart.EmptyDecay");
        registerModule("com.sk89q.craftbook.sponge.mechanics.minecart.ExitRemover");
        registerModule("com.sk89q.craftbook.sponge.mechanics.minecart.ItemPickup");
        registerModule("com.sk89q.craftbook.sponge.mechanics.minecart.MobBlocker");
        registerModule("com.sk89q.craftbook.sponge.mechanics.minecart.MoreRails");
        registerModule("com.sk89q.craftbook.sponge.mechanics.minecart.NoCollide");
        registerModule("com.sk89q.craftbook.sponge.mechanics.minecart.RemoveEntities");
        //Minecart - Block
        registerModule("com.sk89q.craftbook.sponge.mechanics.minecart.block.CartEjector");
        registerModule("com.sk89q.craftbook.sponge.mechanics.minecart.block.CartMessenger");
        registerModule("com.sk89q.craftbook.sponge.mechanics.minecart.block.CartReverser");

        // Legacy
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartCollect");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartDeposit");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartPrint");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartSort");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartLift");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartCraft");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.Station");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.StationClear");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartLaunch");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartLoad");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartDispenser");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartDelay");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartBooster");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartDirection");

        registerModule("com.minecraftonline.legacy.mechanics.PageReader");
        registerModule("com.minecraftonline.legacy.mechanics.MapCopier");
        registerModule("com.minecraftonline.legacy.mechanics.cbwarps.CBWarps");
        registerModule("com.minecraftonline.legacy.mechanics.minecart.CartWarp");
        registerModule("com.minecraftonline.legacy.mechanics.LightNetherrack");
        registerModule("com.minecraftonline.mechanic.megapipes.MegaPipes");


        logger.info("Found " + moduleController.getModules().size());
    }

    private void registerModule(String className) {
        registerModule(className, GameState.SERVER_STARTING);
    }

    private void registerModule(String className, GameState loadState) {
        moduleController.registerModule(className, loadState);
    }

    private void loadMechanics(GameState loadState) {
        moduleController.enableModules(input -> {
            if (loadState == ((SpongeModuleWrapper) input).getLoadState() && (config.enabledMechanics.getValue().contains(input.getName())
                    || "true".equalsIgnoreCase(System.getProperty("craftbook.enable-all"))
                    || "true".equalsIgnoreCase(System.getProperty("craftbook.generate-docs")))) {
                logger.debug("Enabled: " + input.getName());
                return true;
            }

            return false;
        });

        for (ModuleWrapper module : moduleController.getModules()) {
            if (!module.isEnabled()) continue;
            try {
                if (((SpongeModuleWrapper) module).getModuleUnchecked() instanceof SelfTriggeringMechanic && !getSelfTriggerManager().isPresent()) {
                    this.selfTriggerManager = new SpongeSelfTriggerManager();
                    getSelfTriggerManager().ifPresent(SelfTriggerManager::initialize);
                    break;
                }
            } catch(ModuleNotInstantiatedException e) {
                logger.error("Failed to initialize module: " + module.getName(), e);
            }
        }

        if("true".equalsIgnoreCase(System.getProperty("craftbook.generate-docs"))) {
            for (ModuleWrapper module : moduleController.getModules()) {
                if(!module.isEnabled()) continue;
                try {
                    Mechanic mechanic = (Mechanic) ((SpongeModuleWrapper) module).getModuleUnchecked();
                    if(mechanic instanceof DocumentationProvider)
                        DocumentationGenerator.generateDocumentation((DocumentationProvider) mechanic);
                } catch (ModuleNotInstantiatedException e) {
                    logger.error("Failed to generate docs for module: " + module.getName(), e);
                }
            }

            DocumentationGenerator.generateDocumentation(config);
        }
    }

    private void disableMechanics() {
        getSelfTriggerManager().ifPresent(SelfTriggerManager::unload);
        this.selfTriggerManager = null;
        moduleController.disableModules();
    }

    @Override
    public Optional<SelfTriggerManager> getSelfTriggerManager() {
        return Optional.ofNullable(this.selfTriggerManager);
    }

    public SpongeConfiguration getConfig() {
        return config;
    }

    @Override
    public File getWorkingDirectory() {
        return mainConfig.getParentFile();
    }

    public static CraftBookPlugin spongeInst() {
        return inst();
    }

    //public SpongeCommandManager getCommandManager(){return commandManager;}

}
