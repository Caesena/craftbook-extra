/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.util.data;

import static org.spongepowered.api.data.DataQuery.of;

import com.google.common.reflect.TypeToken;
import com.minecraftonline.command.debugstick.ICDebugStick;
import com.sk89q.craftbook.sponge.mechanics.blockbags.EmbeddedBlockBag;
import com.sk89q.craftbook.sponge.mechanics.ics.SerializedICData;
import com.sk89q.craftbook.sponge.util.type.TypeTokens;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.value.mutable.MutableBoundedValue;
import org.spongepowered.api.data.value.mutable.Value;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;

public class CraftBookKeys {

    public CraftBookKeys() {
    }

    public static void init() {
        // TODO: move assignments to here, for now it just force loads the class
    }

    public static Key<MutableBoundedValue<Integer>> LAST_POWER = Key.builder()
            .type(new TypeTokens.MutableBoundedValueIntegerTypeToken())
            .id("lastpower")
            .name("LastPower")
            .query(of("LastPower"))
            .build();

    public static Key<Value<SerializedICData>> IC_DATA = Key.builder()
            .type(new TypeTokens.ICValueTypeToken())
            .id("ic")
            .name("IC")
            .query(of("IC"))
            .build();

    public static Key<Value<ItemStackSnapshot>> KEY_LOCK = Key.builder()
            .type(new TypeTokens.ItemStackSnapshotValueTypeToken())
            .id("key_lock")
            .name("KeyLock")
            .query(of("KeyLock"))
            .build();

    public static Key<Value<String>> NAMESPACE = Key.builder()
            .type(new TypeToken<Value<String>>(){})
            .id("namespace")
            .name("Namespace")
            .query(of("Namespace"))
            .build();

    public static Key<Value<Long>> BLOCK_BAG = Key.builder()
            .type(new TypeTokens.LongValueTypeToken())
            .id("blockbag")
            .name("BlockBag")
            .query(of("BlockBag"))
            .build();

    public static Key<Value<EmbeddedBlockBag>> EMBEDDED_BLOCK_BAG = Key.builder()
            .type(new TypeToken<Value<EmbeddedBlockBag>>() {})
            .id("embeddedblockbag")
            .name("EmbeddedBlockBag")
            .query(of("EmbeddedBlockBag"))
            .build();

    public static Key<Value<Text>> EXTRA_IC_LINES = Key.builder()
            .type(org.spongepowered.api.util.TypeTokens.TEXT_VALUE_TOKEN)
            .id("extraiclines")
            .name("Extra IC Lines")
            .query(of("ExtraICLines"))
            .build();

    public static Key<Value<Boolean>> PLACED_BY_ADMIN = Key.builder()
            .type(org.spongepowered.api.util.TypeTokens.BOOLEAN_VALUE_TOKEN)
            .id("placedbyadmin")
            .name("Placed by admin")
            .query(of("PlacedByAdmin"))
            .build();

    public static Key<Value<Boolean>> IC_DEBUG_STICK = Key.builder()
            .type(org.spongepowered.api.util.TypeTokens.BOOLEAN_VALUE_TOKEN)
            .id("icdebugstick")
            .name("IC Debug Stick")
            .query(of("ICDebugStick"))
            .build();

    public static Key<Value<ICDebugStick.Mode>> IC_DEBUG_STICK_MODE = Key.builder()
            .type(TypeTokens.IC_DEBUG_STICK_MODE_VALUE)
            .id("icdebugstickmode")
            .name("IC Debug Stick Mode")
            .query(of("ICDebugStickMode"))
            .build();
}
