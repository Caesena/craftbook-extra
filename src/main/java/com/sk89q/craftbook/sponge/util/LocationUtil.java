/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.util;

import com.flowpowered.math.GenericMath;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.legacy.ThreadSafeReadClassInheritanceMultiMap;
import com.minecraftonline.util.parsing.item.predicate.ItemPredicate;
import com.sk89q.craftbook.core.util.RegexUtil;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.util.ClassInheritanceMultiMap;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.IBlockAccess;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.block.tileentity.carrier.Chest;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.Carrier;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.AABB;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.common.bridge.world.chunk.ChunkProviderBridge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class LocationUtil {

    /**
     * Gets an Inventory from a location.
     *
     * @param location The location to look for inventories at.
     * @return An inventory, if present.
     */
    public static Optional<Inventory> getInventoryForLocation(Location<World> location) {
        Inventory inventory = null;

        if(location.hasTileEntity()) {
            TileEntity tileEntity = location.getTileEntity().get();
            if (tileEntity instanceof Chest) {
                Chest chest = (Chest)tileEntity;
                inventory = chest.getDoubleChestInventory().orElse(chest.getInventory());
            }
            else if(tileEntity instanceof Carrier) {
                inventory = ((Carrier) tileEntity).getInventory();
            }
        }

        return Optional.ofNullable(inventory);
    }

    public static double getDistanceSquared(Location<World> location1, Location<World> location2) {
        if(!location1.getExtent().equals(location2.getExtent())) {
            return Integer.MAX_VALUE;
        } else {
            return location1.getPosition().distanceSquared(location2.getPosition());
        }
    }

    public static boolean isWithinSphericalRadius(Location<World> location1, Location<World> location2, double radius) {
        return location1.getExtent().equals(location2.getExtent())
                && Math.floor(getDistanceSquared(location1, location2)) <= radius * radius; // Floor for more accurate readings
    }

    public static boolean isWithinRadiusPolygon(Location<World> location1, Location<World> location2, Vector3d radius) {
        if(!location1.getExtent().equals(location2.getExtent())) return false;
        if(location2.getX() < location1.getX() + radius.getX() && location2.getX() > location1.getX() - radius.getX())
            if(location2.getY() < location1.getY() + radius.getY() && location2.getY() > location1.getY() - radius.getY())
                return location2.getZ() < location1.getZ() + radius.getZ() && location2.getZ() > location1.getZ() - radius.getX();
        return false;
    }

    public static boolean isWithinRadius(Location<World> location1, Location<World> location2, Vector3d radius) {
        return radius.getX() == radius.getZ() && radius.getX() == radius.getY()
                && isWithinSphericalRadius(location1, location2, radius.getFloorX())
                || (radius.getX() != radius.getY()
                || radius.getY() != radius.getZ()
                || radius.getX() != radius.getZ())
                && isWithinRadiusPolygon(location1, location2, radius);
    }

    public static Collection<Entity> getNearbyEntities(Location<World> location, Vector3d radius) {
        int chunkRadiusX = radius.getFloorX() < 16 ? 1 : radius.getFloorX() / 16;
        int chunkRadiusZ = radius.getFloorZ() < 16 ? 1 : radius.getFloorZ() / 16;
        HashSet<Entity> radiusEntities = new HashSet<>();
        for (int chX = -chunkRadiusX; chX <= chunkRadiusX; chX++) {
            for (int chZ = -chunkRadiusZ; chZ <= chunkRadiusZ; chZ++) {
                int offChunkX = location.getChunkPosition().getX() + chX;
                int offChunkZ = location.getChunkPosition().getZ() + chZ;
                location.getExtent().getChunk(offChunkX, 0, offChunkZ).ifPresent(chunk -> {
                    for (Entity e : chunk.getEntities()) {
                        if (e == null || e.isRemoved()) {
                            continue;
                        }
                        if (isWithinRadius(location, e.getLocation(), radius)) {
                            radiusEntities.add(e);
                        }
                    }
                });
            }
        }
        return radiusEntities;
    }

    @Deprecated
    public static Collection<Entity> getNearbyEntities(Location<World> location, double radius) {
        Vector3d origin = location.getPosition();
        Set<Chunk> chunks = new HashSet<>();
        World world = location.getExtent();
        // Square radius then chop later.
        for (int x = (int) -radius; x <= radius; x += Math.min(radius, 16)) {
            for (int z = (int) -radius; z <= radius; z += Math.min(radius, 16)) {
                Vector3d pos = origin.add(Vector3d.from(x, 0, z));
                Location<World> loc = new Location<>(world, pos);
                world.getChunk(loc.getChunkPosition()).ifPresent(chunks::add);
            }
        }
        // TODO: chop chunks by location before entities within them
        return chunks.stream()
                .map(Chunk::getEntities)
                .flatMap(Collection::stream)
                .filter(entity -> entity.getLocation().getPosition().distance(origin) <= radius)
                .collect(Collectors.toSet());
    }

    public static Collection<Player> getNearbyPlayers(Location<World> loc, double dist) {
        double distSquared = dist*dist;
        Vector3d origin = loc.getPosition();
        return Sponge.getServer().getOnlinePlayers().stream()
                .filter(p -> p.getLocation().getExtent().equals(loc.getExtent())
                        && p.getLocation().getPosition().distanceSquared(origin) < distSquared)
                .collect(Collectors.toList());
    }

    public static final int CHUNK_SIZE = 16;

    public static <T> Collection<T> getNearbyEntitiesByType(final Location<World> origin, final double radius, Class<T> clazz, Predicate<T> filter, boolean async) {
        final double radiusSquared = radius*radius;

        final List<T> entities = new ArrayList<>();
        final World world = origin.getExtent();

        final double locX = origin.getX();
        final double locY = origin.getY();
        final double locZ = origin.getZ();

        int chunkRadius = (int) Math.ceil(radius / CHUNK_SIZE);

        ChunkProviderBridge chunkProviderBridge = (ChunkProviderBridge) ((net.minecraft.world.World) world).getChunkProvider();

        for (int x = -chunkRadius; x <= chunkRadius; x++) {
            for (int z = -chunkRadius; z <= chunkRadius; z++) {

                // Iterate through x and z first so can re-use the chunk or skip out the y if the chunk isn't loaded
                final int xPos = (int) (locX + (x * CHUNK_SIZE));
                final int zPos = (int) (locZ + (z * CHUNK_SIZE));



                // Sponge does not provide the fine grained control we want over the entity lists
                // Also use chunk provider to skip unloaded or pending unloading chunks.
                final net.minecraft.world.chunk.Chunk mcChunk = chunkProviderBridge.bridge$getLoadedChunkWithoutMarkingActive(xPos >> 4, zPos >> 4);

                if (mcChunk == null || mcChunk.unloadQueued) {
                    continue;
                }

                ChunkPos chunkPos = mcChunk.getPos();

                final double minXDist = getDistanceToChunkOnAxis(locX, chunkPos.getXStart(), chunkPos.getXEnd());
                final double minZDist = getDistanceToChunkOnAxis(locZ, chunkPos.getZStart(), chunkPos.getZEnd());

                final double dist2d = minXDist*minXDist + minZDist*minZDist;
                //System.out.println("dist2d: " + dist2d + " vs " + radiusSquared);
                if (dist2d > radiusSquared) {
                    continue;
                }

                for (int y = -chunkRadius; y <= chunkRadius; y++) {
                    final int yPos = (int) (locY + (y * CHUNK_SIZE));

                    if (yPos < CraftBookPlugin.MINECRAFT_WORLD_HEIGHT_MIN || yPos > CraftBookPlugin.MINECRAFT_WORLD_HEIGHT_MAX) {
                        continue;
                    }

                    // 16 sub-chunks in a chunk, which have their own lists.
                    final int subChunk = yPos >> 4;

                    final int subChunkWorldCord = subChunk * CHUNK_SIZE;
                    final int subChunkWorldCordMax = subChunkWorldCord + 15;
                    final double minYDist = getDistanceToChunkOnAxis(locY, subChunkWorldCord, subChunkWorldCordMax);

                    double dist3d = dist2d + (minYDist*minYDist);

                    //System.out.println("dist 3d: " + dist3d + " vs " + radiusSquared);

                    if (dist3d > radiusSquared) {
                        continue;
                    }

                    //System.out.println("Adding all from chunk: " + mcChunk.getPos() + " (subChunk " + subChunk + ")");

                    // Items are actually mapped by a class inheritance map and by sub-chunks.
                    ClassInheritanceMultiMap<net.minecraft.entity.Entity> map = mcChunk.getEntityLists()[subChunk];
                    Iterable<T> itemsIterable = async ? ((ThreadSafeReadClassInheritanceMultiMap)map).getCopyByClass(clazz) : map.getByClass(clazz);
                    double distanceAllowingSlightlyExtra = radiusSquared + (10*GenericMath.DBL_EPSILON); // We love floaty points.
                    for (final T mcItem : itemsIterable) {
                        net.minecraft.entity.Entity mcEntity = (net.minecraft.entity.Entity) mcItem;
                        double dist = mcEntity.getDistanceSq(locX, locY, locZ);
                        if (!mcEntity.isDead && dist <= distanceAllowingSlightlyExtra && filter.test(mcItem)) {
                            entities.add(mcItem);
                        }
                    }
                }
            }
        }
        return entities;
    }

    public static <T> Collection<T> getNearbyEntitiesByType(final Location<World> origin, final double radius, Class<T> clazz) {
        return getNearbyEntitiesByType(origin, radius, clazz, a -> true, false);
    }

    public static <T> Collection<T> getNearbyEntitiesByTypeAsync(final Location<World> origin, final double radius, Class<T> clazz) {
        return getNearbyEntitiesByType(origin, radius, clazz, a -> true, true);
    }

    public static Collection<net.minecraft.entity.Entity> getNearbyEntities(final Location<World> origin, final double radius, boolean async) {
        return getNearbyEntitiesByType(origin, radius, net.minecraft.entity.Entity.class, e -> true, async);
    }

    public static Collection<EntityItem> getNearbyItems(final Location<World> origin, final double radius) {
        return getNearbyEntitiesByType(origin, radius, EntityItem.class);
    }

    public static Collection<EntityItem> getNearbyItems(final Location<World> origin, final double radius, ItemPredicate itemPredicate) {
        return getNearbyEntitiesByType(origin, radius, EntityItem.class, i -> itemPredicate.test((ItemStack) (Object) i.getItem()), false);
    }

    public static Collection<EntityItem> getNearbyItemsAsync(final Location<World> origin, final double radius, ItemPredicate itemPredicate) {
        return getNearbyEntitiesByType(origin, radius, EntityItem.class, i -> itemPredicate.test((ItemStack) (Object) i.getItem()), true);
    }

    public static double getDistanceToChunkOnAxis(double loc, double chunkMin, double chunkMax) {
        if (chunkMin <= loc && loc <= chunkMax) {
            return 0; // In the chunk
        }

        double distToMin = Math.abs(chunkMin - loc);
        double distToMax = Math.abs(chunkMax - loc);
        return Math.min(
                distToMin,
                distToMax
        );
    }

    public static Set<Chunk> getChunksInRadius(Location<World> origin, double radius) {
        final double radiusSquared = radius*radius;

        final Set<Chunk> chunks = new HashSet<>();
        final World world = origin.getExtent();

        final double locX = origin.getX();
        final double locZ = origin.getZ();

        int chunkRadius = (int) Math.ceil(radius / CHUNK_SIZE);

        for (int x = -chunkRadius; x <= chunkRadius; x++) {
            for (int z = -chunkRadius; z <= chunkRadius; z++) {

                // Iterate through x and z first so can re-use the chunk or skip out the y if the chunk isn't loaded
                final int xPos = (int) (locX + (x * CHUNK_SIZE));
                final int zPos = (int) (locZ + (z * CHUNK_SIZE));
                final Optional<Chunk> chunk = world.getChunk(xPos >> 4, 0, zPos >> 4);
                if (!chunk.isPresent()) {
                    continue;
                }
                // Sponge does not provide the fine grained control we want over the entity lists
                final net.minecraft.world.chunk.Chunk mcChunk = (net.minecraft.world.chunk.Chunk) chunk.get();

                ChunkPos chunkPos = mcChunk.getPos();

                final double minXDist = getDistanceToChunkOnAxis(locX, chunkPos.getXStart(), chunkPos.getXEnd());
                final double minZDist = getDistanceToChunkOnAxis(locZ, chunkPos.getZStart(), chunkPos.getZEnd());

                final double dist2d = minXDist*minXDist + minZDist*minZDist;
                //System.out.println("dist2d: " + dist2d + " vs " + radiusSquared);
                if (dist2d > radiusSquared) {
                    continue;
                }

                chunks.add(chunk.get());
            }
        }
        return chunks;
    }

    public static Set<net.minecraft.world.chunk.Chunk> getChunksWithoutMarkingActiveInRadius(Location<World> origin, double radius) {
        final double radiusSquared = radius*radius;

        final Set<net.minecraft.world.chunk.Chunk> chunks = new HashSet<>();
        final net.minecraft.world.World mcWorld = (net.minecraft.world.World) origin.getExtent();

        final double locX = origin.getX();
        final double locZ = origin.getZ();

        int chunkRadius = (int) Math.ceil(radius / CHUNK_SIZE);

        ChunkProviderBridge chunkProviderBridge = (ChunkProviderBridge) mcWorld.getChunkProvider();

        for (int x = -chunkRadius; x <= chunkRadius; x++) {
            for (int z = -chunkRadius; z <= chunkRadius; z++) {

                // Iterate through x and z first so can re-use the chunk or skip out the y if the chunk isn't loaded
                final int xPos = (int) (locX + (x * CHUNK_SIZE));
                final int zPos = (int) (locZ + (z * CHUNK_SIZE));
                net.minecraft.world.chunk.Chunk mcChunk = chunkProviderBridge.bridge$getLoadedChunkWithoutMarkingActive(xPos >> 4, zPos >> 4);

                final ChunkPos chunkPos;
                if (mcChunk == null) {
                    chunkPos = new ChunkPos(xPos >> 4, zPos >> 4);
                }
                else {
                    chunkPos = mcChunk.getPos();
                }


                final double minXDist = getDistanceToChunkOnAxis(locX, chunkPos.getXStart(), chunkPos.getXEnd());
                final double minZDist = getDistanceToChunkOnAxis(locZ, chunkPos.getZStart(), chunkPos.getZEnd());

                final double dist2d = minXDist*minXDist + minZDist*minZDist;
                //System.out.println("dist2d: " + dist2d + " vs " + radiusSquared);
                if (dist2d > radiusSquared) {
                    continue;
                }

                chunks.add(mcChunk);
            }
        }
        return chunks;
    }

    public static Set<Vector3i> getChunkPositionsInRadius(Location<World> origin, double radius) {
        final double radiusSquared = radius*radius;

        final Set<Vector3i> chunks = new HashSet<>();
        final net.minecraft.world.World mcWorld = (net.minecraft.world.World) origin.getExtent();

        final double locX = origin.getX();
        final double locZ = origin.getZ();

        int chunkRadius = (int) Math.ceil(radius / CHUNK_SIZE);

        for (int x = -chunkRadius; x <= chunkRadius; x++) {
            for (int z = -chunkRadius; z <= chunkRadius; z++) {

                // Iterate through x and z first so can re-use the chunk or skip out the y if the chunk isn't loaded
                final int xPos = (int) (locX + (x * CHUNK_SIZE));
                final int zPos = (int) (locZ + (z * CHUNK_SIZE));

                final ChunkPos chunkPos = new ChunkPos(xPos >> 4, zPos >> 4);

                final double minXDist = getDistanceToChunkOnAxis(locX, chunkPos.getXStart(), chunkPos.getXEnd());
                final double minZDist = getDistanceToChunkOnAxis(locZ, chunkPos.getZStart(), chunkPos.getZEnd());

                final double dist2d = minXDist*minXDist + minZDist*minZDist;
                //System.out.println("dist2d: " + dist2d + " vs " + radiusSquared);
                if (dist2d > radiusSquared) {
                    continue;
                }

                chunks.add(new Vector3i(xPos >> 4, 0, zPos >> 4));
            }
        }
        return chunks;
    }

    /**
     * Uses chunks to get tile entities nearby.
     *
     * @param origin Origin to centre search around
     * @param radius Radius to search
     * @return All nearby tile entities.
     */
    public static Collection<TileEntity> getNearbyTileEntities(final Location<World> origin, final double radius) {

        final double radiusSquared = radius*radius;

        final List<TileEntity> tileEntities = new ArrayList<>();
        final World world = origin.getExtent();

        final double locX = origin.getX();
        final double locY = origin.getY();
        final double locZ = origin.getZ();

        int chunkRadius = (int) Math.ceil(radius / CHUNK_SIZE);
        for (int x = -chunkRadius; x <= chunkRadius; x++) {
            for (int z = -chunkRadius; z <= chunkRadius; z++) {
                // Iterate through x and z first so can re-use the chunk or skip out the y if the chunk isn't loaded
                final int xPos = (int) (locX + (x * CHUNK_SIZE));
                final int zPos = (int) (locZ + (z * CHUNK_SIZE));
                final Optional<Chunk> chunk = world.getChunk(xPos >> 4, 0, zPos >> 4);
                if (!chunk.isPresent()) {
                    continue;
                }
                // Sponge does not provide the fine grained control we want over the tile entity lists

                final net.minecraft.world.chunk.Chunk mcChunk = (net.minecraft.world.chunk.Chunk) chunk.get();

                ChunkPos chunkPos = mcChunk.getPos();

                final double minXDist = getDistanceToChunkOnAxis(locX, chunkPos.getXStart(), chunkPos.getXEnd());
                final double minZDist = getDistanceToChunkOnAxis(locZ, chunkPos.getZStart(), chunkPos.getZEnd());

                final double dist2d = minXDist*minXDist + minZDist*minZDist;
                if (dist2d > radiusSquared) {
                    continue;
                }

                for (Map.Entry<BlockPos, net.minecraft.tileentity.TileEntity> entry : mcChunk.getTileEntityMap().entrySet()) {
                    if (entry.getKey().distanceSq(locX, locY, locZ) > radiusSquared) {
                        continue;
                    }
                    tileEntities.add((TileEntity) entry.getValue());
                }
            }
        }

        return tileEntities;
    }

    public static boolean isEntityInLocation(Location<World> location, Entity entity) {
        AABB boundingBox = entity.getBoundingBox().orElse(null);
        return boundingBox != null && boundingBox.intersects(new AABB(location.getBlockPosition(), location.getBlockPosition().add(1, 1, 1)));
    }

    public static Collection<Entity> getEntitiesAtLocation(Location<World> location) {
        Chunk chunk = location.getExtent().getChunk(location.getChunkPosition()).orElse(null);
        if (chunk == null) {
            return Collections.emptyList();
        }
        return chunk.getEntities(entity -> isEntityInLocation(location, entity));
    }

    public static boolean isLocationWithinWorld(Location<World> location) {
        return location.getBlockY() < location.getExtent().getBlockMax().getY() && location.getBlockY() >= location.getExtent().getBlockMin().getY();
    }

    public static Location<World> locationFromString(String string) {
        String[] parts = RegexUtil.COMMA_PATTERN.split(string);
        if(parts.length < 4)
            return null;

        World world = Sponge.getGame().getServer().getWorld(parts[0]).orElse(null);
        if(world == null)
            return null;

        double x = Double.parseDouble(parts[1]);
        double y = Double.parseDouble(parts[2]);
        double z = Double.parseDouble(parts[3]);

        return world.getLocation(x, y, z);
    }

    /**
     * Gets the offset of the blocks location based on the coordiante grid.
     *
     * @param block   to get offsetfrom
     * @param offsetX to add
     * @param offsetY to add
     * @param offsetZ to add
     *
     * @return block offset by given coordinates
     */
    public static Location<World> getOffset(Location<World> block, int offsetX, int offsetY, int offsetZ) {
        return block.getExtent().getLocation(block.getX() + offsetX, block.getY() + offsetY, block.getZ() + offsetZ);
    }

    public static Location<World> getRelativeOffset(Location<World> block, int offsetX, int offsetY, int offsetZ) {
        return getRelativeOffset(SignUtil.getBackBlock(block),
                SignUtil.getFacing(block),
                offsetX, offsetY, offsetZ);
    }

    /**
     * Gets the block located relative to the signs front. That means that when the sign is attached to a block and
     * the player is looking at it it
     * will add the offsetX to left or right, offsetY is added up or down and offsetZ is added front or back.
     *
     * @param block   to get relative position from
     * @param front   to work with
     * @param offsetX amount to move left(negative) or right(positive)
     * @param offsetY amount to move up(positive) or down(negative)
     * @param offsetZ amount to move back(negative) or front(positive)
     *
     * @return block located at the relative offset position
     */
    public static Location<World> getRelativeOffset(Location<World> block, Direction front, int offsetX, int offsetY, int offsetZ) {
        Direction back;
        Direction right;
        Direction left;

        switch (front) {

            case SOUTH:
                back = Direction.NORTH;
                left = Direction.EAST;
                right = Direction.WEST;
                break;
            case WEST:
                back = Direction.EAST;
                left = Direction.SOUTH;
                right = Direction.NORTH;
                break;
            case NORTH:
                back = Direction.SOUTH;
                left = Direction.WEST;
                right = Direction.EAST;
                break;
            case EAST:
                back = Direction.WEST;
                left = Direction.NORTH;
                right = Direction.SOUTH;
                break;
            default:
                back = Direction.SOUTH;
                left = Direction.EAST;
                right = Direction.WEST;
        }

        // apply left and right offset
        if (offsetX > 0) {
            block = getRelativeBlock(block, right, offsetX);
        } else if (offsetX < 0) {
            block = getRelativeBlock(block, left, offsetX);
        }

        // apply front and back offset
        if (offsetZ > 0) {
            block = getRelativeBlock(block, front, offsetZ);
        } else if (offsetZ < 0) {
            block = getRelativeBlock(block, back, offsetZ);
        }

        // apply up and down offset
        if (offsetY > 0) {
            block = getRelativeBlock(block, Direction.UP, offsetY);
        } else if (offsetY < 0) {
            block = getRelativeBlock(block, Direction.DOWN, offsetY);
        }
        return block;
    }

    /**
     * Get relative block X that way.
     *
     * @param block The location
     * @param facing The direction
     * @param amount The amount
     *
     * @return The block
     */
    private static Location<World> getRelativeBlock(Location<World> block, Direction facing, int amount) {

        amount = Math.abs(amount);
        for (int i = 0; i < amount; i++) {
            block = block.getRelative(facing);
        }
        return block;
    }

    public static String stringFromLocation(Location<World> location) {
        return location.getExtent().getName() + ',' + location.getPosition().getX() + ',' + location.getPosition().getY() + ',' + location.getPosition().getZ();
    }

    /**
     * Takes a cartesian {@link Vector3d} and returns a euler {@link Vector3d}
     *
     * @return A euler vector3d
     */
    public static Vector3d cartesianToEuler(Vector3d cartesian) {
        double x = cartesian.getX();
        double y = cartesian.getY();
        double z = cartesian.getZ();

        if (x == 0 && z == 0) {
            return Vector3d.ZERO;
        }

        double theta = Math.atan2(-x, z);
        float yaw = (float) Math.toDegrees((theta + (2 * Math.PI)) % (2 * Math.PI));
        double xz = Math.sqrt(x*x + z*z);
        float pitch = (float) Math.toDegrees(Math.atan(-y / xz));

        return new Vector3d(pitch, yaw, 0);
    }

    public static Direction getDirectionFromYaw(final double yawIn) {
        final double yaw = Math.floorMod((int) yawIn, 360); // true modulo, as javas modulo is weird for negative values
        if (yaw < 45 || yaw > 270+45) {
            return Direction.SOUTH;
        }
        else if (yaw < 90+45) {
            return Direction.WEST;
        }
        else if (yaw < 180+45) {
            return Direction.NORTH;
        }
        else if (yaw < 270+45) {
            return Direction.EAST;
        }
        else {
            return Direction.NONE;
        }
    }


    public static Optional<Location<World>> findEmptySpot(Location<World> startLocation) {
        for (Location<World> loc = startLocation; loc.getY() <= CraftBookPlugin.MINECRAFT_WORLD_HEIGHT_MAX; loc = loc.add(0,1,0)) {
            BlockType blockType = loc.getBlockType();
            Block block = (Block) blockType;

            if (block.isPassable((IBlockAccess) loc.getExtent(), new BlockPos(loc.getX(), loc.getY(), loc.getZ())))
                return Optional.of(loc);
        }
        return Optional.empty();
    }

    public static Optional<Location<World>> findEmptyPlayerSpot(Location<World> startLocation) {
        IBlockAccess iBlockAccess = (IBlockAccess) startLocation.getExtent();
        for (Location<World> loc = startLocation; loc.getY() <= CraftBookPlugin.MINECRAFT_WORLD_HEIGHT_MAX; loc = loc.add(0,1,0)) {
            BlockType blockType = loc.getBlockType();
            Block block = (Block) blockType;

            if (block.isPassable(iBlockAccess, new BlockPos(loc.getX(), loc.getY(), loc.getZ()))
            && ((Block) loc.add(0, 1, 0).getBlockType()).isPassable(iBlockAccess, new BlockPos(loc.getX(), loc.getY() + 1, loc.getZ())))
                return Optional.of(loc);
        }
        return Optional.empty();
    }
}
