/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics;

import com.flowpowered.math.vector.Vector3d;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.PermissionNode;
import com.sk89q.craftbook.core.util.documentation.DocumentationProvider;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeSignMechanic;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import com.sk89q.craftbook.sponge.util.locale.TranslationsManager;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;

@Module(id = "elevator", name = "Elevator", onEnable="onInitialize", onDisable="onDisable")
public class Elevator extends SpongeSignMechanic implements DocumentationProvider {

    private final ConfigValue<Boolean> allowJumpLifts = new ConfigValue<>("allow-jump-lifts", "Allow lifts that the user can control by jumping, or sneaking.", true);
    private final ConfigValue<Boolean> allowButtonLifts = new ConfigValue<>("allow-button-lifts", "Allow lifts to be controlled by buttons opposite the sign.", true);
    private final ConfigValue<Integer> heightTolerance = new ConfigValue<>("height-tolerance", "How high a sign can be off the ground before saying there is no floor.", 5);

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.elevator", "Allows the user to create Elevators", PermissionDescription.ROLE_USER);
    private final SpongePermissionNode usePermissions = new SpongePermissionNode("craftbook.elevator.use", "Allows the user to use Elevators", PermissionDescription.ROLE_USER);

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        allowJumpLifts.load(config);
        allowButtonLifts.load(config);
        heightTolerance.load(config);

        createPermissions.register();
        usePermissions.register();
    }

    @Override
    public String[] getValidSigns() {
        return new String[] {
                "[Lift Down]", "[Lift Up]", "[Lift]", "[Lift UpDown]"
        };
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return this.createPermissions;
    }

    @Listener
    public void onPlayerInteract(InteractBlockEvent.Secondary.MainHand event, @First Player player) {
        event.getTargetBlock().getLocation().ifPresent((location) -> {
            Location<World> signLocation = location;

            if (allowButtonLifts.getValue() &&
                    signLocation.getBlockType() == BlockTypes.STONE_BUTTON || signLocation.getBlockType() == BlockTypes.WOODEN_BUTTON) {
                Direction backDir = SignUtil.getBack(signLocation);
                signLocation = signLocation.getRelative(backDir).getRelative(backDir);
            }

            if (SignUtil.isSign(signLocation)) {
                Sign sign = (Sign) signLocation.getTileEntity().get();

                Optional<Vector3d> interactionPoint = event.getInteractionPoint();

                // In my opinion we should handle 0.5, so lets make it so that you go down, not that you could ever actually click directly in the middle
                boolean down = "[Lift Down]".equals(SignUtil.getTextRaw(sign, 1)) || ("[Lift UpDown]".equals(SignUtil.getTextRaw(sign, 1)) && interactionPoint.isPresent() && interactionPoint.get().getY() <= 0.5);

                if (down || "[Lift Up]".equals(SignUtil.getTextRaw(sign, 1)) || ("[Lift UpDown]".equals(SignUtil.getTextRaw(sign, 1)) && interactionPoint.isPresent() && interactionPoint.get().getY() > 0.5)) {
                    if (!usePermissions.hasPermission(player)) {
                        player.sendMessage(TranslationsManager.USE_PERMISSIONS);
                        return;
                    }
                    event.setCancelled(true);
                    transportEntity(player, location, down ? Direction.DOWN : Direction.UP);
                }
            }
        });
    }

    @Listener
    public void onEntityMove(MoveEntityEvent event) {
        if(!allowJumpLifts.getValue())
            return;

        if(!LocationUtil.isLocationWithinWorld(event.getToTransform().getLocation()))
            return;

        Location<World> groundLocation = event.getToTransform().getLocation().getRelative(Direction.DOWN);

        //Look for dat sign
        for (Location<World> location : SignUtil.getAttachedSigns(groundLocation)) {
            Sign sign = (Sign) location.getTileEntity().orElse(null);

            if (sign != null && "[Lift UpDown]".equals(SignUtil.getTextRaw(sign, 1))) {
                if (event.getTargetEntity() instanceof Subject) {
                    if (!usePermissions.hasPermission((Subject) event.getTargetEntity())) {
                        return;
                    }
                }

                if (event.getToTransform().getPosition().getY() > event.getFromTransform().getPosition().getY()) {
                    transportEntity(event.getTargetEntity(), location, Direction.UP); //Jump is up
                } else if (event.getTargetEntity().get(Keys.IS_SNEAKING).orElse(false)) {
                    transportEntity(event.getTargetEntity(), location, Direction.DOWN); //Sneak is down
                }

                break;
            }
        }
    }

    private void transportEntity(Entity entity, Location<World> block, Direction direction) {
        Location<World> orginalDestination = findDestination(block, direction);

        if (orginalDestination == block) {
            if (entity instanceof CommandSource) ((CommandSource) entity).sendMessage(Text.of("This lift has no destination!"));
            return; // This elevator has no destination.
        }
        int foundFree = 0;
        boolean foundGround = false;
        Location<World> entityPos = entity.getLocation();
        Vector3d destinationPos = new Vector3d(entityPos.getX(), orginalDestination.getY(), entityPos.getZ());
        Location<World> destination = orginalDestination.copy();
        destination = destination.setPosition(destinationPos);

        for (int i = 0; i < heightTolerance.getValue(); i++) {
            if (destination.getY() == 0) {
                break;
            }
            // If its empty, or ignored
            if (checkBlock(destination)) {
                foundFree++;
            }
            else {
                foundGround = true;
                break;
            }
            destination = destination.getRelative(Direction.DOWN);
        }

        if (!foundGround) {
            if (entity instanceof CommandSource) {
                ((CommandSource)entity).sendMessage(Text.of("No floor!"));
            }
            return;
        }

        if (foundFree == 1 && checkBlock(orginalDestination.getRelative(Direction.UP))) {
            // I.e the block directly below the sign isn't free, but the one above is, so we can
            // teleport the entities' feet there :)
            foundFree++;
        }

        if (foundFree < 2) {
            if (entity instanceof CommandSource) {
                ((CommandSource)entity).sendMessage(Text.of("Obstructed!"));
            }
            return;
        }
        entity.setLocation(destination.getRelative(Direction.UP));

        if (entity instanceof CommandSource) {
            CommandSource player = (CommandSource)entity;
            Sign destSign = (Sign) orginalDestination.getTileEntity().orElse(null);
            Text destFloorNameText = SignUtil.getText(destSign.getSignData(), 0);

            if(!destFloorNameText.isEmpty()){
                player.sendMessage(
                    Text.builder()
                            .append(Text.of("Floor: "))
                            .append(destFloorNameText).build()
                );
            } else {
                player.sendMessage(Text.of("You've gone " + direction.name().toLowerCase() + " a floor!"));
            }
        }

    }

    private boolean checkBlock(Location<World> toCheck) {
        return ((IBlockState) toCheck.getBlock()).getBlock().isPassable((IBlockAccess) toCheck.getExtent(), new BlockPos(toCheck.getX(), toCheck.getY(), toCheck.getZ()));
    }

    /**
     * Gets the destination of an Elevator. If there is none, it returns the start.
     * 
     * @param block The starting block.
     * @param direction The direction to move in.
     * @return The elevator destination.
     */
    private Location<World> findDestination(Location<World> block, Direction direction) {

        int y = block.getBlockY();

        if (direction == Direction.UP || direction == Direction.DOWN) {
            while (direction == Direction.UP ? y < 255 : y > 0) {
                y += direction == Direction.UP ? 1 : -1;

                Location<World> test = block.getExtent().getLocation(block.getBlockX(), y, block.getBlockZ());

                if (allowButtonLifts.getValue() &&
                        test.getBlockType() == BlockTypes.STONE_BUTTON || test.getBlockType() == BlockTypes.WOODEN_BUTTON) {
                    Direction backDir = SignUtil.getBack(test);
                    test = test.getRelative(backDir).getRelative(backDir);
                }

                if (SignUtil.isSign(test)) {
                    // It's a sign.
                    if(isValid(test))
                        return test;
                }
            }
        }

        // We don't currently support non-up/down elevators, this isn't a Roald Dahl novel.

        return block;
    }

    @Override
    public String getPath() {
        return "mechanics/elevator";
    }

    @Override
    public ConfigValue<?>[] getConfigurationNodes() {
        return new ConfigValue<?>[]{
                allowJumpLifts,
                allowButtonLifts
        };
    }

    @Override
    public PermissionNode[] getPermissionNodes() {
        return new PermissionNode[]{
                createPermissions,
                usePermissions
        };
    }
}
