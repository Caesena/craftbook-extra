/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless;

import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Bands {
    // Loaded bands to if they are activated and locations of their receivers.
    private static final Map<Band, BandInfo> loadedBands = new ConcurrentHashMap<>();
    public static ThreadPoolExecutor updateQueue = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);

    /**
     * Sets a band's activation
     * @param band band to set
     */
    public static void setActivatedBand(Band band, boolean activated) {
        updateQueue.execute(() -> {
            BandInfo info = loadedBands
                    .computeIfAbsent(band, v -> new BandInfo(null));
            if (info.activated == null || info.activated != activated) {
                info.activated = activated;
                info.updateReceivers(band);
            }
        });
    }

    public static @Nullable Boolean isActivated(Band band) {
        BandInfo info = loadedBands.get(band);
        if (info != null) {
            return info.activated;
        }
        return null;
    }

    public static void registerReceiver(Band band, WirelessReceiver receiver) {
        BandInfo info = loadedBands
                .computeIfAbsent(band, v -> new BandInfo(null));
        info.registerReceiver(band, receiver);
    }

    public static void unregisterReceiver(Band band, Location<World> loc) {
        BandInfo info = loadedBands.get(band);
        if (info != null)
            info.unregisterReceiver(loc);
    }

    @Nullable
    public static BandInfo getBandInfo(Band band) {
        return loadedBands.get(band);
    }
}
