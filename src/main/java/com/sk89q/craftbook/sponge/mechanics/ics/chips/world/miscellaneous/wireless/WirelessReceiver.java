/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless;

import com.minecraftonline.ic.AbstractIC;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.BandLineParser;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

public class WirelessReceiver extends AbstractIC /*implements SelfTriggeringIC*/ {

    private static final String stString = "[" + new WirelessReceiver.Factory().stVariant() + "]";
    private Band band;
    private boolean shouldLoadLine = false;
    private boolean isST;


    public WirelessReceiver(ICFactory<WirelessReceiver> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, BandLineParser.shortBand(shortBand -> band = new Band("", shortBand)))
                .lineParser(3, BandLineParser.wideBand((wideBand, shouldLoadLine) -> {
                    band = new Band(wideBand, band.getShortBand());
                    this.shouldLoadLine = shouldLoadLine;
                }))
                .build();
    }

    @Override
    public void load() {
        if (this.shouldLoadLine)
            setLine(3, Text.of(this.band.getWideBand()));
        super.load();
        isST = getLine(1).contains(stString);
        if (!this.isCorrupt()) {
            Bands.registerReceiver(this.band, this);
        }
    }

    public boolean isST() {
        return isST;
    }

    public Band getBand() {
        return band;
    }

    @Override
    public void unload() {
        Bands.unregisterReceiver(this.band, getSign().getLocation());
    }

    @Override
    public void onTrigger() {
        if (!isST) {
            Boolean activated = Bands.isActivated(this.band);
            if (activated != null)
                getPinSet().setOutput(0, activated,this);
        }
    }

    public static class Factory implements ICFactory<WirelessReceiver>, HybridIC {

        @Override
        public String stVariant() {return "MC0111";}

        @Override
        public WirelessReceiver createInstance(Location<World> location) {
            return new WirelessReceiver(this, location);
        }

        @Override
        public String[] getLineHelp() {
            return new String[] {
                    "The channel name (narrowband)",
                    "Optional wideband. Becomes UUID if uuid is put"
            };
        }

        @Override
        public String[][] getPinHelp() {
            return new String[][] {
                    new String[] {
                            "Clock"
                    },
                    new String[] {
                            "Wireless Output"
                    }
            };
        }
    }
}
