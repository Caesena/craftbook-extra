/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.powerable;

import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeBlockMechanic;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.util.Tuple;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public abstract class SimplePowerable extends SpongeBlockMechanic {

    public static final Location[] EMPTY_LOCATION_ARRAY = new Location[0];

    public abstract void updateState(Location<?> location, boolean powered);

    public abstract boolean getState(Location<?> location);

    public boolean isRedstoneEnabled() {
        return true;
    }

    public List<Direction> getAdditionalUpdateDirections() {
        return Collections.EMPTY_LIST;
    }

    @Listener
    public void onBlockChange(ChangeBlockEvent.Post event) {
        if (!isRedstoneEnabled()) {
            return;
        }
        event.getTransactions().forEach(blockSnapshotTransaction -> {
            Location<World> baseLocation = blockSnapshotTransaction.getFinal().getLocation().get();
            BlockState endState = blockSnapshotTransaction.getFinal().getExtendedState();

            Tuple<Boolean, Set<Location<World>>> endTuple = ICSocket.getPoweredInState(endState, baseLocation);

            Collection<Location<World>> locations;

            if (endTuple == null) {
                BlockState beginState = blockSnapshotTransaction.getOriginal().getExtendedState();
                if (ICSocket.getPoweredInState(beginState, baseLocation) == null) {
                    return;
                }
                locations = BlockUtil.getAdjacentExcept(baseLocation);
            }
            else {
                locations = endTuple.getSecond();
            }
            for (Direction dir : getAdditionalUpdateDirections()) {
                Location<World> loc = baseLocation.getRelative(dir);
                if (!locations.contains(loc)) {
                    locations.add(loc);
                }
            }


            if (endTuple == null) {
                return;
            }

            boolean powered =  endTuple.getFirst();

            for (Location<World> location : locations) {
                if (!isValid(location)) {
                    continue;
                }
                if (!powered && BlockUtil.getBlockIndirectPowerLevel(location).orElse(0) > 0) {
                    powered = true;
                }
                if (powered != getState(location)) {
                    updateState(location, powered);
                }
            }
        });
    }
}
