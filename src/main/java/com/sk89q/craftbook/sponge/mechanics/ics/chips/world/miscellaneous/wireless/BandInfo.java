/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless;

import com.me4502.modularframework.module.ModuleWrapper;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Handles updating of receivers for a specific band
 * As well as storing information about the band
 */
public class BandInfo {
    private static ICSocket icSocket = null;
    public Boolean activated;
    private final Set<Location<World>> receiverLocations;

    /**
     * Used for creating BandInfo from transmitter and used by other constructor
     * @param activated
     */
    public BandInfo(@Nullable Boolean activated) {
        if (icSocket == null) {
            icSocket = getICSocket().get();
        }

        this.activated = activated;
        receiverLocations = ConcurrentHashMap.newKeySet();
    }

    public static Optional<ICSocket> getICSocket() {
        return CraftBookPlugin.spongeInst().moduleController.getModule("icsocket")
                .flatMap(ModuleWrapper::getModule);
    }

    /**
     * Register a receiver. Ensure it is updated.
     * @param band Band that this receiver is on
     * @param receiver Location where this receiver is
     */
    public void registerReceiver(Band band, WirelessReceiver receiver) {
        receiverLocations.add(receiver.getBlock());
        if (activated == null) {
            return;
        }
        // Make sure it is a valid target.
        if (!receiver.isST() || !receiver.getBand().equals(band)) {
            return;
        }
        if (receiver.getPinSet().getOutput(0, receiver) != activated) {
            updateReceiver(receiver, activated);
        }
    }

    public void unregisterReceiver(Location<World> loc) {
        receiverLocations.remove(loc);
    }

    /**
     * Tell receivers that they need to change
     * Can be called async.
     */
    public void updateReceivers(Band target) {
        for (Iterator<Location<World>> iter = receiverLocations.iterator(); iter.hasNext();) {
            Optional<IC> ic = icSocket.getIC(iter.next());
            // Make sure it is a valid target.
            if (!ic.isPresent() || !(ic.get() instanceof WirelessReceiver)) {
                iter.remove();
                continue;
            }
            WirelessReceiver receiver = (WirelessReceiver)ic.get();
            if (!receiver.isST() || !receiver.getBand().equals(target)) {
                iter.remove();
                continue;
            }
            if (receiver.getPinSet().getOutput(0, receiver) != activated) {
                updateReceiver(receiver, activated);
            }
        }
    }

    public static void updateReceiver(WirelessReceiver receiver, boolean state) {
        Task.builder().name("CBX - Update Receiver " + UUID.randomUUID().toString())
                .execute(() -> receiver.getPinSet().setOutput(0, state, receiver))
                .delayTicks(1)
                .submit(CraftBookPlugin.spongeInst());
    }

    public Set<Location<World>> getReceiverLocations() {
        return receiverLocations;
    }
}
