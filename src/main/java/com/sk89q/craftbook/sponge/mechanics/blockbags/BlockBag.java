/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.blockbags;

import com.google.common.collect.Lists;
import org.spongepowered.api.item.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.function.Predicate;

/**
 * Represents an object that contains an inventory, and can be used as a source of blocks for CraftBook mechanics.
 */
public interface BlockBag {

    /**
     * Determines if this {@link BlockBag} contains the requested {@link ItemStack}s.
     *
     * @param itemStacks The requested items
     * @return if it contains the items
     */
    boolean has(List<ItemStack> itemStacks);

    /**
     * Prefer over {@link #has(List)} because it checks more throughly
     * @param predicate to match ItemStacks to
     * @param amountRequired to match in order to return true1
     * @return if found amountRequired items or more.
     */
    boolean has(Predicate<ItemStack> predicate, int amountRequired);

    /**
     * Checks if this blockbag can fit the itemStacks
     * This should not mutate the given list of itemstacks
     * @param itemStacks to check if can fit
     * @return if it can fit.
     */
    boolean canFit(List<ItemStack> itemStacks);
    /**
     * Adds the given {@link ItemStack}s to this {@link BlockBag}.
     *
     * @param itemStacks The items to add
     * @return All items that could not be added
     */
    List<ItemStack> add(List<ItemStack> itemStacks);

    default ItemStack add(ItemStack itemStack) {
        List<ItemStack> itemStacks = add(Lists.newArrayList(itemStack));
        return itemStacks.isEmpty() ? ItemStack.empty() : itemStacks.get(0);
    }

    /**
     * Removes the given {@link ItemStack}s from this {@link BlockBag}.
     *
     * @param itemStacks The items to remove
     * @return All items that could not be removed
     */
    List<ItemStack> remove(List<ItemStack> itemStacks);

    /**
     * Removes items that match the filter up to max amtToRemove items.
     * @param predicate filter to remove items that match it
     * @param amtToRemove amount to remove
     * @return amount not removed
     */
    int remove(Predicate<ItemStack> predicate, int amtToRemove);

    @Nullable
    ItemStack findStack(Predicate<ItemStack> predicate, int maxSize);
}
