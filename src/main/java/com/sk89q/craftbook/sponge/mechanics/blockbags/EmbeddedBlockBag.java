/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.blockbags;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataSerializable;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.Queries;
import org.spongepowered.api.data.persistence.AbstractDataBuilder;
import org.spongepowered.api.data.persistence.InvalidDataException;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class EmbeddedBlockBag implements DataSerializable, BlockBag {

    private List<ItemStack> itemStacks;

    public EmbeddedBlockBag() {
        super();
        itemStacks = new ArrayList<>();
    }

    @Override
    public boolean has(List<ItemStack> itemStacks) {
        List<ItemStack> testList = new ArrayList<>(itemStacks);
        Iterator<ItemStack> testIterator = testList.iterator();
        while (testIterator.hasNext()) {
            ItemStack testStack = testIterator.next();
            for (ItemStack itemStack : this.itemStacks) {
                if (itemStack.getType() == testStack.getType()) {
                    int newQuantity = testStack.getQuantity();
                    newQuantity -= itemStack.getQuantity();
                    if (newQuantity > 0) {
                        testStack.setQuantity(newQuantity);
                    } else {
                        testIterator.remove();
                    }
                }
            }
        }
        return testList.isEmpty();
    }

    @Override
    public boolean has(Predicate<ItemStack> predicate, int amountRequired) {
        int foundAmt = 0;
        for (ItemStack itemStack : itemStacks) {
            if (predicate.test(itemStack)) {
                foundAmt += itemStack.getQuantity();
            }
            if (foundAmt >= amountRequired) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean canFit(List<ItemStack> itemStacks) {
        return true;
    }

    @Override
    public List<ItemStack> add(List<ItemStack> itemStacks) {
        for (ItemStack itemStack : itemStacks) {
            itemAdd:
            {
                for (ItemStack existingStack : this.itemStacks) {
                    if (itemStack.getType() == existingStack.getType()) {
                        existingStack.setQuantity(existingStack.getQuantity() + itemStack.getQuantity());
                        break itemAdd;
                    }
                }

                this.itemStacks.add(itemStack);
            }
        }
        return Lists.newArrayList();
    }

    @Override
    public List<ItemStack> remove(List<ItemStack> itemStacks) {
        List<ItemStack> unremovable = new ArrayList<>(itemStacks);
        Iterator<ItemStack> itemIterator = unremovable.iterator();
        while (itemIterator.hasNext()) {
            ItemStack itemStack = itemIterator.next();
            int newQuantity = itemStack.getQuantity();

            Iterator<ItemStack> existingIterator = this.itemStacks.iterator();
            while (existingIterator.hasNext()) {
                ItemStack existingStack = existingIterator.next();
                if (itemStack.getType() == existingStack.getType()) {
                    int numToRemove = Math.min(existingStack.getQuantity(), newQuantity);
                    newQuantity -=  numToRemove;
                    if (existingStack.getQuantity() - numToRemove <= 0) {
                        existingIterator.remove();
                    } else {
                        existingStack.setQuantity(existingStack.getQuantity() - numToRemove);
                    }
                }
            }

            if (newQuantity <= 0) {
                itemIterator.remove();
            } else {
                itemStack.setQuantity(newQuantity);
            }
        }

        return unremovable;
    }

    @Override
    public int remove(Predicate<ItemStack> predicate, int amtToRemove) {
        Iterator<ItemStack> iter = itemStacks.listIterator();
        for (ItemStack itemStack = iter.next(); iter.hasNext(); itemStack = iter.next()) {
            if (!predicate.test(itemStack)) {
                continue;
            }
            if (itemStack.getQuantity() > amtToRemove) {
                itemStack.setQuantity(itemStack.getQuantity() - amtToRemove);
                return 0;
            }
            else {
                amtToRemove -= itemStack.getQuantity();
                iter.remove();
            }
        }
        return amtToRemove;
    }

    @Nullable
    @Override
    public ItemStack findStack(Predicate<ItemStack> predicate, int maxSize) {
        // TODO: Check this actually works.
        ItemStack result = null;
        for (ItemStack itemStack : this.itemStacks) {
            if (!predicate.test(itemStack)) {
                continue;
            }
            if (result == null) {
                maxSize = Math.min(itemStack.getMaxStackQuantity(), maxSize);
            }
            if (itemStack.getQuantity() >= maxSize) {
                if (result != null) {
                    result.setQuantity(result.getQuantity() + maxSize);
                    return result;
                }
                ItemStack stack = itemStack.copy();
                stack.setQuantity(maxSize);
                return stack;
            }
            else if (itemStack.getQuantity() < maxSize) {
                if (result == null) {
                    result = itemStack.copy();
                }
                else {
                    result.setQuantity(result.getQuantity() + itemStack.getQuantity());
                }
                maxSize -= itemStack.getQuantity();
            }
        }
        return result;
    }

    @Override
    public int getContentVersion() {
        return 1;
    }

    @Override
    public DataContainer toContainer() {
        return DataContainer.createNew()
                .set(Queries.CONTENT_VERSION, getContentVersion())
                .set(DataQuery.of("Items"), itemStacks);
    }

    public static class EmbeddedBlockBagBuilder extends AbstractDataBuilder<EmbeddedBlockBag> {

        public EmbeddedBlockBagBuilder() {
            super(EmbeddedBlockBag.class, 1);
        }

        @Override
        protected Optional<EmbeddedBlockBag> buildContent(DataView container) throws InvalidDataException {
            EmbeddedBlockBag embeddedBlockBag = new EmbeddedBlockBag();

            embeddedBlockBag.itemStacks = container.getSerializableList(DataQuery.of("Items"), ItemStack.class).orElse(Lists.newArrayList());

            return Optional.of(embeddedBlockBag);
        }
    }
}
