/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.blockbags;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.common.item.inventory.util.ItemStackUtil;

import java.util.List;
import java.util.function.Predicate;

/**
 * An interface for accessing multiple block bags at the same time.
 */
public class MultiBlockBag implements BlockBag {

    protected List<BlockBag> bags;

    public MultiBlockBag(BlockBag... bags) {
        this.bags = Lists.newArrayList(bags);
    }

    protected void addBag(BlockBag bag) {
        this.bags.add(bag);
    }

    public boolean isEmpty() {
        return bags.isEmpty();
    }

    @Override
    public boolean has(List<ItemStack> itemStacks) {
        for (BlockBag bag : bags) {
            if (itemStacks.isEmpty()) break;
            if (bag.has(itemStacks))
                return true;
        }

        return false;
    }

    // Not fully correct but neither is the other has for MultiBlockBag
    @Override
    public boolean has(Predicate<ItemStack> predicate, int amountRequired) {
        for (BlockBag bag : bags) {
            if (bag.has(predicate, amountRequired)) {
                return true;
            }
        }
        return true;
    }

    @Override
    public boolean canFit(List<ItemStack> itemStacks) {
        for (BlockBag bag : bags) {
            if (bag.canFit(itemStacks))
                return true;
        }
        return false;
    }

    @Override
    public List<ItemStack> add(List<ItemStack> itemStacks) {
        for (BlockBag bag : bags) {
            if (itemStacks.isEmpty()) break;
            itemStacks = bag.add(itemStacks);
        }

        return itemStacks;
    }

    @Override
    public List<ItemStack> remove(List<ItemStack> itemStacks) {
        for (BlockBag bag : bags) {
            if (itemStacks.isEmpty()) break;
            itemStacks = bag.remove(itemStacks);
        }

        return itemStacks;
    }

    @Override
    public int remove(Predicate<ItemStack> predicate, int amtToRemove) {
        for (BlockBag bag : bags) {
            amtToRemove = bag.remove(predicate, amtToRemove);
            if (amtToRemove == 0)
                return 0;
        }
        return amtToRemove;
    }

    @Nullable
    @Override
    public ItemStack findStack(Predicate<ItemStack> predicate, int maxSize) {
        ItemStack itemStack = null;
        for (BlockBag bag : bags) {
            ItemStack stack = bag.findStack(predicate, maxSize);
            if (stack == null) {
                continue;
            }
            if (itemStack == null) {
                itemStack = stack;
                predicate = iStack -> ItemStackUtil.compareIgnoreQuantity(stack, iStack);
            }
            else {
                itemStack.setQuantity(itemStack.getQuantity() + stack.getQuantity());
            }
            maxSize -= stack.getQuantity();

            if (maxSize == 0) {
                return itemStack;
            }
        }
        return itemStack;
    }
}
