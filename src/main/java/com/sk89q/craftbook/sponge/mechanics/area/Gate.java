/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.area;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.legacy.blockbags.MultiNearbyChestBlockBag;
import com.minecraftonline.mechanic.SignMechanicCacheManager;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.PermissionNode;
import com.sk89q.craftbook.core.util.documentation.DocumentationProvider;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager;
import com.sk89q.craftbook.sponge.mechanics.blockbags.EmbeddedBlockBag;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.data.mutable.EmbeddedBlockBagData;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.Humanoid;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.util.Tuple;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Module(id = "gate", name = "Gate", onEnable="onInitialize", onDisable="onDisable")
public class Gate extends SimpleArea implements DocumentationProvider {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final ConfigValue<Integer> searchRadius = new ConfigValue<>("search-radius", "The maximum area around the sign the gate can search.", 5);
    private final ConfigValue<Boolean> indirectAccess = new ConfigValue<>("indirect-access", "Allows toggling of gates by clicking the gate material "
            + "rather than the sign.", true);

    @Override
    public void onInitialize() throws CraftBookException {
        super.loadCommonConfig(config);
        super.registerCommonPermissions();

        searchRadius.load(config);
        indirectAccess.load(config);

        cache.registerCache(this, (loc, signData) -> {
            if (!this.isValidNoCache(loc, signData)) {
                return null;
            }
            String line2 = signData.lines().get(1).toPlain().toLowerCase();
            boolean clickable = line2.endsWith("c");
            boolean dGate = line2.contains("dgate");
            Predicate<BlockState> matchesDoor = null;
            if (line2.startsWith("[glass")) {
                matchesDoor = block -> block.getType() == BlockTypes.GLASS_PANE
                        || block.getType() == BlockTypes.STAINED_GLASS_PANE;
            }
            else if (line2.startsWith("[iron")) {
                matchesDoor = block -> block.getType() == BlockTypes.IRON_BARS;
            }
            else if (line2.startsWith("[nether")) {
                matchesDoor = block -> block.getType() == BlockTypes.NETHER_BRICK_FENCE;
            }
            registerRedstoneListener(loc);
            return new GateCachedData(signData.lines().get(), matchesDoor, clickable, dGate);
        });
    }

    @Override
    public Collection<Vector3i> createInputList(Location<World> mechanicLocation) {
        Vector3i pos = mechanicLocation.getBlockPosition();
        return Arrays.asList(
                pos.add(1, 0, 0),
                pos.add(-1, 0, 0),
                pos.add(0, 0, 1),
                pos.add(0, 0, -1)
        );
    }

    public class GateCachedData extends SimpleArea.SimpleAreaCache {
        private final Predicate<BlockState> matchesDoor;
        private final boolean clickable;
        private final boolean dGate;

        public GateCachedData(List<Text> lines, @Nullable Predicate<BlockState> matchesDoor, boolean clickable, boolean dGate) {
            super(lines);
            this.matchesDoor = matchesDoor;
            this.clickable = clickable;
            this.dGate = dGate;
        }

        public boolean matchesDoor(BlockState blockState) {
            if (matchesDoor == null) {
                return true;
            }
            return matchesDoor.test(blockState);
        }

        public boolean isClickable() {
            return clickable;
        }

        public boolean isDGate() {
            return dGate;
        }
    }

    @Listener
    public void onPlayerInteract(InteractBlockEvent.Secondary.MainHand event, @First Humanoid human) {
        super.onPlayerInteract(event, human);

        if (human.get(Keys.IS_SNEAKING).orElse(false)) {
            return;
        }

        event.getTargetBlock().getLocation().ifPresent((location) -> {
            if (BlockUtil.doesStatePassFilters(allowedBlocks.getValue(), location.getBlock())) {
                if (((human instanceof Subject) && !usePermissions.hasPermission((Subject) human))) {
                    //if (human instanceof CommandSource)
                    //    ((CommandSource) human).sendMessage(TranslationsManager.USE_PERMISSIONS);
                    return;
                }

                int x = location.getBlockX();
                int y = location.getBlockY();
                int z = location.getBlockZ();

                for (int x1 = x - searchRadius.getValue(); x1 <= x + searchRadius.getValue(); x1++) {
                    for (int y1 = y - searchRadius.getValue(); y1 <= y + searchRadius.getValue() * 2; y1++) {
                        for (int z1 = z - searchRadius.getValue(); z1 <= z + searchRadius.getValue(); z1++) {
                            Location<World> searchLocation = location.getExtent().getLocation(x1, y1, z1);
                            GateCachedData gateCachedData = cache.getCached(this, searchLocation);
                            if (gateCachedData == null || !gateCachedData.matchesDoor(location.getBlock()) || !gateCachedData.isClickable()) {
                                continue;
                            }

                            BlockBag bag = getBlockBag(searchLocation);

                            if (bag == null) {

                            }

                            for (Map.Entry<Set<GateColumn>, BlockState> gateEntry : findColumns(location, gateCachedData, location.getBlock()).entrySet()) {
                                toggleColumns(gateEntry.getValue(), searchLocation, human, gateEntry.getKey(), null, bag);
                            }

                            return;
                        }
                    }
                }
            }
        });
    }

    private Map<Set<GateColumn>, BlockState> findColumns(Location<World> block, GateCachedData data, BlockState state) {
        int x = block.getBlockX();
        int y = block.getBlockY();
        int z = block.getBlockZ();

        int radius, downRadius;
        if (data.isDGate()) {
            radius = 1;
            downRadius = 2;
        }
        else {
            radius = searchRadius.getValue();
            downRadius = radius;
        }

        Map<Set<GateColumn>, BlockState> result = new LinkedHashMap<>();

        Set<GateColumn> allFound = new HashSet<>();

        for (int x1 = x - radius; x1 <= x + radius; x1++) {
            for (int y1 = y - downRadius; y1 <= y + radius * 2; y1++) {
                for (int z1 = z - radius; z1 <= z + radius; z1++) {
                    BlockState blockState = block.getExtent().getBlock(x1, y1, z1);
                    if (data.matchesDoor(blockState) && BlockUtil.doesStatePassFilters(allowedBlocks.getValue(), blockState)) {

                        Location<World> loc = block.getExtent().getLocation(x1, y1, z1);

                        Set<GateColumn> columns = new HashSet<>();
                        BlockState columnState = searchColumn(loc, columns, state, allFound);

                        if (columnState != null) {
                            result.put(columns, columnState);
                            allFound.addAll(columns);
                        }
                    }
                }
            }
        }

        return result;
    }

    @Nullable
    private BlockState searchColumn(Location<World> block, Set<GateColumn> columns, BlockState state, Set<GateColumn> exclude) {
        if (BlockUtil.doesStatePassFilters(allowedBlocks.getValue(), block.getBlock())) {
            GateColumn column = new GateColumn(block, allowedBlocks);

            if (exclude.contains(column)) {
                return null;
            }

            Location<World> temp = column.topBlock;

            if(temp.getBlockType() != BlockTypes.AIR) {
                if(state == null)
                    state = temp.getBlock();
                // Not much point in 1 tall gates, they would leave the top block, so they do nothing - May be air though because we may be placing not removing
                BlockState blockState = temp.getRelative(Direction.DOWN).getBlock();
                if(state.equals(temp.getBlock()) && blockState.getType() == BlockTypes.AIR || state.equals(blockState))
                    columns.add(column);
                else
                    return state;
            }

            while (BlockUtil.doesStatePassFilters(allowedBlocks.getValue(), temp.getBlock()) || temp.getBlockType() == BlockTypes.AIR) {

                for (Direction dir : BlockUtil.getDirectHorizontalFaces()) {
                    Location<World> sideBlock = temp.getRelative(dir); // They may continue sidewards
                    Location<World> aboveBlock = sideBlock.getRelative(Direction.UP); // and they may arch upwards.

                    if (!columns.contains(new GateColumn(aboveBlock, allowedBlocks))) {
                        state = searchColumn(aboveBlock, columns, state, exclude);
                    }
                    else if (!columns.contains(new GateColumn(sideBlock, allowedBlocks))) {
                        state = searchColumn(sideBlock, columns, state, exclude);
                    }
                }

                temp = temp.getRelative(Direction.DOWN);
            }
        }

        return state;
    }

    /**
     * Toggles a column.
     * @param block
     * @param signLoc
     * @param human
     * @param on
     * @param gateType
     * @param bag
     *
     * @return Whether to keep toggling columns, or whether
     * to stop, because, for example, we ran out of blocks.
     */
    private boolean toggleColumn(Location<World> block, Location<World> signLoc, @Nullable Humanoid human, boolean on, BlockState gateType, BlockBag bag) {
        Direction dir = Direction.DOWN;

        block = block.getRelative(dir);

        ItemStack blockBagItem = ItemStack.builder().fromBlockState(gateType).quantity(1).build();

        if (on) {
            while (block.getBlockType() == BlockTypes.AIR) {
                if (bag.has(Lists.newArrayList(blockBagItem.copy()))) {
                    if (bag.remove(Lists.newArrayList(blockBagItem.copy())).isEmpty()) {
                        block.setBlock(gateType);
                        block = block.getRelative(dir);
                    }
                } else {
                    if (human != null && human instanceof CommandSource) {
                        ((CommandSource) human).sendMessage(Text.of("Out of Blocks"));
                    }
                    return false;
                }
            }
        } else {
            while (block.getBlock().equals(gateType)) {
                block.setBlockType(BlockTypes.AIR);
                if (block.getBlockType() != BlockTypes.AIR) {
                    block = block.getRelative(dir);
                    continue; // Failed to place, probably region protection. Don't give out item.
                }
                block = block.getRelative(dir);
                for (ItemStack leftover : bag.add(Lists.newArrayList(blockBagItem.copy()))) {
                    Item item = (Item) block.getExtent().createEntity(EntityTypes.ITEM, signLoc.getPosition());
                    item.offer(Keys.REPRESENTED_ITEM, leftover.createSnapshot());
                    block.getExtent().spawnEntity(item);
                }
            }
        }

        if (bag instanceof EmbeddedBlockBag) {
            signLoc.offer(new EmbeddedBlockBagData((EmbeddedBlockBag) bag));
        }
        return true;
    }

    @Override
    public boolean triggerMechanic(Location<World> block, SignMechanicCacheManager.CachedData cachedData, Humanoid human, Boolean forceState) {
        boolean foundAnyGate = false;
        BlockBag bag = getBlockBag(block);
        if (bag == null) {
            if(human instanceof CommandSource) {
                ((CommandSource) human).sendMessage(Text.of(TextColors.RED, "You have no where to put the blocks!"));
            }
            return true;
        }
        for (Map.Entry<Set<GateColumn>, BlockState> entry : findColumns(block, (GateCachedData) cachedData, null).entrySet()) {
            foundAnyGate = true;
            toggleColumns(entry.getValue(), block, human, entry.getKey(), forceState, bag);
        }
        if (!foundAnyGate) {
            if (human instanceof CommandSource) ((CommandSource) human).sendMessage(Text.builder("Can't find a gate!").build());
        }

        return true;
    }

    private void toggleColumns(BlockState state, Location<World> signLoc, @Nullable Humanoid human, Set<GateColumn> columns, Boolean forceState, BlockBag bag) {
        if (state == null) {
            state = BlockTypes.FENCE.getDefaultState();
        }
        for (GateColumn vec : columns) {
            Location<World> col = vec.getBlock();
            if (forceState == null) {
                forceState = !BlockUtil.doesStatePassFilters(allowedBlocks.getValue(), col.getRelative(Direction.DOWN).getBlock());
            }
            if (!toggleColumn(col, signLoc, human, forceState, state, bag)) {
                // Stop if it doesn't return true, e.g we are out of blocks.
                return;
            }
        }
    }

    @Override
    public BlockBag getBlockBag(Location<World> location) {
        return BlockBagManager.getOrCreateCachedBlockBag(BlockBagManager.BlockBagType.MULTI_NEARBY_CHEST, location, MultiNearbyChestBlockBag.DEFAULT_RADIUS, MultiNearbyChestBlockBag.getChestBlockTypes());
    }

    @Override
    public String getPath() {
        return "mechanics/gate";
    }

    @Override
    public ConfigValue<?>[] getConfigurationNodes() {
        return new ConfigValue<?>[]{
                allowedBlocks,
                allowRedstone,
                searchRadius,
                indirectAccess
        };
    }

    @Override
    public PermissionNode[] getPermissionNodes() {
        return new PermissionNode[]{
                createPermissions,
                usePermissions
        };
    }

    private static final class GateColumn {

        Location<World> topBlock;

        GateColumn(Location<World> topBlock, ConfigValue<List<SpongeBlockFilter>> allowedBlocks) {
            while (BlockUtil.doesStatePassFilters(allowedBlocks.getValue(), topBlock.getBlock())) {
                topBlock = topBlock.getRelative(Direction.UP);
            }

            topBlock = topBlock.getRelative(Direction.DOWN);

            this.topBlock = topBlock;
        }

        public Location<World> getBlock() {
            return topBlock;
        }

        @Override
        public int hashCode() {
            return topBlock.getExtent().hashCode() + topBlock.getBlockX() + topBlock.getBlockZ();
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof GateColumn && ((GateColumn) o).topBlock.getX() == topBlock.getX() && ((GateColumn) o).topBlock.getZ() == topBlock.getZ();
        }
    }

    @Override
    public String[] getValidSigns() {
        return new String[]{"[Gate]", "[Gate]C","[DGate]","[GlassGate]","[GlassDGate]","[IronGate]","[IronDGate]","[NetherGate]","[NetherDGate]"};
    }

    @Override
    public List<SpongeBlockFilter> getDefaultBlocks() {
        List<SpongeBlockFilter> states = Lists.newArrayList();
        states.addAll(Sponge.getRegistry().getAllOf(BlockType.class).stream()
                .filter(blockType -> blockType.getName().toLowerCase().contains("fence"))
                .map(SpongeBlockFilter::new)
                .collect(Collectors.toList()));
        states.add(new SpongeBlockFilter(BlockTypes.GLASS_PANE));
        states.add(new SpongeBlockFilter(BlockTypes.STAINED_GLASS_PANE));
        states.add(new SpongeBlockFilter(BlockTypes.IRON_BARS));
        return states;
    }
}
