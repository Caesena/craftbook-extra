/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless;

import com.minecraftonline.ic.AbstractIC;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.BandLineParser;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.SerializedICData;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

public class WirelessTransmitter extends AbstractIC {

    private boolean shouldLoadLine = false;
    private Band band;

    public WirelessTransmitter(ICFactory<WirelessTransmitter> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    @Override
    public ParsingConfiguration makeParsingConfiguration() {
        return ParsingConfiguration.builder()
                .lineParser(2, BandLineParser.shortBand(shortBand -> band = new Band("", shortBand)))
                .lineParser(3, BandLineParser.wideBand((wideBand, shouldLoadLine) -> {
                    band = new Band(wideBand, band.getShortBand());
                    this.shouldLoadLine = shouldLoadLine;
                }))
                .build();
    }

    @Override
    public void load() {
        if (shouldLoadLine)
            setLine(3, Text.of(this.band.getWideBand()));
        super.load();
    }

    @Override
    public void onTrigger() {
        boolean enable = getPinSet().isAnyTriggered(this);
        Bands.setActivatedBand(this.band, enable);
    }

    public Band getBand() {
        return band;
    }

    public static class Factory implements ICFactory<WirelessTransmitter> {

        @Override
        public WirelessTransmitter createInstance(Location<World> location) {
            return new WirelessTransmitter(this, location);
        }

        @Override
        public String[] getLineHelp() {
            return new String[] {
                    "The channel name (narrowband)",
                    "Optional wideband. Becomes UUID if uuid is put"
            };
        }

        @Override
        public String[][] getPinHelp() {
            return new String[][] {
                    new String[] {
                            "Wireless Input"
                    },
                    new String[] {
                            "None"
                    }
            };
        }

        public static class WirelessData extends SerializedICData {

            public String wideband;
            public String shortband;

            @Override
            public int getContentVersion() {
                return 1;
            }

            @Override
            public DataContainer toContainer() {
                return super.toContainer()
                        .set(DataQuery.of("ShortBand"), this.shortband)
                        .set(DataQuery.of("WideBand"), this.wideband);
            }
        }
    }
}
