/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.blockbags;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.legacy.blockbags.MultiNearbyChestBlockBag;
import com.minecraftonline.legacy.blockbags.NearbyChestBlockBag;
import com.minecraftonline.util.WithinChunkPos;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.PermissionNode;
import com.sk89q.craftbook.core.util.documentation.DocumentationProvider;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeMechanic;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.world.chunk.UnloadChunkEvent;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.storage.ChunkLayout;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

@Module(id = "blockbag", name = "BlockBag", onEnable = "onInitialize", onDisable = "onDisable")
public class BlockBagManager extends SpongeMechanic implements DocumentationProvider {

    public SpongePermissionNode adminPermissions = new SpongePermissionNode("craftbook.blockbag.admin",
            "Allows usage of admin block bags.", PermissionDescription.ROLE_ADMIN);

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final Map<World, WorldCache> worldCacheMap = new HashMap<>();

    public static class WorldCache {
        private final Map<Vector3i, ChunkCache> chunkCacheMap = new HashMap<>();
        private final Multimap<Vector3i, ChunkCache.BlockBagCache> invalidationMap = HashMultimap.create();

        public static class ChunkCache {
            private final Multimap<WithinChunkPos, BlockBagCache> blockBagCache = HashMultimap.create();

            public static class BlockBagCache {
                private final Set<BlockType> includedTypes;
                private final int radius;
                private final BlockBagType type;
                private final Vector3i centre;
                private final BlockBag blockBag;

                public BlockBagCache(Vector3i centre, int radius, BlockBagType type, Set<BlockType> includedTypes, BlockBag blockBag) {
                    this.includedTypes = includedTypes;
                    this.radius = radius;
                    this.type = type;
                    this.blockBag = blockBag;
                    this.centre = centre;
                }

                public boolean matches(Vector3i centre, int radius, BlockBagType type, Set<BlockType> includedTypes) {
                    return this.centre.equals(centre)
                            && this.matches(radius, type, includedTypes);
                }

                public boolean matches(int radius, BlockBagType type, Set<BlockType> includedTypes) {
                    return this.radius == radius
                            && this.type == type
                            && this.includedTypes.equals(includedTypes);
                }

                public BlockBag getBlockBag() {
                    return blockBag;
                }

                public Vector3i getCentre() {
                    return centre;
                }
            }
        }
    }

    public enum BlockBagType {
        MULTI_NEARBY_CHEST,
        SINGLE_INVENTORY,
    }

    private final Set<BlockType> CONTAINER_BLOCK_TYPES = new HashSet<>(MultiNearbyChestBlockBag.getChestBlockTypes());

    public static BlockBag getOrCreateSingleBlockBag(Location<World> loc) {
        return getOrCreateCachedBlockBag(BlockBagType.SINGLE_INVENTORY, loc, 0, MultiNearbyChestBlockBag.getChestBlockTypes());
    }

    @Nullable
    public static BlockBag getOrCreateMultiNearbyBag(Location<World> loc) {
        return getOrCreateCachedBlockBag(BlockBagType.MULTI_NEARBY_CHEST, loc, MultiNearbyChestBlockBag.DEFAULT_RADIUS, MultiNearbyChestBlockBag.getChestBlockTypes());
    }

    @Nullable
    public static BlockBag getOrCreateCachedBlockBag(BlockBagType type, Location<World> centre, int radius, Set<BlockType> includedTypes) {
        if (BlockBagManager.INSTANCE == null) {
            //throw new IllegalStateException("BlockBagManager is not enabled!");
            return createBlockBag(type, centre, radius, includedTypes);
        }
        if (includedTypes != MultiNearbyChestBlockBag.getChestBlockTypes()) {
            BlockBagManager.INSTANCE.CONTAINER_BLOCK_TYPES.addAll(includedTypes);
        }
        WorldCache worldCache = BlockBagManager.INSTANCE.worldCacheMap.get(centre.getExtent());
        WorldCache.ChunkCache chunkCache;
        if (worldCache != null) {
            chunkCache = worldCache.chunkCacheMap.get(centre.getChunkPosition());
            if (chunkCache != null) {
                Vector3i blockPos = centre.getBlockPosition();
                WithinChunkPos withinChunkPos = WithinChunkPos.fromBlockPos(blockPos.getX(), blockPos.getY(), blockPos.getZ());
                for (WorldCache.ChunkCache.BlockBagCache cachedBlockBag : chunkCache.blockBagCache.get(withinChunkPos)) {
                    if (cachedBlockBag.matches(radius, type, includedTypes)) {
                        return cachedBlockBag.getBlockBag();
                    }
                }
            }
            else {
                chunkCache = new WorldCache.ChunkCache();
                worldCache.chunkCacheMap.put(centre.getChunkPosition(), chunkCache);
            }
        } else {
            worldCache = new WorldCache();
            chunkCache = new WorldCache.ChunkCache();
            worldCache.chunkCacheMap.put(centre.getChunkPosition(), chunkCache);
            BlockBagManager.INSTANCE.worldCacheMap.put(centre.getExtent(), worldCache);
        }

        // Create BlockBag

        final BlockBag blockBag = createBlockBag(type, centre, radius, includedTypes);

        if (blockBag == null) {
            return null;
        }

        // Register new BlockBag to the cache
        Vector3i blockPos = centre.getBlockPosition();
        WithinChunkPos withinChunkPos = WithinChunkPos.fromBlockPos(blockPos.getX(), blockPos.getY(), blockPos.getZ());
        WorldCache.ChunkCache.BlockBagCache blockBagCache = new WorldCache.ChunkCache.BlockBagCache(blockPos, radius, type, includedTypes, blockBag);
        chunkCache.blockBagCache.put(withinChunkPos, blockBagCache);

        // Add to invalidate map.
        // This means that whenever a chunk in the radius of the blockbag has a container-type block in it,
        // it will invalidate the blockbag, as long as the blockbag includes the type of block that was placed.

        for (Chunk chunk : LocationUtil.getChunksInRadius(centre, radius)) {
            worldCache.invalidationMap.put(chunk.getPosition(), blockBagCache);
        }
        return blockBag;
    }

    private static BlockBag createBlockBag(BlockBagType type, Location<World> centre, int radius, Set<BlockType> includedTypes) {
        switch (type) {
            case MULTI_NEARBY_CHEST: {
                return MultiNearbyChestBlockBag.fromSourcePositionNearby(centre, radius, includedTypes);
            }
            case SINGLE_INVENTORY: {
                return LocationUtil.getInventoryForLocation(centre).map(inv -> new NearbyChestBlockBag(inv, centre)).orElse(null);
            }
            default:
                throw new IllegalStateException("Missing case for enum: " + type.name());
        }
    }

    @Listener
    public void onBlockPlace(ChangeBlockEvent.Place e) {
        invalidateFromEvent(e);
    }

    @Listener
    public void onBlockBreak(ChangeBlockEvent.Break e) {
        invalidateFromEvent(e);
    }

    public void invalidateFromEvent(ChangeBlockEvent e) {
        for (Transaction<BlockSnapshot> transaction : e.getTransactions()) {
            BlockType original = transaction.getOriginal().getState().getType();
            BlockType end = transaction.getFinal().getState().getType();

            if (!CONTAINER_BLOCK_TYPES.contains(original) && (original == end || !CONTAINER_BLOCK_TYPES.contains(end))) {
                return;
            }
            Location<World> loc = transaction.getOriginal().getLocation().get();
            invalidateChunk(loc);
        }
    }

    @Listener
    public void onChunkUnload(UnloadChunkEvent event) {
        invalidateChunk(event.getTargetChunk());
    }

    private void invalidateChunk(Chunk chunk) {
        WorldCache worldCache = worldCacheMap.get(chunk.getWorld());
        if (worldCache == null) {
            return;
        }
        ChunkLayout chunkLayout = Sponge.getServer().getChunkLayout();
        for (WorldCache.ChunkCache.BlockBagCache cache : worldCache.invalidationMap.removeAll(chunk.getPosition())) {
            worldCache.chunkCacheMap.remove(chunkLayout.forceToChunk(cache.getCentre()));

            for (Vector3i pos : LocationUtil.getChunkPositionsInRadius(chunk.getWorld().getLocation(cache.centre), cache.radius)) {
                worldCache.invalidationMap.remove(pos, cache);
            }
        }
    }

    private void invalidateChunk(Location<World> loc) {
        loc.getExtent().getChunk(loc.getChunkPosition()).ifPresent(this::invalidateChunk);
    }

    private final Set<BlockBag> blockBags = new HashSet<>();

    public static BlockBagManager INSTANCE;

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        INSTANCE = this;

        adminPermissions.register();
    }

    @Override
    public void onDisable() {
        super.onDisable();
        INSTANCE = null;
    }

    public long getUnusedId() {
        long id = ThreadLocalRandom.current().nextLong();
        while (getBlockBag(id) != null) {
            id = ThreadLocalRandom.current().nextLong();
        }
        return id;
    }

    public IdentifiableBlockBag getBlockBag(long id) {
        if (id == -1) {
            return AdminBlockBag.INSTANCE;
        }
        return blockBags.stream().filter(blockBag -> blockBag instanceof IdentifiableBlockBag)
                .map(blockBag -> (IdentifiableBlockBag) blockBag)
                .filter(bag -> bag.getId() == id)
                .findFirst().orElse(null);
    }

    public void addBlockBag(BlockBag blockBag) {
        blockBags.add(blockBag);
    }

    @Override
    public String getPath() {
        return "mechanics/block_bags";
    }

    @Override
    public PermissionNode[] getPermissionNodes() {
        return new PermissionNode[]{
                adminPermissions
        };
    }
}
