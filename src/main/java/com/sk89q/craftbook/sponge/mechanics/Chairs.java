/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.PermissionNode;
import com.sk89q.craftbook.core.util.documentation.DocumentationProvider;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeBlockMechanic;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.LocationUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import com.sk89q.craftbook.sponge.util.type.TypeTokens;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;
import ninja.leaping.configurate.ConfigurationNode;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.property.AbstractProperty;
import org.spongepowered.api.data.property.block.MatterProperty;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.data.type.PortionTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.block.tileentity.ChangeSignEvent;
import org.spongepowered.api.event.entity.RideEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static com.sk89q.craftbook.sponge.util.locale.TranslationsManager.USE_PERMISSIONS;

/**
 * A decent amount of re-doing from original craftbook chairs (didnt work properly before)
 * plus added healing chairs
 */
@Module(id = "chairs", name = "Chairs", onEnable="onInitialize", onDisable="onDisable")
public class Chairs extends SpongeBlockMechanic implements DocumentationProvider {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final ConfigValue<List<SpongeBlockFilter>> allowedBlocks = new ConfigValue<>("allowed-blocks", "A list of blocks that can be used.", getDefaultBlocks(), new TypeTokens.BlockFilterListTypeToken());
    private final ConfigValue<Boolean> exitAtEntry = new ConfigValue<>("exit-at-last-position", "Moves player's to their entry position when they exit the chair.", false);
    private final ConfigValue<Boolean> requireSigns = new ConfigValue<>("require-sign", "Require signs on the chairs.", false);
    private final ConfigValue<Integer> maxSignDistance = new ConfigValue<>("max-sign-distance", "The distance the sign can be from the clicked chair.", 3);
    private final ConfigValue<Boolean> faceCorrectDirection = new ConfigValue<>("face-correct-direction", "When the player sits, automatically face them the direction of the chair. (If possible)", true);
    private final ConfigValue<Double> healAmount = new ConfigValue<>("heal-amount", "Amount to heal the player by.", 1.0d, TypeToken.of(Double.class));
    private final ConfigValue<Integer> healRate = new ConfigValue<>("heal-rate", "How often to heal the player", 10);

    private final SpongePermissionNode usePermissions = new SpongePermissionNode("craftbook.chairs.use", "Allows the user to sit in chairs.", PermissionDescription.ROLE_USER);
    private final SpongePermissionNode createSitHealPermission = new SpongePermissionNode("craftbook.chairs.createhealchair", "Allows the user to sit in chairs.", PermissionDescription.ROLE_USER);

    private CommandMapping sitCommandMapping;
    private CommandMapping standCommandMapping;
    private List<CustomChair.Factory<?, ArmorStand>> customChairs;
    private Map<UUID, Chair<?>> chairs;

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        customChairs = new ArrayList<>();
        chairs = new HashMap<>();

        allowedBlocks.load(config);
        exitAtEntry.load(config);
        requireSigns.load(config);
        maxSignDistance.load(config);
        faceCorrectDirection.load(config);
        healAmount.load(config);
        healRate.load(config);

        usePermissions.register();
        createSitHealPermission.register();

        // Register all custom chairs
        customChairs.add(new HealingChair.Factory<>(createSitHealPermission));

        CommandSpec sit = CommandSpec.builder()
                .permission(usePermissions.getNode())
                .executor(new Sit(this))
                .build();

        sitCommandMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), sit, "sit").orElse(null);

        CommandSpec stand = CommandSpec.builder()
                .permission(usePermissions.getNode())
                .executor(new Stand(this))
                .build();

        standCommandMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), stand, "stand").orElse(null);

        Sponge.getGame().getScheduler().createTaskBuilder().intervalTicks(healRate.getValue()).execute(task -> {
            for (Map.Entry<UUID, Chair<?>> chair : new HashSet<>(chairs.entrySet())) {
                Player player = Sponge.getGame().getServer().getPlayer(chair.getKey()).orElse(null);
                if (player == null) {
                    removeChair(chair.getValue(), false);
                    return;
                }
                if (chair.getValue() instanceof HealingChair) {
                    if (player.get(Keys.HEALTH).orElse(0d) < player.get(Keys.MAX_HEALTH).orElse(0d)) {
                        player.offer(Keys.HEALTH, Math.min(player.get(Keys.HEALTH).orElse(0d) + healAmount.getValue(), player.get(Keys.MAX_HEALTH).orElse(0d)));
                    }
                }

                if (player.get(Keys.EXHAUSTION).orElse(-20d) > -20d) {
                    player.offer(Keys.EXHAUSTION, player.get(Keys.EXHAUSTION).orElse(-20d) - 0.1d);
                }
            }
        }).submit(CraftBookPlugin.inst());
    }

    @Override
    public void onDisable() {
        super.onDisable();

        if (sitCommandMapping != null) {
            Sponge.getCommandManager().removeMapping(sitCommandMapping);
        }
        if (standCommandMapping != null) {
            Sponge.getCommandManager().removeMapping(standCommandMapping);
        }
        for (Map.Entry<UUID, Chair<?>> chair : chairs.entrySet()) {
            removeChair(chair.getValue(), true);
        }
        chairs.clear();
        customChairs.clear();
    }

    @Override
    public boolean isValid(Location<World> location) {
        return isAllowed(location.getBlock()) &&
                (location.getBlockY() == 0 || !location.getRelative(Direction.DOWN).getBlockType().equals(BlockTypes.AIR)) &&
                location.getRelative(Direction.UP).getBlockType().equals(BlockTypes.AIR);
    }

    private Optional<Sign> getSign(Location<World> location, List<Location<World>> searched, Location<World> original) {

        for (Direction face : BlockUtil.getDirectHorizontalFaces()) {
            Location<World> otherBlock = location.getRelative(face);

            if (searched.contains(otherBlock)) continue;
            searched.add(otherBlock);

            if (location.getPosition().distanceSquared(original.getPosition()) > Math.pow(maxSignDistance.getValue(), 2)) continue;

            if (SignUtil.isSign(otherBlock) && SignUtil.getFront(otherBlock) == face) {
                return Optional.of((Sign)otherBlock.getTileEntity().get());
            }

            if (Objects.equals(location.getBlockType(), otherBlock.getBlockType())) {
                return getSign(otherBlock, searched, original);
            }
        }

        return Optional.empty();
    }

    private void addChair(Player player, @Nullable SignData signData, Location<World> location) {
        Entity entity = location.getExtent().createEntity(EntityTypes.ARMOR_STAND, location.getBlockPosition().toDouble().sub(-0.5, 1.2, -0.5));
        entity.offer(Keys.INVISIBLE, true);
        entity.offer(Keys.HAS_GRAVITY, false);

        if (faceCorrectDirection.getValue() && location.supports(Keys.DIRECTION)) {
            Vector3d euler = LocationUtil.cartesianToEuler(location.get(Keys.DIRECTION).orElse(Direction.NONE).getOpposite().asOffset());
            entity.setRotation(euler);
            player.setRotation(euler);
        }

        Sponge.getCauseStackManager().pushCause(player);
        location.getExtent().spawnEntity(entity);
        Sponge.getCauseStackManager().popCause();
        Chair<?> chair = null;
        if (signData != null) {
            for (CustomChair.Factory<?, ArmorStand> chairType : customChairs) {
                if (chairType.matches(signData)) {
                    chair = chairType.createInstance((ArmorStand) entity, location, player.getLocation());
                    break;
                }
            }
        }
        // Default chair
        if (chair == null)
            chair = new Chair<>((ArmorStand) entity, location, player.getLocation());

        entity.addPassenger(player);
        player.sendMessage(Text.of(TextColors.YELLOW, "You sit down!"));

        chairs.put(player.getUniqueId(), chair);
    }

    private void removeChair(Chair<?> chair, boolean clearPassengers) {
        Player passenger = chair.chairEntity.getPassengers().stream().filter((entity -> entity instanceof Player)).map(e -> (Player) e).findFirst().orElse(null);
        if (passenger != null && clearPassengers) {
            Location<World> originalLocation = passenger.getLocation();
            passenger.setVehicle(null);
            chair.chairEntity.clearPassengers();
            Sponge.getScheduler().createTaskBuilder().delayTicks(5L).execute(() -> {
                if (exitAtEntry.getValue()) {
                    passenger.setLocation(chair.playerExitLocation);
                }
                else {
                    dismountToSafeLocation(passenger, originalLocation);
                }

            }).submit(CraftBookPlugin.inst());
        }
        chair.chairEntity.remove();
        chairs.values().remove(chair);
    }

    /**
     * Use bed tactics to safely put them somewhere,
     * or murder them and stick them above if theres
     * nowhere to put em.
     * @param player Player to safely dismount.
     */
    private void dismountToSafeLocation(Player player, Location<World> originalPosition) {
        Location<World> loc = originalPosition;
        Vector3d pos = Vector3d.from(loc.getX(), loc.getBlockY(), loc.getZ());
        loc = loc.setPosition(pos);

        int[] priorityArray = new int[] {0, -1, 1};

        for (int x : priorityArray) {
            for (int y : priorityArray) {
                for (int z : priorityArray) {
                    Location<World> testLoc = loc.add(x, y, z);
                    if (checkMatter(testLoc, MatterProperty.Matter.SOLID)
                            && checkMatter(testLoc.add(0, 1, 0), MatterProperty.Matter.GAS)
                            && checkMatter(testLoc.add(0, 2, 0), MatterProperty.Matter.GAS)
                    ) {
                        player.setLocation(getTopOfBlock(testLoc));
                        //loc.get(Keys.DIRECTION).ifPresent(dir -> player.setRotation(LocationUtil.cartesianToEuler(dir.getOpposite().asOffset())));
                        return;
                    }
                }
            }
        }
        player.setLocation(player.getLocation().add(0, 1, 0));
    }

    public static Optional<MatterProperty.Matter> getMatter(Location<World> loc) {
        return loc.getBlockType().getProperty(MatterProperty.class).map(AbstractProperty::getValue);
    }

    public static boolean checkMatter(Location<World> loc, MatterProperty.Matter matter) {
        return getMatter(loc).filter(m -> m.equals(matter)).isPresent();
    }

    public static Location<World> getTopOfBlock(Location<World> loc) {
        BlockPos pos = new BlockPos(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        Vec3d end = new Vec3d(loc.getX(), loc.getBlockY(), loc.getZ());
        Vec3d start = end.add(0, 1.1, 0);

        RayTraceResult result = ((IBlockState) loc.getBlock()).collisionRayTrace((net.minecraft.world.World) loc.getExtent(), pos, start, end);
        if (result != null) {
            return loc.setPosition(Vector3d.from(result.hitVec.x, result.hitVec.y, result.hitVec.z));
        }
        return loc;
    }

    /**
     * Gets the chair by the chair entity.
     *
     * @param entity The chair entity.
     * @return The chair
     */
    private Chair<?> getChair(Entity entity) {
        return chairs.values().stream().filter((chair) -> chair.chairEntity.equals(entity)).findFirst().orElse(null);
    }

    /**
     * Gets the chair by the chair block.
     *
     * @param location The chair block.
     * @return The chair
     */
    private Chair<?> getChair(Location<?> location) {
        return chairs.values().stream().filter((chair) -> chair.chairLocation.getBlockPosition().equals(location.getBlockPosition())).findFirst().orElse(null);
    }

    @Listener
    public void onBlockClick(InteractBlockEvent.Secondary.MainHand event, @First Player player) {
        event.getTargetBlock().getLocation().ifPresent(location -> {
            if (!isValid(location))
                return;

            if (player.getItemInHand(HandTypes.MAIN_HAND).filter(item -> item.getType() != ItemTypes.AIR).isPresent()) {
                return;
            }

            if (player.get(Keys.IS_SNEAKING).orElse(false))
                return;

            if (!usePermissions.hasPermission(player)) {
                player.sendMessage(USE_PERMISSIONS);
                return;
            }
            Optional<Sign> optSign = getSign(location, new ArrayList<>(), location);
            if (requireSigns.getValue() && !optSign.isPresent()) {
                return;
            }

            if (event.getTargetSide() == Direction.DOWN
                    || (event.getTargetBlock().supports(Keys.DIRECTION) && event.getTargetSide() == event.getTargetBlock().require(Keys.DIRECTION))) {
                player.sendMessage(Text.of(TextColors.RED, "You cannot reach the chair from below/behind"));
                return;
            }

            if (getChair(location) != null) {
                player.sendMessage(Text.of(TextColors.RED, "Chair already occupied!"));
                return;
            }

            if (chairs.containsKey(player.getUniqueId())) {
                removeChair(chairs.get(player.getUniqueId()), true);
            }

            addChair(player, optSign.map(Sign::getSignData).orElse(null),location);
        });
    }

    @Listener
    public void onBlockBreak(ChangeBlockEvent.Break event) {
        event.getTransactions().forEach((transaction) -> transaction.getOriginal().getLocation().ifPresent((location) -> {
            Chair<?> chair = getChair(location);
            if (chair == null) {
                chair = getChair(location.add(0,1,0));
            }
            if (chair != null)
                removeChair(chair, true);
        }));
    }

    @Listener
    public void onDismount(RideEntityEvent.Dismount event, @First Player player) {
        if (event.getTargetEntity() instanceof ArmorStand) {
            Chair<?> chair = getChair(event.getTargetEntity());
            if (chair != null) {
                Location<World> loc = player.getLocation();
                player.sendMessage(Text.of(TextColors.YELLOW, "You stand up!"));
                Sponge.getScheduler().createTaskBuilder().execute(() -> {
                    removeChair(chair, false);
                    if (!exitAtEntry.getValue()) {
                        Sponge.getGame().getTeleportHelper().getSafeLocation(chair.chairLocation).ifPresent(player::setLocation);
                    }
                    else {
                        dismountToSafeLocation(player, loc);
                    }
                }).submit(CraftBookPlugin.inst());
            }
        }
    }

    @Listener
    public void onLogout(ClientConnectionEvent.Disconnect event) {
        Chair<?> chair = chairs.get(event.getTargetEntity().getUniqueId());
        if (chair != null)
            removeChair(chair, true);
    }

    // Stop players without correct permission to place certain signs
    @Listener
    public void onChangeSignEvent(ChangeSignEvent e, @First Player player) {
        Sign sign = e.getTargetTile();
        Location<World> signLoc = sign.getLocation();
        BlockState chairBlock = signLoc.add(signLoc.require(Keys.DIRECTION).getOpposite().asBlockOffset()).getBlock();
        if (!isAllowed(chairBlock)) {
            return;
        }
        SignData data = e.getText();
        for (CustomChair.Factory<?, ArmorStand> chairType : customChairs) {
            if (chairType.matches(data)) {
                if (!chairType.hasPermission(player)) {
                    player.sendMessage(Text.of(TextColors.RED, "You do not have permission to place a " + chairType.getName() + " sign"));
                    e.setCancelled(true);
                }
                else {
                    player.sendMessage(Text.of(TextColors.GREEN, "Successfully created " + chairType.getName()));
                }
                break;
            }
        }
    }

    private static List<SpongeBlockFilter> getDefaultBlocks() {
        // All stairs facing north,east,south,west
        List<SpongeBlockFilter> states = Lists.newArrayList();
        Sponge.getRegistry().getAllOf(BlockType.class).stream()
                .filter(blockType -> blockType.getName().toLowerCase().contains("stairs"))
                .forEach(blockType -> {
                    for (Direction horizontalDir : BlockUtil.getDirectHorizontalFaces()) {
                        states.add(new SpongeBlockFilter(blockType.getName() + "[" + horizontalDir.toString() + "]"));
                    }
                });
        return states;
    }

    /**
     * WARNING: USE INSTEAD OF {@link BlockUtil}'s methods
     * This checks via BlockType and checks if it is upside-down, allowing
     * different facing stairs and corner stairs etc.
     * @param state to check
     * @return if it is a valid chair block
     */
    private boolean isAllowed(BlockState state) {
        for (SpongeBlockFilter filter : allowedBlocks.getValue()) {
            for (BlockState filterState : filter.getApplicableBlocks()) {
                if (state.getType().equals(filterState.getType())) {
                    if (state.supports(Keys.PORTION_TYPE)) {
                        // Dont allow upside down
                        return state.require(Keys.PORTION_TYPE).equals(PortionTypes.BOTTOM);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String getPath() {
        return "mechanics/chairs";
    }

    @Override
    public ConfigValue<?>[] getConfigurationNodes() {
        return new ConfigValue[]{
                allowedBlocks,
                exitAtEntry,
                requireSigns,
                maxSignDistance,
                faceCorrectDirection,
                healAmount
        };
    }

    @Override
    public PermissionNode[] getPermissionNodes() {
        return new PermissionNode[]{
                usePermissions
        };
    }

    public static class Sit implements CommandExecutor {
        private final Chairs instance;
        Sit(Chairs mechanicInstance) {
            instance = mechanicInstance;
        }
        @NonNull
        @Override
        public CommandResult execute(@NonNull CommandSource src, @NonNull CommandContext args) throws CommandException {
            if (!(src instanceof Player)) {
                throw new CommandException(Text.of("Non-player cannot use this command!"));
            }
            Player player = (Player)src;
            if (player.getBaseVehicle() != player) {
                throw new CommandException(Text.of("You must not be riding anything to use /sit"));
            }
            Location<World> location = player.getLocation();
            // Prevent them negating fall damage or some other stupid shit by checking if they are on the ground.
            if (!player.isOnGround()) {
                throw new CommandException(Text.of("You must be on the ground to use /sit"));
            }
            // Create chair location.
            Vector3d chairLocation = Vector3d.from(player.getLocation().getX(), location.getY() - 0.2, player.getLocation().getZ());
            Entity entity = location.getExtent().createEntity(EntityTypes.ARMOR_STAND, chairLocation);
            entity.offer(Keys.INVISIBLE, true);
            entity.offer(Keys.HAS_GRAVITY, false);
            entity.offer(Keys.ARMOR_STAND_MARKER, true);
            // Incase somehow craftbook doesn't clean up, a quick /kill @e[name=CraftbookChair] would kill all chairs.
            entity.offer(Keys.DISPLAY_NAME, Text.of("CraftbookChair"));

            Sponge.getCauseStackManager().pushCause(player);
            // Incase some protection plugin cancels it.
            if (!location.getExtent().spawnEntity(entity)) {
                throw new CommandException(Text.of("Failed to create a sitable entity for you."));
            }
            Sponge.getCauseStackManager().popCause();

            Location<World> fromLocation = player.getLocation();

            if (!entity.addPassenger(player) /*|| !entity.hasPassenger(player)*/) {
                entity.remove();
                throw new CommandException(Text.of("Failed to sit you down."));
            }
            player.sendMessage(Text.of(TextColors.YELLOW, "You sit down"));

            instance.chairs.put(player.getUniqueId(), new Chair<>((ArmorStand) entity, location, fromLocation));

            return CommandResult.success();
        }
    }

    public static class Stand implements CommandExecutor {
        private final Chairs instance;
        Stand(Chairs mechanicInstance) {
            instance = mechanicInstance;
        }
        @NonNull
        @Override
        public CommandResult execute(@NonNull CommandSource src, @NonNull CommandContext args) throws CommandException {
            if (!(src instanceof Player)) {
                throw new CommandException(Text.of("Non-player cannot use this command!"));
            }
            Player player = (Player)src;
            Chair<?> chair = instance.chairs.get(player.getUniqueId());
            if (chair == null) {
                throw new CommandException(Text.of("You are not sitting on a chair!"));
            }
            instance.removeChair(chair, true);
            return CommandResult.success();
        }
    }

    /**
     * Data class that stores information necessary
     * to the functionality of chairs.
     */
    static class Chair<T extends Entity> {
        private final T chairEntity;
        private final Location<World> chairLocation;
        private final Location<World> playerExitLocation;

        Chair(T chairEntity, Location<World> chairLocation, Location<World> playerExitLocation) {
            this.chairEntity = chairEntity;
            this.chairLocation = chairLocation;
            this.playerExitLocation = playerExitLocation;
        }
    }

    /**
     * Class for custom chairs that implement different behaviour such as HealingChair
     * @see com.sk89q.craftbook.sponge.mechanics.Chairs.HealingChair
     * @param <T> chair entity
     */
    static abstract class CustomChair<T extends Entity> extends Chair<T> {
        CustomChair(T chairEntity, Location<World> chairLocation, Location<World> playerExitLocation) {
            super(chairEntity, chairLocation, playerExitLocation);
        }

        public static abstract class Factory<T extends CustomChair, E extends Entity> {
            /**
             * The permission to be checked when this sign is placed on a chair
             */
            private final SpongePermissionNode placePermission;

            Factory(SpongePermissionNode placePermission) {
                this.placePermission = placePermission;
            }

            public abstract T createInstance(E chairEntity, Location<World> chairLocation, Location<World> playerExitLocation);

            /**
             * Returns true if the sign passed in signifies a chair of this sign
             * @param signData of the sign that was placed
             * @return whether it is this type of chair
             */
            public abstract boolean matches(SignData signData);

            /**
             * Name of chair to be displayed when placed
             * @return String name of type
             */
            public abstract String getName();
            /**
             * Checks if player has permission to place this sign
             * Can be overridden for no permission etc.
             * @param player to check
             * @return true if they have permission
             */
            public boolean hasPermission(Player player) {
                return placePermission.hasPermission(player);
            }
        }
    }

    // Implementations of CustomChair
    static class HealingChair<T extends Entity> extends CustomChair<T> {

        HealingChair(T chairEntity, Location<World> chairLocation, Location<World> playerExitLocation) {
            super(chairEntity, chairLocation, playerExitLocation);
        }

        public static class Factory<E extends Entity> extends CustomChair.Factory<HealingChair, E> {
            Factory(SpongePermissionNode placePermission) {
                super(placePermission);
            }

            @Override
            public HealingChair createInstance(E chairEntity, Location<World> chairLocation, Location<World> playerExitLocation) {
                return new HealingChair<>(chairEntity, chairLocation, playerExitLocation);
            }

            @Override
            public boolean matches(SignData signData) {
                return SignUtil.getTextRaw(signData, 1).equals("[Sit Heal]");
            }

            @Override
            public String getName() {
                return "Sit Heal";
            }
        }
    }
}
