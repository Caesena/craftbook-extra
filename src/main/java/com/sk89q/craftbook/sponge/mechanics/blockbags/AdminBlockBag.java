/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.blockbags;

import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class AdminBlockBag implements IdentifiableBlockBag {

    public static final AdminBlockBag INSTANCE = new AdminBlockBag();

    @Override
    public long getId() {
        return -1;
    }

    @Override
    public void setId(long id) {
    }

    @Override
    public boolean has(List<ItemStack> itemStacks) {
        return true;
    }

    @Override
    public boolean has(Predicate<ItemStack> predicate, int amountRequired) {
        return true;
    }

    @Override
    public boolean canFit(List<ItemStack> itemStacks) {
        return true;
    }

    @Override
    public List<ItemStack> add(List<ItemStack> itemStacks) {
        return new ArrayList<>();
    }

    @Override
    public List<ItemStack> remove(List<ItemStack> itemStacks) {
        return new ArrayList<>();
    }

    @Override
    public int remove(Predicate<ItemStack> predicate, int amtToRemove) {
        return 0;
    }

    @Nullable
    @Override
    public ItemStack findStack(Predicate<ItemStack> predicate, int maxSize) {
        throw new UnsupportedOperationException();
    }
}
