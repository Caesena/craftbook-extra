/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.ics.pinsets;

import com.minecraftonline.util.nms.LocationWorldUtil;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.PinSet;
import com.sk89q.craftbook.sponge.mode.Modes;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public abstract class AbstractPinSet implements PinSet {

    public abstract int getInputCount();

    public abstract int getOutputCount();

    public void setOutput(int outputId, boolean powered, IC ic) {

        if(outputId == -1) return;

        if(ic.getMode().getType() == Modes.INVERT) powered = !powered;

        Location<World> block = getPinLocation(outputId + getInputCount(), ic);
        BlockState blockState = block.getBlock();
        if (blockState.getType() != BlockTypes.LEVER) return;
        if (blockState.get(Keys.POWERED).orElse(false) != powered) {

            //if (!block.supports(Keys.POWERED)) return;

            //block.offer(Keys.POWERED, powered);
            //TODO Replace this call when sponge fixes no Block Updates
            //LocationWorldUtil.setLeverSate(block.getLocatableBlock().get().getLocation(),ic.getBackBlock(),powered);
            LocationWorldUtil.toggleLever(block, false);
            //LocationWorldUtil.updateLever(ic.getBackBlock());
            //LocationWorldUtil.updateLever(ic.getBackBlock().getRelative(Direction.UP));
        }
    }

    public int getPinForLocation(IC ic, Location<World> location) {
        for(int i = 0; i < getInputCount() + getOutputCount(); i++)
            if(getPinLocation(i, ic).getBlockPosition().equals(location.getBlockPosition()))
                return i;
        return -1;
    }

    public boolean getInput(int inputId, IC ic) {
        Location<World> pinLocation = getPinLocation(inputId, ic);
        return inputId != -1 && (pinLocation.get(Keys.POWERED).orElse(false)
                || pinLocation.get(Keys.POWER).orElse(0) > 0)
                || pinLocation.getBlockType() == BlockTypes.REDSTONE_TORCH
                || pinLocation.getBlockType() == BlockTypes.POWERED_REPEATER
                || pinLocation.getBlockType() == BlockTypes.POWERED_COMPARATOR;
    }

    @Override
    public int getInputPowerLevel(int inputId, IC ic) {
        Location<World> pinLocation = getPinLocation(inputId, ic);
        return BlockUtil.getBlockPowerLevel(pinLocation).orElse(0);
    }

    public boolean getOutput(int outputId, IC ic) {
        return outputId != -1 && getPinLocation(getInputCount() + outputId, ic).get(Keys.POWERED).orElse(false);
    }

    public boolean isValid(int id, IC ic) {
        // TODO Check directions here to make sure it's actually powering the face
        return BlockUtil.isPowerSource(getPinLocation(id, ic));
    }

    public boolean isTriggered(int id, IC ic) {
        return id == ic.getTriggeredPin();
    }

    public boolean isAnyTriggered(IC ic){
        for (int i = 0; i < ic.getPinSet().getInputCount(); i++) {
            if (ic.getPinSet().isValid(i, ic)) {
                if (ic.getPinSet().getInput(i, ic)){
                    return true;
                }
            }
        }
        return false;
    }

    public abstract String getName();

    public abstract Location<World> getPinLocation(int id, IC ic);
}
