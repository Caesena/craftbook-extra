/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.area;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.legacy.blockbags.MultiNearbyChestBlockBag;
import com.minecraftonline.legacy.ics.chips.world.block.BlockPlacingIC;
import com.minecraftonline.mechanic.SignMechanicCacheManager;
import com.minecraftonline.util.IDUtil;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.PermissionNode;
import com.sk89q.craftbook.core.util.documentation.DocumentationProvider;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.Humanoid;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Module(id = "door", name = "Door", onEnable="onInitialize", onDisable="onDisable")
public class Door extends SimpleArea implements DocumentationProvider {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final ConfigValue<Integer> maximumLength = new ConfigValue<>("maximum-length", "The maximum length the door can be.", 16);
    private final ConfigValue<Integer> maximumWidth = new ConfigValue<>("maximum-width", "The maximum width each side of the door can be. The overall max width is this*2 + 1.", 5);

    @Override
    public void onInitialize() throws CraftBookException {
        super.loadCommonConfig(config);
        super.registerCommonPermissions();

        maximumLength.load(config);
        maximumWidth.load(config);

        cache.registerCache(this, (loc, signData) -> {
            if (!isValidNoCache(loc, signData)) {
                return null;
            }
            registerRedstoneListener(loc);
            return new DoorCachedData(signData.lines().get());
        });
    }

    @Override
    public Collection<Vector3i> createInputList(Location<World> mechanicLocation) {
        Vector3i pos = mechanicLocation.getBlockPosition();
        return Arrays.asList(
                pos.add(1, 0, 0),
                pos.add(-1, 0, 0),
                pos.add(0, 0, 1),
                pos.add(0, 0, -1)
        );
    }

    public class DoorCachedData extends SimpleArea.SimpleAreaCache {
        protected DoorCachedData(List<Text> lines) {
            super(lines);
        }
    }

    @Override
    public boolean triggerMechanic(Location<World> signBlock, SignMechanicCacheManager.CachedData cachedData, Humanoid human, Boolean forceState) {
        String line2 = cachedData.getLinePlain(1);

        if (line2.equals("[Door]")) {
            return false; // These can't be activated.
        }

        Direction direction = "[Door Up]".equals(line2) ? Direction.UP : Direction.DOWN;

        Location<World> baseBlock = signBlock.getRelative(direction);

        if (!BlockUtil.doesStatePassFilters(allowedBlocks.getValue(), baseBlock.getBlock())) {
            if (human instanceof CommandSource)
                ((CommandSource) human).sendMessage(Text.builder("Can't use this material for a door!").build());
            return true;
        }

        Location<World> otherSide = BlockUtil.getNextMatchingSign(signBlock, direction, maximumLength.getValue(), this::isMechanicSign);
        if (otherSide == null) {
            if (human instanceof CommandSource) ((CommandSource) human).sendMessage(missingOtherEnd);
            return true;
        }

        final Location<World> bottomSign;
        final Location<World> topSign;

        if (direction == Direction.UP) {
            bottomSign = signBlock;
            topSign = otherSide;
        }
        else {
            topSign = signBlock;
            bottomSign = otherSide;
        }

        final Location<World> bottomBlock = bottomSign.getRelative(Direction.UP);
        final Location<World> topBlock = topSign.getRelative(Direction.DOWN);

        if (!bottomBlock.getBlock().equals(topBlock.getBlock())) {
            if (human instanceof CommandSource)
                ((CommandSource) human).sendMessage(Text.builder("Both ends must be the same material!").build());
            return true;
        }

        final Direction leftDir = SignUtil.getLeft(signBlock);
        final Direction rightDir = SignUtil.getRight(signBlock);

        BlockState block = baseBlock.getBlock();

        final int leftBlocks = countLength(bottomBlock, topBlock.getBlockY() - bottomBlock.getBlockY(), leftDir, block);
        final int rightBlocks = countLength(bottomBlock, topBlock.getBlockY() - bottomBlock.getBlockY(), rightDir, block);

        World world = signBlock.getExtent();
        Vector3i bottomLeftPos = bottomBlock.add(leftDir.asBlockOffset().mul(leftBlocks)).getBlockPosition();
        Vector3i topRightPos = topBlock.add(rightDir.asBlockOffset().mul(rightBlocks)).getBlockPosition();

        Vector3i min = bottomLeftPos.min(topRightPos).add(0, 1, 0);
        Vector3i max = bottomLeftPos.max(topRightPos).sub(0, 1, 0);

        final boolean filling;
        final int emptyBlocks;
        {
            emptyBlocks = countEmpty(world, min, max);
            // Fill gaps unless every spot is full.
            filling = emptyBlocks != 0;
        }

        ItemStack blockBagItem = IDUtil.getItemStackFromBlockState(block);
        BlockBag blockBag = getBlockBag(signBlock);

        if (blockBag == null) {
            ((CommandSource) human).sendMessage(Text.of(TextColors.RED, "You have no where to put the blocks!"));
            return false;
        }

        if (filling) {
            int requiredItems = emptyBlocks * blockBagItem.getQuantity();
            int failedToTake = blockBag.remove(BlockPlacingIC.getPredicate(blockBagItem), requiredItems);
            requiredItems -= failedToTake;
            int remainder = requiredItems % blockBagItem.getQuantity();
            if (remainder != 0) {
                // Refund if we took an amount that couldn't place a full block. (If we're working with half-slabs etc.
                requiredItems -= remainder;
                blockBag.add(BlockPlacingIC.getPickupBlocks(blockBagItem, remainder));
            }
            fillEmpty(world, min, max, baseBlock.getBlock(), requiredItems / blockBagItem.getQuantity());
        } else {
            int toPickup = countPickupBlocks(world, min, max, baseBlock.getBlock());

            List<ItemStack> itemStacks = BlockPlacingIC.getPickupBlocks(blockBagItem, toPickup);
            if (blockBag.canFit(itemStacks)) {
                int amt = pickupBlocks(world, min, max, baseBlock.getBlock());
                if (toPickup != amt) {
                    itemStacks = BlockPlacingIC.getPickupBlocks(blockBagItem, amt);
                }
                blockBag.add(itemStacks);
            }
        }
        return true;
    }

    public int countLength(final Location<World> startBottom, final int height, final Direction dir, BlockState block) {
        if (height < 1) {
            throw new IllegalStateException("Height must be more than 1, was: " + height);
        }
        int dist = 0;
        final int max = maximumWidth.getValue();
        Location<World> curBottom = startBottom;
        while (dist <= max) {
            curBottom = curBottom.getRelative(dir);
            if (!curBottom.getBlock().equals(block) || !curBottom.add(0, height, 0).getBlock().equals(block)) {
                break;
            }
            dist++;
        }
        return dist;
    }

    public int countEmpty(World world, Vector3i min, Vector3i max) {
        int placeableBlocks = 0;
        for (int z = min.getZ(); z <= max.getZ(); z++)
            for (int y = min.getY(); y <= max.getY(); y++)
                for (int x = min.getX(); x <= max.getX(); x++)
                    if (world.getBlockType(x, y, z) == BlockTypes.AIR) placeableBlocks++;

        return placeableBlocks;
    }

    public int fillEmpty(World world, Vector3i min, Vector3i max, BlockState blockstate, int maxToFill) {
        int affected = 0;
        for (int x = min.getX(); x <= max.getX(); x++)
            for (int y = min.getY(); y <= max.getY(); y++)
                for (int z = min.getZ(); z <= max.getZ(); z++) {
                    if (affected >= maxToFill) {
                        return affected;
                    }
                    if (world.getBlockType(x, y, z) == BlockTypes.AIR && world.setBlock(x, y, z, blockstate)) {
                        affected++;
                    }
                }

        return affected;
    }

    private int countPickupBlocks(World world, Vector3i min, Vector3i max, BlockState blockstate) {
        int blocks = 0;
        for (int x = min.getX(); x <= max.getX(); x++) {
            for (int y = min.getY(); y <= max.getY(); y++) {
                for (int z = min.getZ(); z <= max.getZ(); z++) {
                    if (world.getBlock(x, y, z).equals(blockstate)) {
                        blocks++;
                    }
                }
            }
        }

        return blocks;
    }

    private int pickupBlocks(World world, Vector3i min, Vector3i max, BlockState blockState) {
        int affected = 0;
        for (int x = min.getX(); x <= max.getX(); x++)
            for (int y = min.getY(); y <= max.getY(); y++)
                for (int z = min.getZ(); z <= max.getZ(); z++) {
                    if (world.getBlock(x, y, z).equals(blockState) && world.setBlockType(x, y, z, BlockTypes.AIR)) {
                        affected++;
                    }
                }

        return affected;
    }

    @Override
    public BlockBag getBlockBag(Location<World> location) {
        return BlockBagManager.getOrCreateCachedBlockBag(BlockBagManager.BlockBagType.MULTI_NEARBY_CHEST, location,
                MultiNearbyChestBlockBag.DEFAULT_RADIUS, MultiNearbyChestBlockBag.getChestBlockTypes());
    }

    @Override
    public String[] getValidSigns() {
        return new String[]{"[Door Up]", "[Door Down]", "[Door]"};
    }

    @Override
    public List<SpongeBlockFilter> getDefaultBlocks() {
        List<SpongeBlockFilter> states = Lists.newArrayList();
        states.add(new SpongeBlockFilter(BlockTypes.PLANKS));
        states.add(new SpongeBlockFilter(BlockTypes.STONEBRICK));
        states.add(new SpongeBlockFilter(BlockTypes.COBBLESTONE));
        return states;
    }

    @Override
    public String getPath() {
        return "mechanics/door";
    }

    @Override
    public ConfigValue<?>[] getConfigurationNodes() {
        return new ConfigValue<?>[]{
                allowedBlocks,
                allowRedstone,
                maximumLength,
                maximumWidth
        };
    }

    @Override
    public PermissionNode[] getPermissionNodes() {
        return new PermissionNode[]{
                createPermissions,
                usePermissions
        };
    }
}
