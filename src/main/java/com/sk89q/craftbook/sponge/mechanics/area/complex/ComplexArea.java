/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.area.complex;


import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.mechanic.SignMechanicCacheManager;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.PermissionNode;
import com.sk89q.craftbook.core.util.documentation.DocumentationProvider;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.CraftbookTriggeredMechanic;
import com.sk89q.craftbook.sponge.mechanics.area.complex.command.DeleteCommand;
import com.sk89q.craftbook.sponge.mechanics.area.complex.command.ListCommand;
import com.sk89q.craftbook.sponge.mechanics.area.complex.command.SaveCommand;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeSignMechanic;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import com.sk89q.craftbook.sponge.util.data.mutable.LastPowerData;
import com.sk89q.craftbook.sponge.util.data.mutable.NamespaceData;
import ninja.leaping.configurate.ConfigurationNode;
import org.apache.commons.lang3.StringUtils;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.CauseStackManager;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.block.tileentity.ChangeSignEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Tuple;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static com.sk89q.craftbook.sponge.util.locale.TranslationsManager.USE_PERMISSIONS;

@Module(id = "area", name = "Area", onEnable="onInitialize", onDisable="onDisable")
public class ComplexArea extends SpongeSignMechanic implements DocumentationProvider {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.area", "Allows the user to create the ToggleArea mechanic.", PermissionDescription.ROLE_USER);
    private final SpongePermissionNode createSavePermissions = new SpongePermissionNode("craftbook.area.create.save", "Allows the user to create the ToggleArea Save-Only mechanic.", PermissionDescription.ROLE_USER);
    private final SpongePermissionNode createGlobalPermissions = new SpongePermissionNode("craftbook.area.create.global", "Allows the user to create global ToggleArea mechanics.", PermissionDescription.ROLE_STAFF);
    private final SpongePermissionNode createOtherPermissions = new SpongePermissionNode("craftbook.area.create.other", "Allows the user to create ToggleArea mechanics with other namespaces.", PermissionDescription.ROLE_STAFF);
    private final SpongePermissionNode usePermissions = new SpongePermissionNode("craftbook.area.use", "Allows the user to use the ToggleArea mechanic.", PermissionDescription.ROLE_USER);

    private final SpongePermissionNode commandSavePermissions = new SpongePermissionNode("craftbook.area.save", "Allows the user to save ToggleArea regions.", PermissionDescription.ROLE_USER);
    public SpongePermissionNode commandSaveOtherPermissions = new SpongePermissionNode("craftbook.area.save.other", "Allows the user to save ToggleArea regions.", PermissionDescription.ROLE_STAFF);
    public SpongePermissionNode commandSaveBypassLimitPermissions = new SpongePermissionNode("craftbook.area.save.bypass-limit", "Allows the user to bypass area limits.", PermissionDescription.ROLE_ADMIN);
    private final SpongePermissionNode commandDeletePermissions = new SpongePermissionNode("craftbook.area.delete", "Allows the user to delete ToggleArea regions.", PermissionDescription.ROLE_USER);
    public SpongePermissionNode commandDeleteOtherPermissions = new SpongePermissionNode("craftbook.area.delete.other", "Allows the user to delete ToggleArea regions.", PermissionDescription.ROLE_STAFF);
    private final SpongePermissionNode commandListPermissions = new SpongePermissionNode("craftbook.area.list", "Allows the user to list ToggleArea regions.", PermissionDescription.ROLE_USER);
    public SpongePermissionNode commandListOtherPermissions = new SpongePermissionNode("craftbook.area.list.other", "Allows the user to list ToggleArea regions.", PermissionDescription.ROLE_STAFF);

    public ConfigValue<Integer> maxAreaSize = new ConfigValue<>("maximum-size", "The maximum amount of blocks that the saved areas can contain. -1 to disable limit.", 5000);
    public ConfigValue<Integer> maxPerUser = new ConfigValue<>("maximum-per-user", "The maximum amount of areas that a namespace can own. 0 to disable limit.", 30);
    public ConfigValue<Boolean> allowRedstone = new ConfigValue<>("allow-redstone", "Whether to allow redstone to be used to trigger this mechanic or not", true);

    private CommandMapping areaCommandMapping;

    private static final Pattern MARKER_PATTERN = Pattern.compile("^-[A-Za-z0-9_]*?-$");

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        maxAreaSize.load(config);
        maxPerUser.load(config);
        allowRedstone.load(config);

        CommandSpec saveCommand = CommandSpec.builder()
                .description(Text.of("Command to save selected WorldEdit regions."))
                .permission(commandSavePermissions.getNode())
                .executor(new SaveCommand(this))
                .arguments(GenericArguments.onlyOne(GenericArguments.string(Text.of("id"))), GenericArguments.optional(GenericArguments.onlyOne(GenericArguments.string(Text.of("namespace")))))
                .build();

        CommandSpec deleteCommand = CommandSpec.builder()
                .description(Text.of("Command to delete saved ToggleArea regions."))
                .permission(commandDeletePermissions.getNode())
                .executor(new DeleteCommand(this))
                .arguments(GenericArguments.onlyOne(GenericArguments.string(Text.of("id"))), GenericArguments.optional(GenericArguments.onlyOne(GenericArguments.string(Text.of("namespace")))))
                .build();

        CommandSpec listCommand = CommandSpec.builder()
                .description(Text.of("Command to list saved ToggleArea regions."))
                .permission(commandListPermissions.getNode())
                .executor(new ListCommand(this))
                .arguments(GenericArguments.optional(GenericArguments.onlyOne(GenericArguments.string(Text.of("namespace")))))
                .build();

        CommandSpec mainAreaCommand = CommandSpec.builder()
                .description(Text.of("Base command for the ToggleArea system."))
                .child(saveCommand, "save")
                .child(deleteCommand, "delete", "del")
                .child(listCommand, "list")
                .build();

        areaCommandMapping = Sponge.getCommandManager().register(CraftBookPlugin.inst(), mainAreaCommand, "area", "togglearea").orElse(null);

        SignMechanicCacheManager.getInstance().registerCache(this, ((location, signData) -> {
            if (!isValidNoCache(location)) {
                return null;
            }
            return new ComplexAreaCachedData(signData.lines().get());
        }));
    }

    public static class ComplexAreaCachedData extends SignMechanicCacheManager.CachedData {
        protected ComplexAreaCachedData(List<Text> lines) {
            super(lines);
        }
    }

    @Override
    public void onDisable() {
        super.onDisable();

        if (areaCommandMapping != null) {
            Sponge.getCommandManager().removeMapping(areaCommandMapping);
        }
    }

    @Listener
    public void onSignChange(ChangeSignEvent event, @First Player player) {
        for(String line : getValidSigns()) {
            if(SignUtil.getTextRaw(event.getText(), 1).equalsIgnoreCase(line)) {
                if(!createPermissions.hasPermission(player) && !("[SaveArea]".equals(line) && createSavePermissions.hasPermission(player))) {
                    player.sendMessage(Text.of(TextColors.RED, "You do not have permission to create this mechanic!"));
                    event.setCancelled(true);
                } else {
                    List<Text> lines = event.getText().lines().get();
                    lines.set(1, Text.of(line));

                    String namespace = lines.get(0).toPlain();
                    boolean personal = false;
                    if ("global".equalsIgnoreCase(namespace)) {
                        if (!createGlobalPermissions.hasPermission(player)) {
                            player.sendMessage(Text.of(TextColors.RED, "You do not have permission to create global ToggleAreas!"));
                            event.setCancelled(true);
                            return;
                        } else {
                            namespace = "GLOBAL";
                        }
                    } else if (namespace.trim().isEmpty() || !createOtherPermissions.hasPermission(player)) {
                        namespace = player.getUniqueId().toString();
                        personal = true;
                    }

                    lines.set(0, Text.of(personal ? player.getName() : namespace));

                    if(!isValidArea(namespace, lines.get(2).toPlain(), lines.get(3).toPlain())) {
                        player.sendMessage(Text.of(TextColors.RED, "This area is missing!"));
                        event.setCancelled(true);
                        return;
                    }

                    event.getTargetTile().offer(new NamespaceData(namespace));

                    event.getText().set(Keys.SIGN_LINES, lines);

                    player.sendMessage(Text.of(TextColors.YELLOW, "Created ToggleArea!"));
                }

                break;
            }
        }
    }

    @Listener
    public void onBlockChange(ChangeBlockEvent.Post event) {
        if(!allowRedstone.getValue())
            return;

        event.getTransactions().forEach(blockSnapshotTransaction -> {
            Location<World> baseLocation = blockSnapshotTransaction.getFinal().getLocation().get();
            BlockState endState = blockSnapshotTransaction.getFinal().getExtendedState();

            Tuple<Boolean, Set<Location<World>>> endTuple = ICSocket.getPoweredInState(endState, baseLocation);

            Collection<Location<World>> locations;

            if (endTuple == null) {
                locations = BlockUtil.getAdjacentExcept(baseLocation);
            }
            else {
                locations = endTuple.getSecond();
            }

            boolean powered = endTuple != null && endTuple.getFirst();

            for (Location<World> location : locations) {
                SignMechanicCacheManager.CachedData cachedData = cache.getCached(this, location);
                if (cachedData == null) {
                    continue;
                }
                triggerMechanic(location, cachedData, powered);
                int powerLevelFake = powered ? 15 : 0; // TODO: get real power level?
                location.offer(new LastPowerData(powerLevelFake));
            }
        });
    }

    @Listener
    public void onRightClick(InteractBlockEvent.Secondary.MainHand event, @First Player human) {
        event.getTargetBlock().getLocation().ifPresent(location -> {
            ComplexAreaCachedData cachedData = SignMechanicCacheManager.getInstance().getCached(this, location);
            if (cachedData != null) {
                try (CauseStackManager.StackFrame frame = Sponge.getCauseStackManager().pushCauseFrame()) {
                    frame.pushCause(new CraftbookTriggeredMechanic(location));

                    location.getTileEntity().ifPresent((sign -> {
                        if (human == null || usePermissions.hasPermission(human)) {
                            if (triggerMechanic(location, cachedData, null)) {
                                event.setCancelled(true);
                                if (human != null)
                                    human.sendMessage(Text.of(TextColors.YELLOW, "Toggled!"));
                            }
                        } else {
                            human.sendMessage(USE_PERMISSIONS);
                        } }));
                }
            }
        });
    }

    public boolean triggerMechanic(Location<World> location, SignMechanicCacheManager.CachedData cachedData, Boolean forceState) {

        boolean save = "[SaveArea]".equals(cachedData.getLinePlain(1));


        String namespace = location.get(CraftBookKeys.NAMESPACE).orElse(null);
        String id = StringUtils.replace(cachedData.getLinePlain(2), "-", "").toLowerCase();
        String inactiveID = StringUtils.replace(cachedData.getLinePlain(3), "-", "").toLowerCase();

        if (namespace == null) {
            return false;
        }

        try {
            CuboidCopy copy;

            boolean state = checkToggleState(cachedData);
            if (forceState != null) {
                state = forceState;
            }

            if (state) {
                copy = CopyManager.load(location.getExtent(), namespace, id);

                if (save) {
                    copy.copy();
                    CopyManager.save(namespace, id, copy);
                }

                if (!inactiveID.isEmpty() && !"--".equals(inactiveID)) {
                    copy = CopyManager.load(location.getExtent(), namespace, inactiveID);
                    copy.paste();
                } else {
                    copy.clear();
                }

                setToggleState(cachedData, location, false);
            } else {
                if (save && !inactiveID.isEmpty() && !"--".equals(inactiveID)) {
                    copy = CopyManager.load(location.getExtent(), namespace, inactiveID);
                    copy.copy();
                    CopyManager.save(namespace, inactiveID, copy);
                }

                copy = CopyManager.load(location.getExtent(), namespace, id);
                copy.paste();
                setToggleState(cachedData, location, true);
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean isValid(Location<World> location) {
        return SignMechanicCacheManager.getInstance().getCached(this, location) != null;
    }

    public boolean isValidNoCache(Location<World> location) {
        return super.isValid(location);
    }

    private static boolean isValidArea(String namespace, String areaOn, String areaOff) {
        if (CopyManager.isExistingArea(CraftBookPlugin.inst().getWorkingDirectory(), namespace, areaOn)) {
            if (areaOff == null || areaOff.isEmpty() || "--".equals(areaOff)) return true;
            return CopyManager.isExistingArea(CraftBookPlugin.inst().getWorkingDirectory(), namespace, areaOff);
        }
        return false;
    }

    private static boolean checkToggleState(SignMechanicCacheManager.CachedData cachedData) {
        String line3 = cachedData.getLinePlain(2).toLowerCase();
        String line4 = cachedData.getLinePlain(3).toLowerCase();
        return MARKER_PATTERN.matcher(line3).matches() || !("--".equals(line4) || MARKER_PATTERN.matcher(line4).matches());
    }

    private static void setToggleState(SignMechanicCacheManager.CachedData cachedData, Location<World> location, boolean state) {
        int toToggleOn = state ? 2 : 3;
        int toToggleOff = state ? 3 : 2;
        List<Text> lines = cachedData.lines;
        lines.set(toToggleOff, Text.of(cachedData.lines.get(toToggleOff).toPlain().replace("-", "")));
        lines.set(toToggleOn, Text.of("-", cachedData.lines.get(toToggleOn), "-"));
        location.offer(Keys.SIGN_LINES, lines);
    }

    public String[] getValidSigns() {
        return new String[]{
                "[SaveArea]",
                "[Area]"
        };
    }

    @Override
    public String getName() {
        return "ToggleArea";
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Override
    public String getPath() {
        return "mechanics/togglearea";
    }

    @Override
    public ConfigValue<?>[] getConfigurationNodes() {
        return new ConfigValue<?>[]{
                maxAreaSize,
                maxPerUser,
                allowRedstone
        };
    }

    @Override
    public PermissionNode[] getPermissionNodes() {
        return new PermissionNode[]{
                createPermissions,
                usePermissions,
                createSavePermissions,
                createGlobalPermissions,
                createOtherPermissions,
                commandSavePermissions,
                commandSaveOtherPermissions,
                commandSaveBypassLimitPermissions,
                commandListPermissions,
                commandListOtherPermissions,
                commandDeletePermissions,
                commandDeleteOtherPermissions
        };
    }
}
