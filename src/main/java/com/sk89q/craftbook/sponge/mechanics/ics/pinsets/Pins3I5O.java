/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.ics.pinsets;

import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class Pins3I5O extends AbstractPinSet {
    @Override
    public int getInputCount() {
        return 3;
    }

    @Override
    public int getOutputCount() {
        return 5;
    }

    @Override
    public String getName() {
        return "3I50";
    }

    @Override
    public Location<World> getPinLocation(int id, IC ic) {
        Location<World> loc = ic.getBlock();
        final Direction facing = ic.getFront();
        switch (id) {
            // Inputs
            case 0:
                return loc.getRelative(facing);
            case 1:
                return loc.getRelative(SignUtil.getClockWise(facing));
            case 2:
                return loc.getRelative(SignUtil.getCounterClockWise(facing));
            // Outputs
            case 3:
                return ic.getBlock().add(facing.asBlockOffset().mul(-3));
            case 4:
                return ic.getBlock().add(facing.asBlockOffset().mul(-2)).getRelative(SignUtil.getCounterClockWise(facing));
            case 5:
                return ic.getBlock().add(facing.asBlockOffset().mul(-2)).getRelative(SignUtil.getClockWise(facing));
            case 6:
                return ic.getBlock().getRelative(facing).getRelative(SignUtil.getCounterClockWise(facing));
            case 7:
                return ic.getBlock().getRelative(facing).getRelative(SignUtil.getClockWise(facing));

            default:
                return null;
        }
    }
}
