/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.ics;

import com.minecraftonline.ic.*;
import com.minecraftonline.legacy.ics.BetweenTime;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.legacy.ics.chips.logic.Monoflop;
import com.minecraftonline.legacy.ics.chips.logic.Multiplexer;
import com.minecraftonline.legacy.ics.chips.world.block.Bridge;
import com.minecraftonline.legacy.ics.chips.world.block.Door;
import com.minecraftonline.legacy.ics.chips.world.block.FlexSet;
import com.minecraftonline.legacy.ics.chips.world.block.FlexSetAdmin;
import com.minecraftonline.legacy.ics.chips.world.block.SetBlockAbove;
import com.minecraftonline.legacy.ics.chips.world.block.SetBlockBelow;
import com.minecraftonline.legacy.ics.chips.world.entity.AreaPlanter;
import com.minecraftonline.legacy.ics.chips.world.entity.ArrowBarrage;
import com.minecraftonline.legacy.ics.chips.world.entity.ArrowShooter;
import com.minecraftonline.legacy.ics.chips.world.entity.CBWarpArea;
import com.minecraftonline.legacy.ics.chips.world.entity.ChestCollector;
import com.minecraftonline.legacy.ics.chips.world.entity.ChestDispenser;
import com.minecraftonline.legacy.ics.chips.world.entity.ContainerDispenser;
import com.minecraftonline.legacy.ics.chips.world.entity.Destination;
import com.minecraftonline.legacy.ics.chips.world.entity.EggBarrage;
import com.minecraftonline.legacy.ics.chips.world.entity.EggShooter;
import com.minecraftonline.legacy.ics.chips.world.entity.FireballShooter;
import com.minecraftonline.legacy.ics.chips.world.entity.Fireworks;
import com.minecraftonline.legacy.ics.chips.world.entity.HitMobAbove;
import com.minecraftonline.legacy.ics.chips.world.entity.HitPlayerAbove;
import com.minecraftonline.legacy.ics.chips.world.entity.HumansOnly;
import com.minecraftonline.legacy.ics.chips.world.entity.ItemSpawner;
import com.minecraftonline.legacy.ics.chips.world.entity.Lightning;
import com.minecraftonline.legacy.ics.chips.world.entity.MobZapper;
import com.minecraftonline.legacy.ics.chips.world.entity.ParticleEmitter;
import com.minecraftonline.legacy.ics.chips.world.entity.Planter;
import com.minecraftonline.legacy.ics.chips.world.entity.SnowBarrage;
import com.minecraftonline.legacy.ics.chips.world.entity.SnowShooter;
import com.minecraftonline.legacy.ics.chips.world.entity.Transporter;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.CommandControlled;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.Marquee;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.MarqueeTransmitter;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.MessageAll;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.MessageNearby;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.NamedNearby;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.PasswordControlled;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.SendMessage;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.ServerLog;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.ServerLogNearby;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.ServerLogNearbyPlus;
import com.minecraftonline.legacy.ics.chips.world.redstone.Pulse;
import com.minecraftonline.legacy.ics.chips.world.redstone.TriggerReader;
import com.minecraftonline.legacy.ics.chips.world.sensor.BlockDetector;
import com.minecraftonline.legacy.ics.chips.world.sensor.InArea;
import com.minecraftonline.legacy.ics.chips.world.sensor.MobAbove;
import com.minecraftonline.legacy.ics.chips.world.sensor.MobNear;
import com.minecraftonline.legacy.ics.chips.world.sensor.PlayerAbove;
import com.minecraftonline.legacy.ics.chips.world.sensor.PlayerNear;
import com.minecraftonline.legacy.ics.chips.world.sensor.PlayerOnline;
import com.minecraftonline.legacy.ics.chips.world.sensor.RainSensor;
import com.minecraftonline.legacy.ics.chips.world.sensor.SenseLava;
import com.minecraftonline.legacy.ics.chips.world.sensor.SenseLight;
import com.minecraftonline.legacy.ics.chips.world.sensor.SenseWater;
import com.minecraftonline.legacy.ics.chips.world.sensor.StormSensor;
import com.minecraftonline.legacy.ics.chips.world.weather.FalseWeather;
import com.minecraftonline.legacy.ics.chips.world.weather.HolySmite;
import com.minecraftonline.legacy.ics.chips.world.weather.SimpleWeatherControl;
import com.minecraftonline.legacy.ics.chips.world.weather.WeatherControl;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.AndGate;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.Clock;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.CombinationLock;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.Counter;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.Dispatcher;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.DownCounter;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.EdgeTriggerDFlipFlop;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.FullAdder;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.FullSubtractor;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.HalfAdder;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.HalfSubtractor;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.InvertedRSNandLatch;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.Inverter;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.JKFlipFlop;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.LevelTriggerDFlipFlop;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.MemoryAccess;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.MemorySetter;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.NandGate;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.RSNandLatch;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.RSNorLatch;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.RandomBit;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.Repeater;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.ToggleFlipFlop;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.WorldTimeModulus;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.XnorGate;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.logic.XorGate;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.block.BlockReplacer;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.entity.EntitySpawner;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.entity.ItemDispenser;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.ProgrammableFireworksDisplay;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.ZeusBolt;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless.WirelessReceiver;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.miscellaneous.wireless.WirelessTransmitter;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.sensor.DaySensor;
import com.sk89q.craftbook.sponge.mechanics.ics.chips.world.weather.TimeControlAdvanced;
import com.sk89q.craftbook.sponge.mechanics.ics.factory.SerializedICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.plc.PlcFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.plc.lang.Perlstone;
import org.spongepowered.api.Sponge;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class ICManager {

    private static final SortedSet<ICType<? extends IC>> registeredICTypes = new TreeSet<>(Comparator.comparing(ICType::getModel));
    private static final Map<Class<? extends ICFactory>, ICType<?>> icFactoryTypeMap = new HashMap<>();
    private static final Map<Class<? extends ICFactory>, ICType<?>> icFactorySelfTriggeringTypeMap = new HashMap<>();
    private static final Set<Class<?>> registeredBuilderClasses = new HashSet<>();

    static {
        //SISO
        registerICType(new ICType<>("MC1000", "REPEATER", "Repeater", "Repeats a redstone signal.", new Repeater.Factory(), "AISO"));
        registerICType(new ICType<>("MC1001", "INVERTER", "Inverter", "Inverts a redstone signal.", new Inverter.Factory(), "AISO"));

        registerICType(new ICType<>("MC1017", "RE T FLIP", "Toggle Flip Flop RE", "Toggles output on high.", new ToggleFlipFlop.Factory(true)));
        registerICType(new ICType<>("MC1018", "FE T FLIP", "Toggle Flip Flip FE", "Toggles output on low.", new ToggleFlipFlop.Factory(false)));

        registerICType(new ICType<>("MC1020", "RANDOM BIT", "Random Bit", "Randomly sets the output on high.", new RandomBit.Factory()));

        registerICType(new ICType<>("MC1025", "TIME MODULUS", "World Time Modulus", "Outputs high when the world time is odd.", new WorldTimeModulus.Factory()));

        registerICType(new ICType<>("MC1110", "TRANSMITTER", "Wireless Transmitter", "Transmits a wireless redstone signal.", new WirelessTransmitter.Factory(), "AIZO"));
        registerICType(new ICType<>("MC1111", "RECEIVER", "Wireless Receiver", "Receives a wireless redstone signal.", new WirelessReceiver.Factory()));
        registerICType(new ICType<>("MC0111", "RECEIVER", "Wireless Receiver", "Receives a wireless redstone signal.", new WirelessReceiver.Factory()));

        registerICType(new ICType<>("MC1200", "SPAWNER", "Entity Spawner", "Spawns an entity with specified data.", new EntitySpawner.Factory(), "AISO"));
        registerICType(new ICType<>("MC1201", "DISPENSER", "Item Dispenser", "Spawns in items with specified data.", new ItemDispenser.Factory(), "AISO"));

        registerICType(new ICType<>("MC1203", "ZEUS BOLT", "Zeus Bolt", "Strikes a location with lightning.", new ZeusBolt.Factory(), "AISO"));

        registerICType(new ICType<>("MC1230", "SENSE DAY", "Daylight Sensor", "Outputs high if it is day.", new DaySensor.Factory()));

        registerICType(new ICType<>("MC1249", "BLOCK REPLACER", "Block Replacer", "Searches a nearby area and replaces blocks accordingly.", new BlockReplacer.Factory()));

        registerICType(new ICType<>("MC1253", "FIREWORK", "Programmable Firework Display", "Plays a firework show from a file.", new ProgrammableFireworksDisplay.Factory(), "AISO"));

        registerICType(new ICType<>("MC1420", "CLOCK", "Clock", "Outputs high every X ticks when input is high.", new Clock.Factory()));

        registerICType(new ICType<>("MC1500","PLAYER ONLINE?","Player Online?","Outputs when a specified player is online.", new PlayerOnline.Factory()));

        //SI3O
        registerICType(new ICType<>("MC2020", "RANDOM 3", "Random 3-Bit", "Randomly sets the outputs on high.", new RandomBit.Factory(), "SI3O"));

        registerICType(new ICType<>("MC2300", "ROM GET", "ROM Get", "Gets the memory state from a file for usage in the MemorySetter/Access IC group.", new MemoryAccess.Factory(), "SI3O"));

        //3ISO
        registerICType(new ICType<>("MC3002", "AND", "And Gate", "Outputs high if all inputs are high.", new AndGate.Factory(), "3ISO"));
        registerICType(new ICType<>("MC3003", "NAND", "Nand Gate", "Outputs high if all inputs are low.", new NandGate.Factory(), "3ISO"));
        registerICType(new ICType<>("MC3020", "XOR", "Xor Gate", "Outputs high if the inputs are different", new XorGate.Factory(), "3ISO"));
        registerICType(new ICType<>("MC3021", "XNOR", "Xnor Gate", "Outputs high if the inputs are the same", new XnorGate.Factory(), "3ISO"));

        registerICType(new ICType<>("MC3030", "RS-NOR", "RS-Nor Latch", "A compact RS-Nor Latch", new RSNorLatch.Factory(), "3ISO"));
        registerICType(new ICType<>("MC3031", "INV RS-NAND", "Inverse RS-Nand Latch", "A compact Inverse RS-Nand Latch", new InvertedRSNandLatch.Factory(), "3ISO"));
        registerICType(new ICType<>("MC3032", "JK FLIP", "JK Flip Flip", "A compact JK Flip Flop", new JKFlipFlop.Factory(), "3ISO"));
        registerICType(new ICType<>("MC3033", "RS-NAND", "RS-Nand Latch", "A compact RS-Nand Latch", new RSNandLatch.Factory(), "3ISO"));
        registerICType(new ICType<>("MC3034", "EDGE-D", "Edge-Trigger D Flip Flop", "A compact Edge-D Flip Flop", new EdgeTriggerDFlipFlop.Factory(), "3ISO"));
        registerICType(new ICType<>("MC3036", "LEVEL-D", "Level-Trigger D Flip Flop", "A compact Level-D Flip Flop", new LevelTriggerDFlipFlop.Factory(), "3ISO"));

        registerICType(new ICType<>("MC3050", "COMBO", "Combination Lock", "Outputs high if the correct combination is inputed", new CombinationLock.Factory(), "3ISO"));

        registerICType(new ICType<>("MC3101", "DOWN COUNTER", "Down Counter", "Decrements on redstone signal, outputs high when reset.", new DownCounter.Factory(), "3ISO"));
        registerICType(new ICType<>("MC3102", "COUNTER", "Counter", "Increments on redstone signal, outputs high when reset.", new Counter.Factory(), "3ISO"));

        registerICType(new ICType<>("MC3231", "T CONTROL ADV", "Time Control Advanced", "Changes the time of day when the clock input goes from low to high.", new TimeControlAdvanced.Factory(), "3ISO"));

        registerICType(new ICType<>("MC3300", "ROM SET", "ROM Set", "Sets the memory state for a file for usage in the MemorySetter/Access IC group.", new MemorySetter.Factory(), "3ISO"));

        //3I3O
        registerICType(new ICType<>("MC4000", "FULL ADDER", "Full Adder", "A compact full-adder", new FullAdder.Factory(), "3I3O"));
        registerICType(new ICType<>("MC4010", "HALF ADDER", "Half Adder", "A compact half-adder", new HalfAdder.Factory(), "3I3O"));

        registerICType(new ICType<>("MC4100", "FULL SUBTR", "Full Subtractor", "A compact full-subtractor", new FullSubtractor.Factory(), "3I3O"));
        registerICType(new ICType<>("MC4110", "HALF SUBTR", "Half Subtractor", "A compact half-subtractor", new HalfSubtractor.Factory(), "3I3O"));
        registerICType(new ICType<>("MC4200", "DISPATCH", "Dispatcher", "Outputs the centre input on the appropriate outputs when input is high.", new Dispatcher.Factory(), "3I3O"));

        //PLC
        registerICType(new ICType<>("MC5000", "PERLSTONE", "Perlstone 3ISO Programmable Logic Chip", "3ISO PLC programmable with Perlstone.", new PlcFactory<>(new Perlstone()), "3ISO"));

        registerICType(new ICType<>("MC6020", "RANDOM 5", "Random 5-Bit", "Randomly sets the outputs on high.", new RandomBit.Factory(), "SI5O"));

        // 3I5O
        registerICType(new ICType<>("MC4040", "DEMULTIPLEXER", "Demultiplexer 2-Bit", "Demultiplexes the input", new Demultiplexer.Factory(), "3I5O"));

        /* Register Legacy IC's here */
        registerICType(new ICType<>("MCX010","PULSE","Pulse","Sends out a Pulse when toggeled.", new Pulse.Factory()));

        registerICType(new ICType<>("MCX027", "BETWEEN TIME", "Between Time", "Outputs high if the time is between the specified ticks", new BetweenTime.Factory()));

        registerICType(new ICType<>("MCX112", "TRANSPORTER", "Transporter", "Transports players to a destination ic", new Transporter.Factory()));
        registerICType(new ICType<>("MCU113", "DESTINATION", "Destination", "A destination for a Transporter", new Destination.Factory()));

        registerICType(new ICType<>("MCM116","MOB ABOVE?","Mob Above","Outputs when specified entity is above", new MobAbove.Factory()));

        registerICType(new ICType<>("MCX116","PLAYER ABOVE?","Player Above","Outputs when specified playertype is above", new PlayerAbove.Factory()));
        registerICType(new ICType<>("MCX118","PLAYER NEAR?","Player Near","Output when specified player is near.",new PlayerNear.Factory()));
        registerICType(new ICType<>("MCX119", "MOB NEAR?", "Mob Near", "Output when a mob is near", new MobNear.Factory()));
        registerICType(new ICType<>("MCX120", "COMMAND CTRL", "Command Controlled IC", "Output when command is turned on", new CommandControlled.Factory()));
        registerICType(new ICType<>("MCX121", "PASSWORD CTRL", "Command Controlled IC", "Output when command is turned on", new PasswordControlled.Factory()));
        registerICType(new ICType<>("MCX130", "MOB ZAPPER", "Mob Zapper", "Removes mobs within radius", new MobZapper.Factory(), "SISO"));
        registerICType(new ICType<>("MCX131", "HIT PLAYER ABV", "Hit Player Above", "Hits players above with specified damage", new HitPlayerAbove.Factory(), "UISO"));
        registerICType(new ICType<>("MCX132", "HIT MOB ABOVE", "Hit Mob Above", "Hits mobs above with specified damage", new HitMobAbove.Factory(), "UISO"));
        registerICType(new ICType<>("MCX133", "HUMANS ONLY", "Humans Only", "Removes all nonhuman entities", new HumansOnly.Factory(), "SISO"));
        registerICType(new ICType<>("MCX140", "IN AREA", "In Area", "Outputs if specified entity is in area", new InArea.Factory(), "UISO"));
        registerICType(new ICType<>("MCX144", "AREA CBWARP", "CBWarp Area", "Teleports entities to CBWarp within an area", new CBWarpArea.Factory(), "UISO"));


        registerICType(new ICType<>("MCX200", "ENTITY SPAWNER", "Entity Spawner", "Spawns entites above itself", new com.minecraftonline.legacy.ics.chips.world.entity.EntitySpawner.Factory()));
        registerICType(new ICType<>("MCX201", "ITEM SPAWNER", "Item Spawner", "Drops items above itself, with id/damage/amount", new ItemSpawner.Factory(), "AISO"));
        registerICType(new ICType<>("MCX202", "CHEST DISPENSER", "Chest Dispenser", "Dispenses a specified item and supports color/ damage.", new ChestDispenser.Factory(), "AISO"));
        registerICType(new ICType<>("MCX203", "CHEST COLLECTOR", "Chest Collector", "Collects dropped items within a specified radius.", new ChestCollector.Factory(), "AISO"));
        registerICType(new ICType<>("MCX205", "DETECT BLOCK","Block Detector", "Detects a block above or below",new BlockDetector.Factory(), "AISO"));
        registerICType(new ICType<>("MCX206","FLEX SET", "Flex Set","Set a block at a specified location", new FlexSet.Factory(), "AISO"));
        registerICType(new ICType<>("MCX207","BRIDGE","Bridge","Place a set type and amount of blocks",new Bridge.Factory(), "AISO"));
        registerICType(new ICType<>("MCX208","DOOR","Door","Place a set type and amount of blocks",new Door.Factory(), "AISO"));
        registerICType(new ICType<>("MCX209","BRIDGE+","Bridge+","Place a set type and amount of blocks",new Bridge.ForcingFactory(), "AISO"));
        registerICType(new ICType<>("MCX210","DOOR+","Door+","Place a set type and amount of blocks",new Door.ForcingFactory(), "AISO"));
        registerICType(new ICType<>("MCX211", "TOGGLE BLOCK", "ToggleBlock", "Toggles between two given blocks on signal change", new ToggleBlock.Factory(), "AISO"));

        registerICType(new ICType<>("MCX213", "HARVESTER", "Harvester", "Harvest a set type and amount of blocks", new Harvester.Factory(), "AISO"));

        registerICType(new ICType<>("MCX215","AREA PLANTER","Area Planter","Plants crop entities on nearby farmland.", new AreaPlanter.Factory(), "AISO"));
        registerICType(new ICType<>("MCX216","PLANTER","Planter","Plant a crop entity on nearby farmland.", new Planter.Factory(), "AISO"));

        registerICType(new ICType<>("MCX230","IS IT RAIN","Rain Sensor","Output when Raining",new RainSensor.Factory()));
        registerICType(new ICType<>("MCX231", "IS IT A STORM","Storm Sensor","Output when Storming", new StormSensor.Factory()));
        registerICType(new ICType<>("MCX233","WEATHER CONTROL","Simple Weather Control","When the input is toggled on, it will turn the rain/snow weather on for the set duration", new SimpleWeatherControl.Factory()));
        registerICType(new ICType<>("MCX235","FALSE WEATHER","False Weather","Send a false weather packet to nearby players",new FalseWeather.Factory()));

        registerICType(new ICType<>("MCX242","SNOW SHOOTER","Snow Shooter","Shoot snowball",new SnowShooter.Factory()));
        registerICType(new ICType<>("MCX243","SNOW BARRAGE","Snow Barrage","Shoot snowballs.",new SnowBarrage.Factory()));
        registerICType(new ICType<>("MCX244","EGG SHOOTER","Egg Shooter","Shoot an egg.",new EggShooter.Factory()));
        registerICType(new ICType<>("MCX245","EGG BARRAGE","Egg Barrage","Shoot 5 eggs",new EggBarrage.Factory()));
        registerICType(new ICType<>("MCX250","PARTICLE","Particle","Emits particles", new ParticleEmitter.Factory()));
        registerICType(new ICType<>("MCX246","FIREBALL","Fireball","Shoot a ghast fireball",new FireballShooter.Factory()));
        registerICType(new ICType<>("MCX255", "LIGHTNING", "Lightning", "Summons a lightning bolt when powered", new Lightning.Factory()));
        registerICType(new ICType<>("MCX256", "HOLY SMITE", "Holy Smite", "Summons a lightning bolt on all entities in range", new HolySmite.Factory()));
        registerICType(new ICType<>("MCX295", "TRIGGER READER", "Trigger Reader", "Output the powered state of a redstone trigger", new TriggerReader.Factory()));

        registerICType(new ICType<>("MCX512","MESSAGENEARBY","Message Nearby","Message all nearby players.",new MessageNearby.Factory()));
        registerICType(new ICType<>("MCX513","NAMED NEARBY","Message Named Nearby","Message a Named player nearby.", new NamedNearby.Factory(), "AISO"));
        registerICType(new ICType<>("MCX515","SERVER LOG","Server Log","Send Info Log to server", new ServerLog.Factory()));
        registerICType(new ICType<>("MCX516","S-LOG NEARBY","Server Log Nearby","Logs message to the server", new ServerLogNearby.Factory()));
        registerICType(new ICType<>("MCX517","S-LOG NEARBY+","Server Log Nearby+","Logs message to the server", new ServerLogNearbyPlus.Factory()));
        registerICType(new ICType<>("MCT233","WEATHER CONTROL","Weather Control","Set rain/ storm using 3 inputs.", new WeatherControl.Factory(),"3ISO"));
        registerICType(new ICType<>("MCU440", "^MONOFLOP", "Monoflop", "Variable downcount timer.", new Monoflop.Factory(), "AISO"));

        registerICType(new ICType<>("MC1202","CONTAINER DISPENSER","Container Dispenser", "Dispenses an Item when triggered.",new ContainerDispenser.Factory()));
        registerICType(new ICType<>("MC1205","SET ABOVE","Set Block Above","Set a block above IC Block.", new SetBlockAbove.Factory()));
        registerICType(new ICType<>("MC1206","SET BELOW","Set Block Below", "Set a block below the IC block.",new SetBlockBelow.Factory()));
        registerICType(new ICType<>("MC1207","FLEX SET ADMIN", "Flex Set Admin","Set a block at a specified location", new FlexSetAdmin.Factory()));

        registerICType(new ICType<>("MC1240","ARROW SHOOTER","Arrow Shooter","Shoot a single arrow when powered.",new ArrowShooter.Factory(), "AISO"));
        registerICType(new ICType<>("MC1241","ARROW BARRAGE","Arrow Barrage","Shoot 5 arrows when powered",new ArrowBarrage.Factory(), "AISO"));
        registerICType(new ICType<>("MC1250","FIREWORKS","Fireworks","Shoots TNT-based fireworks",new Fireworks.Factory(), "AISO"));
        registerICType(new ICType<>("MC1260","SENSE WATER","Water Sensor","Output high if water is detected.",new SenseWater.Factory()));
        registerICType(new ICType<>("MC1261","SENSE LAVA","Lava Sensor","Output high if lava is detected.",new SenseLava.Factory()));
        registerICType(new ICType<>("MC1262","SENSE LIGHT","Light Sensor","Output high if specified light level is detected.",new SenseLight.Factory()));

        registerICType(new ICType<>("MC1510","MESSAGE PLAYER","Player Messenger","Send message to player. Output high if message sent successfully",new SendMessage.Factory()));
        registerICType(new ICType<>("MC1511","MESSAGE ALL","Message All","Message All online players",new MessageAll.Factory()));

        registerICType(new ICType<>("MC2022", "BITSHIFT", "Bit Shift", "Shift a queue of bits in memory", new BitShift.Factory(), "3ISO"));

        registerICType(new ICType<>("MC2999","MARQUEE","Marquee","Cycle through all three outputs.",new Marquee.Factory(), "SI3O")); // Was CSI30 but that pinset doesn't exist..

        registerICType(new ICType<>("MC3040","MULTIPLEXER","Multiplexer","Output Input 1 or 2 depending on state of Input 0", new Multiplexer.Factory(), "3ISO"));
        registerICType(new ICType<>("MC3456","MARQUEETRANSMIT","Marquee Transmitter","Transmit to a set number of receivers with the same base bandname", new MarqueeTransmitter.Factory()));

        registerICType(new ICType<>("MC4010", "HALF ADDER","Half Adder","Adds 2 one bit inputs and outputs the sum", new HalfAdder.Factory(), "3I3O"));

        registerICType(new ICType<>("MC6543", "REDCODER", "Analog Transmitter", "Triggers various bands depending on input signal strength", new Redcoder.Factory(), "AISO"));
    }

    public static void registerICType(ICType<? extends IC> ic) {

        Class<? extends ICFactory> factoryClazz = ic.getFactory().getClass();

        if(ic.getFactory() instanceof HybridIC){
            ICType stVariant = new ICType<>(((HybridIC) ic.getFactory()).stVariant(),ic.getShorthand(),ic.getName() + " (Self-Triggering)",ic.getDescription(),ic.getFactory());
            registeredICTypes.add(stVariant);
            icFactorySelfTriggeringTypeMap.put(factoryClazz, stVariant);
        }
        registeredICTypes.add(ic);
        icFactoryTypeMap.put(factoryClazz, ic);

        if (ic.getFactory() instanceof SerializedICFactory) {
            SerializedICFactory serializedICFactory = ((SerializedICFactory) ic.getFactory());
            if (!registeredBuilderClasses.contains(serializedICFactory.getRequiredClass())) {
                Sponge.getDataManager().registerBuilder(serializedICFactory.getRequiredClass(), serializedICFactory);
                registeredBuilderClasses.add(serializedICFactory.getRequiredClass());
            }
        }
    }

    public static ICType<? extends IC> getICType(String id) {
        id = id.toUpperCase();
        for (ICType<? extends IC> icType : registeredICTypes) {
            if (id.contains('[' + icType.getModel() + ']')
                    || id.equalsIgnoreCase('=' + icType.getShorthand())
                    || (id.equalsIgnoreCase('[' + icType.getModel() + "]S")
                    || id.equalsIgnoreCase('=' + icType.getShorthand() + " ST")))
                return icType;
        }

        return null;
    }

    public static ICType<? extends IC> getICType(ICFactory<?> icFactory) {
        for (ICType<? extends IC> icType : registeredICTypes) {
            if (icType.getFactory().equals(icFactory)) {
                return icType;
            }
        }

        return null;
    }

    public static <T extends IC> ICType<T> getICType(Class<? extends ICFactory> icFactoryClass, boolean stVariant) {
        Map<Class<? extends ICFactory>, ICType<? extends IC>> map = stVariant ? icFactorySelfTriggeringTypeMap : icFactoryTypeMap;
        return (ICType<T>) map.get(icFactoryClass);
    }

    public static Set<ICType<? extends IC>> getICTypes() {
        return registeredICTypes;
    }
}
