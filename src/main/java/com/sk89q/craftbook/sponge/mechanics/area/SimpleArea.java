/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.area;

import com.flowpowered.math.vector.Vector3i;
import com.me4502.modularframework.module.ModuleWrapper;
import com.minecraftonline.legacy.blockbags.NearbyChestBlockBag;
import com.minecraftonline.mechanic.SignMechanicCacheManager;
import com.minecraftonline.util.ChangeBlockPostListenerManager;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.CraftbookTriggeredMechanic;
import com.sk89q.craftbook.sponge.mechanics.blockbags.AdminBlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeSignMechanic;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import com.sk89q.craftbook.sponge.util.data.mutable.BlockBagData;
import com.sk89q.craftbook.sponge.util.data.mutable.LastPowerData;
import com.sk89q.craftbook.sponge.util.locale.TranslationsManager;
import com.sk89q.craftbook.sponge.util.type.TypeTokens;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.Humanoid;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.CauseStackManager;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.TranslatableText;
import org.spongepowered.api.text.translation.ResourceBundleTranslation;
import org.spongepowered.api.util.Tuple;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.sk89q.craftbook.sponge.util.locale.TranslationsManager.USE_PERMISSIONS;

public abstract class SimpleArea extends SpongeSignMechanic {

    private final Map<Location<World>, RedstoneListener> listeners = new HashMap<>();

    SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook." + getName().toLowerCase(), "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);
    SpongePermissionNode usePermissions = new SpongePermissionNode("craftbook." + getName().toLowerCase() + ".use", "Allows the user to use the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    ConfigValue<List<SpongeBlockFilter>> allowedBlocks = new ConfigValue<>("allowed-blocks", "A list of blocks that can be used.", getDefaultBlocks(), new TypeTokens.BlockFilterListTypeToken());
    ConfigValue<Boolean> allowRedstone = new ConfigValue<>("allow-redstone", "Whether to allow redstone to be used to trigger this mechanic or not", true);

    TranslatableText missingOtherEnd = TranslatableText.of(new ResourceBundleTranslation("area.missing-other-end", TranslationsManager.getResourceBundleFunction()));

    public void loadCommonConfig(ConfigurationNode config) {
        allowedBlocks.load(config);
        allowRedstone.load(config);
    }

    public void registerCommonPermissions() {
        createPermissions.register();
        usePermissions.register();
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }

    @Listener
    public void onPlayerInteract(InteractBlockEvent.Secondary.MainHand event, @First Humanoid human) {
            event.getTargetBlock().getLocation().ifPresent(location -> {
                SignMechanicCacheManager.CachedData cachedData = cache.getCached(this, location);
                if (cachedData != null) {
                    try (CauseStackManager.StackFrame frame = Sponge.getCauseStackManager().pushCauseFrame()) {
                        frame.pushCause(new CraftbookTriggeredMechanic(location));
                        location.getTileEntity().ifPresent((sign -> {
                            if ((!(human instanceof Subject) || usePermissions.hasPermission((Subject) human))) {
                                if (triggerMechanic(location, cachedData, human, null)) {
                                    event.setCancelled(true);
                                }
                            } else if (human instanceof CommandSource) {
                                ((CommandSource) human).sendMessage(USE_PERMISSIONS);
                            }
                        }));
                    }
                }
            });
    }

    public class SimpleAreaCache extends SignMechanicCacheManager.CachedData {

        public SimpleAreaCache(List<Text> lines) {
            super(lines);
        }

        @Override
        public void onUnload(Location<World> loc) {
            super.onUnload(loc);
            RedstoneListener redstoneListener = listeners.remove(loc);
            if (redstoneListener != null) {
                CraftBookPlugin.spongeInst().getChangeBlockPostListenerManager().unregisterAll(redstoneListener);
            }
        }
    }

    public void registerRedstoneListener(Location<World> loc) {
        if (!allowRedstone.getValue()) {
            return;
        }
        CraftBookPlugin.spongeInst().getChangeBlockPostListenerManager().register(loc.getExtent(), createInputList(loc), new RedstoneListener(loc));
    }

    public abstract Collection<Vector3i> createInputList(Location<World> mechanicLocation);

    public class RedstoneListener implements ChangeBlockPostListenerManager.ChangeBlockPostListener {

        private final Location<World> loc;

        public RedstoneListener(Location<World> loc) {
            this.loc = loc;
        }

        @Override
        public void onChange(Transaction<BlockSnapshot> transaction, Cause cause) {
            Location<World> baseLocation = transaction.getFinal().getLocation().get();
            BlockState endState = transaction.getFinal().getExtendedState();

            Tuple<Boolean, Set<Location<World>>> endTuple = ICSocket.getPoweredInState(endState, baseLocation);

            Collection<Location<World>> locations;

            if (endTuple == null) {
                locations = BlockUtil.getAdjacentExcept(baseLocation);
            }
            else {
                locations = endTuple.getSecond();
            }

            boolean powered = endTuple != null && endTuple.getFirst();

            for (Location<World> location : locations) {
                if (!loc.equals(location)) {
                    continue;
                }
                boolean wasPowered = location.get(CraftBookKeys.LAST_POWER).orElse(0) != 0;
                if (powered == wasPowered) {
                    return;
                }
                SignMechanicCacheManager.CachedData cachedData = cache.getCached(SimpleArea.this, location);
                if (cachedData == null) {
                    continue;
                }
                triggerMechanic(location, cachedData, cause.first(Player.class).orElse(null), powered);
                int powerLevelFake = powered ? 15 : 0; // TODO: get real power level?
                location.offer(new LastPowerData(powerLevelFake));
            }
        }
    }

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        String line0 = SignUtil.getTextRaw(signData, 0);
        if (!line0.isEmpty()) {
            Optional<ModuleWrapper> moduleWrapper = CraftBookPlugin.spongeInst().moduleController.getModule("blockbag");
            if (moduleWrapper.isPresent() && moduleWrapper.get().isEnabled()) {
                if ("ADMIN".equals(line0)) {
                    if ((player == null || ((BlockBagManager) moduleWrapper.get().getModule().get()).adminPermissions.hasPermission(player))) {
                        location.offer(new BlockBagData(-1));
                    } else {
                        signData.setElement(0, Text.of());
                    }
                }
            }
        }
        if (super.verifyLines(location, signData, player)) {
            cache.mechanicCreated(this, location, signData);
            return true;
        }
        return false;
    }


    public BlockBag getBlockBag(Location<World> location) {
        Optional<ModuleWrapper> moduleWrapper = CraftBookPlugin.spongeInst().moduleController.getModule("blockbag");
        if (moduleWrapper.isPresent() && moduleWrapper.get().isEnabled()) {
            return NearbyChestBlockBag.fromSourcePositionNearby(location);
        }

        return AdminBlockBag.INSTANCE;
    }

    @Override
    public boolean isValid(Location<World> location) {
        return cache.getCached(this, location) != null;
    }

    public boolean isValidNoCache(Location<World> location, SignData signData) {
        return isMechanicSign(signData);
    }

    /**
     * Triggers the mechanic.
     * 
     * @param block The block the mechanic is being triggered at
     * @param cachedData The cached information about the mechanic
     * @param human The triggering human, if applicable
     * @param forceState If the mechanic should forcibly enter a specific state
     */
    public abstract boolean triggerMechanic(Location<World> block, SignMechanicCacheManager.CachedData cachedData, Humanoid human, Boolean forceState);

    public abstract List<SpongeBlockFilter> getDefaultBlocks();
}
