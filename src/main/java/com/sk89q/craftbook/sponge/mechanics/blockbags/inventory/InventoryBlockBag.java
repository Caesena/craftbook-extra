/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.blockbags.inventory;

import com.google.common.collect.Lists;
import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.InventoryUtil;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class InventoryBlockBag implements BlockBag {

    protected Inventory inventory;

    public InventoryBlockBag(Inventory inventory) {
        this.inventory = inventory;
    }

    /**
     * DO NOT USE FOR THIS CLASS OR CLASSES EXTENDING IT
     * Hugely prefer {@link #has(Predicate, int)}
     * @param itemStacks The requested items
     */
    @Override
    public boolean has(List<ItemStack> itemStacks) {
        for(ItemStack stack : itemStacks) {
            if(!inventory.contains(stack))
                return false;
        }
        return true;
    }

    @Override
    public boolean has(Predicate<ItemStack> predicate, int amountRequired) {
        return hasHowMany(predicate) >= amountRequired;
    }

    public int hasHowMany(Predicate<ItemStack> predicate) {
        int count = 0;
        for (Inventory slot : inventory.slots()) {
            count += slot.peek().filter(predicate).map(ItemStack::getQuantity).orElse(0);
        }
        return count;
    }

    @Override
    public boolean canFit(List<ItemStack> items) {
        List<ItemStack> itemStacks = new ArrayList<>(items);
        int emptySlots = 0;
        for (Inventory slot : inventory.slots()) {
            if (itemStacks.size() <= emptySlots) {
                return true;
            }
            Optional<ItemStack> optItemStack = slot.peek();
            if (!optItemStack.isPresent() || optItemStack.get().isEmpty()) {
                emptySlots++;
                continue;
            }
            ItemStack itemStack = optItemStack.get();
            int maxStackSize = itemStack.getMaxStackQuantity();
            if (itemStack.getQuantity() == maxStackSize)
                continue;
            // Potential to fit some stacks.
            Iterator<ItemStack> iter = itemStacks.listIterator();
            int room = maxStackSize - itemStack.getQuantity();
            for (ItemStack toFitStack = iter.next().copy(); iter.hasNext(); iter.next()) {
                if (IDUtil.areItemStacksEqualIgnoringQuantity(itemStack, toFitStack)) {
                    if (room > toFitStack.getQuantity()) {
                        iter.remove();
                        room -= toFitStack.getQuantity();
                        if (room == 0) {
                            break;
                        }
                    }
                    else {
                        toFitStack.setQuantity(toFitStack.getQuantity() - room);
                        break; // No more space in this slot.
                    }
                }
            }
        }
        return itemStacks.size() <= emptySlots;
    }

    /**
     * Removed items from the list or reduces quantity that it is able to fit in own inventory
     * This should not mutate the arguments.
     * @param items
     * @return List of ItemStacks it couldn't fit
     */
    public List<ItemStack> canFitHowMuch(List<ItemStack> items) {
        List<ItemStack> itemStacks = new ArrayList<>(items);
        int emptySlots = 0;
        for (Inventory slot : inventory.slots()) {
            Optional<ItemStack> optItemStack = slot.peek();
            if (!optItemStack.isPresent() || optItemStack.get().isEmpty()) {
                emptySlots++;
                if (itemStacks.size() <= emptySlots) {
                    return Lists.newArrayList();
                }
                continue;
            }
            ItemStack itemStack = optItemStack.get();
            int maxStackSize = itemStack.getMaxStackQuantity();
            if (itemStack.getQuantity() == maxStackSize)
                continue;
            // Potential to fit some stacks.
            Iterator<ItemStack> iter = itemStacks.listIterator();
            while (iter.hasNext()) {
                ItemStack toFitStack = iter.next().copy();
                if (IDUtil.areItemStacksEqualIgnoringQuantity(itemStack, toFitStack)) {
                    int room = maxStackSize - itemStack.getQuantity();
                    if (room >= toFitStack.getQuantity()) {
                        iter.remove();
                        room -= toFitStack.getQuantity();
                        if (room == 0) {
                            break;
                        }
                    }
                    else {
                        toFitStack.setQuantity(toFitStack.getQuantity() - room);
                        break; // No more space in this slot.
                    }
                }
            }
        }
        if (emptySlots > 0) {
            if (emptySlots >= itemStacks.size()) {
                return Lists.newArrayList();
            }
            itemStacks.subList(0, emptySlots).clear();
        }
        return itemStacks;
    }

    @Override
    public List<ItemStack> add(List<ItemStack> itemStacks) {
        List<ItemStack> output = new ArrayList<>();
        for(ItemStack stack : itemStacks) {
            output.add(InventoryUtil.offerInventoryQuick(inventory, stack));
        }
        return output;
    }

    @Override
    public List<ItemStack> remove(List<ItemStack> itemStacks) {
        List<ItemStack> output = new ArrayList<>();
        for(ItemStack stack : itemStacks) {
            Inventory inv = inventory.query(QueryOperationTypes.ITEM_STACK_IGNORE_QUANTITY.of(stack));
            if (inv.totalItems() < stack.getQuantity()) {
                ItemStack copyStack = stack.copy();
                copyStack.setQuantity(stack.getQuantity() - inv.totalItems());
                output.add(copyStack);
            }
            inv.poll(stack.getQuantity());
        }
        return output;
    }

    @Override
    public int remove(Predicate<ItemStack> predicate, int amtToRemove) {
        Inventory inv = inventory.query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(predicate));
        int result = amtToRemove - inv.totalItems();
        inv.poll(amtToRemove);
        return Math.max(result, 0);
    }

    @Nullable
    @Override
    public ItemStack findStack(Predicate<ItemStack> predicate, int maxSize) {
        return inventory.query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(predicate)).peek(maxSize)
                .orElse(null);
    }

    public Inventory getInventory() {
        return inventory;
    }
}
