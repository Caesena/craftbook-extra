/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.ics;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.ModuleWrapper;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.command.AddExtraLinesExecutor;
import com.minecraftonline.command.ExtraLinesHelpExecutor;
import com.minecraftonline.command.ShowExtraLinesExecutor;
import com.minecraftonline.command.debugstick.ICDebugStick;
import com.minecraftonline.data.ICDebugStickModeData;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.legacy.ics.chips.world.block.BlockPlacingIC;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.CommandControlled;
import com.minecraftonline.legacy.ics.chips.world.miscellaneous.PasswordControlled;
import com.minecraftonline.legacy.ics.pinsets.PinsMISO;
import com.minecraftonline.legacy.ics.pinsets.PinsUISO;
import com.minecraftonline.util.ChangeBlockPostListenerManager;
import com.minecraftonline.util.WithinChunkPos;
import com.minecraftonline.util.WorldChunkObjectMap;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.documentation.DocumentationGenerator;
import com.sk89q.craftbook.core.util.documentation.DocumentationProvider;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.IC;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.PinSet;
import com.sk89q.craftbook.sponge.mechanics.ics.command.SetDataCommand;
import com.sk89q.craftbook.sponge.mechanics.ics.command.ShowDataCommand;
import com.sk89q.craftbook.sponge.mechanics.ics.factory.SerializedICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.pinsets.Pins3I3O;
import com.sk89q.craftbook.sponge.mechanics.ics.pinsets.Pins3I5O;
import com.sk89q.craftbook.sponge.mechanics.ics.pinsets.Pins3ISO;
import com.sk89q.craftbook.sponge.mechanics.ics.pinsets.PinsAISO;
import com.sk89q.craftbook.sponge.mechanics.ics.pinsets.PinsAIZO;
import com.sk89q.craftbook.sponge.mechanics.ics.pinsets.PinsSI3O;
import com.sk89q.craftbook.sponge.mechanics.ics.pinsets.PinsSI5O;
import com.sk89q.craftbook.sponge.mechanics.ics.pinsets.PinsSISO;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeBlockMechanic;
import com.sk89q.craftbook.sponge.st.SelfTriggeringMechanic;
import com.sk89q.craftbook.sponge.st.SpongeSelfTriggerManager;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import com.sk89q.craftbook.sponge.util.data.mutable.ICData;
import com.sk89q.craftbook.sponge.util.type.TypeTokens;
import net.minecraft.block.BlockRedstoneWire;
import net.minecraft.block.state.IBlockState;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.GameState;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.data.type.StoneTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.block.tileentity.ChangeSignEvent;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingEvent;
import org.spongepowered.api.event.item.inventory.InteractItemEvent;
import org.spongepowered.api.event.world.chunk.LoadChunkEvent;
import org.spongepowered.api.event.world.chunk.UnloadChunkEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.entity.MainPlayerInventory;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.item.recipe.crafting.Ingredient;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.channel.MessageReceiver;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.util.Tuple;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.sk89q.craftbook.core.util.documentation.DocumentationGenerator.createStringOfLength;
import static com.sk89q.craftbook.core.util.documentation.DocumentationGenerator.padToLength;

@Module(id = "icsocket", name = "ICSocket", onEnable = "onInitialize", onDisable = "onDisable")
public class ICSocket extends SpongeBlockMechanic implements SelfTriggeringMechanic, DocumentationProvider {

    public static final String DEBUG_STICK_PERMISSION = "craftbook.ic.debug";

    public static final HashMap<String, PinSet> PINSETS = new HashMap<>();
    private static final Pattern IC_TABLE_PATTERN = Pattern.compile("%IC_TABLE%", Pattern.LITERAL);


    public static final Map<Location<World>, MessageReceiver> DEBUG_MESSAGE_RECEIVERS = new HashMap<>();

    static {
        PINSETS.put("SISO", new PinsSISO());
        PINSETS.put("AISO", new PinsAISO());
        PINSETS.put("AIZO", new PinsAIZO());
        PINSETS.put("SI3O", new PinsSI3O());
        PINSETS.put("3ISO", new Pins3ISO());
        PINSETS.put("3I3O", new Pins3I3O());
        PINSETS.put("SI5O", new PinsSI5O());

        //CBX PinSets

        PINSETS.put("MISO", new PinsMISO());
        PINSETS.put("UISO", new PinsUISO());

        // Tyh PinSets

        PINSETS.put("3I5O", new Pins3I5O());
    }

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    public ConfigValue<Double> maxRadius = new ConfigValue<>("max-radius", "Maximum radius of IC mechanics.", 10d, TypeToken.of(Double.class));
    public static ConfigValue<List<SpongeBlockFilter>> allowedICBlocks = new ConfigValue<>("allowedICblocks", "A list of blocks that a Block Placing IC can use.", getDefaultBlocks(), new TypeTokens.BlockFilterListTypeToken());
    public static ConfigValue<Integer> maximumLength = new ConfigValue<>("maximum-length", "The maximum length the bridge can be.", 16);
    public static ConfigValue<Integer> maximumWidth = new ConfigValue<>("maximum-width", "The maximum width each side of the bridge can be. The overall max width is this*2 + 1.", 5);
    public static ConfigValue<Integer> maxWidthAreaPlanter = new ConfigValue<>("maximum-width-planter", "The maximum width/length an area planter can planter. The overall max width is this*2 + 1.", 4);
    public static ConfigValue<Boolean> debugStickCraftable = new ConfigValue<>("debug-stick-craftable", "Whether the debug stick is craftable", true);

    private final WorldChunkObjectMap<IC> loadedICs = new WorldChunkObjectMap<>();

    private final Map<IC, ICRedstoneListener> icRedstoneListener = new HashMap<>();

    private CommandMapping icCommandMapping;
    private CommandMapping CommandControlledMapping;
    private CommandMapping commandControlledListMapping;
    private CommandMapping PasswordControlledMapping;
    private CommandMapping passwordControlledListMapping;
    private CommandMapping PasswordControlledPassMapping;
    private CommandMapping addExtraICLinesDataMapping;
    private CommandMapping icDebugStickCommand;

    public class ICRedstoneListener implements ChangeBlockPostListenerManager.ChangeBlockPostListener {

        private final Location<World> loc;

        public ICRedstoneListener(Location<World> loc) {
            this.loc = loc;
        }

        @Override
        public void onChange(Transaction<BlockSnapshot> transaction, Cause cause) {
            Location<World> loc = transaction.getOriginal().getLocation().get();
            /*ChunkProviderBridge chunkProviderServer = (ChunkProviderBridge) ((net.minecraft.world.World) loc.getExtent()).getChunkProvider();
            Vector3i chunkPos = loc.getChunkPosition();
            net.minecraft.world.chunk.Chunk mcChunk = chunkProviderServer.bridge$getLoadedChunkWithoutMarkingActive(chunkPos.getX(), chunkPos.getZ());
            boolean shouldUnloadAfter = mcChunk == null || mcChunk.unloadQueued;
            if (shouldUnloadAfter) {
                CraftBookPlugin.spongeInst().getLogger().info("Unload queued for chunk: " + (mcChunk == null ? null : mcChunk.getPos()));
            }*/
            BlockState oldState = transaction.getOriginal().getExtendedState();
            BlockState endState = transaction.getFinal().getExtendedState();

            Tuple<Boolean, Set<Location<World>>> oldTuple = getPoweredInState(oldState, loc);
            Tuple<Boolean, Set<Location<World>>> newTuple = getPoweredInState(endState, loc);

            if (newTuple == null) {
                return;
            }

            final Set<Location<World>> icCheckSpots = newTuple.getSecond();
            final boolean powered = newTuple.getFirst();

            for (Location<World> location : icCheckSpots) {
                if (location.equals(this.loc)) {
                    IC ic = getIC(this.loc).orElseThrow(() -> new IllegalStateException("IC at " + this.loc + " no longer exists but still has a listener!"));
                    int pin = ic.getPinSet().getPinForLocation(ic, loc);

                    if (pin >= 0) {
                        // Has the state changed?

                        boolean oldPowered = oldTuple != null && Boolean.TRUE.equals(oldTuple.getFirst());
                        if (powered != oldPowered) {
                            ic.setTriggeredPin(pin);
                            Sponge.getScheduler().createTaskBuilder().delayTicks(2).execute(() -> {
                                //Vector3i chunkPos2 = ic.getBlock().getChunkPosition();
                                //net.minecraft.world.chunk.Chunk mcChunk2 = chunkProviderServer.bridge$getLoadedChunkWithoutMarkingActive(chunkPos2.getX(), chunkPos2.getZ());
                                //boolean shouldUnloadAfter2 = mcChunk2 == null || mcChunk2.unloadQueued;
                                //if (shouldUnloadAfter2) {
                                //    CraftBookPlugin.spongeInst().getLogger().info("2: Unload queued for chunk: " + (mcChunk == null ? null : mcChunk.getPos()));
                                //}
                                ic.trigger();
                            }).submit(CraftBookPlugin.spongeInst().container);
                        }
                    }
                }
            }
        }

        @Override
        public int hashCode() {
            return loc.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (!(obj instanceof ICRedstoneListener)) return false;
            ICRedstoneListener that = (ICRedstoneListener) obj;
            return this.loc.equals(that.loc);
        }
    }

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        maxRadius.load(config);
        maximumLength.load(config);
        maximumWidth.load(config);
        maxWidthAreaPlanter.load(config);
        allowedICBlocks.load(config);
        debugStickCraftable.load(config);

        ICManager.getICTypes().stream().map(ICType::getPermissionNode).forEach(SpongePermissionNode::register);

        if ("true".equalsIgnoreCase(System.getProperty("craftbook.generate-docs"))) {
            ICManager.getICTypes().forEach(DocumentationGenerator::generateDocumentation);
        }

        CommandSpec showDataCommand = CommandSpec.builder()
                .arguments(GenericArguments.optional(GenericArguments.location(Text.of("block"))))
                .executor(new ShowDataCommand(this))
                .build();

        CommandSpec setDataCommand = CommandSpec.builder()
                .arguments(
                        GenericArguments.string(Text.of("variable")),
                        GenericArguments.string(Text.of("value")),
                        GenericArguments.optional(GenericArguments.location(Text.of("block")))
                )
                .executor(new SetDataCommand(this))
                .build();

        CommandSpec dataCommand = CommandSpec.builder()
                .child(showDataCommand, "show")
                .child(setDataCommand, "set")
                .build();

        CommandSpec icCommand = CommandSpec.builder()
                .description(Text.of("Base command for Integrated Circuits."))
                .child(dataCommand, "data")
                .build();

        icCommandMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), icCommand, "ic", "ics", "integratedcircuit").orElse(null);

        CommandSpec CommandControlledSpec = CommandSpec.builder()
                .description(Text.of("Controls command controlled ics."))
                .arguments(GenericArguments.string(Text.of("Band")),
                        GenericArguments.optional(GenericArguments.string(Text.of("Mode"))))
                .executor(new CommandControlled.MainCommand())
                .build();

        CommandControlledMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), CommandControlledSpec, "mcx120", "commandcontrolledic").orElse(null);

        CommandSpec commandControlledListSpec = CommandSpec.builder()
                .description(Text.of("Lists command controlled ics."))
                .permission("craftbook.commandcontrolled.list")
                .executor(new CommandControlled.ListCommand())
                .build();

        commandControlledListMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), commandControlledListSpec, "mcx120list", "commandcontrolledlist").orElse(null);

        CommandSpec PasswordControlledSpec = CommandSpec.builder()
                .description(Text.of("Controls password controlled ics."))
                .arguments(GenericArguments.string(Text.of("Band")),
                        GenericArguments.string(Text.of("Password")),
                        GenericArguments.optional(GenericArguments.string(Text.of("Mode"))))
                .executor(new PasswordControlled.MainCommand())
                .build();

        PasswordControlledMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), PasswordControlledSpec, "mcx121", "passwordcontrolledic").orElse(null);

        CommandSpec passwordControlledListSpec = CommandSpec.builder()
                .description(Text.of("List password controlled bands"))
                .permission("craftbook.passwordcontrolled.list")
                .executor(new PasswordControlled.ListPasswordControlled())
                .build();

        passwordControlledListMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), passwordControlledListSpec, "mcx121list", "passwordcontrolledlist").orElse(null);

        CommandSpec AddPassword = CommandSpec.builder()
                .arguments(GenericArguments.string(Text.of("Band")),
                        GenericArguments.string(Text.of("Password")))
                .executor(new PasswordControlled.AddPassword())
                .build();

        CommandSpec ChangePassword = CommandSpec.builder()
                .arguments(GenericArguments.string(Text.of("Band")),
                        GenericArguments.string(Text.of("OldPassword")),
                        GenericArguments.string(Text.of("NewPassword")))
                .executor(new PasswordControlled.ChangePassword())
                .build();

        CommandSpec HasPassword = CommandSpec.builder()
                .arguments(GenericArguments.string(Text.of("Band")))
                .executor(new PasswordControlled.HasPassword())
                .build();

        CommandSpec ChangePasswordControlledSpec = CommandSpec.builder()
                .description(Text.of("Changes Password for Password Controlled ics."))
                .child(AddPassword, "add")
                .child(ChangePassword, "change")
                .child(HasPassword, "has")
                .build();

        PasswordControlledPassMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), ChangePasswordControlledSpec, "mcx121pass").orElse(null);

        CommandSpec setExtraData = CommandSpec.builder()
                .description(Text.of("For setting extra lines of ics"))
                .permission("craftbook.ic.extralines.set")
                .executor(new AddExtraLinesExecutor(this))
                .build();

        CommandSpec showExtraData = CommandSpec.builder()
                .description(Text.of("For showing extra lines of ics"))
                .permission("craftbook.ic.extralines.show")
                .executor(new ShowExtraLinesExecutor(this))
                .build();

        CommandSpec extraDataHelp = CommandSpec.builder()
                .description(Text.of("Shows general help, or when looking at an ic, shows its help"))
                .permission("craftbook.ic.extralines.help")
                .executor(new ExtraLinesHelpExecutor(this))
                .build();

        CommandSpec extraData = CommandSpec.builder()
                .description(Text.of("For management of extra lines of data to an ic with a book and quil"))
                .permission("craftbook.ic.extralines")
                .child(setExtraData, "set")
                .child(showExtraData, "show")
                .child(extraDataHelp, "help")
                .build();

        addExtraICLinesDataMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), extraData, "extraicdata").orElse(null);

        icDebugStickCommand = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(),
                CommandSpec.builder()
                        .executor((src, args) -> {
                            if (!(src instanceof Player)) {
                                throw new CommandException(Text.of("You must be a player to use this command!"));
                            }
                            if (!src.hasPermission(DEBUG_STICK_PERMISSION + ".command")) {
                                throw new CommandException(Text.of("This is now admin only. Use the craft recipe (Shovel with a sign as head)"));
                            }
                            ItemStack debugStick = ICDebugStick.createDebugStick();
                            Inventory inventory = ((Player) src).getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class));
                            if (!inventory.canFit(debugStick)) {
                                throw new CommandException(Text.of("You have no room in your inventory!"));
                            }
                            inventory.offer(debugStick);
                            return CommandResult.success();
                        })
                        .build(),
                "craftbookdebugstick"
        ).orElse(null);

        if (debugStickCraftable.getValue()) {
            final String id = "ic_debug_stick";
            final String name = "IC Debug Stick";
            final String group = "minecraftonline";

            CraftingRecipe mainRecipe = CraftingRecipe.shapedBuilder()
                    .aisle(" S ",
                           " T ",
                           " T ")
                    .where('S', Ingredient.of(ItemTypes.SIGN))
                    .where('T', Ingredient.of(ItemTypes.STICK))
                    .result(ICDebugStick.createDebugStick())
                    .id(id)
                    .name(name)
                    .group(group)
                    .build();

            Sponge.getRegistry().getCraftingRecipeRegistry().register(mainRecipe);

            CraftingRecipe sideRecipe = CraftingRecipe.shapedBuilder()
                    .aisle("   ",
                           "TTS",
                           "   ")
                    .where('S', Ingredient.of(ItemTypes.SIGN))
                    .where('T', Ingredient.of(ItemTypes.STICK))
                    .result(ICDebugStick.createDebugStick())
                    .id(id + "_side")
                    .name(name + " Sideways")
                    .group(group)
                    .build();

            Sponge.getRegistry().getCraftingRecipeRegistry().register(sideRecipe);

            CraftingRecipe diagonalRecipe = CraftingRecipe.shapedBuilder()
                    .aisle("  S",
                           " T ",
                           "T  ")
                    .where('S', Ingredient.of(ItemTypes.SIGN))
                    .where('T', Ingredient.of(ItemTypes.STICK))
                    .result(ICDebugStick.createDebugStick())
                    .id(id + "_diagonal")
                    .name(name + " Diagonal")
                    .group(group)
                    .build();

            Sponge.getRegistry().getCraftingRecipeRegistry().register(diagonalRecipe);
        }
    }

    @Override
    public void onDisable() {
        super.onDisable();

        loadedICs.forEachValue(ic -> {
            unloadIC(ic, false);
        });
        loadedICs.clear();

        maxRadius.save(config);
        maximumLength.save(config);
        maximumWidth.save(config);
        maxWidthAreaPlanter.save(config);
        allowedICBlocks.save(config);
        debugStickCraftable.save(config);

        if (icCommandMapping != null) {
            Sponge.getCommandManager().removeMapping(icCommandMapping);
        }
        if (CommandControlledMapping != null) {
            Sponge.getCommandManager().removeMapping(CommandControlledMapping);
        }
        if (PasswordControlledMapping != null) {
            Sponge.getCommandManager().removeMapping(PasswordControlledMapping);
        }
        if (PasswordControlledPassMapping != null) {
            Sponge.getCommandManager().removeMapping(PasswordControlledPassMapping);
        }
        if (addExtraICLinesDataMapping != null) {
            Sponge.getCommandManager().removeMapping(addExtraICLinesDataMapping);
        }
        if (icDebugStickCommand != null) {
            Sponge.getCommandManager().removeMapping(icDebugStickCommand);
        }
    }

    public static Optional<ICSocket> getInstance() {
        return CraftBookPlugin.spongeInst().moduleController.getModule("icsocket")
                .flatMap(ModuleWrapper::getModule);
    }

    @Override
    public String getName() {
        return "ICs";
    }

    @Listener
    public void onChangeSign(ChangeSignEvent event, @First Player player) {
        ICType<? extends IC> icType = ICManager.getICType((event.getText().lines().get(1)).toPlain());
        if (icType == null) return;

        if (!icType.getPermissionNode().hasPermission(player)) {
            player.sendMessage(Text.of(TextColors.RED, "You don't have permission to create this IC!"));
            event.setCancelled(true);
        } else {
            List<Text> lines = event.getText().lines().get();
            if (!icType.getFactory().usesFirstLine())
                lines.set(0, Text.of(icType.getShorthand().toUpperCase()));
            String idLine = SignUtil.getTextRaw(lines.get(1));

            if (idLine.length() - 1 != idLine.lastIndexOf("]")) {
                String fixed = idLine.substring(0, idLine.lastIndexOf("]") + 1).toUpperCase() + idLine.substring(idLine.lastIndexOf("]") + 1);
                lines.set(1, Text.of(fixed));
            } else {
                lines.set(1, Text.of(idLine.toUpperCase()));
            }

            try {
                createICData(event.getTargetTile().getLocation(), lines, player);
                player.sendMessage(Text.of(TextColors.YELLOW, "Created " + icType.getName()));

                event.getText().set(Keys.SIGN_LINES, lines);
            } catch (InvalidICException e) {
                event.setCancelled(true);
                Text msg = Text.builder().append(Text.of("Failed to create IC. ")).append(e.getText()).build();
                player.sendMessage(msg);
            }
        }
    }

    @Listener
    public void onBlockBreak(ChangeBlockEvent.Break event) {
        event.getTransactions().stream().map(transaction -> transaction.getOriginal().getLocation().get()).forEach(location -> {
            IC ic = loadedICs.remove(location);
            if (ic != null) {
                unloadIC(ic, false);
            }
        });
    }

    public static Tuple<Boolean, Set<Location<World>>> getPoweredInState(BlockState blockState, Location<World> loc) {
        BlockType blockType = blockState.getType();

        final boolean powered;

        final Set<Location<World>> icCheckSpots = new HashSet<>();

        if (blockType == BlockTypes.REDSTONE_WIRE) {
            // Redstone wire

            powered = blockState.get(Keys.POWER).orElse(0) > 0;

            IBlockState blockState1 = (IBlockState) blockState;

            List<Direction> directions = new ArrayList<>();

            if (blockState1.getValue(BlockRedstoneWire.NORTH) == BlockRedstoneWire.EnumAttachPosition.SIDE) {
                directions.add(Direction.NORTH);
            }
            if (blockState1.getValue(BlockRedstoneWire.SOUTH) == BlockRedstoneWire.EnumAttachPosition.SIDE) {
                directions.add(Direction.SOUTH);
            }
            if (blockState1.getValue(BlockRedstoneWire.EAST) == BlockRedstoneWire.EnumAttachPosition.SIDE) {
                directions.add(Direction.EAST);
            }
            if (blockState1.getValue(BlockRedstoneWire.WEST) == BlockRedstoneWire.EnumAttachPosition.SIDE) {
                directions.add(Direction.WEST);
            }

            if (directions.size() == 0) {
                for (Direction dir : BlockUtil.getDirectHorizontalFaces()) {
                    icCheckSpots.add(loc.getRelative(dir));
                }
            } else if (directions.size() == 1) {
                // Must be a line redstone. Include both ends.
                Direction dir = directions.get(0);
                icCheckSpots.add(loc.getRelative(dir));
                icCheckSpots.add(loc.getRelative(dir.getOpposite()));
            } else {
                // Either a dot, or not a line.
                directions.forEach(dir -> icCheckSpots.add(loc.getRelative(dir)));
            }
        } else if (blockType == BlockTypes.POWERED_REPEATER
                || blockType == BlockTypes.POWERED_COMPARATOR) {
            // Powered directional
            powered = true;

            Direction dir = blockState.require(Keys.DIRECTION);
            icCheckSpots.add(loc.getRelative(dir));
        } else if (blockType == BlockTypes.UNPOWERED_REPEATER
                || blockType == BlockTypes.UNPOWERED_COMPARATOR) {
            // unpowered directional
            powered = false;

            Direction dir = blockState.require(Keys.DIRECTION);
            icCheckSpots.add(loc.getRelative(dir));

        } else if (blockType == BlockTypes.LEVER
                || blockType == BlockTypes.REDSTONE_TORCH
                || blockType == BlockTypes.UNLIT_REDSTONE_TORCH
        ) {
            // powered/unpowered all-directional
            powered = blockType == BlockTypes.REDSTONE_TORCH || (blockType == BlockTypes.LEVER && blockState.get(Keys.POWERED).orElse(false));

            for (Direction dir : BlockUtil.getDirectFaces()) {
                icCheckSpots.add(loc.getRelative(dir));
            }
        } else {
            return null;
        }

        return Tuple.of(powered, icCheckSpots);
    }

    // Fire Late to try to be after world-edit so we can translate old ids.
    @Listener(order = Order.LATE)
    public void gameStartedEvent(GameStartedServerEvent e) {
        for (World world : Sponge.getServer().getWorlds()) {
            for (Chunk chunk : world.getLoadedChunks()) {
                loadICsInChunk(chunk);
            }
        }
    }

    @Listener
    public void onChunkLoad(LoadChunkEvent e) {
        if (Sponge.getGame().getState().equals(GameState.SERVER_STARTED)) {
            loadICsInChunk(e.getTargetChunk());
        }
    }

    private void loadICsInChunk(Chunk chunk) {
        for (TileEntity tileEntity : chunk.getTileEntities()) {

            if (!isValid(tileEntity.getLocation()))
                continue;

            Optional<IC> ic = getIC(tileEntity.getLocation());
            if (!ic.isPresent())
                continue;

            loadIC(ic.get());
        }
    }

    /**
     * Used on loading ics to activate UISO/MISO. Will not call .trigger().
     *
     * @param ic ic to check if it is being powered
     */
    private void setTriggeredPinIfPowered(IC ic) {
        Location<World> loc = ic.getBlock();
        for (Direction dir : BlockUtil.getDirectHorizontalFaces()) {
            Location<World> searchLoc = loc.getBlockRelative(dir);
            BlockType type = searchLoc.getBlockType();
            if ((type == BlockTypes.LEVER && searchLoc.require(Keys.POWERED))
                    || type == BlockTypes.REDSTONE_TORCH
                    || type == BlockTypes.REDSTONE_BLOCK
                    || (type == BlockTypes.REDSTONE_WIRE
                    && searchLoc.require(Keys.POWER) > 0
                    && searchLoc.get(Keys.WIRE_ATTACHMENTS).map(Map::keySet).filter(set -> set.contains(dir.getOpposite())).isPresent())
                    || ((type == BlockTypes.POWERED_REPEATER || type == BlockTypes.POWERED_COMPARATOR)
                    && searchLoc.require(Keys.DIRECTION).equals(dir.getOpposite()))
            ) {
                int pin = (ic.getPinSet().getInputCount() == 1) ? 0 : ic.getPinSet().getPinForLocation(ic, searchLoc);
                if (pin >= 0)
                    ic.setTriggeredPin(pin);
                return;
            }
        }
    }

    public boolean loadIC(IC ic) {
        try {
            ic.setMode();
            if (ic.getFactory() instanceof SerializedICFactory) {
                SerializedICFactory serializedICFactory = (SerializedICFactory) ic.getFactory();
                SerializedICData data = ic.getBlock().get(CraftBookKeys.IC_DATA).orElse(
                        serializedICFactory.getDataFromSign(ic)
                );
                if (data == null) {
                    CraftBookPlugin.spongeInst().getLogger().warn("Broken IC at " + ic.getBlock() + ". This is only be possible for a few ics");
                    return false;
                }
                serializedICFactory.setData(ic, data);
            }

            ic.load();

            if (isSTVariant(ic)) {
                setTriggeredPinIfPowered(ic);
                ((SpongeSelfTriggerManager) CraftBookPlugin.spongeInst().getSelfTriggerManager().get()).register(this, ic.getBlock());
            }

            loadedICs.put(ic.getBlock(), ic);

            PinSet pinSet = ic.getPinSet();

            List<Vector3i> inputPins = new ArrayList<>();

            for (int i = 0; i < pinSet.getInputCount(); i++) {
                inputPins.add(pinSet.getPinLocation(i, ic).getBlockPosition());
            }
            ICRedstoneListener redstoneListener = new ICRedstoneListener(ic.getBlock());
            CraftBookPlugin.spongeInst().getChangeBlockPostListenerManager().register(ic.getBlock().getExtent(), inputPins, redstoneListener);
            icRedstoneListener.put(ic, redstoneListener);
        } catch (Exception e) {
            CraftBookPlugin.spongeInst().getLogger().error("Failed to load ic " + ic.getClass() + " at " + ic.getBlock(), e);
        }

        return true;
    }

    public void unloadIC(IC ic, boolean removeFromLoadedICList) {
        ic.unload();
        ICDebugStick.onICUnload(ic.getBlock());
        if (removeFromLoadedICList)
            this.loadedICs.remove(ic.getBlock());
        ICRedstoneListener listener = this.icRedstoneListener.remove(ic);
        CraftBookPlugin.spongeInst().getChangeBlockPostListenerManager().unregisterAll(listener);
        //if (isSTVariant(ic)) {
            CraftBookPlugin.spongeInst().getSelfTriggerManager().ifPresent(manager -> ((SpongeSelfTriggerManager) manager).unregister(this, ic.getBlock()));
        //}
    }

    @Listener
    public void onChunkUnload(UnloadChunkEvent e) {
        Map<WithinChunkPos, IC> map = this.loadedICs.removeChunk(e.getTargetChunk());
        if (map == null) {
            return;
        }
        for (IC ic : map.values()) {
            unloadIC(ic, false);
        }
    }

    @Listener
    public void onGameStopping(GameStoppingEvent e) {
        // Cancel receiver updates.
        Iterator<IC> ics = this.loadedICs.valueIterator();
        ics.forEachRemaining(ic -> unloadIC(ic, false));
        Sponge.getScheduler().getTasksByName("receiver update").forEach(Task::cancel);
    }

    @Override
    public void onThink(Location<World> block) {
        Optional<IC> icOptional = getIC(block);
        /*MessageReceiver debugReceiver = DEBUG_MESSAGE_RECEIVERS.get(block);
        if (debugReceiver != null) {
            debugReceiver.sendMessage(Text.of("Thinking about ic at " + block.getBlockPosition()));
        }
        if (debugReceiver != null) {
            debugReceiver.sendMessage(Text.of("Is there an IC there: " + icOptional.isPresent()));
            icOptional.ifPresent(ic -> {
                debugReceiver.sendMessage(Text.of("Is UISO or MISO: " + (ic.getPinSet().getName().equals("UISO") || ic.getPinSet().getName().equals("MISO"))));
                debugReceiver.sendMessage(Text.of("Is any triggered: " + (ic.getPinSet().isAnyTriggered(icOptional.get()))));
                debugReceiver.sendMessage(Text.of("Is instanceof SelfTriggeringIC: " + (ic instanceof SelfTriggeringIC)));
            });
        }*/
        icOptional.filter(ic -> (ic.getPinSet() instanceof PinsUISO || ic.getPinSet() instanceof PinsMISO) ?
                ic.getPinSet().isAnyTriggered(ic) : ic instanceof SelfTriggeringIC)
                .map(ic -> (SelfTriggeringIC) ic)
                .ifPresent(ic -> {
                    //if (debugReceiver != null) {
                    //    debugReceiver.sendMessage(Text.of("Really thinking."));
                    //}
                    try {
                        ic.think();
                    } catch (Exception e) {
                        CraftBookPlugin.spongeInst().getLogger().error("Error thinking on " + ic.getClass().getSimpleName(), e);
                    }

                });
    }

    @Override
    public boolean isValid(Location<World> location) {
        if (!SignUtil.isSign(location)) {
            return false;
        }
        TileEntity tileEntity = location.getTileEntity().get();
        return ICManager.getICType(SignUtil.getTextRaw((Sign) tileEntity, 1)) != null;
    }

    public boolean isSTVariant(IC ic) {
        if (!(ic instanceof SelfTriggeringIC))
            return false;

        if ((((SelfTriggeringIC) ic).isAlwaysST()) || ic.getLine(1).endsWith("S")) {
            return true;
        }

        if (ic.getFactory() instanceof HybridIC) {
            String hybridID = ic.getLine(1).substring(ic.getLine(1).indexOf("[") + 1, ic.getLine(1).indexOf("]"));
            return hybridID.equalsIgnoreCase(((HybridIC) ic.getFactory()).stVariant());
        }

        return false;
    }

    public Optional<IC> getIC(Location<World> location) {
        if (loadedICs.get(location) == null) {
            if (location.getBlockType() == BlockTypes.WALL_SIGN) {
                ICType<? extends IC> icType = ICManager.getICType(SignUtil.getTextRaw(location.getTileEntity().get().get(Keys.SIGN_LINES).get().get(1)));
                if (icType != null) {
                    IC ic = icType.getFactory().createInstance(location);
                    if (!loadIC(ic)) {
                        return Optional.empty();
                    }
                    return Optional.of(ic);
                }
            } else {
                return Optional.empty();
            }
        }
        return Optional.ofNullable(loadedICs.get(location));
    }

    private void createICData(Location<World> block, List<Text> lines, Player player) throws InvalidICException {
        if (block.getBlockType() == BlockTypes.WALL_SIGN) {
            ICType<? extends IC> icType = ICManager.getICType(SignUtil.getTextRaw(lines.get(1)));
            if (icType == null) {
                throw new InvalidICException("Invalid IC Type");
            }

            IC ic = icType.getFactory().create(player, lines, block);
            ic.setMode();

            if (ic instanceof BlockPlacingIC && !((BlockPlacingIC) ic).doesForce()) {
                player.sendMessage(Text.of(TextColors.AQUA, ic.getLine(1) + ": " + "To prevent Craftbook-griefing and -mining, this IC will"));
                player.sendMessage(Text.of(TextColors.AQUA, "           not work until all blocks of the type it is set to have"));
                player.sendMessage(Text.of(TextColors.AQUA, "           been removed from its specified area!"));
                //ic.getSign().getSignData().setElement(1,Text.of(ic.getLine(1), "*"));
                lines.set(1, Text.of(lines.get(1), "*"));
            }
            ic.create(player, lines);


            if (icType.getFactory() instanceof SerializedICFactory) {
                block.offer(new ICData(((SerializedICFactory) icType.getFactory()).getData(ic)));
            }

            Sponge.getScheduler().createTaskBuilder().execute(task -> {
                loadIC(ic);
            }).submit(CraftBookPlugin.spongeInst().getContainer());
        } else {
            throw new InvalidICException("Block is not a sign");
        }
    }

    @Override
    public String getPath() {
        return "mechanics/ics/index";
    }

    @Override
    public ConfigValue<?>[] getConfigurationNodes() {
        return new ConfigValue[]{
                maxRadius,
                allowedICBlocks,
                maximumLength,
                maximumWidth
        };
    }

    public static List<SpongeBlockFilter> getDefaultBlocks() {
        List<SpongeBlockFilter> states = Lists.newArrayList();
        states.add(new SpongeBlockFilter(BlockTypes.PLANKS));
        states.add(new SpongeBlockFilter(BlockTypes.STONEBRICK));
        states.add(new SpongeBlockFilter(BlockTypes.COBBLESTONE));
        states.add(new SpongeBlockFilter(BlockState.builder().blockType(BlockTypes.STONE).build().with(Keys.STONE_TYPE, StoneTypes.GRANITE).get()));
        states.add(new SpongeBlockFilter(BlockTypes.STONE));
        states.add(new SpongeBlockFilter(BlockTypes.LOG));
        states.add(new SpongeBlockFilter(BlockTypes.LOG2));
        states.add(new SpongeBlockFilter(BlockTypes.DOUBLE_STONE_SLAB));
        return states;
    }

    @Override
    public String performCustomConversions(String input) {
        StringBuilder icTable = new StringBuilder();

        icTable.append(".. toctree::\n");
        icTable.append("    :hidden:\n");
        icTable.append("    :glob:\n");
        icTable.append("    :titlesonly:\n\n");
        icTable.append("    *\n\n");


        icTable.append("ICs\n");
        icTable.append("===\n\n");

        int idLength = "IC ID".length(),
                shorthandLength = "Shorthand".length(),
                nameLength = "Name".length(),
                descriptionLength = "Description".length(),
                familiesLength = "Family".length(),
                stLength = "Self Triggering".length();

        for (ICType<? extends IC> icType : ICManager.getICTypes()) {
            if ((":doc:`" + icType.getModel() + '`').length() > idLength)
                idLength = (":doc:`ics/" + icType.getModel() + '`').length();
            if (icType.getShorthand().length() > shorthandLength)
                shorthandLength = icType.getShorthand().length();
            if (icType.getName().length() > nameLength)
                nameLength = icType.getName().length();
            if (icType.getDescription().length() > descriptionLength)
                descriptionLength = icType.getDescription().length();
            if (icType.getDefaultPinSet().length() > familiesLength)
                familiesLength = icType.getDefaultPinSet().length();
            if ((SelfTriggeringIC.class.isAssignableFrom(icType.getFactory().createInstance(null).getClass()) ? "Yes" : "No").length() > stLength)
                stLength = (SelfTriggeringIC.class.isAssignableFrom(icType.getFactory().createInstance(null).getClass()) ? "Yes" : "No").length();
        }

        String border = createStringOfLength(idLength, '=') + ' '
                + createStringOfLength(shorthandLength, '=') + ' '
                + createStringOfLength(nameLength, '=') + ' '
                + createStringOfLength(descriptionLength, '=') + ' '
                + createStringOfLength(familiesLength, '=') + ' '
                + createStringOfLength(stLength, '=');

        icTable.append(border).append('\n');
        icTable.append(padToLength("IC ID", idLength + 1))
                .append(padToLength("Shorthand", shorthandLength + 1))
                .append(padToLength("Name", nameLength + 1))
                .append(padToLength("Description", descriptionLength + 1))
                .append(padToLength("Family", familiesLength + 1))
                .append(padToLength("Self Triggering", stLength + 1))
                .append('\n');
        icTable.append(border).append('\n');
        for (ICType<? extends IC> icType : ICManager.getICTypes()) {
            icTable.append(padToLength(":doc:`" + icType.getModel() + '`', idLength + 1))
                    .append(padToLength(icType.getShorthand(), shorthandLength + 1))
                    .append(padToLength(icType.getName(), nameLength + 1))
                    .append(padToLength(icType.getDescription(), descriptionLength + 1))
                    .append(padToLength(icType.getDefaultPinSet(), familiesLength + 1))
                    .append(padToLength((SelfTriggeringIC.class.isAssignableFrom(icType.getFactory().createInstance(null).getClass()) ? "Yes" : "No"), stLength + 1))
                    .append('\n');
        }
        icTable.append(border).append('\n');

        return IC_TABLE_PATTERN.matcher(input).replaceAll(Matcher.quoteReplacement(icTable.toString()));
    }

    //Prevent player modification of IC outputs.
    @Listener
    public void onLeverFlick(InteractBlockEvent.Secondary event, @Root Player player) {
        /*
            It doesn't matter what IC the lever belongs to, we just need to prevent it from getting flipped!

            We need to check lots of different locations and compare them to the IC's pinset
            // https://craftbook.enginehub.org/en/5.0.0/mechanics/ics/icfamilies/

                [o]
            [o][n/o][o]
            [o][n/o][o]
            [o] [b] [o]
            [i] [s] [i]
                [i]

            [b] = Block the IC is mounted to
            [i] = Potential input
            [s] = Sign / IC
            [o] = Potential output
            [n] = Potential block
        */

        if (event.getTargetBlock().getState().getType() != BlockTypes.LEVER
                || !event.getTargetBlock().getLocation().isPresent()) {
            return;
        }
        final Location<World> lever = event.getTargetBlock().getLocation().get();
        final Direction leverDirection = lever.require(Keys.DIRECTION);
        final Direction blockDir = leverDirection.getOpposite();

        final Location<World> block = lever.getRelative(blockDir);

        // Possible blocks that ics are attached to
        final List<Location<World>> possibleICAttachedBlocks = new ArrayList<>();
        possibleICAttachedBlocks.add(block); // The block the lever is attached to

        for (Direction dir : BlockUtil.getDirectHorizontalFaces()) {
            if (dir == leverDirection) {
                continue; // No need to check this way.
            }
            final int DEPTH = 3; // Currently, max distance away from a sign is 3 blocks.
            final Vector3i dirVec = dir.asBlockOffset();
            for (int i = 1; i <= DEPTH; i++) {
                possibleICAttachedBlocks.add(block.add(dirVec.mul(i)));
            }
        }

        for (Location<World> backBlock : possibleICAttachedBlocks) {
            // Some lazy evaluation never hurt anyone :)
            for (Location<World> icBlock : SignUtil.getAttachedSigns(backBlock)) {
                IC ic = getIC(icBlock).orElse(null);
                if (ic == null) {
                    continue;
                }
                final PinSet pinSet = ic.getPinSet();
                final int pinId = pinSet.getPinForLocation(ic, lever);
                if (pinId >= pinSet.getInputCount()) {
                    // Must be an output lever
                    event.setCancelled(true);
                    return;
                }
            }
        }
    }

    @Listener
    public void onBlockRightBlock(InteractBlockEvent event, @Root Player player) {
        BlockState blockState = event.getTargetBlock().getExtendedState();
        if (!SignUtil.isSign(blockState)) {
            return;
        }
        Optional<ItemStackSnapshot> debugStick = event.getContext().get(EventContextKeys.USED_ITEM)
                .filter(item -> item.get(CraftBookKeys.IC_DEBUG_STICK).orElse(false));
        if (!debugStick.isPresent()) {
            return; // Not using debug stick.
        }
        event.setCancelled(true); // Using debug stick, don't allow side effects.
        Location<World> icLoc = event.getTargetBlock().getLocation().get();
        Optional<IC> optIc = getIC(icLoc);
        if (!optIc.isPresent()) {
            player.sendMessage(Text.of("The block you clicked was not a (loaded) ic."));
            return;
        }
        IC ic = optIc.get();

        final ICDebugStickModeData debugStickModeData = debugStick.get().createStack().get(ICDebugStickModeData.class)
                .orElseGet(ICDebugStickModeData::new);

        ICDebugStick.runClick(player, debugStickModeData.require(CraftBookKeys.IC_DEBUG_STICK_MODE), ic);
    }

    @Listener
    public void onRightClickAir(InteractItemEvent.Secondary.MainHand event, @Root Player player) {
        if (!player.get(Keys.IS_SNEAKING).orElse(true)) {
            return;
        }
        HandType handType = event.getHandType();
        Optional<ItemStack> debugStick = player.getItemInHand(handType)
                .filter(item -> item.get(CraftBookKeys.IC_DEBUG_STICK).orElse(false));

        if (!debugStick.isPresent()) {
            return;
        }
        event.setCancelled(true);

        ICDebugStick.cycleMode(player, debugStick.get());
    }
}
