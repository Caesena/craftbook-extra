/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.area;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.legacy.blockbags.MultiNearbyChestBlockBag;
import com.minecraftonline.legacy.ics.chips.world.block.BlockPlacingIC;
import com.minecraftonline.mechanic.SignMechanicCacheManager;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.PermissionNode;
import com.sk89q.craftbook.core.util.documentation.DocumentationProvider;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBag;
import com.sk89q.craftbook.sponge.mechanics.blockbags.BlockBagManager;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import com.sk89q.craftbook.sponge.util.SpongeBlockFilter;
import com.sk89q.craftbook.sponge.util.locale.TranslationsManager;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.Humanoid;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.TranslatableText;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.translation.ResourceBundleTranslation;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

@Module(id = "bridge", name = "Bridge", onEnable="onInitialize", onDisable="onDisable")
public class Bridge extends SimpleArea implements DocumentationProvider {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final ConfigValue<Integer> maximumLength = new ConfigValue<>("maximum-length", "The maximum length the bridge can be.", 16);
    private final ConfigValue<Integer> maximumWidth = new ConfigValue<>("maximum-width", "The maximum width each side of the bridge can be. The overall max width is this*2 + 1.", 5);

    private final TranslatableText notAllowedMaterial = TranslatableText.of(new ResourceBundleTranslation("bridge.cant-use-material", TranslationsManager.getResourceBundleFunction()));

    @Override
    public void onInitialize() throws CraftBookException {
        super.loadCommonConfig(config);
        super.registerCommonPermissions();

        maximumLength.load(config);
        maximumWidth.load(config);

        cache.registerCache(this, (loc, signData) -> {
            if (!isValidNoCache(loc, signData)) {
                return null;
            }
            registerRedstoneListener(loc);
            return new BridgeCachedData(signData.lines().get());
        });
    }

    @Override
    public Collection<Vector3i> createInputList(Location<World> mechanicLocation) {
        Vector3i pos = mechanicLocation.getBlockPosition();
        return Arrays.asList(
                pos.add(1, 0, 0),
                pos.add(-1, 0, 0),
                pos.add(0, 0, 1),
                pos.add(0, 0, -1)
        );
    }

    public class BridgeCachedData extends SimpleArea.SimpleAreaCache {
        protected BridgeCachedData(List<Text> lines) {
            super(lines);
        }
    }

    // Where the bridge can be relative to the bridge sign
    final Direction[] allowedSides = new Direction[] {Direction.DOWN, Direction.UP};

    @Override
    public boolean triggerMechanic(Location<World> block, SignMechanicCacheManager.CachedData cachedData, Humanoid human, Boolean forceState) {
        if (cachedData.getLinePlain(1).equals("[Bridge End]")) {
            sendMessage(human, Text.of(TextColors.RED, "You cannot activate the bridge from this side"));
            return false;
        }

        Direction back = SignUtil.getBack(block);

        final Location<World> baseBlock;
        Direction bridgeSignSide = null; // Side that the base block is relative to the sign.

        for (Direction side : allowedSides) {
            if (BlockUtil.doesStatePassFilters(allowedBlocks.getValue(), block.getRelative(side).getBlock())) {
                bridgeSignSide = side;
                break;
            }
        }

        if (bridgeSignSide == null) {
            sendMessage(human, Text.of(TextColors.RED, "No allowed blocks"));
            return false;
        }

        baseBlock = block.getRelative(bridgeSignSide);
        BlockState baseBlockState = baseBlock.getBlock();

        Location<World> otherSide = BlockUtil.getNextMatchingSign(block, back, maximumLength.getValue(), this::isMechanicSign);
        if (otherSide == null) {
            sendMessage(human, Text.of(TextColors.RED, "No other side of bridge found"));
            return false;
        }

        Location<World> otherBaseBlock = otherSide.getRelative(bridgeSignSide);
        if (!otherBaseBlock.getBlock().equals(baseBlockState)) {
            sendMessage(human, Text.of(TextColors.RED, "Mismatching block on the other side of the bridge!"));
            return false;
        }

        BlockBag bag = getBlockBag(block);

        if (bag == null) {
            sendMessage(human, Text.of(TextColors.RED, "You have nowhere to store the blocks!"));
            return false;
        }

        Direction left = SignUtil.getLeft(block);
        Direction right = SignUtil.getRight(block);

        // Find the maximum width thats valid for both
        int leftWidth = Math.min(
                countWidth(left, baseBlock, baseBlockState),
                countWidth(left, otherBaseBlock, otherBaseBlock.getBlock())
                );
        int rightWidth = Math.min(
                countWidth(right, baseBlock, baseBlockState),
                countWidth(right, otherBaseBlock, otherBaseBlock.getBlock())
        );

        int totalWidth = leftWidth + 1 + rightWidth;
        int length = (int) block.getPosition().distance(otherSide.getPosition()) - 1; // take away start

        boolean forcing = forceState != null && forceState;

        int alreadyFilledBlocks = 0;
        // Need to check for obstructing blocks
        Location<World> startPos = baseBlock.add(left.asBlockOffset().mul(leftWidth)).getRelative(back);
        for (int i = 0; i < totalWidth; i++) {
            for (int j = 0; j < length; j++) {
                Location<World> pos = startPos.add(right.asBlockOffset().mul(i)).add(back.asBlockOffset().mul(j));
                if (pos.getBlock().getType() != BlockTypes.AIR) {
                    if (pos.getBlock().equals(baseBlockState)) {
                        alreadyFilledBlocks++;
                    }
                    else if (!forcing) {
                        sendMessage(human, Text.of(TextColors.RED, "A block is obstructing the bridge!"));
                        return false;
                    }
                }
            }
        }
        ItemStack item = ItemStack.builder().fromBlockState(baseBlockState).build();

        boolean filling = alreadyFilledBlocks == 0;

        if (filling) {
            int requiredBlocks = (totalWidth * length) - alreadyFilledBlocks;

            Predicate<ItemStack> takePredicate = BlockPlacingIC.getPredicate(item);
            int actualTaken = 0;

            if (bag.has(takePredicate, requiredBlocks)) {
                for (int i = 0; i < totalWidth; i++) {
                    for (int j = 0; j < length; j++) {
                        Location<World> pos = startPos.add(right.asBlockOffset().mul(i)).add(back.asBlockOffset().mul(j));
                        if (pos.getBlock().getType() == BlockTypes.AIR) {
                            pos.setBlock(baseBlockState);
                            if (pos.getBlock().equals(baseBlockState)) {
                                actualTaken++;
                            }
                        }
                    }
                }
                bag.remove(takePredicate, actualTaken);
            }
            else {
                sendMessage(human, Text.of(TextColors.RED, "Not enough blocks!"));
                return false;
            }
        }
        else {
            List<ItemStack> itemStacks = BlockPlacingIC.getPickupBlocks(item, alreadyFilledBlocks);
            int actualRemoved = 0;

            if (bag.canFit(itemStacks)) {
                for (int i = 0; i < totalWidth; i++) {
                    for (int j = 0; j < length; j++) {
                        Location<World> pos = startPos.add(right.asBlockOffset().mul(i)).add(back.asBlockOffset().mul(j));
                        if (pos.getBlockType() == BlockTypes.AIR) {
                            continue;
                        }
                        pos.setBlockType(BlockTypes.AIR);
                        if (pos.getBlock().getType() == BlockTypes.AIR) {
                            actualRemoved++;
                        }
                    }
                }
                if (actualRemoved != alreadyFilledBlocks) {
                    itemStacks = BlockPlacingIC.getPickupBlocks(item, actualRemoved);
                }
                bag.add(itemStacks);
            }
            else {
                sendMessage(human, Text.of(TextColors.RED, "Not enough space"));
                return false;
            }
        }

        return true;
    }

    private int countWidth(Direction dir, Location<World> location, BlockState blockToMatch) {
        int i = 0;
        for (; i < maximumLength.getValue(); i++) {
            location = location.getRelative(dir);
            if (!location.getBlock().equals(blockToMatch)) {
                break;
            }
        }
        return i;
    }

    private void sendMessage(Humanoid human, Text text) {
        if (human instanceof CommandSource) {
            ((CommandSource) human).sendMessage(text);
        }
    }

    @Override
    public BlockBag getBlockBag(Location<World> location) {
        return BlockBagManager.getOrCreateCachedBlockBag(BlockBagManager.BlockBagType.MULTI_NEARBY_CHEST, location, MultiNearbyChestBlockBag.DEFAULT_RADIUS, MultiNearbyChestBlockBag.getChestBlockTypes());
    }

    @Override
    public String[] getValidSigns() {
        return new String[]{"[Bridge]", "[Bridge End]"};
    }

    @Override
    public List<SpongeBlockFilter> getDefaultBlocks() {
        List<SpongeBlockFilter> states = Lists.newArrayList();
        states.add(new SpongeBlockFilter(BlockTypes.PLANKS));
        states.add(new SpongeBlockFilter(BlockTypes.STONEBRICK));
        states.add(new SpongeBlockFilter(BlockTypes.COBBLESTONE));
        return states;
    }

    @Override
    public String getPath() {
        return "mechanics/bridge";
    }

    @Override
    public ConfigValue<?>[] getConfigurationNodes() {
        return new ConfigValue<?>[]{
                allowedBlocks,
                allowRedstone,
                maximumLength,
                maximumWidth
        };
    }

    @Override
    public PermissionNode[] getPermissionNodes() {
        return new PermissionNode[]{
                createPermissions,
                usePermissions
        };
    }
}
