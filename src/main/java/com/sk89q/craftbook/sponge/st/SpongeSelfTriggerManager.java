/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.st;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.ImmutableMap;
import com.me4502.modularframework.exception.ModuleNotInstantiatedException;
import com.me4502.modularframework.module.ModuleWrapper;
import com.me4502.modularframework.module.SpongeModuleWrapper;
import com.minecraftonline.util.nms.LocationWorldUtil;
import com.sk89q.craftbook.core.st.SelfTriggerClock;
import com.sk89q.craftbook.core.st.SelfTriggerManager;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeBlockMechanic;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeMechanic;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.world.LoadWorldEvent;
import org.spongepowered.api.event.world.UnloadWorldEvent;
import org.spongepowered.api.event.world.chunk.LoadChunkEvent;
import org.spongepowered.api.event.world.chunk.UnloadChunkEvent;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class SpongeSelfTriggerManager implements SelfTriggerManager {

    //private Map<Location<World>, SelfTriggeringMechanic> selfTriggeringMechanics = new ConcurrentHashMap<>();

    public static class WorldChunk {
        public final Vector3i chunkPos;
        public final UUID uuid;

        public WorldChunk(Vector3i chunkPos, UUID uuid) {
            this.chunkPos = chunkPos;
            this.uuid = uuid;
        }

        public static WorldChunk from(Location<World> loc) {
            return new WorldChunk(loc.getChunkPosition(), loc.getExtent().getUniqueId());
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof WorldChunk)) return false;
            WorldChunk that = (WorldChunk) o;
            return chunkPos.equals(that.chunkPos) && uuid.equals(that.uuid);
        }

        @Override
        public int hashCode() {
            return Objects.hash(chunkPos, uuid);
        }
    }

    private final Map<WorldChunk, Map<Vector3i, SelfTriggeringMechanic>> selfTriggeringMechanics = new ConcurrentHashMap<>();

    @Override
    public void initialize() {
        Sponge.getGame().getScheduler().createTaskBuilder().intervalTicks(2L).execute(new SelfTriggerClock()).submit(CraftBookPlugin.inst());
        Sponge.getGame().getEventManager().registerListeners(CraftBookPlugin.inst(), this);

        for(World world : Sponge.getGame().getServer().getWorlds()) {
            registerAll(world);
        }
    }

    @Override
    public void unload() {
        selfTriggeringMechanics.clear();
    }

    public Map<WorldChunk, Map<Vector3i, SelfTriggeringMechanic>> getSelfTriggeringMechanics() {
        return ImmutableMap.copyOf(selfTriggeringMechanics);
    }

    public void register(SelfTriggeringMechanic mechanic, Location<World> location) {
        WorldChunk worldChunk = WorldChunk.from(location);
        selfTriggeringMechanics.compute(worldChunk, (k,v) -> {
            if (v == null) {
                v = new ConcurrentHashMap<>();
            }
            v.put(location.getBlockPosition(), mechanic);
            return v;
        });
    }

    public void unregister(SelfTriggeringMechanic mechanic, Location<World> location) {
        WorldChunk worldChunk = WorldChunk.from(location);
        Map<Vector3i, SelfTriggeringMechanic> mechanicMap = selfTriggeringMechanics.get(worldChunk);
        if (mechanicMap == null) {
            return;
        }
        mechanicMap.remove(location.getBlockPosition(), mechanic);
    }

    public boolean isRegistered(SelfTriggeringMechanic mechanic, Location<World> location) {
        Map<Vector3i, SelfTriggeringMechanic> mechanicMap = selfTriggeringMechanics.get(WorldChunk.from(location));

        return mechanicMap != null && mechanicMap.get(location.getBlockPosition()) == mechanic;
    }

    private void registerAll(World world) {
        for (Chunk chunk : world.getLoadedChunks()) {
            registerAll(chunk);
        }
    }

    private void registerAll(Chunk chunk) {
        for (TileEntity tileEntity : chunk.getTileEntities()) {
            for (ModuleWrapper module : CraftBookPlugin.spongeInst().moduleController.getModules()) {
                if (!module.isEnabled()) continue;
                try {
                    SpongeMechanic mechanic = (SpongeMechanic) ((SpongeModuleWrapper) module).getModuleUnchecked();
                    if (mechanic instanceof SpongeBlockMechanic && mechanic instanceof SelfTriggeringMechanic && !(mechanic instanceof ICSocket)) {
                        if (((SpongeBlockMechanic) mechanic).isValid(tileEntity.getLocation())) {
                            register((SelfTriggeringMechanic) mechanic, tileEntity.getLocation());
                        }
                    }
                } catch (ModuleNotInstantiatedException e) {
                    CraftBookPlugin.spongeInst().getLogger().error("Failed to register self-triggering mechanic for module: " + module.getName(), e);
                }
            }
        }
    }

    @Override
    public void think() {
        for (Entry<WorldChunk, Map<Vector3i, SelfTriggeringMechanic>> entry : selfTriggeringMechanics.entrySet()) {
            World world = Sponge.getServer().getWorld(entry.getKey().uuid).get();
            if (LocationWorldUtil.isChunkQueuedForUnload(world, entry.getKey().chunkPos)) {
                //CraftBookPlugin.spongeInst().getLogger().info("Not self triggering at " + entry.getKey().chunkPos + " because its queued for unload");
                continue; // Don't activate self triggering ics if an unload is queued.
            }
            for (Entry<Vector3i, SelfTriggeringMechanic> mechanicEntry : entry.getValue().entrySet()) {
                mechanicEntry.getValue().onThink(world.getLocation(mechanicEntry.getKey()));
            }
        }
    }

    @Listener
    public void onChunkLoad(LoadChunkEvent event) {
        registerAll(event.getTargetChunk());
    }

    @Listener
    public void onChunkUnload(UnloadChunkEvent event) {
        //Sponge.getServer().getConsole().getMessageChannel().send(Text.of("Unloading chunk at: " + event.getTargetChunk().getPosition()));
        WorldChunk worldChunk = new WorldChunk(event.getTargetChunk().getPosition(), event.getTargetChunk().getWorld().getUniqueId());
        selfTriggeringMechanics.remove(worldChunk);
    }

    @Listener
    public void onWorldLoad(LoadWorldEvent event) {
        registerAll(event.getTargetWorld());
    }

    @Listener
    public void onWorldUnload(UnloadWorldEvent event) {
        selfTriggeringMechanics.entrySet().removeIf(entry -> entry.getKey().uuid.equals(event.getTargetWorld().getUniqueId()));
    }
}
