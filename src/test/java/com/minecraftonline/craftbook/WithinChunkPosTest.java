/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook;

import com.minecraftonline.util.WithinChunkPos;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class WithinChunkPosTest {
    @Test
    public void checkForCollisions() {

        Map<Integer, WithinChunkPos> hashCodes = new HashMap<>();
        for (byte x = 0; x < 16; x++) {
            for (short y = 0; y < 256; y++) {
                for (byte z = 0; z < 16; z++) {
                    //System.out.println("Testing: " + x + ", " + y + ", " + z);
                    WithinChunkPos withinChunkPos = new WithinChunkPos(x, y, z);
                    int hashCode = withinChunkPos.hashCode();
                    WithinChunkPos collision = hashCodes.get(hashCode);
                    if (collision != null) {
                        Assert.fail("Collided hashcode " + hashCode + ", " + withinChunkPos + " and " + collision + "\n"
                        + "Binary: " + Integer.toString(hashCode, 2) + ", " + withinChunkPos.toString(2) + " and " + collision.toString(2));
                    }
                    hashCodes.put(hashCode, withinChunkPos);
                }
            }
        }
    }

    @Test
    public void checkWorldCordToChunkPos() {
        // Gathered from ingame F3
        assertEquals(WithinChunkPos.fromBlockPos(-94, 63, 357), new WithinChunkPos((byte) 2, (short) 63,(byte) 5));
    }
}
