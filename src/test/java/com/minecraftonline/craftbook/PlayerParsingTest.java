/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook;

import com.minecraftonline.util.entity.PlayerParsingUtil;
import org.junit.Assert;
import org.junit.Test;

public class PlayerParsingTest {

    @Test
    public void test() {
        check("player", new PlayerParsingUtil.AnyHumanoid());
        check("p:tyhdefu", new PlayerParsingUtil.SpecificPlayer("tyhdefu", false));
        check("p:!tyhdefu", new PlayerParsingUtil.SpecificPlayer("tyhdefu", true));
        check("m:tyh", new PlayerParsingUtil.HumanoidNameMatch("tyh", false));
        check("m:!tyh", new PlayerParsingUtil.HumanoidNameMatch("tyh", true));
        check("g:mods", new PlayerParsingUtil.PlayerGroup("mods", false));
        check("g:!mods", new PlayerParsingUtil.PlayerGroup("mods", true));
    }

    @Test
    public void testFail() {
        Assert.assertNull(PlayerParsingUtil.parse("adwikdjkawkd"));
        Assert.assertNull(PlayerParsingUtil.parse("potato"));
        Assert.assertNull(PlayerParsingUtil.parse("potato:pie"));
        Assert.assertNull(PlayerParsingUtil.parse("i:pie"));
        Assert.assertNull(PlayerParsingUtil.parse("p:"));
        Assert.assertNull(PlayerParsingUtil.parse("i:"));
    }

    public static void check(final String string, final PlayerParsingUtil.HumanoidMatcher expected) {
        PlayerParsingUtil.HumanoidMatcher matcher = PlayerParsingUtil.parse(string);
        Assert.assertNotNull("Failed to parse object", matcher);
        Assert.assertEquals("Didn't get the expected parsing result", matcher, expected);
    }
}
