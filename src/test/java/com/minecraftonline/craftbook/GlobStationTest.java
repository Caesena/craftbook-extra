/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.craftbook;

import com.minecraftonline.legacy.mechanics.minecart.CartSorting;
import org.junit.Assert;
import org.junit.Test;

public class GlobStationTest {

    @Test
    public void globTest() {
        Assert.assertTrue(CartSorting.matchGlobStation("stromhurst", "strom*"));
        Assert.assertTrue(CartSorting.matchGlobStation("stromburst", "strom*"));

        Assert.assertTrue(CartSorting.matchGlobStation("beep", "beep*"));
        Assert.assertTrue(CartSorting.matchGlobStation("beep", "beep"));

        Assert.assertTrue(CartSorting.matchGlobStation("annaisle", "*isle"));
        Assert.assertTrue(CartSorting.matchGlobStation("naanisle", "*isle"));
        Assert.assertTrue(CartSorting.matchGlobStation("stromisle", "*isle"));

        Assert.assertTrue(CartSorting.matchGlobStation("misle", "***isle"));

        Assert.assertTrue(CartSorting.matchGlobStation("tyhiscool", "*is*"));
        Assert.assertTrue(CartSorting.matchGlobStation("annaissilly", "*is*"));

        Assert.assertTrue(CartSorting.matchGlobStation("AnNaissilly", "anna*"));
        Assert.assertTrue(CartSorting.matchGlobStation("ANNA", "anna"));
        Assert.assertTrue(CartSorting.matchGlobStation("anna", "ANNA"));

        Assert.assertFalse(CartSorting.matchGlobStation( "beep", "bee"));
        Assert.assertFalse(CartSorting.matchGlobStation("beepboop", "beep"));
    }
}
