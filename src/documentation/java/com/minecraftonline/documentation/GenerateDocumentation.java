/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.documentation;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.minecraftonline.ic.AbstractIC;
import com.minecraftonline.legacy.ics.HybridIC;
import com.minecraftonline.util.CraftBookPermissions;
import com.minecraftonline.util.ParsingConfiguration;
import com.minecraftonline.util.parsing.lineparser.Limitation;
import com.minecraftonline.util.parsing.lineparser.LineParserFactory;
import com.minecraftonline.util.parsing.lineparser.Restriction;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.mechanics.ics.ICManager;
import com.sk89q.craftbook.sponge.mechanics.ics.ICType;
import com.sk89q.craftbook.sponge.mechanics.ics.SelfTriggeringIC;
import net.steppschuh.markdowngenerator.table.Table;
import net.steppschuh.markdowngenerator.table.TableRow;
import net.steppschuh.markdowngenerator.text.code.Code;
import net.steppschuh.markdowngenerator.text.emphasis.BoldText;
import net.steppschuh.markdowngenerator.text.heading.Heading;
import org.junit.Test;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.lwts.runner.LaunchWrapperTestRunner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static com.minecraftonline.util.ParsingConfiguration.LineParser;
import static com.minecraftonline.util.ParsingConfiguration.LineParserInfo;

public class GenerateDocumentation {

    private static final String OUTPUT_FILE_SYSTEM_PROPERTY = "outputFile";

    public static void main(String[] args) throws Throwable {
        String filePath = args[0];
        System.setProperty(OUTPUT_FILE_SYSTEM_PROPERTY, filePath);
        AtomicReference<Failure> possibleFailure = new AtomicReference<>();
        RunNotifier notifier = new RunNotifier();
        notifier.addListener(new RunListener() {
            @Override
            public void testFailure(Failure failure) {
                possibleFailure.set(failure);
            }
        });
        new LaunchWrapperTestRunner(GenerateDocumentation.class).run(notifier);
        // Pull exception from test method into the main.
        if (possibleFailure.get() != null) {
            throw new IllegalStateException("Method failed: " + possibleFailure.get().getTestHeader(), possibleFailure.get().getException());
        }
    }

    // Not really a test, just so that we get it running in a state with mixins and sponge working properly.
    @Test
    public void generateICList() throws IOException {
        List<ICType<?>> icTypes = new ArrayList<>(ICManager.getICTypes());
        icTypes.sort(Comparator.comparing(ICType::getName));
        String outputFilePath = System.getProperty(OUTPUT_FILE_SYSTEM_PROPERTY);
        if (outputFilePath == null) {
            throw new IllegalStateException("You must specify the output file system property '" + OUTPUT_FILE_SYSTEM_PROPERTY + "'");
        }
        System.out.println("Output: " + outputFilePath);
        File file = new File(outputFilePath, "documentation.md");
        file.getParentFile().mkdirs();
        file.createNewFile();
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(new Heading("IC list", 1).toString());
            bw.newLine();
            for (ICType<?> icType : icTypes) {
                String stVariant = null;
                ICFactory<?> factory = icType.getFactory();
                if (factory instanceof HybridIC) {
                    stVariant = ((HybridIC) factory).stVariant();
                    if (icType.getModel().equals(stVariant)) {
                        continue;
                    }
                }
                AbstractIC ic = (AbstractIC) factory.createInstance(null);
                ParsingConfiguration configuration = ic.makeParsingConfiguration();
                Map<LineParser, LineParserInfo> map = configuration.getParsers();

                Table.Builder tableBuilder = new Table.Builder();

                tableBuilder.addRow(icType.getName(), "");
                tableBuilder.addRow("ID", icType.getModel());
                tableBuilder.addRow("Self Triggered Variant", stVariant == null ? "None" : stVariant);
                tableBuilder.addRow("PinSet", icType.getDefaultPinSet());
                tableBuilder.addRow("Permission", new Code(icType.getPermissionNode().getNode()));

                tableBuilder.addRow(new BoldText("Sign Format"));

                List<TableRow> icLines = new ArrayList<>();

                icLines.add(new TableRow(Arrays.asList("Line 1", icType.getName())));
                icLines.add(new TableRow(Arrays.asList("Line 2", "[" + icType.getModel() + "]")));

                Map<LineParserInfo, LineParserFactory<?, ?>> factoryMap = new LinkedHashMap<>();

                for (Map.Entry<LineParser, LineParserInfo> entry : map.entrySet()) {
                    LineParser lineParser = entry.getKey();
                    LineParserInfo info = entry.getValue();

                    Text text = lineParser.getHelp();
                    String s;
                    if (text == null) {
                        System.out.println("WARNING: LineParser in ic " + icType.getName() + " for " + info.getLinesDescription() + " has no help. Using null for generated markdown file.");
                        s = null;
                    } else {
                        s = text.toPlain().replaceAll("\\|", "&#124;");
                    }

                    TableRow row = new TableRow(Arrays.asList(capitaliseFirst(info.getLinesDescription()), s));
                    if (info.includesTopLine()) {
                        icLines.set(0, row);
                    } else {
                        icLines.add(row);
                    }
                    if (lineParser instanceof LineParserFactory) {
                        factoryMap.put(info, (LineParserFactory<?, ?>) lineParser);
                    }

                }
                for (TableRow row : icLines) {
                    tableBuilder.addRow(row);
                }

                bw.write("<a name=" + icType.getName().toLowerCase() + "></a>");
                bw.write(new Heading(icType.getName(), 2).toString());
                bw.newLine();
                bw.write(tableBuilder.build().toString());
                bw.newLine();
                bw.write(icType.getDescription());
                bw.newLine();
                bw.newLine();

                boolean hasRestrictions = false;
                StringBuilder restrictionsBuilder = new StringBuilder();

                restrictionsBuilder.append(new Heading("Restrictions", 3)).append("\n")
                        .append("Restrictions are requirements that all players must fulfil, regardless of permissions.")
                        .append("\n");

                boolean hasLimitations = false;
                StringBuilder limitationsBuilder = new StringBuilder();

                limitationsBuilder.append(new Heading("Limitations", 3)).append("\n")
                        .append("Limitations are requirements that regular players must fulfil. They are for gameplay, and sometimes configurable. Players with the permission ")
                        .append(CraftBookPermissions.UNLIMITED)
                        .append(" can ignore these.")
                        .append("\n");

                for (Map.Entry<LineParserInfo, LineParserFactory<?, ?>> entry : factoryMap.entrySet()) {
                    LineParserFactory<?, ?> lineParserFactory = entry.getValue();
                    LineParserInfo info = entry.getKey();
                    if (!lineParserFactory.getRestrictions().isEmpty()) {
                        hasRestrictions = true;
                        restrictionsBuilder.append(new Heading(capitaliseFirst(info.getLinesDescription()), 4)).append("\n");
                        for (Restriction<?> restriction : lineParserFactory.getRestrictions()) {
                            String desc = restriction.getDescription();
                            if (desc == null) {
                                System.out.println("WARNING: Restriction in ic " + icType.getName() + " for " + info.getLinesDescription() + " has no help message. Using 'null' for markdown file generation");
                            }
                            restrictionsBuilder.append("- ").append(desc).append("\n");
                        }
                    }
                    if (!lineParserFactory.getLimitations().isEmpty()) {
                        hasLimitations = true;
                        limitationsBuilder.append(new Heading(capitaliseFirst(info.getLinesDescription()), 4)).append("\n");
                        for (Limitation<?> limitation : lineParserFactory.getLimitations()) {
                            String desc = limitation.getDescription();
                            if (desc == null) {
                                System.out.println("WARNING: Limitation in ic " + icType.getName() + " for " + info.getLinesDescription() + " has no help message. Using 'null' for markdown file generation");
                            }
                            limitationsBuilder.append("- ").append(desc).append("\n");
                        }
                    }
                }

                if (hasRestrictions) {
                    bw.write(restrictionsBuilder.toString());
                    bw.newLine();
                }
                if (hasLimitations) {
                    bw.write(limitationsBuilder.toString());
                    bw.newLine();
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Test
    public void generateJson() {
        List<ICType<?>> icTypes = new ArrayList<>(ICManager.getICTypes());
        icTypes.sort(Comparator.comparing(ICType::getName));
        JsonObject json = new JsonObject();
        json.addProperty("version", "1.0");
        JsonArray ics = new JsonArray();
        json.add("ics", ics);
        for (ICType<?> icType : icTypes) {
            String stVariant = null;
            ICFactory<?> factory = icType.getFactory();
            if (factory instanceof HybridIC) {
                stVariant = ((HybridIC) factory).stVariant();
                if (icType.getModel().equals(stVariant)) {
                    System.out.println("Skipping model " + icType.getModel());
                    continue;
                }
            }
            JsonObject icJson = new JsonObject();
            ics.add(icJson);
            AbstractIC ic = (AbstractIC) factory.createInstance(null);
            boolean selfTriggering = ic instanceof SelfTriggeringIC;
            ParsingConfiguration configuration = ic.makeParsingConfiguration();

            // Basic data.
            icJson.addProperty("model", icType.getModel());
            icJson.addProperty("name", icType.getName());
            icJson.addProperty("pinset", icType.getDefaultPinSet());
            if (stVariant != null) {
                icJson.addProperty("self_triggering_variant", stVariant);
            }
            else {
                icJson.addProperty("self_triggering", selfTriggering);
            }
            icJson.addProperty("permission", icType.getPermissionNode().getNode());
            icJson.addProperty("description", icType.getDescription());
            JsonObject parameters = new JsonObject();
            icJson.add("parameters", parameters);
            Map<LineParser, LineParserInfo> map = configuration.getParsers();
            for (Map.Entry<LineParser, LineParserInfo> entry : map.entrySet()) {
                LineParser lineParser = entry.getKey();
                LineParserInfo info = entry.getValue();
                JsonArray linesArray = parameters.getAsJsonArray("lines");
                if (linesArray == null) {
                    linesArray = new JsonArray();
                    parameters.add("lines", linesArray);
                }
                JsonObject lineInfo = new JsonObject();

                if (info instanceof ParsingConfiguration.SingleLineParserInfo) {
                    ParsingConfiguration.SingleLineParserInfo singleLineParserInfo = (ParsingConfiguration.SingleLineParserInfo) info;
                    linesArray.add(lineInfo);
                    lineInfo.addProperty("type", "single");
                    lineInfo.addProperty("line", singleLineParserInfo.getLine());
                }
                else if (info instanceof ParsingConfiguration.MultiLineParserInfo) {
                    ParsingConfiguration.MultiLineParserInfo multiLineParserInfo = (ParsingConfiguration.MultiLineParserInfo) info;
                    linesArray.add(lineInfo);
                    lineInfo.addProperty("type", "multi");
                    JsonArray includedLines = new JsonArray();
                    for (int line : multiLineParserInfo.getLines()) {
                        includedLines.add(line);
                    }
                    lineInfo.add("lines", includedLines);
                }
                else {
                    System.out.println("WARNING: Unknown Line Parser Info type: " + info.getClass().getSimpleName());
                }
                if (lineParser.getHelp() != null) {
                    lineInfo.addProperty("help", lineParser.getHelp().toPlain());
                }
                if (lineParser instanceof LineParserFactory.FactoryLineParser) {
                    LineParserFactory<?, ?> lineParserFactory = ((LineParserFactory.FactoryLineParser<?>) lineParser).getFactoryRef();
                    if (lineParserFactory.getDefaultValue() != null) {
                        lineInfo.addProperty("default", String.valueOf(lineParserFactory.getDefaultValue()));
                    }
                    if (!lineParserFactory.getLimitations().isEmpty()) {
                        JsonArray limitations = new JsonArray();
                        lineInfo.add("limitations", limitations);
                        for (Limitation<?> limitation : lineParserFactory.getLimitations()) {
                            if (limitation.getDescription() != null) {
                                limitations.add(limitation.getDescription());
                            }
                        }
                    }
                    if (!lineParserFactory.getRestrictions().isEmpty()) {
                        JsonArray restrictions = new JsonArray();
                        lineInfo.add("restrictions", restrictions);
                        for (Restriction<?> restriction : lineParserFactory.getRestrictions()) {
                            if (restriction.getDescription() != null) {
                                restrictions.add(restriction.getDescription());
                            }
                        }
                    }
                }
            }
        }
        File file = new File("iclist.json");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(json.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void enumerateRecipes() {
        String outputFilePath = System.getProperty(OUTPUT_FILE_SYSTEM_PROPERTY);
        if (outputFilePath == null) {
            throw new IllegalStateException("You must specify the output file system property '" + OUTPUT_FILE_SYSTEM_PROPERTY + "'");
        }
        System.out.println("Output: " + outputFilePath);
        File folder = new File(outputFilePath, "cartcraft");
        folder.mkdirs();
        File file = new File(folder, "recipes.md");
        try {
            file.createNewFile();
            BufferedWriter br = new BufferedWriter(new FileWriter(file));
            br.write("This is a list of all minecraft recipes that cartcraft will accept.");
            br.newLine();
            List<String> toSort = new ArrayList<>();
            for (CraftingRecipe craftingRecipe : Sponge.getRegistry().getCraftingRecipeRegistry().getAll()) {
                String id = craftingRecipe.getId();
                toSort.add(id.split(":")[1].replaceAll("_",""));
            }
            toSort.sort(null);
            for (String id : toSort) {
                br.write("- " + id);
                br.newLine();
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String capitaliseFirst(String s) {
        return Character.toUpperCase(s.charAt(0)) + s.substring(1);
    }
}
