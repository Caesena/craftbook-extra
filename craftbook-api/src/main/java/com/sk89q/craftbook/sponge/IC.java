/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge;

import com.sk89q.craftbook.sponge.mode.Mode;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

public interface IC {

	PinSet getPinSet();

	int getTriggeredPin();

	void setTriggeredPin(int triggeredPin);

	/**
	 * Called whenever an IC is created for the first time. Setup constant data here.
	 *
	 * @param player The creating player
	 * @param lines The sign lines
	 * @throws InvalidICException Thrown if there is an issue with this IC
	 */
	void create(Player player, List<Text> lines) throws InvalidICException;

	/**
	 * Called when an IC is loaded into the world.
	 */
	void load();

	void unload();

	Sign getSign();

	String getLine(int line);

	void setLine(int line, Text text);

	void setAdminAppearance();

	void setAdminAppearanceOnLoad();

	Location<World> getBlock();

	Location<World> getBlockCentre();

	Location<World> getBackBlock();

	Location<World> getBackBlockCentre();

	ICFactory<? extends IC> getFactory();

	void trigger();

	Mode getMode();

	void setMode();

	boolean hasMode();

	/**
	 * Returns the cached facing direction of the sign.
	 * @return The direction the sign is facing.
	 */
	Direction getFront();
}
