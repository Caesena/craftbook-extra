/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge;

import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public interface PinSet {
	int getInputCount();

	int getOutputCount();

	void setOutput(int outputId, boolean powered, IC ic);

	int getPinForLocation(IC ic, Location<World> location);

	boolean getInput(int inputId, IC ic);

	int getInputPowerLevel(int inputId, IC ic);

	boolean getOutput(int outputId, IC ic);

	boolean isValid(int id, IC ic);

	boolean isTriggered(int id, IC ic);

	boolean isAnyTriggered(IC ic);

	String getName();

	Location<World> getPinLocation(int id, IC ic);
}
