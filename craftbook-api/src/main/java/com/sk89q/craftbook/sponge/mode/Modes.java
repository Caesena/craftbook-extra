/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mode;

public enum Modes {
    INVERT,
    CYCLE_OFF,
    NO_Y_OFFSET,
    REVERSE,
    SWAP_IO,
    THUNDER_STORM,
    HUMANSONLYEXTRA, // come up with better name but it basically means stuff like minecarts/fishing hooks dont get killed.
    LOGTONEARBYPLAYERS,
    NONE,
    TELEPORT_PAD,
    TELEPORT_PAD_FORCED_PRESSURE_PLATE,
}
