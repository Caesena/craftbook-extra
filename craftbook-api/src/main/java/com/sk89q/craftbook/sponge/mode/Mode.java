/*
 * CraftBook Copyright (C) 2010-2022 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2022 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mode;

import com.google.common.primitives.Ints;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Brendan  (doublehelix457)
 *
 * Class for automatically constructing and implementing Optional Modes.
 * A feature that CraftBook Extra is most famous for.
 */
public class Mode {

    private final String mode;
    private final Modes type;
    private final HashMap<Character,Integer> defaultPinConfiguration = new HashMap<>();
    {
        defaultPinConfiguration.put('a',0);
        defaultPinConfiguration.put('b',1);
        defaultPinConfiguration.put('c',2);
        defaultPinConfiguration.put('d',3);
        defaultPinConfiguration.put('e',4);
        defaultPinConfiguration.put('f',5);
    }

    /**
     *
     * @param mode String after IC Model ID
     */
    public Mode(String mode){
        //store "mode" in var to use later
        this.mode = mode;
        //scan for pin configuration mode, sort to match

        switch(mode.trim()){
            case "+":
                type = Modes.LOGTONEARBYPLAYERS;
                break;
            case "!":
                type = Modes.INVERT;
                break;
            case "1":
                type = Modes.CYCLE_OFF;
                break;
            case "=":
                type = Modes.NO_Y_OFFSET;
                break;
            case "r":
                type = Modes.REVERSE;
                break;
            case "t":
                type = Modes.THUNDER_STORM;
                break;
            case "-":
                type = Modes.HUMANSONLYEXTRA;
                break;
            case "p":
                type = Modes.TELEPORT_PAD;
                break;
            case "P": {
                type = Modes.TELEPORT_PAD_FORCED_PRESSURE_PLATE;
                break;
            }
            default:
                if(mode.matches(".*[a-f].*")) type=Modes.SWAP_IO;
                        else type = Modes.NONE;
        }
    }

    public Modes getType(){ return type; }

    public int[] getPinConfiguration(){
        //make sure the pin configuration is > 1. only letters a-f. only appears once
        if(mode.length() > 6 && mode.substring(1,3).matches(".*[a-c].*") && mode.substring(4,6).matches(".*[d-f].*") && mode.substring(1).length() == mode.substring(1).chars().distinct().count()){
            List<Integer> customPinConfig = new ArrayList<>();
            for(int x=0 ;x<mode.substring(1).length(); x++){
                customPinConfig.add(defaultPinConfiguration.get(mode.substring(1).charAt(x)));
            }
            return Ints.toArray(customPinConfig);
        }

        int[] defaultPinConfig = {0,1,2,3,4,5};
        return defaultPinConfig;


    }

    @Override
    public String toString() {
        return "Mode{" +
                "mode='" + mode + '\'' +
                ", type=" + type.name() +
                ", defaultPinConfiguration=" + defaultPinConfiguration +
                '}';
    }
}
