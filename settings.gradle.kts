rootProject.name = "craftbook"

include(":craftbook-api")
include(":craftbook-core")