import net.minecraftforge.gradle.mcp.tasks.GenerateSRG

buildscript {
    println("""
        *******************************************
         You are building CraftBook Extra!

         If you encounter trouble:
         1) Read COMPILING.md if you haven't yet
         2) Use gradlew and not gradle
         3) If you still need help, ask on IRC! irc.esper.net #sk89q

         Output files will be in build/libs
        *******************************************
    """.trimIndent())

    repositories {
        mavenCentral()
        maven(url = "https://files.minecraftforge.net/maven")
        maven(url = "https://oss.sonatype.org/content/repositories/snapshots/")
        maven(url = "https://plugins.gradle.org/m2/")
        maven(url = "https://maven.minecraftforge.net")
        maven(url = "https://repo.spongepowered.org/repository/maven-public/")
        google()
    }

    dependencies {
        classpath(group = "net.minecraftforge.gradle", name = "ForgeGradle", version = "5.1.+")
        classpath("org.spongepowered:mixingradle:0.7.32")
    }
}

plugins {
    `java-library`
    id("org.spongepowered.gradle.plugin") version "2.0.2"
    id("com.dorongold.task-tree") version "2.1.0"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("org.cadixdev.licenser") version "0.6.1"
}

// TODO: processTestResources task https://gitlab.com/minecraftonline/craftbook-extra/-/blob/sponge/build.gradle#L180

apply(plugin = "net.minecraftforge.gradle")
apply(plugin = "org.spongepowered.mixin")

val spongeVersion = "7.3"
val spongeFullVersion = "$spongeVersion.0"
val pluginVersion = "extra-sponge-" + "0.0.88"

allprojects {
    group = "com.sk89q.craftbook"
    version = pluginVersion

    apply(plugin = "java")
    apply(plugin = "com.github.johnrengelman.shadow")
    apply(plugin = "org.cadixdev.licenser")

    repositories {
        mavenCentral()
        maven(url = "https://repo.spongepowered.org/repository/maven-public/")
    }

    dependencies {
        testImplementation("junit:junit:4.13.2")
    }

    project.ext["spongeVersion"] = spongeVersion
    project.ext["spongeFullVersion"] = spongeFullVersion

    license {
        header(file("HEADER.txt"))
        include("**/*.java")
        exclude("**/Metrics.java")
        exclude("sponge/**")

        properties {
            ext["year"] = 2022
        }

        ignoreFailures(false)
        newLine(false)
    }
}

configure<net.minecraftforge.gradle.userdev.UserDevExtension> {
    accessTransformer(rootProject.file("src/main/resources/META-INF/craftbook_at.cfg"))
    mappings("snapshot", "20180814-1.12")
}

configure<org.spongepowered.asm.gradle.plugins.MixinExtension> {
    add(sourceSets.main.get(), "mixins.craftbook.refmap.json")
    config("craftbook.json")
    reobfSrgFile = file("$buildDir/fixMcpToSrg/output.tsrg")
}

sponge {
    apiVersion(spongeFullVersion)
    loader {
        name(org.spongepowered.gradle.plugin.config.PluginLoaders.JAVA_PLAIN)
        version("1.0")
    }
    license(file("LICENSE.txt").readText())
    plugin("example") {
        displayName("CraftBook")
        description("CraftBook is a heavily customizable plugin adding exciting new gameplay elements and advanced redstone functions!")
        entrypoint("com.sk89q.craftbook.sponge.CraftBookPlugin")
        version(pluginVersion)
    }
}

repositories {
    maven(url = "https://jitpack.io")
    maven(url = "https://maven.enginehub.org/repo/")
    maven(url = "https://repo.maven.apache.org/maven2")
    maven(url = "https://oss.sonatype.org/content/groups/public/")
    maven(url = "https://oss.sonatype.org/content/repositories/snapshots/")
    maven(url = "https://repo.codemc.org/repository/maven-public")
    google()
}

dependencies {
    implementation(project(":craftbook-core"))
    implementation(project(":craftbook-api"))

    // minecarft()
    "minecraft"("net.minecraftforge:forge:1.12.2-14.23.5.2859")

    api("org.spongepowered:spongeapi:$spongeFullVersion")
    //annotationProcessor "org.spongepowered:spongeapi:${spongeFullVersion}" Gradle 5

    compileOnly("org.spongepowered:spongecommon:1.12.2-7.3.0:dev")

    implementation("org.spongepowered:mixin:0.8.2")

    annotationProcessor("org.spongepowered:mixin:0.8.1-SNAPSHOT:processor")

    implementation("org.jetbrains:annotations:15.0")

    implementation("com.sk89q.worldedit:worldedit-core:6.1.4-SNAPSHOT")
    implementation("com.sk89q.worldedit:worldedit-sponge:6.1.7-SNAPSHOT")

    testImplementation("org.mockito:mockito-core:2.25.0")

    implementation("org.spongepowered:spongevanilla:1.12.2-7.3.0:dev")
    implementation("org.spongepowered:lwts:1.0.0")

    implementation("org.apache.commons:commons-collections4:4.4")

    implementation(files("libs/modularframework-core-1.9.4-SNAPSHOT.jar"))
    implementation(files("libs/modularframework-sponge-1.9.4-SNAPSHOT.jar"))

    //integrationTestCompile sourceSets.test.output
    //documentationCompile sourceSets.integrationTest.output

    // docs
    implementation("net.steppschuh.markdowngenerator:markdowngenerator:1.3.2")
}

sourceSets {
    fun setupDefaultClasspath(ss: SourceSet) {
        ss.compileClasspath = ss.compileClasspath.plus(sourceSets.getByName("test").compileClasspath)
        ss.runtimeClasspath = ss.runtimeClasspath.plus(sourceSets.getByName("test").runtimeClasspath)

        ss.compileClasspath = ss.compileClasspath.plus(sourceSets.getByName("test").output)
        ss.runtimeClasspath = ss.compileClasspath.plus(sourceSets.getByName("test").output)
    }

    main {
        ext["refMap"] = "mixins.craftbook.refmap.json"
    }

    register("documentation") {
        java.srcDir("src/documentation/java")
        setupDefaultClasspath(this)
    }
    register("integrationTest") {
        java.srcDir("src/integrationTest/java")
        setupDefaultClasspath(this)
    }
}

tasks.jar {
    classifier = "original"
    manifest {
        attributes["TweakClass"] = "org.spongepowered.asm.launch.MixinTweaker"
        attributes["MixinConfigs"] = "craftbook.json"
        attributes["FMLAT"] = "craftbook_at.cfg"
    }
}

tasks.shadowJar {
    dependencies {
        include(dependency(":craftbook-api"))
        include(dependency(":craftbook-core"))
        include(dependency("org.apache.commons:commons-collections4:4.4"))
        relocate("com.me4502.modularframework", "com.sk89q.craftbook.util.modularframework") {
            include(dependency("com.me4502:modularframework-core"))
            include(dependency("com.me4502:modularframework-sponge"))
            exclude("com.me4502.modularframework.modularframework-core")
            exclude("com.me4502.modularframework.modularframework-sponge")
        }
    }

    exclude("com/me4502/modularframework/ModularFramework.class")
}

// https://github.com/SpongePowered/MixinGradle/issues/33
tasks.named<GenerateSRG>("createMcpToSrg") {
    outputs.upToDateWhen { false }
    doLast {
        val inFile = output.get().asFile
        val outFile = file("$buildDir/fixMcpToSrg/output.tsrg")

        outFile.parentFile.mkdirs()
        val toWrite = StringBuilder()
        inFile.readLines().forEachIndexed { i, l ->
            if (i != 0 || l.trim() == "static") toWrite.append("$l\n")
        }
        outFile.writeText(toWrite.toString())
    }
}

tasks.register<Test>("integrationTest") {
    systemProperty("lwts.tweaker", "com.minecraftonline.tweaker.MyTestTweaker")

    workingDir = this.temporaryDir

    testClassesDirs = sourceSets.named("integrationTest").get().output.classesDirs
    classpath = sourceSets.named("integrationTest").get().runtimeClasspath
}

// from documentation.gradle

tasks.register<JavaExec>("generateDocumentation") {
    group = "documentation"
    systemProperty("lwts.tweaker", "com.minecraftonline.tweaker.MyTestTweaer")

    classpath(sourceSets.named("documentation").get().runtimeClasspath)
    main = "com.minecraftonline.documentation.GenerateDocumentation"
    args("gitbook/generated")
}

tasks.named("clean") {
    delete(fileTree("gitbook/generated"))
}