dependencies {
    compileOnly("org.slf4j:slf4j-api:1.7.22")
    compileOnly("org.apache.commons:commons-lang3:3.3.2")

    implementation("ninja.leaping.configurate:configurate-hocon:3.7.1")
}