Bug Reporting
====

This is a guide to bug reporting in craftbook.

All issues are located on https://gitlab.com/minecraftonline/craftbook-extra.
You can create a new issue by using the + symbol at the top of the page, or using this link:

https://gitlab.com/minecraftonline/craftbook-extra/-/issues/new

### Issue Templates

Issue templates give you a way to make sure that you include all the necessary
information in your bug report, and that your bug is solved as quickly as possible.

To choose a template, simply click `Choose a template`.

`General` is for generic craftbook issues, such as commands like /sit.

`IC` Is for ICs, ICs are the signs with the ids like `[MCZ219]` on the second line.
Choose this if you're reporting about a specific ic.

![](https://i.imgur.com/yUmhZaL.png)

Happy bug reporting!