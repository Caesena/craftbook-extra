### What does this affect
E.g the name of the command, or particular command arguments that don't work.
If there is location/direction involved, please also include that.

### What is the intended/previous behaviour
E.g What did the command do before.

### What is the current behaviour
Also include whether is partially broken, completely broken.
